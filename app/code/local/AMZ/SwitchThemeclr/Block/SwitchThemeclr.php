<?php
class AMZ_SwitchThemeclr_Block_SwitchThemeclr extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getSwitchThemeclr()     
     { 
        if (!$this->hasData('switchthemeclr')) {
            $this->setData('switchthemeclr', Mage::registry('switchthemeclr'));
        }
        return $this->getData('switchthemeclr');
        
    }
}