<?php
class AMZ_SwitchThemeclr_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$this->loadLayout();     
		$this->renderLayout();
    }
	
	public function resetAction()
    {
		Mage::getSingleton('core/session')->setSavedDemoData();
		$this->_redirect('/');
	}
	
	public function saveAction()
    {
		if($this->getRequest()->getPost()) {
			$savedData = array();
			if(Mage::getSingleton('core/session')->getSavedDemoData())
				$savedData = Mage::getSingleton('core/session')->getSavedDemoData();
			if($this->getRequest()->getParam('google_font')) {

				$savedData['themesetting/style/body_font'] = $this->getRequest()->getParam('google_font');
			}
			if($this->getRequest()->getParam('google_font1')) {

				$savedData['themesetting/style/font_1'] = $this->getRequest()->getParam('google_font1');
			}
			if($this->getRequest()->getParam('google_font2')) {

				$savedData['themesetting/style/font_2'] = $this->getRequest()->getParam('google_font2');
			}
			if($this->getRequest()->getParam('color1')) {

				$savedData['themesetting/style/highlight1'] = $this->getRequest()->getParam('color1');
			}
			if($this->getRequest()->getParam('color2')) {

				$savedData['themesetting/style/highlight2'] = $this->getRequest()->getParam('color2');
			}
			if($this->getRequest()->getParam('color3')) {

				$savedData['themesetting/style/highlight3'] = $this->getRequest()->getParam('color3');
			}
			if($this->getRequest()->getParam('color4')) {

				$savedData['themesetting/style/highlight4'] = $this->getRequest()->getParam('color4');
			}
			
			if($this->getRequest()->getParam('color5')) {
	
				$savedData['themesetting/style/highlight5'] = $this->getRequest()->getParam('color5');
			}
			if($this->getRequest()->getParam('bd_bg_image')) {

				$savedData['themesetting/style/bkg_images'] = $this->getRequest()->getParam('bd_bg_image');
			}
			if($this->getRequest()->getParam('maxwidth')) {

				$savedData['themesetting/style/max_width'] = $this->getRequest()->getParam('maxwidth');
			}
			if($this->getRequest()->getParam('customcart')) {

				$savedData['themesetting/general/customcart'] = $this->getRequest()->getParam('customcart');
			}
			if($this->getRequest()->getParam('customcheckout')) {

				$savedData['themesetting/general/customcheckout'] = $this->getRequest()->getParam('customcheckout');
			}
			Mage::getSingleton('core/session')->setSavedDemoData($savedData);

		}
		$this->_redirect('/');
    }
}