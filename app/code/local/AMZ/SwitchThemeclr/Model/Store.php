<?php

class AMZ_SwitchThemeclr_Model_Store extends Mage_Core_Model_Store
{    

	 /**
     * Process config value
     *
     * @param string $fullPath
     * @param string $path
     * @param Varien_Simplexml_Element $node
     * @return string
     */
    protected function _processConfigValue($fullPath, $path, $node)
    {
		$savedDemoData = array();
        if (isset($this->_configCache[$path])) {
            return $this->_configCache[$path];
        }

        if ($node->hasChildren()) {
            $aValue = array();
            foreach ($node->children() as $k => $v) {
                $aValue[$k] = $this->_processConfigValue($fullPath . '/' . $k, $path . '/' . $k, $v);
            }
            $this->_configCache[$path] = $aValue;
            return $aValue;
        }

        $sValue = (string) $node;
        if (!empty($node['backend_model']) && !empty($sValue)) {
            $backend = Mage::getModel((string) $node['backend_model']);
            $backend->setPath($path)->setValue($sValue)->afterLoad();
            $sValue = $backend->getValue();
        }

        if (is_string($sValue) && strpos($sValue, '{{') !== false) {
            if (strpos($sValue, '{{unsecure_base_url}}') !== false) {
                $unsecureBaseUrl = $this->getConfig(self::XML_PATH_UNSECURE_BASE_URL);
                $sValue = str_replace('{{unsecure_base_url}}', $unsecureBaseUrl, $sValue);
            } elseif (strpos($sValue, '{{secure_base_url}}') !== false) {
                $secureBaseUrl = $this->getConfig(self::XML_PATH_SECURE_BASE_URL);
                $sValue = str_replace('{{secure_base_url}}', $secureBaseUrl, $sValue);
            } elseif (strpos($sValue, '{{base_url}}') !== false) {
                $sValue = Mage::getConfig()->substDistroServerVars($sValue);
            }
        }
		
		if (in_array($path , array(
				'themesetting/style/body_font',
				'themesetting/style/font_1',
				'themesetting/style/font_2',
				'themesetting/style/highlight1',
				'themesetting/style/highlight2',
				'themesetting/style/highlight3',
				'themesetting/style/highlight4',
				'themesetting/style/highlight5',
				'themesetting/style/bkg_images',
				'themesetting/style/max_width',
				'themesetting/general/customcart',
				'themesetting/general/customcheckout'
			))) {
			$savedDemoData = Mage::getSingleton('core/session')->getSavedDemoData();
		}
		
		if(isset($savedDemoData[$path]) && $savedDemoData[$path] !='') {
			$sValue = $savedDemoData[$path];
		}

        $this->_configCache[$path] = $sValue;
		
        return $sValue;
    }
	
}