<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Layer attribute filter
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Model_Catalog_Layer_Filter_Discount extends Mage_Catalog_Model_Layer_Filter_Abstract
{
    const OPTIONS_ONLY_WITH_RESULTS = 1;
	const XML_PATH_RANGE_CALCULATION    = 'catalog/layered_navigation/discount_range_calculation';
    const XML_PATH_RANGE_STEP           = 'catalog/layered_navigation/discount_range_step';

    const RANGE_CALCULATION_AUTO    = 'auto';
    const RANGE_CALCULATION_MANUAL  = 'manual';
    const MIN_RANGE_POWER = 10;

    
    /**
     * Resource instance
     *
     * @var Mage_Catalog_Model_Resource_Eav_Mysql4_Layer_Filter_Attribute
     */
    protected $_resource;

    /**
     * Construct attribute filter
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->_requestVar = 'discount';
    }

    /**
     * Retrieve resource instance
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Layer_Filter_Attribute
     */
    protected function _getResource()
    {
        if (is_null($this->_resource)) {
            $this->_resource = Mage::getResourceModel('navigationlayered/layer_filter_discount');
        }
        return $this->_resource;
    }
    /**
     * Get price range for building filter steps
     *
     * @return int
     */
    public function getDiscountRange()
    {
        $range = $this->getData('discount_range');
        if (!$range) { 
            /* $currentCategory = Mage::registry('current_category_filter');
            if ($currentCategory) {
                $range = $currentCategory->getFilterPriceRange();
            } else {
                $range = $this->getLayer()->getCurrentCategory()->getFilterPriceRange();
            }

            $maxPrice = $this->getMaxPriceInt();
            if (!$range) {
                $calculation = Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_CALCULATION);
                if ($calculation == self::RANGE_CALCULATION_AUTO) {
                    $index = 1;
                    do {
                        $range = pow(10, (strlen(floor($maxPrice)) - $index));
                        $items = $this->getRangeItemCounts($range);
                        $index++;
                    }
                    while($range > self::MIN_RANGE_POWER && count($items) < 2);
                } else {
                    $range = Mage::app()->getStore()->getConfig(self::XML_PATH_RANGE_STEP);
                }
            }

            while (ceil($maxPrice / $range) > 25) {
                $range *= 10;
            } */
			$range = 10;
            $this->setData('discount_range', $range);
        }

        return $range;
    }

    /**
     * Get maximum price from layer products set
     *
     * @return float
     */
    public function getMaxPriceInt()
    {
        $maxPrice = $this->getData('max_price_int');
        
        return $maxPrice;
    }

    /**
     * Get information about products count in range
     *
     * @param   int $range
     * @return  int
     */
    public function getRangeItemCounts($range)
    {
        $rangeKey = 'range_item_counts_' . $range;
        $items = $this->getData($rangeKey);
        if (is_null($items)) {
            $items = $this->_getResource()->getCount($this, $range);
            $this->setData($rangeKey, $items);
        }

        return $items;
    }

    /**
     * Prepare text of item label
     *
     * @param   int $range
     * @param   float $value
     * @return  string
     */
    protected function _renderItemLabel($range, $value)
    {
        $store      = Mage::app()->getStore();
        $fromPrice  = $range . '%';
        $toPrice    = $value . '%';
		if($value > 50)
		{
			$fromPrice = 50 . '%';
			$toPrice = 100 . '%';
		}
        return Mage::helper('catalog')->__('%s - %s', $fromPrice, $toPrice);
    }

    /**
     * Get price aggreagation data cache key
     * @deprecated after 1.4
     * @return string
     */
    protected function _getCacheKey()
    {
        $key = $this->getLayer()->getStateKey()
            . '_DISCOUNT_GRP_' . Mage::getSingleton('customer/session')->getCustomerGroupId()
            . '_CURR_' . Mage::app()->getStore()->getCurrentCurrencyCode()
            . '_ATTR_' . $this->getAttributeModel()->getAttributeCode()
            . '_LOC_'
            ;
        $taxReq = Mage::getSingleton('tax/calculation')->getRateRequest(false, false, false);
        $key.= implode('_', $taxReq->getData());

        return $key;
    }

    /**
     * Get data for build price filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
		$range      = $this->getDiscountRange();
        $dbRanges   = $this->getRangeItemCounts($range);
        $data       = array();
		for($i=0; $i <= 5; $i++)
		{
			$data[$i] = array(
				'label' => $this->_renderItemLabel($i*$range, ($i+1)*$range),
                'value' => $i*$range . ',' . ($i+1)*$range,
                'count' => 0,
			);
		}
        foreach ($dbRanges as $index=>$count) {
            $data[$index] = array(
                'label' => $this->_renderItemLabel($index*$range, ($index + 1)*$range),
                'value' => $index*$range . ',' . ($index + 1)*$range,
                'count' => $count,
            );
        }
		$data[5]['label'] = $this->_renderItemLabel(50, 100);
		$data[5]['value'] = 50 . ',' . 100;
        return $data;
    }

    /**
     * Apply price range filter to collection
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param $filterBlock
     *
     * @return Mage_Catalog_Model_Layer_Filter_Price
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        /**
         * Filter must be string: $index,$range
         */
        $filter = $request->getParam($this->getRequestVar());
        if (!$filter) {
            return $this;
        }
		
        $filter = explode(',', $filter);
        if (count($filter) != 2) {
            return $this;
        }

        list($index, $range) = $filter;

        if ((int)$range) {
            $this->setPriceRange((int)$range);

            $this->_applyToCollection($range, $index);
            $this->getLayer()->getState()->addFilter(
                $this->__createItem($this->_renderItemLabel($index, $range), $filter)
            );
        }
        return $this;
    }
	protected function _initItems()
    {
        $data = $this->_getItemsData();
        $items=array();
        foreach ($data as $itemData) {
            $items[] = $this->__createItem(
                $itemData['label'],
                $itemData['value'],
                $itemData['count']
            );
        }
        $this->_items = $items;
        return $this;
    }
	protected function __createItem($label, $value, $count=0)
    {
        return Mage::getModel('navigationlayered/catalog_layer_filter_discount')
            ->setFilter($this)
            ->setLabel($label)
            ->setValue($value)
            ->setCount($count);
    }
	public function getName()
	{
		return Mage::helper('catalog')->__('Discount');
	}
    /**
     * Apply filter value to product collection based on filter range and selected value
     *
     * @param int $range
     * @param int $index
     * @return Mage_Catalog_Model_Layer_Filter_Price
     */
    protected function _applyToCollection($range, $index)
    {
        $this->_getResource()->applyFilterToCollection($this, $range, $index);
        return $this;
    }
    public function getRemoveUrl()
    {
        $query = array(
            $this->getFilter()->getRequestVar() => $this->getFilter()->getResetValue(),
            Mage::getBlockSingleton('page/html_pager')->getPageVarName() => null // exclude current page from urls
        );
        $url = Mage::helper('navigationlayered/url')->getFullUrl($query);
        return $url;        
    } 
	public function getUrl()
    {
		$selected = Mage::app()->getRequest()->getParam($this->getFilter()->getRequestVar());
		if($selected && $selected == $this->getValue()):
			return $this->getRemoveUrl();
		else:
			$query = array(
				$this->getFilter()->getRequestVar()=>$this->getValue(),
				Mage::getBlockSingleton('page/html_pager')->getPageVarName() => null // exclude current page from urls
			);
		endif;
        $url = Mage::helper('navigationlayered/url')->getFullUrl($query);
        return $url;
    }
	public function getClassCss()
	{
		$selected = Mage::app()->getRequest()->getParam($this->getFilter()->getRequestVar());
		$class = array();
		if($selected && $selected == $this->getValue()):
			$class[] = '-selected';
		endif;
		return implode(' ', $class);
	}
}
