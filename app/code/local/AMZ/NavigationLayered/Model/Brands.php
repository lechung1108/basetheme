<?php

class AMZ_NavigationLayered_Model_Brands extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('navigationlayered/brands');
    }
}