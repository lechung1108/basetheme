<?php

class AMZ_Shirts_Model_Source_Access
{
    /*
     * Returns array of desiger users
     * @return array designer array like user_id => user_firstname + user_lastname
     */
    public static function toShortOptionArray()
    {
        $result = array(VISIBILITY_PUBLIC=>'Public',VISIBILITY_ALLDESIGNER=>'All designers',VISIBILITY_ADMIN=>'Indoor Designers');
        $collection = Mage::getResourceModel('customer/customer_collection')
            ->addNameToSelect()
            ->addAttributeToSelect('email')
            ->addAttributeToSelect('created_at')
            ->addAttributeToFilter('designer', array('in'=>array(DESIGNER,INDOOR_DESIGNER)))
            ->addAttributeToSelect('group_id');

        foreach($collection as $item)
			$result[$item->getId()] = $item->getName();

        return $result;
    }

    public static function toOptionArray()
    {
        $options = self::toShortOptionArray();
        $res = array();

        foreach($options as $k => $v)
            $res[] = array(
                'value' => $k,
                'label' => $v
            );

        return $res;
    }

}