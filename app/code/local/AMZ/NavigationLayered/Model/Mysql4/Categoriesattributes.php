<?php

class AMZ_NavigationLayered_Model_Mysql4_Categoriesattributes extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the navigationlayered_id refers to the key field in your database table.
        $this->_init('navigationlayered/categoriesattributes', 'id');
    }
	public function loadSettingByCatId($categoryId)
	{
		$attributes = Mage::getModel('navigationlayered/attributes')->getCollection();
		$attributeIds = array();
		foreach($attributes as $attribute)
		{
			$attributeIds[] = $attribute->getAttributeId();
		}
		$db = $this->_getReadAdapter();
		$sql = $db->select()
				->from(array('a'=>$this->getTable('navigationlayered/categoriesattributes')), array('*'))	
				->where('a.category_id = ?', $categoryId)
				->where('a.attribute_id IN (?)', $attributeIds);
		 $res = $db->fetchAll($sql);
		 $exist = array();
		 foreach($res as $row)
		 {
			$exist[] = $row['attribute_id'];
		 }
		$insert = array_diff($attributeIds,$exist);
		if(!empty($insert))
		{
			foreach($insert as $value)
			{
				$data['category_id'] = $categoryId;
				$data['attribute_id'] = $value;
				$db->insert($this->getTable('navigationlayered/categoriesattributes'), $data);
			}
		}
		return true;
            
	}
	/**
     *
     * @param Mage_Core_Model_Abstract $object
     */
   
}