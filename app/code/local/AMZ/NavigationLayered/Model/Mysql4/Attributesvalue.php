<?php

class AMZ_NavigationLayered_Model_Mysql4_Attributesvalue extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the navigationlayered_id refers to the key field in your database table.
		Mage::app()->getRequest()->getParam('store') ? $this->store = Mage::app()->getRequest()->getParam('store') : (Mage::app()->isSingleStoreMode() ? $this->store = 0 : $this->store = Mage::app()->getStore()->getId());//$this->store = Mage::app()->getStore()->getId();
        $this->_init('navigationlayered/attributesvalue', 'option_id');
    }
	
	public function saveOptionCats($Object)
	{
		$storeId = $this->store;
		if(is_array($Object->getCatsData()))
		{
			
			$CatIds = array();
			foreach($data = $Object->getCatsData() as $key=>$value)
			{
				$CatIds[] = $key;
			}
			$db = $this->_getReadAdapter();
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesoptions')),array('*'))
							->where('a.category_id IN (?)',$CatIds)
							->where('a.option_id = ?',$Object->getId());
			$res = $db->fetchAll($sqlIds);
			$exist = array();
			$path = Mage::getBaseDir('media') . DS . 'attributes' . DS;
			foreach($res as $row)
			{
				$exist[] = $row['category_id'];
				$nameField = 'cat_image_'.$row['category_id'];
				if(!isset($data[$row['category_id']]))
					continue;
				if(isset($_FILES[$nameField ]['name']) && $_FILES[$nameField]['name'] != '')
				{
					try {	
							$uploader = new Varien_File_Uploader($nameField);
							$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
							$uploader->setAllowRenameFiles(false);
							$uploader->setFilesDispersion(false);
							$uploader->save($path, $_FILES[$nameField]['name'] );
							$storeInfo['image'] = $_FILES[$nameField]['name'];
						}
						catch (Exception $e) {
							//die('ccccccccccccc');
						}
					
				}
				if(isset($data[$row['category_id']]['del_image']['checkbox']) && $data[$row['category_id']]['del_image']['checkbox']){
					$storeInfo['image']='';
					if(file_exists($path.$data[$row['category_id']]['del_image']['hidden']))
						unlink($path.$data[$row['category_id']]['del_image']['hidden']);
				}
				$info['featured_category'] 		= $data[$row['category_id']]['featured_category'];
				$info['multiple_selections'] 	= $data[$row['category_id']]['multiple_selections'];
				$info['filter_visible'] 		= $data[$row['category_id']]['filter_visible'];
				$info['canonical_url']			= $data[$row['category_id']]['canonical_url'];
				$info['meta_robots']			= $data[$row['category_id']]['meta_robots'];
				// save slide show 02/04
				
				$condition = $this->_getWriteAdapter()->quoteInto('option_id=?', $Object->getId());
				$condition .= ' and '.$this->_getWriteAdapter()->quoteInto('category_id=?', $row['category_id']);
				$db->update($this->getTable('navigationlayered/categoriesoptions'),$info ,$condition );
				// insert table store
				$storeInfo['entity_id'] 			= $row['id'];
				$storeInfo['store_id'] 				= $storeId;
				$storeInfo['cms_block'] 			= $data[$row['category_id']]['cms_block'];
				$storeInfo['meta_title'] 			= $data[$row['category_id']]['meta_title'];
				$storeInfo['meta_description'] 		= $data[$row['category_id']]['meta_description'];
				$storeInfo['description'] 			= $data[$row['category_id']]['description'];
				$storeInfo['slider_id']				= $data[$row['category_id']]['slider_id'];
				$storeInfo['slider_num_product']	= $data[$row['category_id']]['slider_num_product'];
				$storeInfo['slider_status']			= $data[$row['category_id']]['slider_status'];
				$storeInfo['slider_line_display']	= $data[$row['category_id']]['slider_line_display'];
				$storeInfo['slider_align']			= $data[$row['category_id']]['slider_align'];
				$storeInfo['popup_slide']			= $data[$row['category_id']]['popup_slide'];
				$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catoptionstore'))
							->where('entity_id = ?', $row['id'])->where('store_id = ?',$storeId);
				
				if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
				{
					$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catoptionstore'), $storeInfo);
				}
				else
				{
					$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catoptionstore'), $storeInfo, 'entity_id='.$row['id'].' and store_id ='.$storeId);
				}
				//end
				$select = '';
				$storeInfo = array();
				$info = array();
			}
			$insert = array_diff($CatIds,$exist);
			if(!empty($insert))
			{
				foreach($insert as $value)
				{
					$nameField = 'cat_image_'.$value;
					if(isset($_FILES[$nameField ]['name']) && $_FILES[$nameField]['name'] != '')
					{
						try {	
								$uploader = new Varien_File_Uploader($nameField);
								$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
								$uploader->setAllowRenameFiles(false);
								$uploader->setFilesDispersion(false);
								$uploader->save($path, $_FILES[$nameField]['name']);
								$storeInfo['image'] = $_FILES[$nameField]['name'];
							}
							catch (Exception $e) {
								//die('ccccccccccccc');
							}
						
					}
					
					$info['featured_category'] 		= $data[$value]['featured_category'];
					$info['multiple_selections'] 	= $data[$value]['multiple_selections'];
					$info['filter_visible'] 		= $data[$value]['filter_visible'];
					$info['canonical_url'] 			= $data[$value]['canonical_url'];
					$info['meta_robots'] 			= $data[$value]['meta_robots'];
					$info['group_id'] 				= $Object->getAttributeId();
					$info['option_id'] 				= $Object->getId();
					$info['category_id'] 			= $value;
					// save slide show 02/04
					
					//print_r($info);
					$model = Mage::getModel('navigationlayered/categoriesoptions')->setData($info)->save();
					//insert table store
					$storeInfo['entity_id'] 			= $model->getId();//$row['id'];
					$storeInfo['store_id'] 				= $storeId;
					$storeInfo['cms_block'] 			= $data[$value]['cms_block'];
					$storeInfo['meta_title'] 			= $data[$value]['meta_title'];
					$storeInfo['meta_description']		= $data[$value]['meta_description'];
					$storeInfo['description']			= $data[$value]['description'];
					$storeInfo['slider_id']				= $data[$value]['slider_id'];
					$storeInfo['slider_num_product']	= $data[$value]['slider_num_product'];
					$storeInfo['slider_status']			= $data[$value]['slider_status'];
					$storeInfo['slider_line_display']	= $data[$value]['slider_line_display'];
					$storeInfo['slider_align']			= $data[$value]['slider_align'];
					$storeInfo['popup_slide']			= $data[$value]['popup_slide'];
					$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catoptionstore'))
							->where('entity_id = ?', $model->getId())->where('store_id = ?',$storeId);
				
					if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
					{
						$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catoptionstore'), $storeInfo);
					}
					else
					{
						$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catoptionstore'), $storeInfo, 'entity_id='.$model->getId().' and store_id ='.$storeId);
					}
					//end
					$select = '';
					$storeInfo = array();
					$info = array();
				}
			}
		}
		return;
	}
	public function saveCatOptions($Object)
	{
		$storeId = $this->store;
		if(is_array($Object->getCatsData()))
		{
			
			$optionIds = array();
			foreach($data = $Object->getCatsData() as $key=>$value)
			{
				$optionIds[] = $key;
			}
			$db = $this->_getReadAdapter();
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesoptions')),array('*'))
							->where('a.option_id IN (?)',$optionIds)
							->where('a.category_id = ?',$Object->getCategoryId());
			$res = $db->fetchAll($sqlIds);
			$exist = array();
			$path = Mage::getBaseDir('media') . DS . 'attributes' . DS;
			foreach($res as $row)
			{
				$exist[] = $row['option_id'];
				$nameField = 'cat_image_'.$row['option_id'];
				if(isset($_FILES[$nameField ]['name']) && $_FILES[$nameField]['name'] != '')
				{
					try {	
							$uploader = new Varien_File_Uploader($nameField);
							$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
							$uploader->setAllowRenameFiles(false);
							$uploader->setFilesDispersion(false);
							$uploader->save($path, $_FILES[$nameField]['name'] );
							$storeInfo['image'] = $_FILES[$nameField]['name'];
						}
						catch (Exception $e) {
							//die('ccccccccccccc');
						}
					
				}
				if(isset($data[$row['option_id']]['del_image']['checkbox']) && $data[$row['option_id']]['del_image']['checkbox']){
					$storeInfo['image']='';
					if(file_exists($path.$data[$row['option_id']]['del_image']['hidden']))
						unlink($path.$data[$row['option_id']]['del_image']['hidden']);
				}
				
				$info['featured_category'] = $data[$row['option_id']]['featured_category'];
				$info['multiple_selections'] = $data[$row['option_id']]['multiple_selections'];
				$info['filter_visible'] = $data[$row['option_id']]['filter_visible'];
				$info['canonical_url'] = $data[$row['option_id']]['canonical_url'];
				$info['meta_robots'] = $data[$row['option_id']]['meta_robots'];
				$condition = $this->_getWriteAdapter()->quoteInto('option_id=?', $row['option_id']);
				$condition .= ' and '.$this->_getWriteAdapter()->quoteInto('category_id=?', $Object->getCategoryId());
				$db->update($this->getTable('navigationlayered/categoriesoptions'),$info ,$condition );
				// insert table store
				$storeInfo['entity_id'] 			= $row['id'];
				$storeInfo['store_id'] 				= $storeId;
				$storeInfo['cms_block'] 			= $data[$row['option_id']]['cms_block'];
				$storeInfo['meta_title'] 			= $data[$row['option_id']]['meta_title'];
				$storeInfo['meta_description'] 		= $data[$row['option_id']]['meta_description'];
				$storeInfo['description'] 			= $data[$row['option_id']]['description'];
				$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catoptionstore'))
							->where('entity_id = ?', $row['id'])->where('store_id = ?',$storeId);
					if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
					{
						$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catoptionstore'), $storeInfo);
					}
					else
					{
						$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catoptionstore'), $storeInfo,'entity_id='.$row['id'] .' and store_id ='.$storeId );
					}
				//end
				$select = '';
				$storeInfo = array();
				$info = array();
			}
			$insert = array_diff($optionIds,$exist);
			if(!empty($insert))
			{
				foreach($insert as $value)
				{
					$nameField = 'cat_image_'.$value;
					if(isset($_FILES[$nameField ]['name']) && $_FILES[$nameField]['name'] != '')
					{
						try {	
								$uploader = new Varien_File_Uploader($nameField);
								$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
								$uploader->setAllowRenameFiles(false);
								$uploader->setFilesDispersion(false);
								$uploader->save($path, $_FILES[$nameField]['name']);
								$storeInfo['image'] = $_FILES[$nameField]['name'];
							}
							catch (Exception $e) {
								//die('ccccccccccccc');
							}
						
					}
					$info['featured_category'] 		= $data[$value]['featured_category'];
					$info['multiple_selections'] 	= $data[$value]['multiple_selections'];
					$info['filter_visible'] 		= $data[$value]['filter_visible'];
					$info['canonical_url'] 			= $data[$value]['canonical_url'];
					$info['meta_robots'] 			= $data[$value]['meta_robots'];
					$info['group_id']				= $Object->getId();
					$info['option_id'] 				= $value;
					$info['category_id'] 			= $Object->getCategoryId();
					//print_r($info);
					$model = Mage::getModel('navigationlayered/categoriesoptions')->setData($info)->save();
					//insert table store
					$storeInfo['entity_id'] 		= $model->getId();//$row['id'];
					$storeInfo['store_id'] 			= $storeId;
					$storeInfo['cms_block'] 		= $data[$value]['cms_block'];
					$storeInfo['meta_title'] 		= $data[$value]['meta_title'];
					$storeInfo['meta_description']	= $data[$value]['meta_description'];
					$storeInfo['description']		= $data[$value]['description'];
					//insert table store
					$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catoptionstore'))
							->where('entity_id = ?', $model->getId())->where('store_id = ?',$storeId);
				
					if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
					{
						$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catoptionstore'), $storeInfo);
					}
					else
					{
						$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catoptionstore'), $storeInfo, 'entity_id='.$model->getId().' and store_id ='.$storeId);
					}
					//end
					$select = '';
					$storeInfo = array();
					$info = array();
				}
				
			}
		}
		return;
	}
	public function saveCatBrands($Object)
	{
		
		if(is_array($Object->getCatsData()))
		{
			$optionIds = array();
			foreach($data = $Object->getCatsData() as $key=>$value)
			{
				$optionIds[] = $key;
			}
			$db = $this->_getReadAdapter();
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesoptions')),array('*'))
							->where('a.option_id IN (?)',$optionIds)
							->where('a.category_id = ?',$Object->getCategoryId());
			$res = $db->fetchAll($sqlIds);
			$exist = array();
			foreach($res as $row)
			{
				$exist[] = $row['option_id'];
				$info['cms_block'] 				= $data[$row['option_id']]['cms_block'];
				// $info['meta_title'] 			= $row['meta_title'];
				// $info['meta_description'] 		= $row['meta_description'];
				$info['description']			= $data[$row['option_id']]['description'];
				
				$condition = $this->_getWriteAdapter()->quoteInto('option_id=?', $row['option_id']);
				$condition .= ' and '.$this->_getWriteAdapter()->quoteInto('category_id=?', $Object->getCategoryId());
				$db->update($this->getTable('navigationlayered/categoriesoptions'),$info ,$condition );
				$info = array();
			}
			$insert = array_diff($optionIds,$exist);
			if(!empty($insert))
			{
				foreach($insert as $value)
				{
					$info['cms_block'] 				= $data[$value]['cms_block'];
					$info['description']			= $data[$value]['description'];
					$info['group_id']				= $Object->getId();
					$info['option_id'] 				= $value;
					$info['category_id'] 			= $Object->getCategoryId();
					//print_r($info);
					$db->insert($this->getTable('navigationlayered/categoriesoptions'), $info);
					$info = array();
				}
				
			}
		}
		return;
	}
	/**
     *
     * @param Mage_Core_Model_Abstract $object
     */
	 protected function getStoreId()
	{
		return $this->store;
	}
	protected function _afterSave(Mage_Core_Model_Abstract $object){
		$storeId = $this->getStoreId();
		$select = $this->_getReadAdapter()->select()
            ->from($this->getTable('navigationlayered/optionstore'))
            ->where('entity_id = ?', $object->getId())->where('store_id = ?',$storeId);
		$storeArray = array();
		$storeArray['entity_id'] = $object->getId();
		$storeArray['store_id'] = $storeId;
		if($object->getData('title'))
		{
			$storeArray['title'] = $object->getData('title');
			$storeArray['meta_title'] = $object->getData('meta_title');
			$storeArray['cms_block'] = $object->getData('cms_block');
			$storeArray['meta_description'] = $object->getData('meta_description');
			$storeArray['description'] = $object->getData('description');
			$storeArray['base_url'] = $object->getData('base_url');
			if (!$data = $this->_getReadAdapter()->fetchRow($select))
			{
				$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/optionstore'), $storeArray);
			}
			else
			{
				$this->_getWriteAdapter()->update($this->getTable('navigationlayered/optionstore'), $storeArray, 'id='.$data['id'].' and store_id ='.$storeId);
			}
		}
		return parent::_afterSave($object);
   }
   public function loadOptionCats($array)
   {
		$optionId = $array['option_id'];
		$option = Mage::getModel('navigationlayered/attributesvalue')->load($optionId);
		$brand = 0;
		if(!$option->getData())
			return;
		$attributeId = $option->getAttributeId();
		if($attributeId == 607)
			$brand = 1;
		$catOptions = Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->addStoreFilter()->getByOptionId($optionId);
		$arrayCatOptions = array();
		foreach($catOptions as $catOpt)
		{
			$arrayCatOptions[$catOpt->getCategoryId()] = 1;
		}
		$categories = Mage::getModel('catalog/category')->getCollection()->addIsActiveFilter()->addAttributeToFilter('level',array('gteq'=>1));
		$model = Mage::getModel('navigationlayered/attributes')->load($attributeId);
		foreach($categories as $category)
		{
			if(!isset($arrayCatOptions[$category->getId()]))
			{
				$dataCatOpts[$option->getId()] = array('featured_category'=>1,
												'canonical_url'=>$brand,
												'multiple_selections'=>0,
												'filter_visible'=>1,
												'meta_robots'=>$brand,
												'cms_block'=>'',
												'meta_title'=>'',
												'meta_description'=>'',
												'description'=>''
											);
				$model->setCategoryId($category->getId());
				$model->setCatsData($dataCatOpts);
				Mage::getResourceModel('navigationlayered/attributesvalue')->saveCatOptions($model);
			}
		}
		return;
   }
   public function LoadDefaultOptionCats(Mage_Core_Model_Abstract $model)
   {
		$storeId = $this->store;
		$categories = Mage::getModel('catalog/category')
						->getCollection()
						->addAttributeToSelect('category_id')
						->addIsActiveFilter()
						->addAttributeToFilter('level',array('gteq'=>1));
		$optionIds = array();
		$options = $collection = Mage::getModel('navigationlayered/attributesvalue')->getCollection()->addFieldToFilter('main_table.attribute_id', $model->getId());
		foreach($options as $option)
		{
			$optionIds[] = $option->getId();
		}
		$db = $this->_getReadAdapter();
		foreach($categories as $category)
		{
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesoptions')),array('*'))
						->where('a.option_id IN (?)',$optionIds)
						->where('a.category_id = ?',$category->getId());
			$res = $db->fetchAll($sqlIds);
			$exist = array();
			foreach($res as $row)
			{
				$exist[] = $row['option_id'];
			}
			$insert = array_diff($optionIds,$exist);
			if(!empty($insert))
			{
				foreach($insert as $value)
				{
					$info['group_id']				= $model->getId();
					$info['option_id'] 				= $value;
					$info['category_id'] 			= $category->getId();
					$CatOption = Mage::getModel('navigationlayered/categoriesoptions')->setData($info)->save();
					//insert table store
					$storeInfo['entity_id'] 		= $CatOption->getId();//$row['id'];
					$storeInfo['store_id'] 			= 0;
					//insert table store
					$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catoptionstore'))
							->where('entity_id = ?', $CatOption->getId())->where('store_id = ?', 0);
				
					if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
					{
						$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catoptionstore'), $storeInfo);
					}
					$select = '';
					$storeInfo = array();
					$info = array();
				}
			}
		}
		return ;
   }
   
   protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {	
		$storeId = $this->getStoreId();
		$collection = Mage::getModel('navigationlayered/attributesvalue')->getCollection()->addStoreFilter()->addFieldToFilter('option_id',$object->getId());
        if (count($collection)) {   
			foreach($collection as $data){
				$object->setData('title', $data->getTitle());
				$object->setData('meta_title', $data->getMetaTitle());
				$object->setData('meta_description', $data->getMetaDescription());
				$object->setData('cms_block', $data->getCmsBlock());
				$object->setData('description', $data->getDescription());
				$object->setData('base_url', $data->getBaseUrl());
			}
        }
		
        return parent::_afterLoad($object);
    }
	protected function _afterDelete(Mage_Core_Model_Abstract $object)
	{
		$where = '';
        if ($object->getId()) {
            $where = $this->_getReadAdapter()->quoteInto('entity_id = ?', $object->getId());
        }
        if ($where) {
            $this->_getWriteAdapter()->delete(
                $this->getTable('optionstore'), $where);
        }
		return parent::_afterDelete($object);
	}
   
}