<?php
class AMZ_NavigationLayered_Model_Mysql4_Categoriesoptions_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	private $store;
    public function _construct()
    {
		Mage::app()->getRequest()->getParam('store') ? $this->store = Mage::app()->getRequest()->getParam('store') : $this->store = Mage::app()->getStore()->getId();//$this->store = Mage::app()->getStore()->getId();
        parent::_construct();
        $this->_init('navigationlayered/categoriesoptions');
    }
	public function getByOptionId($optionId)
	{
		$this->addFieldToFilter('main_table.option_id',$optionId);
		return $this;
	}
	public function getByCategoryId($CategoryId)
	{
		$this->addFieldToFilter('main_table.category_id',$CategoryId);
		//echo $this->getSelect();die;
		return $this;
	}
	public function getByOptionInArray($array)
	{
		$this->addFieldToFilter('main_table.option_id', array('in'=>$array));
		return $this;
	}
	public function getByOptionNinArray($array)
	{
		$this->addFieldToFilter('main_table.option_id', array('nin'=>$array));
		return $this;
	}
	public function getJoinOption()
	{
		$this->getSelect()->joinLeft(
            array('ov' => $this->getTable('navigationlayered/attributesvalue')), 
            'main_table.option_id = ov.option_id', 
            array('title','option_meta_title'=>'ov.meta_title', 'option_meta_description'=>'ov.meta_description','option_description'=>'ov.description', 'navigation_icon', 'listing_icon', 'detail_icon')
        );
		// echo $this->getSelect();die;
		return $this;
	}
	public function getByAttributeId($attId)
	{
		$this->addFieldToFilter('group_id',$attId);
		//echo $this->getSelect();die;
		return $this;
	}
	public function getTitleOptions()
	{
		$this->getSelect()->joinLeft(
			array('at' => $this->getTable('navigationlayered/attributesvalue')),
				'main_table.option_id = at.option_id',
				array('title')
		);
		
		return $this;
	}
	public function setLimit($data)
	{
		$this->getSelect()->limit($data['limit'], ($data['page']-1) * $data['limit']);
		// echo $this->getSelect();die;
		return $this;
	}
	/*
	 * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     */
	public function addStoreFilter($store = false)
    {
		if(!$store)
			$store = $this->store;
		if (!Mage::app()->isSingleStoreMode()) {
			if ($store instanceof Mage_Core_Model_Store) {
				$store = $store->getId();
			}
			$this->getSelect()->join(array('store_default'=>$this->getTable('catoptionstore')),
                    'store_default.entity_id = main_table.id',
                    array('default_meta_title'=>'meta_title'))
                ->joinLeft(array('store'=>$this->getTable('catoptionstore')),
                    'store.entity_id=main_table.id AND '.$this->getConnection()->quoteInto('store.store_id=?', $store),
                    array(	'store_meta_title'=>'meta_title','store_cms_block'=>'cms_block','store_description'=>'description','store_meta_description'=>'meta_description','store_image'=>'image',
							'store_slider_id'			=> 'slider_id',
							'store_slider_num_product'	=> 'slider_num_product',
							'store_slider_status'		=> 'slider_status',
							'store_slider_line_display'	=>'slider_line_display',
							'store_slider_align'		=>'slider_align',
							'store_popup_slide'			=>'popup_slide',
							'slider_id'				=> new Zend_Db_Expr('IF(store.ids>0, store.slider_id,store_default.slider_id)'),
							'slider_num_product'	=> new Zend_Db_Expr('IF(store.ids>0, store.slider_num_product,store_default.slider_num_product)'),
							'slider_status'			=> new Zend_Db_Expr('IF(store.ids>0, store.slider_status,store_default.slider_status)'),
							'slider_line_display'	=> new Zend_Db_Expr('IF(store.ids>0, store.slider_line_display,store_default.slider_line_display)'),
							'slider_align'			=> new Zend_Db_Expr('IF(store.ids>0, store.slider_align,store_default.slider_align)'),
							'popup_slide'			=> new Zend_Db_Expr('IF(store.ids>0, store.popup_slide,store_default.popup_slide)'),
							'cms_block' 			=> new Zend_Db_Expr('IF(store.ids>0, store.cms_block,store_default.cms_block)'),
							'meta_title' 			=> new Zend_Db_Expr('IF(store.ids>0, store.meta_title,store_default.meta_title)'),
							'meta_description'	 	=> new Zend_Db_Expr('IF(store.ids>0, store.meta_description,store_default.meta_description)'),
							'description' 			=> new Zend_Db_Expr('IF(store.ids>0, store.description,store_default.description)'),
							'image' 				=> new Zend_Db_Expr('IF(store.ids>0, store.image,store_default.image)')
					))
                ->where($this->getConnection()->quoteInto('store_default.store_id=?', 0));
				//echo $this->getSelect();die;
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('store_table' => $this->getTable('catoptionstore')),
				'main_table.id = store_table.entity_id',
				array('*')
			)
			->where('store_table.store_id = ?', $store);
			return $this;
		}
		return $this;
	}
}