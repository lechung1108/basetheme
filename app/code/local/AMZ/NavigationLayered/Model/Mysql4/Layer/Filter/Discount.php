<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog Layer Attribute Filter Resource Model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Model_Mysql4_Layer_Filter_Discount extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize connection and define main table name
     *
     */
	protected $_discount = null;
	protected function _construct()
    {
        $this->_init('catalog/product_index_price', 'entity_id');
    }

    /**
     * Retrieve joined price index table alias
     *
     * @return string
     */
    protected function _getIndexTableAlias()
    {
        return 'price_index';
    }
	protected function _prepareSelect($filter, $clone=false)
    {
        $collection = $filter->getLayer()->getProductCollection();
        $select     = $collection->getSelect();
        $ret  = $clone ? clone $select : $select;
        //echo $select;
        if ($this->_discount){ 
            // we have already added all necessary joins and calculations
            // in the applyFromToFilter method
            // but need to remove FROM condition
            $oldWhere = $ret->getPart(Varien_Db_Select::WHERE);
            $newWhere = array();
            foreach ($oldWhere as $cond){
               if (false === strpos($cond, $this->_discount))
                   $newWhere[] = $cond;
            }
            if ($newWhere && substr($newWhere[0], 0, 3) == 'AND')
               $newWhere[0] = substr($newWhere[0], 3); 
                      
            $ret->setPart(Varien_Db_Select::WHERE, $newWhere);             
        }
        else {
            $response   = $this->_dispatchPreparePriceEvent($filter, $ret);
            // will be used in the count function
            $this->_discount = "(100 - ((price_index.final_price / price_index.price) * 100))";
        }

        if ($clone){
            $ret->reset(Zend_Db_Select::COLUMNS);
            $ret->reset(Zend_Db_Select::ORDER);
            $ret->reset(Zend_Db_Select::LIMIT_COUNT);
            $ret->reset(Zend_Db_Select::LIMIT_OFFSET);                   
        } 
        
        return $ret;
    }    
    /**
     * Retrieve clean select with joined price index table
     *
     * @param Mage_Catalog_Model_Layer_Filter_discount $filter
     * @return Varien_Db_Select
     */
    protected function _getSelect($filter)
    {
        $collection = $filter->getLayer()->getProductCollection();
        $collection->addPriceData($filter->getCustomerGroupId(), $filter->getWebsiteId());

        // clone select from collection with filters
        $select = clone $collection->getSelect();
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        return $select;
    }
	
    /**
     * Prepare response object and dispatch prepare price event
     * Return response object
     *
     * @param Mage_Catalog_Model_Layer_Filter_discount $filter
     * @param Varien_Db_Select $select
     * @return Varien_Object
     */
    protected function _dispatchPreparePriceEvent($filter, $select)
    {
        // prepare response object for event
        $response = new Varien_Object();
        $response->setAdditionalCalculations(array());

        // prepare event arguments
        $eventArgs = array(
            'select'          => $select,
            'table'           => $this->_getIndexTableAlias(),
            'store_id'        => $filter->getStoreId(),
            'response_object' => $response
        );

        /**
         * @deprecated since 1.3.2.2
         */
        Mage::dispatchEvent('catalogindex_prepare_discount_select', $eventArgs);

        /**
         * @since 1.4
         */
        Mage::dispatchEvent('catalog_prepare_discount_select', $eventArgs);

        return $response;
    }

    /**
     * Retrieve maximal price for attribute
     *
     * @param Mage_Catalog_Model_Layer_Filter_discount $filter
     * @return float
     */

    /**
     * Retrieve array with products counts per price range
     *
     * @param Mage_Catalog_Model_Layer_Filter_discount $filter
     * @param int $range
     * @return array
     */
    public function getCount($filter, $range)
    {
        $select     = $this->_prepareSelect($filter, true);
        $connection = $this->_getReadAdapter();
        $response   = $this->_dispatchPreparePriceEvent($filter, $select);
        $table      = $this->_getIndexTableAlias();

        $additional = join('', $response->getAdditionalCalculations());
        $rate       = $filter->getCurrencyRate();

        /**
         * Check and set correct variable values to prevent SQL-injections
         */
        $rate       = floatval($rate);
        $range      = floatval($range);
        if ($range == 0) {
            $range = 1;
        }
        $countExpr  = new Zend_Db_Expr('COUNT(*)');
        $rangeExpr  = new Zend_Db_Expr("if(FLOOR(10 - (({$table}.final_price / {$table}.price )* 10)) < 5, FLOOR(10 - (({$table}.final_price / {$table}.price )* 10)),5)");
		$select->columns(array(
            'range' => $rangeExpr,
            'count' => $countExpr
        ));
        $select->group($rangeExpr);
        return $connection->fetchPairs($select);
    }

    /**
     * Apply attribute filter to product collection
     *
     * @param Mage_Catalog_Model_Layer_Filter_discount $filter
     * @param int $range
     * @param int $index    the range factor
     * @return Mage_Catalog_Model_Resource_Layer_Filter_discount
     */
    public function applyFilterToCollection($filter, $range, $index)
    {	
        $select     = $this->_prepareSelect($filter);
        $select	->where($this->_discount . '>= ?', $index)
				->where($this->_discount . '< ?', $range );

        return $this;
    }
    /**
     * Retrieve array with products counts per attribute option
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @return array
     */ 
}
