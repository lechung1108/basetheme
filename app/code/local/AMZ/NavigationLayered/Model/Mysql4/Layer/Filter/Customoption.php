<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog Layer Attribute Filter Resource Model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Model_Mysql4_Layer_Filter_Customoption extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize connection and define main table name
     *
     */
    protected function _construct()
    {
        $this->_init('catalog/product_option', 'option_id');
    }

    /**
     * Apply attribute filter to product collection
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @param int $value
     * @return Mage_Catalog_Model_Resource_Layer_Filter_Attribute
     */
    public function applyFilterToCollection($layer, $value)
    {
		if(!$layer->getHasCustomOptionFilter()):
			if(is_array($value) && isset($value['alloy']))
				$value = $value['alloy'];
			$layer->setHasCustomOptionFilter(true);
			$opCollection = Mage::getModel('catalog/product_option')->getCollection();
			$opCollection->addFieldToSelect('product_id');
			$collection = $layer->getProductCollection();
			$connection = $this->_getReadAdapter();
			$tableAliasTitle = 'customoptitle';
			$tableAliasType = 'customoptype';
			$tableAliasTypeTitle = 'customoptypetitle';
			$tableAliasTypePrice = 'customoptypeprice';
			$table = 'alloy';
			$conditions = array(
				"{$table}.product_id = e.entity_id"
			);
			$conditionOptionType = array(
				"{$tableAliasType}.option_id = main_table.option_id",
				$connection->quoteInto("{$tableAliasType}.sku = ?", $value)
			);
			$conditionOptionTitle = array(
				"{$tableAliasTitle}.option_id = main_table.option_id",
				$connection->quoteInto("{$tableAliasTitle}.store_id = ?", 0),
				$connection->quoteInto("{$tableAliasTitle}.title like ?", 'alloy/colour')
			);
			$conditionOptionTypePrice = array(
				"{$tableAliasTypePrice}.option_type_id = {$tableAliasType}.option_type_id",
				$connection->quoteInto("{$tableAliasTypePrice}.store_id = ?", 0)
			);
			$opCollection->getSelect()->join(
				array($tableAliasTitle => $this->getTable('catalog/product_option_title')),
				implode(' AND ', $conditionOptionTitle),
				array()
			);
			$opCollection->getSelect()->join(
				array($tableAliasType => $this->getTable('catalog/product_option_type_value')),
				implode(' AND ', $conditionOptionType),
				array()
			);
			$opCollection->getSelect()->join(
				array($tableAliasTypePrice => $this->getTable('catalog/product_option_type_price')),
				implode(' AND ', $conditionOptionTypePrice),
				array('price_custom'=>"{$tableAliasTypePrice}.price", 'price_type_custom'=>"{$tableAliasTypePrice}.price_type")
			);
			$opCollection->getSelect()->group('product_id');
			$abc = $opCollection->getSelectSql();
			$CustomPrice = $connection->getCheckSql("{$table}.price_type_custom = 'fixed'","price_index.price + {$table}.price_custom","price_index.price + ({$table}.price_custom * price_index.price)/100");
			$collection->getSelect()->join(
					array($table => $abc ),
					implode(' AND ', $conditions),
					array('price_custom'=>"{$table}.price_custom", 'price_type_custom'=>"{$table}.price_type_custom",'custom_price'=>$CustomPrice)
				);
			$layer->setCustomSqlPrice($CustomPrice);
		endif;
        return $this;
    } 
	public function applyFilterDiamondsToCollection($layer, $value)
	{
		if(!$layer->getHasCustomOptionFilter()):
			$layer->setHasCustomOptionFilter(true);
			$collection = $layer->getProductCollection();
			$connection = $this->_getReadAdapter();
			$alloy = Mage::getStoreConfig('navigationlayered/general/default_alloy');
			$diamond = Mage::getStoreConfig('navigationlayered/general/default_diamond');
			if(isset($value['alloy']) && $value['alloy'])
				$alloy = $value['alloy'];
			if(isset($value['diamond']) && $value['diamond'])
				$diamond = $value['diamond'];
				
			$AlloySql = $this->getSelectQueryCustomoption('alloy/colour', $alloy);
			$tableAlloy = 'alloy';
			$conditionAlloy = array(
				"{$tableAlloy}.product_id = e.entity_id"
			);
			$CustomPriceAlloy = $connection->getCheckSql("{$tableAlloy}.price_type_custom = 'fixed'","price_index.price + {$tableAlloy}.price_custom","price_index.price + ({$tableAlloy}.price_custom * price_index.price)/100");
			$collection->getSelect()->join(
				array($tableAlloy => $AlloySql ),
				implode(' AND ', $conditionAlloy),
				array()
			);
			$DiamondSql = $this->getSelectQueryCustomoption('stone/diamonds', $diamond);
			$tableDiamond = 'diamond';
			$conditionDiamond = array(
				"{$tableDiamond}.product_id = e.entity_id"
			);
			$CustomPriceDiamond = $connection->getCheckSql("{$tableDiamond}.price_type_custom = 'fixed'","{$tableDiamond}.price_custom","({$tableDiamond}.price_custom * price_index.price)/100");
			$collection->getSelect()->join(
				array($tableDiamond => $DiamondSql ),
				implode(' AND ', $conditionDiamond),
				array('custom_price'=>"$CustomPriceAlloy + $CustomPriceDiamond")
			);
			$layer->setCustomSqlPrice("$CustomPriceAlloy + $CustomPriceDiamond");
		endif;
        return $this;
	}
	protected function getSelectQueryCustomoption($optionTitle, $skuValue)
	{
		$opCollection = Mage::getModel('catalog/product_option')->getCollection();
		$opCollection->addFieldToSelect('product_id');
		
		$connection = $this->_getReadAdapter();
		$tableAliasTitle = 'customoptitle';
		$tableAliasType = 'customoptype';
		$tableAliasTypeTitle = 'customoptypetitle';
		$tableAliasTypePrice = 'customoptypeprice';
		
		$conditionOptionType = array(
			"{$tableAliasType}.option_id = main_table.option_id",
			$connection->quoteInto("{$tableAliasType}.sku = ?", $skuValue)
		);
		$conditionOptionTitle = array(
			"{$tableAliasTitle}.option_id = main_table.option_id",
			$connection->quoteInto("{$tableAliasTitle}.store_id = ?", 0),
			$connection->quoteInto("{$tableAliasTitle}.title like ?", $optionTitle)
		);
		$conditionOptionTypePrice = array(
			"{$tableAliasTypePrice}.option_type_id = {$tableAliasType}.option_type_id",
			$connection->quoteInto("{$tableAliasTypePrice}.store_id = ?", 0)
		);
		$opCollection->getSelect()->join(
			array($tableAliasTitle => $this->getTable('catalog/product_option_title')),
			implode(' AND ', $conditionOptionTitle),
			array()
		);
		$opCollection->getSelect()->join(
			array($tableAliasType => $this->getTable('catalog/product_option_type_value')),
			implode(' AND ', $conditionOptionType),
			array()
		);
		$opCollection->getSelect()->join(
			array($tableAliasTypePrice => $this->getTable('catalog/product_option_type_price')),
			implode(' AND ', $conditionOptionTypePrice),
			array('price_custom'=>"{$tableAliasTypePrice}.price", 'price_type_custom'=>"{$tableAliasTypePrice}.price_type")
		);
		$opCollection->getSelect()->group('product_id');
		$sql = $opCollection->getSelectSql();
		return $sql;
	}
    /**
     * Retrieve array with products counts per attribute option
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @return array
     */ 
}
