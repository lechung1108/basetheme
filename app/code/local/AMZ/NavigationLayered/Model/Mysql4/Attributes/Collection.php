<?php
class AMZ_NavigationLayered_Model_Mysql4_Attributes_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    private $store;
    public function _construct()
    {
		Mage::app()->getRequest()->getParam('store') ? $this->store = Mage::app()->getRequest()->getParam('store') : (Mage::app()->isSingleStoreMode() ? $this->store = 0 : $this->store = Mage::app()->getStore()->getId());//$this->store = Mage::app()->getStore()->getId();
        parent::_construct();
        $this->_init('navigationlayered/attributes');
    }
	public function getSettingWithCatCollection($CatId)
	{
		$this->getSelect()->joinLeft(
            array('ov' => $this->getTable('navigationlayered/categoriesattributes')), 
            'main_table.attribute_id = ov.attribute_id AND ov.category_id=' . $CatId, 
            array('ov.featured_category', 'ov.canonical_url', 'ov.multiple_selections', 'ov.filter_visible', 'attcat_metarobots'=>'ov.meta_robots', 'ov.collapse', 'ov.show_above_toolbar' )
        );
		if (!Mage::app()->isSingleStoreMode()) {
			$store = $this->store;
			$this->getSelect()->join(array('attcat_store_default'=>$this->getTable('catattstore')),
                    'attcat_store_default.entity_id = ov.id',
                    array('default_meta_title'=>'meta_title'))
                ->joinLeft(array('attcat_store'=>$this->getTable('catattstore')),
                    'attcat_store.entity_id=ov.id AND '.$this->getConnection()->quoteInto('attcat_store.store_id=?', $store),
                    array(	'store_meta_title'=>'meta_title','store_cms_block'=>'cms_block','store_description'=>'description','store_meta_description'=>'meta_description','store_image'=>'image',
							'cms_block' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.cms_block,attcat_store_default.cms_block)'),
							'meta_title' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.meta_title,attcat_store_default.meta_title)'),
							'meta_description' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.meta_description,attcat_store_default.meta_description)'),
							'description' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.description,attcat_store_default.description)'),
							'image' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.image,attcat_store_default.image)')
					))
                ->where($this->getConnection()->quoteInto('attcat_store_default.store_id=?', 0));
				//echo $this->getSelect();die;
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('attcat_store_table' => $this->getTable('catattstore')),
				'ov.id = attcat_store_table.entity_id',
				array('attcat_store_table.cms_block', 'attcat_store_table.meta_title', 'attcat_store_table.meta_description', 'attcat_store_table.description', 'attcat_store_table.image')
			)
			->where('attcat_store_table.store_id = ?', 0);
			return $this;
		}
		return $this;
	}
	public function getByAttributeCodeInArray($array)
	{
		$this->addFieldToFilter('attribute_code', array('in'=>$array));
		return $this;
	}
	public function getByAttributeInArray($array)
	{
		$this->addFieldToFilter('main_table.attribute_id', array('in'=>$array));
		return $this;
	}
	public function getByAttributeNinArray($array)
	{
		$this->addFieldToFilter('main_table.attribute_id', array('nin'=>$array));
		return $this;
	}
	
	/*
	 * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     */
	public function addStoreFilter($store = false)
    {
		if(!$store)
			$store = $this->store;
		if (!Mage::app()->isSingleStoreMode()) {
			if ($store instanceof Mage_Core_Model_Store) {
				$store = $store->getId();
			}
			$this->getSelect()->join(array('store_default'=>$this->getTable('attstore')),
                    'store_default.entity_id=main_table.attribute_id',
                    array('default_frontend_label'=>'frontend_label'))
                ->joinLeft(array('store'=>$this->getTable('attstore')),
                    'store.entity_id=main_table.attribute_id AND '.$this->getConnection()->quoteInto('store.store_id=?', $store),
                    array('store_frontend_label'=>'frontend_label',
                    'frontend_label' => new Zend_Db_Expr('IF(store.id>0, store.frontend_label,store_default.frontend_label)')
					))
                ->where($this->getConnection()->quoteInto('store_default.store_id=?', 0));
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('store_table' => $this->getTable('attstore')),
				'main_table.attribute_id = store_table.entity_id',
				array('frontend_label')
			)
			->where('store_table.store_id = ?', $store);
			return $this;
		}
		return $this;
	}
}