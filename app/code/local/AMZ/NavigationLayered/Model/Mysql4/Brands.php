<?php

class AMZ_NavigationLayered_Model_Mysql4_Brands extends Mage_Core_Model_Mysql4_Abstract
{
	 public function _construct()
    {    
        // Note that the navigationlayered_id refers to the key field in your database table.
        $this->_init('navigationlayered/brands', 'brand_id');
    }
	public function changeAttBrands($id)
	{
		$db = $this->_getReadAdapter();
		$o = $this->getTable('navigationlayered/brands');
		$catBr = $this->getTable('navigationlayered/brandsattributes');
        $db->raw_query("TRUNCATE TABLE $o");
        $db->raw_query("TRUNCATE TABLE $catBr");
		$options = Mage::getModel('navigationlayered/attributesvalue')->getCollection()->getByAttributeId($id);
		//print_r(count($options));
		foreach($options as $option)
		{
			//die($option->getTitle());
			$data['brand_id'] = $option->getId();   
			$data['attribute_id'] = $id;    
            $db->insert($this->getTable('navigationlayered/brands'), $data);  
		}
		//die;
		return;
	}
	public function loadBrands($model)
	{
		$db = $this->_getReadAdapter();
		//$att = Mage::getModel('navigationlayered/brands')->getCollection()->getFirstItem();
		$sql = $db->select()
            ->from(array('o' => $this->getTable('navigationlayered/attributesvalue')), array('o.option_id', 'o.attribute_id'))
			->joinLeft(array('v' => $this->getTable('navigationlayered/brands')), 'v.brand_id = o.option_id', array())
            ->where('o.attribute_id = '.$model->getAttributeId())
            ->where('v.brand_id IS NULL');
		$newBrands = $db->fetchAll($sql);
		$insertSql = 'INSERT INTO ' . $this->getTable('navigationlayered/brands') . '(brand_id, attribute_id) ' .  (string)$sql;
        $db->raw_query($insertSql);
		$sqlIds = $db->select()
				->from(array('d' => $this->getTable('navigationlayered/attributesvalue')), array('option_id'))
				->where('d.attribute_id = '.$model->getAttributeId());
				
        $o = $this->getTable('navigationlayered/brands');
        $db->raw_query("DELETE FROM $o WHERE $o.brand_id NOT IN ($sqlIds)");
		Mage::helper('navigationlayered')->LoadBrandsWithAttribute($newBrands);
		return;
	}
	public function saveBrandAttributes($Object)
	{
		if(is_array($Object->getBrandsData()))
		{
			
			$BrandIds = array();
			foreach($data = $Object->getBrandsData() as $key=>$value)
			{
				$BrandIds[] = $key;
			}
			$db = $this->_getReadAdapter();
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/brandsattributes')),array('*'))
							->where('a.attribute_id IN (?)',$BrandIds)
							->where('a.brand_id = ?',$Object->getId());
			$res = $db->fetchAll($sqlIds);
			$exist = array();
			$path = Mage::getBaseDir('media') . DS . 'attributes' . DS;
			foreach($res as $row)
			{
				$exist[] = $row['attribute_id'];
				$info['include_page'] 			= $data[$row['attribute_id']]['include_page'];
				$info['meta_robots'] 			= $data[$row['attribute_id']]['meta_robots'];
				$condition 						= $this->_getWriteAdapter()->quoteInto('brand_id=?', $Object->getId());
				$condition 						.= ' and '.$this->_getWriteAdapter()->quoteInto('attribute_id =?', $row['attribute_id']);
				$db->update($this->getTable('navigationlayered/brandsattributes'),$info ,$condition );
				$info = array();
			}
			$insert = array_diff($BrandIds,$exist);
			if(!empty($insert))
			{
				foreach($insert as $value)
				{
					$info['include_page'] 				= $data[$value]['include_page'];
					$info['meta_robots'] 				= $data[$value]['meta_robots'];
					$info['brand_id'] 					= $Object->getId();
					$info['attribute_id'] 				= $value;
					//print_r($info);
					$db->insert($this->getTable('navigationlayered/brandsattributes'), $info);
					$info = array();
				}
				
			}
		}
		return;
	}
	/**
     *
     * @param Mage_Core_Model_Abstract $object
     */
   
}