<?php
class AMZ_NavigationLayered_Model_Mysql4_Categoriesbrands_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('navigationlayered/categoriesbrands');
    }
	public function getByBrandId($optionId)
	{
		$this->addFieldToFilter('brand_id',$optionId);
		return $this;
	}
	public function getByCategoryId($CategoryId)
	{
		$this->addFieldToFilter('category_id',$CategoryId);
		//echo $this->getSelect();die;
		return $this;
	}
	public function getTitleOptions()
	{
		$this->getSelect()->joinLeft(
			array('at' => $this->getTable('navigationlayered/attributesvalue')),
				'main_table.brand_id = at.option_id',
				array('title')
		);
		
		return $this;
	}
	public function setLimit($data)
	{
		$this->getSelect()->limit($data['limit'], ($data['page']-1) * $data['limit']);
		// echo $this->getSelect();die;
		return $this;
	}
	/*
	 * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     */
}