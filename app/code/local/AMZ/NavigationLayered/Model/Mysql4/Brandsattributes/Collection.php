<?php
class AMZ_NavigationLayered_Model_Mysql4_Brandsattributes_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('navigationlayered/brandsattributes');
    }
	public function getByBrandId($brandId)
	{
		$this->addFieldToFilter('brand_id',$brandId);
		return $this;
	}
	public function getTitleOptions()
	{
		$this->getSelect()->joinLeft(
			array('at' => $this->getTable('navigationlayered/attributesvalue')),
				'main_table.brand_id = at.option_id',
				array('title')
		);
		return $this;
	}
	public function setLimit($data)
	{
		$this->getSelect()->limit($data['limit'], ($data['page']-1) * $data['limit']);
		// echo $this->getSelect();die;
		return $this;
	}
	/*
	 * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     */
}