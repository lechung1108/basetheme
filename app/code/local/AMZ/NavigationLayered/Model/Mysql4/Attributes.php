<?php

class AMZ_NavigationLayered_Model_Mysql4_Attributes extends Mage_Core_Model_Mysql4_Abstract
{
	 public function _construct()
    {    
        // Note that the navigationlayered_id refers to the key field in your database table.
		Mage::app()->getRequest()->getParam('store') ? $this->store = Mage::app()->getRequest()->getParam('store') : (Mage::app()->isSingleStoreMode() ? $this->store = 0 : $this->store = Mage::app()->getStore()->getId());//$this->store = Mage::app()->getStore()->getId();
        $this->_init('navigationlayered/attributes', 'attribute_id');
    }
	 public function createAttributes()
    {    
		$categories = Mage::getModel('catalog/category')
						->getCollection()
						->addAttributeToSelect('category_id')
						->addIsActiveFilter()
						->addAttributeToFilter('level',array('gteq'=>1));
        $db = $this->_getReadAdapter();
        
        //clean values from already removed filters 
        $sqlIds = (string)$db->select()
                ->from(array('f' => $this->getTable('navigationlayered/attributes')), array('attribute_id'));
	
        $o = $this->getTable('navigationlayered/attributesvalue');
        $ocat = $this->getTable('navigationlayered/categoriesoptions');
        $db->raw_query("DELETE FROM $o WHERE $o.attribute_id NOT IN(($sqlIds))");
        $db->raw_query("DELETE FROM $ocat WHERE $ocat.group_id NOT IN(($sqlIds))");

        // select from attributes
        $sql = $db->select()
            ->from(array('a'=>$this->getTable('eav/attribute')), array('a.attribute_id','a.attribute_code','a.frontend_label'))
            ->joinLeft(array('f' => $this->getTable('navigationlayered/attributes')), 'a.attribute_id = f.attribute_id', array())
            ->where('a.frontend_input IN (?)', array('select', 'multiselect'))  
            ->where('a.entity_type_id IN (?)', array(4))  
            ->where('f.attribute_id IS NULL');
            
        if (Mage::helper('navigationlayered')->isVersionLessThan(1, 4)){
            $sql     
                ->where('a.is_filterable = 1') 
                ->where('a.entity_type_id = ?', Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getEntityTypeId());
        }
        else{
		
            $sql           
                ->joinInner(array('ca' => $this->getTable('catalog/eav_attribute')), 'a.attribute_id = ca.attribute_id', array())
                ->where('ca.is_filterable > 0');
        } 
       
        $res = $db->fetchAll($sql);  
        
        $data = array();
        $attIds = array();
        foreach ($res as $row) {
            $data['attribute_id'] = $row['attribute_id'];   
            $data['attribute_code'] = $row['attribute_code'];   
            $attIds[] = $data['attribute_id'];
            $db->insert($this->getTable('navigationlayered/attributes'), $data);
			// insert data store;
			$dataStore['entity_id'] = $row['attribute_id'];
			$dataStore['frontend_label'] = $row['frontend_label']; 			
            $db->insert($this->getTable('navigationlayered/attstore'), $dataStore);
			// end insert store
			$storeId = 0;
			foreach($categories as $category)
			{
				$info['category_id']=$category->getId();
				$info['attribute_id'] = $row['attribute_id'];
				$model = Mage::getModel('navigationlayered/categoriesattributes')->setData($info)->save();//$db->insert($this->getTable('navigationlayered/categoriesattributes'), $info);
				$storeInfo['entity_id'] 		= $model->getId();
				$storeInfo['store_id'] 			= $storeId;
				$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catattstore'))
							->where('entity_id = ?', $model->getId())->where('store_id = ?',$storeId);
				if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
				{
					$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catattstore'), $storeInfo);
				}
				else
				{
					$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catattstore'), $storeInfo, 'entity_id='.$model->getId().' and store_id ='.$storeId);
				}
				$select = '';
				$storeInfo = array();
				$info = array();
			}
        }
		
        // create options     
        $sql = $db->select()
            ->from(array('o' => $this->getTable('eav/attribute_option')), array())
            ->joinInner(array('f' => $this->getTable('navigationlayered/attributes')), 'o.attribute_id = f.attribute_id', array('f.attribute_id'))
            ->joinInner(array('ov' => $this->getTable('eav/attribute_option_value')), 'o.option_id = ov.option_id', array('ov.option_id'))
            ->joinLeft(array('v' => $this->getTable('navigationlayered/attributesvalue')), 'v.option_id = o.option_id', array())
            ->where('ov.store_id = 0');
			
		$optionNew = $db->fetchAll($sql); 
        $insertSql = $sql->insertFromSelect(array('i'=>$this->getTable('navigationlayered/attributesvalue')), array('attribute_id', 'option_id'));
        $db->raw_query($insertSql);
		
		// create option store
		 $sqlStore = $db->select()
            ->from(array('o' => $this->getTable('eav/attribute_option')), array())
            ->joinInner(array('f' => $this->getTable('navigationlayered/attributes')), 'o.attribute_id = f.attribute_id', array(''))
            ->joinInner(array('ov' => $this->getTable('eav/attribute_option_value')), 'o.option_id = ov.option_id', array('entity_id'=>'ov.option_id','title'=>'ov.value'))
            ->joinInner(array('v' => $this->getTable('navigationlayered/attributesvalue')), 'v.option_id = o.option_id', array())
            ->where('ov.store_id = 0');
		$queryInsert = $sqlStore->insertFromSelect(array('i'=>$this->getTable('navigationlayered/optionstore')), array('entity_id', 'title'));
        //$insertSqlStore = 'INSERT INTO ' . $this->getTable('navigationlayered/optionstore') . '(entity_id,title) ' .  (string)$sqlStore;
		//echo $queryInsert; die;
        $db->raw_query($queryInsert);
		
		
		/*load default setting opt cat */
		foreach($optionNew as $dataOption)
		{
			Mage::getResourceModel('navigationlayered/attributesvalue')->loadOptionCats($dataOption);
		}
		//remove options, removed from attrributes
        $sqlIds = (string)$db->select()
                ->from(array('o' => $this->getTable('eav/attribute_option')), array('option_id'));
        $o = $this->getTable('navigationlayered/attributesvalue');
        $ocat = $this->getTable('navigationlayered/categoriesoptions');
        $db->raw_query("DELETE FROM $o WHERE $o.option_id NOT IN(($sqlIds))");
        $db->raw_query("DELETE FROM $ocat WHERE $ocat.option_id NOT IN(($sqlIds))");
		if( $attIds )
		{
			foreach($attIds as $attid)
			{
				$modelAtt = Mage::getModel('navigationlayered/attributes')->load($attid);
				if($modelAtt->getData()){
					Mage::getResourceModel('navigationlayered/attributesvalue')->LoadDefaultOptionCats($modelAtt);
				}
			}
		}
        return true;
    }
	
	
	public function saveAttributeCats($Object)
	{
		$storeId = $this->store;
		if(is_array($Object->getCatsData()))
		{
			$CatIds = array();
			foreach($data = $Object->getCatsData() as $key=>$value)
			{
				$CatIds[] = $key;
			}
			$_FILES = $Object->getCatsFileData();
			$db = $this->_getReadAdapter();
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesattributes')),array('*'))
							->where('a.category_id IN (?)',$CatIds)
							->where('a.attribute_id = ?',$Object->getId());
			//echo $sqlIds;die;
			$res = $db->fetchAll($sqlIds);
			$exist = array();
			$path = Mage::getBaseDir('media') . DS . 'attributes' . DS;
			foreach($res as $row)
			{
				
				$exist[] = $row['category_id'];
				$nameField = 'cat_image_'.$row['category_id'];
				if(isset($_FILES[$nameField ]['name']) && $_FILES[$nameField]['name'] != '')
				{
					try {	
							$uploader = new Varien_File_Uploader($nameField);
							$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
							$uploader->setAllowRenameFiles(false);
							$uploader->setFilesDispersion(false);
							$uploader->save($path, $_FILES[$nameField]['name'] );
							$storeInfo['image'] = $_FILES[$nameField]['name'];
						}
						catch (Exception $e) {
							//die('ccccccccccccc');
						}
					
				}
				if(isset($data[$row['category_id']]['del_image']['checkbox']) && $data[$row['category_id']]['del_image']['checkbox']){
					$storeInfo['image']='';
					if(file_exists($path.$data[$row['category_id']]['del_image']['hidden']))
						unlink($path.$data[$row['category_id']]['del_image']['hidden']);
				}
				// print_r($_FILES);
				// die();
				
				$info['featured_category'] 		= $data[$row['category_id']]['featured_category'];
				$info['multiple_selections'] 	= $data[$row['category_id']]['multiple_selections'];
				$info['filter_visible'] 		= $data[$row['category_id']]['filter_visible'];
				$info['canonical_url'] 			= $data[$row['category_id']]['canonical_url'];
				$info['meta_robots'] 			= $data[$row['category_id']]['meta_robots'];
				$info['show_above_toolbar'] 	= $data[$row['category_id']]['show_above_toolbar'];
				$info['collapse'] 				= $data[$row['category_id']]['collapse'];
				$condition 						= $this->_getWriteAdapter()->quoteInto('attribute_id=?', $Object->getId());
				$condition 						.= ' and '.$this->_getWriteAdapter()->quoteInto('category_id=?', $row['category_id']);
				$db->update($this->getTable('navigationlayered/categoriesattributes'),$info ,$condition );
				// insert table store
				$storeInfo['entity_id'] 		= $row['id'];
				$storeInfo['store_id'] 			= $storeId;
				$storeInfo['cms_block'] 		= $data[$row['category_id']]['cms_block'];
				$storeInfo['meta_title'] 		= $data[$row['category_id']]['meta_title'];
				$storeInfo['meta_description'] 	= $data[$row['category_id']]['meta_description'];
				$storeInfo['description'] 		= $data[$row['category_id']]['description'];
				$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catattstore'))
							->where('entity_id = ?', $row['id'])->where('store_id = ?',$storeId);
				
				if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
				{
					$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catattstore'), $storeInfo);
				}
				else
				{
					$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catattstore'), $storeInfo, 'entity_id='.$row['id'].' and store_id ='.$storeId);
				}
				//end
				$select = '';
				$storeInfo = array();
				$info = array();
			}
			$insert = array_diff($CatIds,$exist);
			if(!empty($insert))
			{
				foreach($insert as $value)
				{
					$nameField = 'cat_image_'.$value;
					if(isset($_FILES[$nameField ]['name']) && $_FILES[$nameField]['name'] != '')
					{
						try {	
								$uploader = new Varien_File_Uploader($nameField);
								$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
								$uploader->setAllowRenameFiles(false);
								$uploader->setFilesDispersion(false);
								$uploader->save($path, $_FILES[$nameField]['name']);
								$storeInfo['image'] = $_FILES[$nameField]['name'];//$info['image'] = $_FILES[$nameField]['name'];
							}
							catch (Exception $e) {
								//die('ccccccccccccc');
							}
						
					}
					$info['featured_category'] 		= $data[$value]['featured_category'];
					$info['multiple_selections'] 	= $data[$value]['multiple_selections'];
					$info['filter_visible'] 		= $data[$value]['filter_visible'];
					$info['canonical_url'] 			= $data[$value]['canonical_url'];
					$info['meta_robots'] 			= $data[$value]['meta_robots'];
					$info['show_above_toolbar'] 	= $data[$value]['show_above_toolbar'];
					$info['collapse'] 				= $data[$value]['collapse'];
					$info['attribute_id'] 			= $Object->getId();
					$info['category_id'] 			= $value;
					//print_r($info);
					$model = Mage::getModel('navigationlayered/categoriesattributes')->setData($info)->save();
					//$db->insert($this->getTable('navigationlayered/categoriesattributes'), $info);
				// insert table store
					
					$storeInfo['entity_id'] 		= $model->getId();//$row['id'];
					$storeInfo['store_id'] 			= $storeId;
					$storeInfo['cms_block'] 		= $data[$value]['cms_block'];
					$storeInfo['meta_title'] 		= $data[$value]['meta_title'];
					$storeInfo['meta_description'] 	= $data[$value]['meta_description'];
					$storeInfo['description'] 		= $data[$value]['description'];
					$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catattstore'))
							->where('entity_id = ?', $model->getId())->where('store_id = ?',$storeId);
					if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
					{
						$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catattstore'), $storeInfo);
					}
					else
					{
						$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catattstore'), $storeInfo, 'entity_id='.$model->getId().' and store_id ='.$storeId);
					}
					$select = '';
					$storeInfo = array();
					$info = array();
				}
			}
		}
		return;
	}
	public function saveCatAttributes($Object)
	{
		$storeId = $this->store;
		if(is_array($Object->getCatsData()))
		{
			
			$AttIds = array();
			foreach($data = $Object->getCatsData() as $key=>$value)
			{
				$AttIds[] = $key;
			}
			$db = $this->_getReadAdapter();
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesattributes')),array('*'))
							->where('a.attribute_id IN (?)',$AttIds)
							->where('a.category_id = ?',$Object->getId());
			$res = $db->fetchAll($sqlIds);
			$exist = array();
			$path = Mage::getBaseDir('media') . DS . 'attributes' . DS;
			foreach($res as $row)
			{
				$exist[] = $row['attribute_id'];
				$nameField = 'cat_image_'.$row['attribute_id'];
				if(isset($_FILES[$nameField ]['name']) && $_FILES[$nameField]['name'] != '')
				{
					try {	
							$uploader = new Varien_File_Uploader($nameField);
							$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
							$uploader->setAllowRenameFiles(false);
							$uploader->setFilesDispersion(false);
							$uploader->save($path, $_FILES[$nameField]['name'] );
							$storeInfo['image'] = $_FILES[$nameField]['name'];
						}
						catch (Exception $e) {
							//die('ccccccccccccc');
						}
					
				}
				if(isset($data[$row['attribute_id']]['del_image']['checkbox']) &&  $data[$row['attribute_id']]['del_image']['checkbox']) {
					$storeInfo['image']='';
					if(file_exists($path.$data[$row['attribute_id']]['del_image']['hidden']))
						unlink($path.$data[$row['attribute_id']]['del_image']['hidden']);
				}
				// print_r($_FILES);
				/* 
				$info['cms_block'] 				= $data[$row['attribute_id']]['cms_block'];
				$info['meta_title'] 			= $data[$row['attribute_id']]['meta_title'];
				$info['meta_description'] 		= $data[$row['attribute_id']]['meta_description'];
				$info['description'] 			= $data[$row['attribute_id']]['description']; 
				*/
				$info['featured_category'] 		= $data[$row['attribute_id']]['featured_category'];
				$info['multiple_selections'] 	= $data[$row['attribute_id']]['multiple_selections'];
				$info['filter_visible'] 		= $data[$row['attribute_id']]['filter_visible'];
				$info['canonical_url'] 			= $data[$row['attribute_id']]['canonical_url'];
				$info['meta_robots'] 			= $data[$row['attribute_id']]['meta_robots'];
				$info['show_above_toolbar'] 	= $data[$row['attribute_id']]['show_above_toolbar'];
				$info['collapse'] 				= $data[$row['attribute_id']]['collapse'];
				$condition 						= $this->_getWriteAdapter()->quoteInto('category_id=?', $Object->getId());
				$condition 						.= ' and '.$this->_getWriteAdapter()->quoteInto('attribute_id=?', $row['attribute_id']);
				$db->update($this->getTable('navigationlayered/categoriesattributes'),$info ,$condition );
				// insert table store
				$storeInfo['entity_id'] 		= $row['id'];
				$storeInfo['store_id'] 			= $storeId;
				$storeInfo['cms_block'] 		= $data[$row['attribute_id']]['cms_block'];
				$storeInfo['meta_title'] 		= $data[$row['attribute_id']]['meta_title'];
				$storeInfo['meta_description'] 	= $data[$row['attribute_id']]['meta_description'];
				$storeInfo['description'] 		= $data[$row['attribute_id']]['description'];
				$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catattstore'))
							->where('entity_id = ?', $row['id'])->where('store_id = ?',$storeId);
				
				if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
				{
					$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catattstore'), $storeInfo);
				}
				else
				{
					$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catattstore'), $storeInfo, 'entity_id='.$row['id'].' and store_id ='.$storeId);
				}
				//end
				$select = '';
				$storeInfo = array();
				$info = array();
			}
			$insert = array_diff($AttIds,$exist);
			if(!empty($insert))
			{
				foreach($insert as $value)
				{
					$nameField = 'cat_image_'.$value;
					if(isset($_FILES[$nameField ]['name']) && $_FILES[$nameField]['name'] != '')
					{
						try {	
								$uploader = new Varien_File_Uploader($nameField);
								$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
								$uploader->setAllowRenameFiles(false);
								$uploader->setFilesDispersion(false);
								$uploader->save($path, $_FILES[$nameField]['name']);
								$storeInfo['image'] = $_FILES[$nameField]['name'];
							}
							catch (Exception $e) {
								//die('ccccccccccccc');
							}
						
					}
				/* 
					$info['cms_block'] 				= $data[$value]['cms_block'];
					$info['meta_title'] 			= $data[$value]['meta_title'];
					$info['meta_description']		= $data[$value]['meta_description'];
					$info['description']			= $data[$value]['description']; */
					$info['featured_category'] 		= $data[$value]['featured_category'];
					$info['multiple_selections'] 	= $data[$value]['multiple_selections'];
					$info['filter_visible'] 		= $data[$value]['filter_visible'];
					$info['canonical_url'] 			= $data[$value]['canonical_url'];
					$info['meta_robots'] 			= $data[$value]['meta_robots'];
					$info['show_above_toolbar'] 	= $data[$value]['show_above_toolbar'];
					$info['collapse'] 				= $data[$value]['collapse'];
					$info['category_id'] 			= $Object->getId();
					$info['attribute_id'] 			= $value;
					//print_r($info);
					$model = Mage::getModel('navigationlayered/categoriesattributes')->setData($info)->save();
					$storeInfo['entity_id'] 		= $model->getId();//$row['id'];
					$storeInfo['store_id'] 			= $storeId;
					$storeInfo['cms_block'] 		= $data[$value]['cms_block'];
					$storeInfo['meta_title'] 		= $data[$value]['meta_title'];
					$storeInfo['meta_description'] 	= $data[$value]['meta_description'];
					$storeInfo['description'] 		= $data[$value]['description'];
					$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catattstore'))
							->where('entity_id = ?', $model->getId())->where('store_id = ?',$storeId);
					if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
					{
						$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catattstore'), $storeInfo);
					}
					else
					{
						$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catattstore'), $storeInfo, 'entity_id='.$model->getId().' and store_id ='.$storeId);
					}
					$select = '';
					$storeInfo = array();
					$info = array();
				}
				
			}
		}
		return;
	}
	public function saveNewCatAttribute($categoryId, $attributeId, $dataAttribute)
	{
		$storeId = $this->store;
		if(is_array($dataAttribute) && $categoryId && $attributeId)
		{
			
			$db = $this->_getReadAdapter();
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesattributes')),array('*'))
							->where('a.attribute_id = (?)',$attributeId)
							->where('a.category_id = ?',$Object->getId());
			$res = $db->fetchAll($sqlIds);
			$exist = array();
			$path = Mage::getBaseDir('media') . DS . 'attributes' . DS;
			
			$insert = array_diff($AttIds,$exist);
			if(count($res) < 1)
			{
					$info['featured_category'] 		= $dataAttribute['featured_category'];
					$info['multiple_selections'] 	= $dataAttribute['multiple_selections'];
					$info['filter_visible'] 		= $dataAttribute['filter_visible'];
					$info['canonical_url'] 			= $dataAttribute['canonical_url'];
					$info['meta_robots'] 			= $dataAttribute['meta_robots'];
					$info['show_above_toolbar'] 	= $dataAttribute['show_above_toolbar'];
					$info['collapse'] 				= $dataAttribute['collapse'];
					$info['category_id'] 			= $categoryId;
					$info['attribute_id'] 			= $attributeId;
					//print_r($info);
					$model = Mage::getModel('navigationlayered/categoriesattributes')->setData($info)->save();
					$storeInfo['entity_id'] 		= $model->getId();//$row['id'];
					$storeInfo['store_id'] 			= $storeId;
					$storeInfo['cms_block'] 		= $dataAttribute['cms_block'];
					$storeInfo['meta_title'] 		= $dataAttribute['meta_title'];
					$storeInfo['meta_description'] 	= $dataAttribute['meta_description'];
					$storeInfo['description'] 		= $dataAttribute['description'];
					$select = $this->_getReadAdapter()->select()
							->from($this->getTable('navigationlayered/catattstore'))
							->where('entity_id = ?', $model->getId())->where('store_id = ?',$storeId);
					if (!$dataStore = $this->_getReadAdapter()->fetchRow($select))
					{
						$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/catattstore'), $storeInfo);
					}
					else
					{
						$this->_getWriteAdapter()->update($this->getTable('navigationlayered/catattstore'), $storeInfo, 'entity_id='.$model->getId().' and store_id ='.$storeId);
					}
			}
		}
		return;
	}
	public function deleteCategory(Mage_Core_Model_Abstract $category)
	{
		$where = '';
        if ($category->getId()) {
            $where = $this->_getReadAdapter()->quoteInto('category_id = ?', $category->getId());
        }
        if ($where) {
			$db = $this->_getReadAdapter();
			$sqlIds	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesattributes')),array('*'))
							->where('a.category_id = ?',$category->getId());
			$catoptionsSql	= $db->select()->from(array('a' => $this->getTable('navigationlayered/categoriesoptions')),array('*'))
							->where('a.category_id = ?',$category->getId());
			$res = $db->fetchAll($sqlIds);
			$catOpts = $db->fetchAll($catoptionsSql);
			foreach($res as $row)
			{
				if(!isset($row['id']) || !$row['id'])
					continue;
				$whereStore = $this->_getReadAdapter()->quoteInto('entity_id = ?', $row['id']);
				$this->_getWriteAdapter()->delete($this->getTable('catattstore'), $whereStore);
			}
			foreach($catOpts as $opt)
			{
				if(!isset($row['id']) || !$row['id'])
					continue;
				$whereStore = $this->_getReadAdapter()->quoteInto('entity_id = ?', $row['id']);
				$this->_getWriteAdapter()->delete($this->getTable('catoptionstore'), $whereStore);
			}
			$this->_getWriteAdapter()->delete($this->getTable('categoriesattributes'), $where);
			$this->_getWriteAdapter()->delete($this->getTable('categoriesoptions'), $where);
        }
	}
	protected function getIdAttributeActiveCat()
	{
		if(!$this->__idActive)
		{
			$this->__idActive = Mage::getModel('eav/entity_attribute')->getIdByCode('catalog_category','is_active');
		}
		return $this->__idActive;
	}
	/**
     *
     * @param Mage_Core_Model_Abstract $object
     */
	public function loadDefaultWithCategory($attribute)
	{
		$prefix = Mage::getConfig()->getTablePrefix();
		$attributeActiveId = $this->getIdAttributeActiveCat();
		$select = $this->_getReadAdapter()->select()
            ->from(array('main'=>$this->getTable('catalog/category')), array('category_id'=>'entity_id'));
		$select->join(array('eav_int' => $prefix . 'catalog_category_entity_int'), "main.entity_id = eav_int.entity_id and eav_int.attribute_id ={$attributeActiveId} and eav_int.store_id = 0", array());
		$select->where('main.entity_type_id =?', 3);
		$select->where('main.level >=?', 1);
		$select->where('eav_int.value =?', 1);
		$select->columns(array('attribute_id'=>"floor(".$attribute->getId().")"));
		$select->columns(array('featured_category'=>"floor(1)"));
		$select->columns(array('filter_visible'=>"floor(1)"));
		$query = $select->insertFromSelect(array('i'=>$this->getTable('navigationlayered/categoriesattributes')), array('category_id', 'attribute_id', 'featured_category', 'filter_visible'));
		$this->_getWriteAdapter()->query($query);
		return true;
	}
	public function addAllProductsToRootCat()
	{
		$prefix = Mage::getConfig()->getTablePrefix();
		$select = $this->_getReadAdapter()->select()
            ->from(array('main'=>$this->getTable('catalog/product')), array('product_id'=>'entity_id'));
		$visibilityId = Mage::getModel('eav/entity_attribute')->getIdByCode('catalog_product','visibility');
		$select->join(array('eav_int' => $prefix . 'catalog_product_entity_int'), "main.entity_id = eav_int.entity_id and eav_int.attribute_id ={$visibilityId} and eav_int.store_id = 0", array());
		$select->where('main.entity_type_id =?', 4);
		$select->where('eav_int.value !=?', 1);
		$select->columns(array('category_id'=>"floor(2)"));
		$select->columns(array('position'=>"floor(0)"));
		$query = $select->insertFromSelect(array('i'=>$this->getTable('catalog/category_product')), array('product_id', 'category_id', 'position'));
		$this->_getWriteAdapter()->query($query);
		return true;
	}
	protected function getStoreId()
	{
		return $this->store;
	}
   protected function _afterSave(Mage_Core_Model_Abstract $object){
		$storeId = $this->getStoreId();
		$select = $this->_getReadAdapter()->select()
            ->from($this->getTable('navigationlayered/attstore'))
            ->where('entity_id = ?', $object->getId())->where('store_id = ?',$storeId);
		$storeArray = array();
		$storeArray['entity_id'] = $object->getId();
		$storeArray['store_id'] = $storeId;
		if($object->getData('frontend_label')):
			$storeArray['frontend_label'] = $object->getData('frontend_label');
			if (!$data = $this->_getReadAdapter()->fetchRow($select))
			{
				$this->_getWriteAdapter()->insert($this->getTable('navigationlayered/attstore'), $storeArray);
			}
			else
			{
				$this->_getWriteAdapter()->update($this->getTable('navigationlayered/attstore'), $storeArray, 'id='.$data['id'].' and store_id ='.$storeId);
			}
		endif;
		return parent::_afterSave($object);
   }
   protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {	
		$storeId = $this->getStoreId();
		$collection = Mage::getModel('navigationlayered/attributes')->getCollection()->addStoreFilter()->addFieldToFilter('attribute_id',$object->getId());
        if (count($collection)) {   
			foreach($collection as $data){
				$object->setData('frontend_label',$data->getFrontendLabel());
			}
        }
		
        return parent::_afterLoad($object);
    }
	
	protected function _afterDelete(Mage_Core_Model_Abstract $object)
	{
		$where = '';
        if ($object->getId()) {
            $where = $this->_getReadAdapter()->quoteInto('entity_id = ?', $object->getId());
        }
        if ($where) {
            $this->_getWriteAdapter()->delete(
                $this->getTable('attstore'), $where);
        }
		return parent::_afterDelete($object);
	}
}