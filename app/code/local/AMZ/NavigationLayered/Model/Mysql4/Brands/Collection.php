<?php
class AMZ_NavigationLayered_Model_Mysql4_Brands_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    private $store;
    public function _construct()
    {
		Mage::app()->getRequest()->getParam('store') ? $this->store = Mage::app()->getRequest()->getParam('store') : $this->store = Mage::app()->getStore()->getId();//$this->store = Mage::app()->getStore()->getId();
		parent::_construct();
        $this->_init('navigationlayered/brands');
    }
	public function getParentsCollection()
	{
		$this->addFieldToFilter('parent_brand', array('null', 0));
		return $this;
	}
	public function getChildrenCollection($brandId)
	{
		$this->addFieldToFilter('parent_brand', array($brandId));
		return $this;
	} 
	public function getJoinOptions()
	{
		$this	->getSelect()
				->join(array('table_alias'=>$this->getTable('navigationlayered/attributesvalue')), 'main_table.brand_id = table_alias.option_id', array('*'));
		if (!Mage::app()->isSingleStoreMode()) {
			$store = $this->store;
			$this->getSelect()->join(array('store_default'=>$this->getTable('optionstore')),
                    'store_default.entity_id = table_alias.option_id',
                    array('default_title'=>'title'))
                ->joinLeft(array('store'=>$this->getTable('optionstore')),
                    'store.entity_id=table_alias.option_id AND '.$this->getConnection()->quoteInto('store.store_id=?', $store),
                    array(	'store_title'=>'title','store_description'=>'description','store_meta_title'=>'meta_title','store_meta_description'=>'meta_description','store_cms_block'=>'cms_block',
							'title' => new Zend_Db_Expr('IF(store.id>0, store.title,store_default.title)'),
							'cms_block' => new Zend_Db_Expr('IF(store.id>0, store.cms_block,store_default.cms_block)'),
							'meta_title' => new Zend_Db_Expr('IF(store.id>0, store.meta_title,store_default.meta_title)'),
							'meta_description' => new Zend_Db_Expr('IF(store.id>0, store.meta_description,store_default.meta_description)'),
							'description' => new Zend_Db_Expr('IF(store.id>0, store.description,store_default.description)')
					))
                ->where($this->getConnection()->quoteInto('store_default.store_id=?', 0));
				//echo $this->getSelect();die;
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('store_table' => $this->getTable('optionstore')),
				'main_table.option_id = store_table.entity_id',
				array('*')
			)
			->where('store_table.store_id = ?', $store);
			return $this;
		}
		return $this;
	}
	public function getJoinAttribute()
	{
		$this-> getSelect()
				->	joinLeft(
			array('at' => $this->getTable('navigationlayered/attributes')),
				'main_table.attribute_id = at.attribute_id',
				array('*')
		)
		->order('at.weight DESC');
		
		return $this;
	}
	public function getJoinSettingAttributes()
	{
		$this->getSelect()
				->joinLeft(
					array('br_att' => $this->getTable('navigationlayered/brandsattributes')),
					'main_table.brand_id = br_att.brand_id',
					array('include_page','meta_robots', 'setting_attribute_id'=>'br_att.attribute_id')
				);
		return $this;
	}
	public function getAttributesInArray($array)
	{
		$this->addFieldToFilter('br_att.attribute_id', array('in'=> $array));
		return $this;
	}
	/*
	 * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     */
}