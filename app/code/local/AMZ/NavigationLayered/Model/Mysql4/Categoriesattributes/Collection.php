<?php
class AMZ_NavigationLayered_Model_Mysql4_Categoriesattributes_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    private $store;
    public function _construct()
    {
		Mage::app()->getRequest()->getParam('store') ? $this->store = Mage::app()->getRequest()->getParam('store') : $this->store = Mage::app()->getStore()->getId();//$this->store = Mage::app()->getStore()->getId();
        parent::_construct();
        $this->_init('navigationlayered/categoriesattributes');
    }
	public function getByAttributeId($attributeId)
	{
		$this->addFieldToFilter('attribute_id',$attributeId);
		return $this;
	}
	public function getByCategoryId($CategoryId)
	{
		$this->addFieldToFilter('category_id',$CategoryId);
		//echo $this->getSelect();die;
		return $this;
	}
	public function getNameAttributes()
	{
		$this->getSelect()->joinLeft(
			array('at' => $this->getTable('navigationlayered/attributes')),
				'main_table.attribute_id = at.attribute_id',
				array('attribute_code')
		);
		return $this;
	}
	public function setLimit($data)
	{
		$this->getSelect()->limit($data['limit'], ($data['page']-1) * $data['limit']);
		// echo $this->getSelect();die;
		return $this;
	}
	public function getByAttributeInArray($array)
	{
		$this->addFieldToFilter('main_table.attribute_id', array('in'=>$array));
		return $this;
	}
	public function getByAttributeNinArray($array)
	{
		$this->addFieldToFilter('main_table.attribute_id', array('nin'=>$array));
		return $this;
	}
	public function getJoinAttribute()
	{
		$this->getSelect()->joinLeft(
            array('ov' => $this->getTable('navigationlayered/attributes')), 
            'main_table.attribute_id = ov.attribute_id', 
            array('attribute_code', 'frontend_label', 'weight', 'at_attribute_id'=>'ov.attribute_id','display_type')
        );
		// echo $this->getSelect();die;
		return $this;
	}
	public function getByAttributeCodeInArray($array)
	{
		$this->addFieldToFilter('ov.attribute_code', array('in'=>$array));
		return $this;
	}
	
	/*
	 * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     */
	public function addStoreFilter($store = false)
    {
		if(!$store)
			$store = $this->store;
		if (!Mage::app()->isSingleStoreMode()) {
			$this->getSelect()->join(array('store_default'=>$this->getTable('catattstore')),
                    'store_default.entity_id = main_table.id',
                    array('default_meta_title'=>'meta_title'))
                ->joinLeft(array('store'=>$this->getTable('catattstore')),
                    'store.entity_id=main_table.id AND '.$this->getConnection()->quoteInto('store.store_id=?', $store),
                    array(	'store_meta_title'=>'meta_title','store_cms_block'=>'cms_block','store_description'=>'description','store_meta_description'=>'meta_description','store_image'=>'image',
							'cms_block' => new Zend_Db_Expr('IF(store.ids>0, store.cms_block,store_default.cms_block)'),
							'meta_title' => new Zend_Db_Expr('IF(store.ids>0, store.meta_title,store_default.meta_title)'),
							'meta_description' => new Zend_Db_Expr('IF(store.ids>0, store.meta_description,store_default.meta_description)'),
							'description' => new Zend_Db_Expr('IF(store.ids>0, store.description,store_default.description)'),
							'image' => new Zend_Db_Expr('IF(store.ids>0, store.image,store_default.image)')
					))
                ->where($this->getConnection()->quoteInto('store_default.store_id=?', 0));
				//echo $this->getSelect();die;
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('store_table' => $this->getTable('catattstore')),
				'main_table.id = store_table.entity_id',
				array('*')
			)
			->where('store_table.store_id = ?', $store);
			return $this;
		}
		return $this;
	}
}