<?php
class AMZ_NavigationLayered_Model_Mysql4_Attributesvalue_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	private $store;
    public function _construct()
    {
		Mage::app()->getRequest()->getParam('store') ? $this->store = Mage::app()->getRequest()->getParam('store') : (Mage::app()->isSingleStoreMode() ? $this->store = 0 : $this->store = Mage::app()->getStore()->getId());//$this->store = Mage::app()->getStore()->getId();
		parent::_construct();
        $this->_init('navigationlayered/attributesvalue');
    }
	public function getByAttributeId($id)
	{
		$this->addFieldToFilter('attribute_id',$id);
		// echo $this->getSelect();die;
		return $this;
	}
	 public function addPositions()
    {
        if (empty($this->_map))
            $this->_map = array();
            
        $this->_map['fields']['option_id'] = 'main_table.option_id';
        
        $this->getSelect()->joinInner(
            array('o'=> $this->getTable('eav/attribute_option')), 
            'main_table.option_id = o.option_id', 
            array('o.sort_order')
        );  
        return $this;
    } 
	public function addValue()
    {
        $storeId = Mage::app()->getStore()->getId();
        $this->getSelect()->joinLeft(
            array('ov' => $this->getTable('eav/attribute_option_value')), 
            'main_table.option_id = ov.option_id AND ov.store_id=' . $storeId, 
            array('ov.value')
        );
        return $this;
    }    
	public function getSettingWithCatCollection($CatId)
	{
		$this->getSelect()->joinLeft(
            array('set_op' => $this->getTable('navigationlayered/categoriesoptions')), 
            'main_table.option_id = set_op.option_id AND set_op.category_id=' . $CatId, 
            array( 'set_op.featured_category', 'set_op.canonical_url', 'set_op.multiple_selections', 'set_op.filter_visible', 'set_op.meta_robots')
        );
		
		if (!Mage::app()->isSingleStoreMode()) {
			$store = $this->store;
			$this->getSelect()->joinLeft(array('set_op_store_default'=>$this->getTable('catoptionstore')),
                    'set_op_store_default.entity_id = set_op.id',
                    array('default_meta_title'=>'meta_title'))
                ->joinLeft(array('set_op_store'=>$this->getTable('catoptionstore')),
                    'set_op_store.entity_id=set_op.id AND '.$this->getConnection()->quoteInto('set_op_store.store_id=?', $store),
                    array(	'set_op_store_meta_title'		=>'meta_title',
							'set_op_store_cms_block'		=>'cms_block',
							'set_op_store_description'		=>'description',
							'set_op_store_meta_description'	=>'meta_description',
							'set_op_store_image'			=>'image',
							'set_op_slider_id'				=> 'slider_id',
							'set_op_slider_num_product'		=> 'slider_num_product',
							'set_op_slider_status'			=> 'slider_status',
							'set_op_slider_line_display'	=>'slider_line_display',
							'set_op_slider_align'			=>'slider_align',
							'set_op_popup_slide'			=>'popup_slide',
							'slider_id'				=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.slider_id,set_op_store_default.slider_id)'),
							'slider_num_product'	=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.slider_num_product,set_op_store_default.slider_num_product)'),
							'slider_status'			=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.slider_status,set_op_store_default.slider_status)'),
							'slider_line_display'	=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.slider_line_display,set_op_store_default.slider_line_display)'),
							'slider_align'			=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.slider_align,set_op_store_default.slider_align)'),
							'popup_slide'			=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.popup_slide,set_op_store_default.popup_slide)'),
							'cat_cms_block' 		=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.cms_block,set_op_store_default.cms_block)'),
							'cat_meta_title'		=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.meta_title,set_op_store_default.meta_title)'),
							'cat_meta_description'	=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.meta_description,set_op_store_default.meta_description)'),
							'cat_description' 		=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.description,set_op_store_default.description)'),
							'image' 				=> new Zend_Db_Expr('IF(set_op_store.ids>0, set_op_store.image,set_op_store_default.image)')
					))
                ->where($this->getConnection()->quoteInto('set_op_store_default.store_id=?', 0));
				//echo $this->getSelect();die;
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('set_op_store_table' => $this->getTable('catoptionstore')),
				'set_op.id = set_op_store_table.entity_id',
				array('cat_cms_block'=>'set_op_store_table.cms_block', 'cat_meta_title'=>'set_op_store_table.meta_title', 'cat_meta_description'=>'set_op_store_table.meta_description', 'cat_description'=>'set_op_store_table.description', 'set_op_store_table.image')
			)
			->where('set_op_store_table.store_id = ?', $store);
			return $this;
		}
		// had store
		
		return $this;
	}
	public function getJoinAttributes()
	{
		$this->getSelect()
			->joinLeft(array('at' => $this->getTable('navigationlayered/attributes')),
							'main_table.attribute_id = at.attribute_id',
							array('at.display_type','at.weight', 'at.attribute_code')
						)->order('at.weight ASC');
		if (!Mage::app()->isSingleStoreMode()) {
			$store = $this->store;
			$this->getSelect()->joinLeft(array('at_store_default'=>$this->getTable('attstore')),
                    'at_store_default.entity_id = at.attribute_id',
                    array('default_frontend_label'=>'frontend_label'))
                ->joinLeft(array('at_store'=>$this->getTable('attstore')),
                    'at_store.entity_id=at.attribute_id AND '.$this->getConnection()->quoteInto('at_store.store_id=?', $store),
                    array(	'store_frontend_label'=>'frontend_label',
							'frontend_label' => new Zend_Db_Expr('IF(at_store.id>0, at_store.frontend_label,at_store_default.frontend_label)')
					))
                ->where($this->getConnection()->quoteInto('at_store_default.store_id=?', 0));
				//echo $this->getSelect();die;
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('at_store' => $this->getTable('attstore')),
				'at.attribute_id = at_store.entity_id',
				array('*')
			)
			->where('at_store.store_id = ?', $store);
			return $this;
		}
		// had store
		return $this;
	}
	public function getJoinSettingAttributesCat($catId)
	{
		$this->getSelect()->joinLeft(
            array('attcat' => $this->getTable('navigationlayered/categoriesattributes')), 
            'main_table.attribute_id = attcat.attribute_id AND attcat.category_id=' . $catId, 
            array('cat_att_featured_category'=>'attcat.featured_category', 'cat_att_canonical_url'=>'attcat.canonical_url', 'cat_att_multiple_selections'=>'attcat.multiple_selections', 'cat_att_filter_visible'=>'attcat.filter_visible', 'cat_att_meta_robots' => 'attcat.meta_robots')
        );
		if (!Mage::app()->isSingleStoreMode()) {
			$store = $this->store;
			$this->getSelect()->joinLeft(array('attcat_store_default'=>$this->getTable('catattstore')),
                    'attcat_store_default.entity_id = attcat.id',
                    array('default_meta_title'=>'meta_title'))
                ->joinLeft(array('attcat_store'=>$this->getTable('catattstore')),
                    'attcat_store.entity_id=attcat.id AND '.$this->getConnection()->quoteInto('attcat_store.store_id=?', $store),
                    array(	'attcat_store_meta_title'=>'meta_title','attcat_store_cms_block'=>'cms_block','attcat_store_description'=>'description','attcat_store_meta_description'=>'meta_description','attcat_store_image'=>'image',
							'cat_att_cms_block' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.cms_block,attcat_store_default.cms_block)'),
							'cat_att_meta_title' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.meta_title,attcat_store_default.meta_title)'),
							'cat_att_meta_description' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.meta_description,attcat_store_default.meta_description)'),
							'cat_att_description' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.description,attcat_store_default.description)'),
							'cat_att_image' => new Zend_Db_Expr('IF(attcat_store.ids>0, attcat_store.image,attcat_store_default.image)')
					))
                ->where($this->getConnection()->quoteInto('attcat_store_default.store_id=?', 0));
				//echo $this->getSelect();die;
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('attcat_store_table' => $this->getTable('catattstore')),
				'attcat.id = attcat_store_table.entity_id',
				array('cat_att_cms_block'=>'attcat_store_table.cms_block', 'cat_att_meta_title'=>'attcat_store_table.meta_title', 'cat_att_meta_description'=>'attcat_store_table.meta_description', 'cat_att_description'=>'attcat_store_table.description', 'cat_att_image'=>'attcat_store_table.image')
			)
			->where('attcat_store_table.store_id = ?', $store);
			return $this;
		}
		// echo $this->getSelect();die;
		return $this;
	}
	public function getJoinBrands() 
	{
		$this->getSelect()->JoinLeft(array('brands'=>$this->getTable('navigationlayered/brands')),
							'main_table.option_id = brands.brand_id',
							array('is_brand' => 'brands.attribute_id' , 'parent_brand')
							);
		// echo $this->getSelect();die;
		return $this;
	}
	public function getJoinBrandAttributes()
	{
		$this	->getSelect()
				->JoinLeft(array('brand_att'	=>	$this->getTable('navigationlayered/brandsattributes')),
						'main_table.option_id = brand_att.brand_id',
						array('include_page','brand_attribute_id'=> 'brand_att.attribute_id')
				);
		//echo $this->getSelect();die;
		return $this;
	}
	public function getByOptionInArray($array)
	{
		$this->addFieldToFilter('main_table.option_id', array('in'=>$array));
		return $this;
	}
	public function getByOptionNinArray($array)
	{
		$this->addFieldToFilter('main_table.option_id', array('nin'=>$array));
		return $this;
	}
	/*
	 * Add Filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     */
	 public function addStoreFilter($store = false)
    {
		if(!$store)
			$store = $this->store;
		if (!Mage::app()->isSingleStoreMode()) {
			if ($store instanceof Mage_Core_Model_Store) {
				$store = $store->getId();
			}
			$this->getSelect()->joinLeft(array('store_default'=>$this->getTable('optionstore')),
                    'store_default.entity_id = main_table.option_id',
                    array('default_title'=>'title', 'default_meta_title'=>'meta_title', 'default_meta_description'=>'meta_description', 'default_description'=>'description', 'default_cms_block'=>'cms_block', 'default_base_url' => 'base_url'))
                ->joinLeft(array('store'=>$this->getTable('optionstore')),
                    'store.entity_id=main_table.option_id AND '.$this->getConnection()->quoteInto('store.store_id=?', $store),
                    array(	'store_title'=>'title','store_description'=>'description','store_meta_title'=>'meta_title','store_meta_description'=>'meta_description', 'store_cms_block'=>'cms_block',
							'title' => new Zend_Db_Expr('IF(store.id>0, store.title,store_default.title)'),
							'cms_block' => new Zend_Db_Expr('IF(store.id>0, store.cms_block,store_default.cms_block)'),
							'base_url' => new Zend_Db_Expr('IF(store.id>0, store.base_url,store_default.base_url)'),
							'meta_title' => new Zend_Db_Expr('IF(store.id>0, store.meta_title,store_default.meta_title)'),
							'meta_description' => new Zend_Db_Expr('IF(store.id>0, store.meta_description,store_default.meta_description)'),
							'description' => new Zend_Db_Expr('IF(store.id>0, store.description,store_default.description)')
					))
                ->where($this->getConnection()->quoteInto('store_default.store_id=?', 0));
				//echo $this->getSelect();die;
			return $this;
		}else{
			$this->getSelect()->joinLeft(
				array('store_table' => $this->getTable('optionstore')),
				'main_table.option_id = store_table.entity_id',
				array('*')
			)
			->where('store_table.store_id = ?', 0);
			return $this;
		}
		return $this;
	}
}