<?php

class AMZ_NavigationLayered_Model_Mysql4_Categoriesoptions extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the navigationlayered_id refers to the key field in your database table.
		Mage::app()->getRequest()->getParam('store') ? $this->store = Mage::app()->getRequest()->getParam('store') : (Mage::app()->isSingleStoreMode() ? $this->store = 0 : $this->store = Mage::app()->getStore()->getId());//$this->store = Mage::app()->getStore()->getId();
        $this->_init('navigationlayered/categoriesoptions', 'id');
    }
	protected function getStoreId()
	{
		return $this->store;
	}
	public function loadSettingByCatId($categoryId, $attributeId)
	{
		$attributes = Mage::getModel('navigationlayered/attributesvalue')->getCollection()->getByAttributeId($attributeId);
		$attributeIds = array();
		foreach($attributes as $attribute)
		{
			$attributeIds[] = $attribute->getOptionId();
		}
		$db = $this->_getReadAdapter();
		$sql = $db->select()
				->from(array('a'=>$this->getTable('navigationlayered/categoriesoptions')), array('*'))	
				->where('a.category_id = ?', $categoryId)
				->where('a.option_id IN (?)', $attributeIds);
		 $res = $db->fetchAll($sql);
		 $exist = array();
		 foreach($res as $row)
		 {
			$exist[] = $row['option_id'];
		 }
		$insert = array_diff($attributeIds,$exist);
		if(!empty($insert))
		{
			foreach($insert as $value)
			{
				$data['category_id'] = $categoryId;
				$data['group_id'] = $attributeId;
				$data['option_id'] = $value;
				$db->insert($this->getTable('navigationlayered/categoriesoptions'), $data);
			}
		}
		return true;
            
	}
	
	/**
     *
     * @param Mage_Core_Model_Abstract $object
     */
   
}