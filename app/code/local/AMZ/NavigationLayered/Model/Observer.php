<?php

class AMZ_NavigationLayered_Model_Observer
{
    public function handleControllerFrontInitRouters($observer) 
    {
        $observer->getEvent()->getFront()
            ->addRouter('navigationlayered', new AMZ_NavigationLayered_Controller_Router());
    }
    
    public function handleCatalogControllerCategoryInitAfter($observer)
    {
        if (!Mage::getStoreConfig('navigationlayered/seo/urls'))
            return;
            
        $controller = $observer->getEvent()->getControllerAction();
        $cat = $observer->getEvent()->getCategory();
        
        if (!Mage::helper('navigationlayered/url')->saveParams($controller->getRequest())){
            if ($cat->getId()  == Mage::app()->getStore()->getRootCategoryId()){
                $cat->setId(0);
                return;
            } 
            else { // no way to tell controler to show 404, do it manually
				$controller->forwardNoroute('noRoute');
				$controller->setNoRoute();
                //$controller->getResponse()->setHeader('HTTP/1.1','404 Not Found');
                //$controller->getResponse()->setHeader('Status','404 File not found');
        
                // $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
                // if (!Mage::helper('cms/page')->renderPage($controller, $pageId)) {
                    // header('Location: /');
                    // exit;
                // }  
               // $controller->getResponse()->sendResponse();
                //exit;                
            }
        } 
        
        if ($cat->getDisplayMode() == 'PAGE' && Mage::registry('navigationlayered_current_params')){
            $cat->setDisplayMode('PRODUCTS');
        }  
    }
	public function category_prepare_save(Varien_Event_Observer $observer)
	{
		$category = $observer->getCategory();
		$request = $observer->getRequest();
		if(!$category->getId())
		{
			$category->setData("use_post_data_config", $request->getPost('use_config'));

            try {
                $validate = $category->validate();
                if ($validate !== true) {
                    foreach ($validate as $code => $error) {
                        if ($error === true) {
                            Mage::throwException(Mage::helper('catalog')->__('Attribute "%s" is required.', $category->getResource()->getAttribute($code)->getFrontend()->getLabel()));
                        }
                        else {
                            Mage::throwException($error);
                        }
                    }
                }
                if ($useDefaults = $request->getPost('use_default')) {
                    foreach ($useDefaults as $attributeCode) {
                        $category->setData($attributeCode, false);
                    }
                }
                $postedProduct = $category->getPostedProducts();
				$category->setPostedProducts(array());
                $category->unsetData('use_post_data_config');
                $category->save();
                $category->setPostedProducts($postedProduct);
				
				$attributes = Mage::getModel('navigationlayered/attributes')->getCollection()->addStoreFilter();
				$dataCatAtt = array();
				foreach ($attributes as $_attribute) {
				   $dataCatAtt[$_attribute->getId()] = array('featured_category'=>0,
														'multiple_selections'=>0,
														'filter_visible'=>0,
														'canonical_url'=>0,
														'meta_robots'=>0,
														'show_above_toolbar'=>0,
														'collapse'=>0,
														'cms_block'=>'',
														'meta_title'=>'',
														'meta_description'=>'',
														'description'=>''
													); 
					try {
						$attributeId = $_attribute->getId();
						$categoryId = $category->getId();
						$options = Mage::getModel('navigationlayered/attributesvalue')->getCollection()->addStoreFilter()->getByAttributeId($attributeId)->setOrder('option_id', 'ASC');
						$dataCatOpts = array();
						foreach ($options as $option) {
						   $dataCatOpts[$option->getId()] = array('featured_category'=>1,
																'canonical_url'=>0,
																'multiple_selections'=>0,
																'filter_visible'=>1,
																'meta_robots'=>0,
																'cms_block'=>'',
																'meta_title'=>'',
																'meta_description'=>'',
																'description'=>''
															);
						}
						$model = Mage::getModel('navigationlayered/attributes')->load($attributeId);
						$model->setCategoryId($categoryId);
						$model->setCatsData($dataCatOpts);
						Mage::getResourceModel('navigationlayered/attributesvalue')->saveCatOptions($model);
					} catch (Exception $e) {
						
					}
				}
				$category->setCatsData($dataCatAtt);
				Mage::getResourceModel('navigationlayered/attributes')->saveCatAttributes($category);
            }
            catch (Exception $e){
                
            }
		}
	}
	public function category_delete(Varien_Event_Observer $observer)
	{
		Mage::getResourceModel('navigationlayered/attributes')->deleteCategory($observer->getCategory());
	}
	public function clearProductCache($observer)
    {
        $_product = $observer['item']->getProduct();
        if(trim(Mage::getStoreConfig('catalog/frontend/refresh_cache_when_stock_is')) == "") {
            $_product->cleanCache();
            return;
        }
        $_currentStock = $_product->getStockItem()->getQty();
        $_futureStock = $_currentStock - $observer['item']->getQty();
        $stocks = explode(',',','.Mage::getStoreConfig('catalog/frontend/refresh_cache_when_stock_is'));
        foreach($stocks as $stock) {
            $stock = trim($stock);
            if(
                $stock &&
                $_currentStock > $stock && $_futureStock <= $stock
            ) {
                $_product->cleanCache();
                return;
            }
        }
    }
	public function controller_action_layout_generate_blocks_after(Varien_Event_Observer $observer)
	{
		$action = $observer->getEvent()->getAction();
		Mage::dispatchEvent('layout_generate_blocks_after_' . $action->getFullActionName(), array('action'=>$action) );
		return $this;
	}
	public function checkNotfoundItem(Varien_Event_Observer $observer)
	{
		$action = $observer->getEvent()->getAction();
		$layer = Mage::getSingleton('catalog/layer');
		if($layer->getProductCollection()->getSize() == 0)
		 	$action->setNoRoute();
		return $this;
	}
	protected function getSession()
	{
		return Mage::getSingleton('catalog/session'); 
	}
	public function addCustomOptionFilter(Varien_Event_Observer $observer)
	{
		$customFilter = $this->getSession()->getCustomFilter();
		$product = $observer->getEvent()->getProduct();
		if($customFilter && Mage::app()->getRequest()->getControllerName() == 'product' && $product)
		{
			$product->setCustomOptionLayered($customFilter);
		}
		return $this;
	}
}