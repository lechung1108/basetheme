<?php

class AMZ_NavigationLayered_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $filters = null;
    protected $icons   = null;
    protected $allOptionsCount   = array();
    protected $defaultDiamond = false;
    protected function _getFilters($mode='view')
    {
        if (is_null($this->filters)){
            //get all possible filters as collection
			$category = Mage::registry('current_category');
			($category && $category->getId())? $id = $category->getId() : $id = 2;
			
            $filterCollection = Mage::getResourceModel('navigationlayered/attributes_collection')
								->addStoreFilter()
								->getSettingWithCatCollection($id);
			if($mode=='view')
			{
				$filterCollection->addFieldToFilter('show_in_view', 1);
			}
			else
			{
				if($mode=='list')
					$filterCollection->addFieldToFilter('show_in_list', 1);
			}
			
            // convert to array        
            $filters = array();    
            foreach ($filterCollection as $filter){
                $filters[$filter->getId()] = $filter;
            }   
            $this->filters = $filters;
        }
        return $this->filters;
    }
    
    public function init()
    {
        // make sure we call this only once
        if (!is_null($this->icons))
            return;
            
        $filters = $this->_getFilters();

        $optionCollection = Mage::getResourceModel('navigationlayered/attributesvalue_collection')
			->addStoreFilter()
            ->addPositions()
            ->addValue();  
            
        $this->icons = array();
        if (!$filters)
            return;
        
        $hlp = Mage::helper('navigationlayered/url');
        foreach ($optionCollection as $opt){
            if (empty($filters[$opt->getAttributeId()]))// it is possible when "use on viev" = "flase"
                continue;
                
            $filter = $filters[$opt->getAttributeId()];
                            
            // seo urls fix when different values        
            $opt->setTitle($opt->getValue() ? $opt->getValue() : $opt->getTitle());
                
            $img  = $opt->getListingIcon();
			
            $code = $filter->getAttributeCode();
            $url  = $hlp->getOptionUrl($code, $opt->getTitle(), $opt->getId());
			$this->icons[$opt->getId()] = array(
					'url'   => str_replace('___SID=U&','', $url),
					'title' => $opt->getTitle(),
					'pos'   => $filter->getWeight(),  
					'pos2'  => $opt->getSortOrder(),  
				);   
			if($img)
			{
				 $this->icons[$opt->getId()]['img'] = Mage::getBaseUrl('media') . 'attributes/' . $img;
			}
        }
        
    }    
    
    /**
     * Returns HTML with attribute images
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $mode (view, list, grid)
     * @param array $names arrtibute codes to show images for
     * @param bool $exclude flag to indicate taht we need to show all attributes beside specified in $names
     * @return unknown
     */
    public function showLinks($product, $mode='view', $names=array(), $exclude=false)
    {
        if ('view' == $mode){
            $this->init(array($product));    
        }
        $filters = $this->_getFilters();
        
        $items = array();
        foreach ($filters as $filter){
            $code = $filter->getAttributeCode(); 
            if (!$code){
                continue;
            }
            
            if ($names && in_array($code, $names) && $exclude)
                continue;
                
            if ($names && !in_array($code, $names) && !$exclude)
                continue;
            
            $optIds  = trim($product->getData($code), ','); 
            if (!$optIds && $product->isConfigurable()){
                $usedProds = $product->getTypeInstance(true)->getUsedProducts(null, $product);
                foreach ($usedProds as $child){
                    if ($child->getData($code)){
                        $optIds .= $child->getData($code) . ',';
                    }
                }
            }
            
            if ($optIds){
                $optIds = explode(',', $optIds);
                $optIds = array_unique($optIds);
                foreach ($optIds as $id){
                    if (!empty($this->icons[$id])){
                        $items[] = $this->icons[$id];
                    }
                }
            }
        }  
       
        //sort by position in the layered navigation
        usort($items, array('AMZ_NavigationLayered_Helper_Data', '_srt'));
        //create block
        $block = Mage::getModel('core/layout')->createBlock('core/template')
            ->setArea('frontend')
            ->setTemplate('amz_navigationlayered/links.phtml');
        $block->assign('_type', 'html')
            ->assign('_section', 'body')         
            ->setLinks($items)
            ->setMode($mode); // to be able to created different html
             
        return $block->toHtml();          
    }
    public function showLinksByCat($product, $mode='view', $names=array(), $category = false, $displayTitleAtt = false, $exclude=false)
    {
        if ('view' == $mode){
            $this->init(array($product));    
        }
        $filters = $this->_getFilters();
        
        $items = array();
        foreach ($filters as $filter){
            $code = $filter->getAttributeCode(); 
            if (!$code){
                continue;
            }
            
            if ($names && in_array($code, $names) && $exclude)
                continue;
                
            if ($names && !in_array($code, $names) && !$exclude)
                continue;
            
            $optIds  = trim($product->getData($code), ','); 
            if (!$optIds && $product->isConfigurable()){
                $usedProds = $product->getTypeInstance(true)->getUsedProducts(null, $product);
                foreach ($usedProds as $child){
                    if ($child->getData($code)){
                        $optIds .= $child->getData($code) . ',';
                    }
                }
            }
            
            if ($optIds){
                $optIds = explode(',', $optIds);
                $optIds = array_unique($optIds);
                foreach ($optIds as $id){
                    if (!empty($this->icons[$id])){
						if($displayTitleAtt)
							$this->icons[$id]['title'] = $filter->getFrontendLabel() . ' ' . $this->icons[$id]['title'];
						$this->icons[$id]['url'] = Mage::helper('navigationlayered/url')->getFullCatUrl(array($code => $id), false, $category);
                        $items[] = $this->icons[$id];
                    }
                }
            }
        }  
       
        //sort by position in the layered navigation
        usort($items, array('AMZ_NavigationLayered_Helper_Data', '_srt'));
        //create block
        $block = Mage::getModel('core/layout')->createBlock('core/template')
            ->setArea('frontend')
            ->setTemplate('amz_navigationlayered/links.phtml');
        $block->assign('_type', 'html')
            ->assign('_section', 'body')         
            ->setLinks($items)
            ->setMode($mode); // to be able to created different html
             
        return $block->toHtml();          
    }
    public static function _srt($a, $b)
     {
        $res = ($a['pos'] < $b['pos']) ? -1 : 1;
        if ($a['pos'] == $b['pos']){ 
            if ($a['pos2'] == $b['pos2'])
                $res = 0;
            else 
                $res = ($a['pos2'] < $b['pos2']) ? -1 : 1;
        }
        
        return $res;
     }
    
    public function isVersionLessThan($major=5, $minor=3)
    {
        $curr = explode('.', Mage::getVersion()); // 1.3. compatibility
        $need = func_get_args();
        foreach ($need as $k => $v){
            if ($curr[$k] != $v)
                return ($curr[$k] < $v);
        }
        return false;
    }
    
    /**
     * Gets params (6,17,89) from the request as array and sanitize them
     *
     * @param string $key attribute code
     * @return array
     */
    public function getRequestValues($key)
    {
		if(Mage::app()->getRequest()->getQuery())
		{
			if(!Mage::registry('cat_hadfilter')) Mage::register('cat_hadfilter', true);
		}
       $v = Mage::app()->getRequest()->getParam($key);
    
       if (is_array($v)){//smth goes wrong
           return array();
       }
       
       if (preg_match('/^[0-9,]+$/', $v)){
            $v = array_unique(explode(',', $v));
       }
       else { 
            $v = array();
       }
       
       return $v;       
    } 
	 public function removeCatShowAll($url)
	{
		$url = str_replace('?cat=showall','', $url);
		$url = str_replace('&cat=showall','', $url);
		$arrayUrl = explode('?',$url);
		if(count($arrayUrl) == 1)
		{
			$changeUrl = explode('&', $url);
			if(count($changeUrl) >1):
				$newUrl = '';
				for($i=0; $i<(count($changeUrl)); $i++)
				{
					if($i==0)
					$newUrl .= $changeUrl[$i].'?';
					elseif($i == count($changeUrl)-1)
					$newUrl .= $changeUrl[$i];
					else
					$newUrl .=  $changeUrl[$i] . '&';
				}
				return $newUrl;
			endif;
		}
		return $url;
	}
	public function FormatTextToCompare($text)
	{
		return strtolower(str_replace(' ','',trim($text)));
	}
	public function getStoreDetail($store_code){
		$stores = array_keys(Mage::app()->getStores());
		foreach($stores as $id){  
			$store = Mage::app()->getStore($id);
			if($store->getCode()==$store_code){
				return $store; 
			}
		}
		return Mage::app()->getStore();
	} 
	public function LoadBrandsWithAttribute($arrayBrands)
	{
		if(!$arrayBrands || !is_array($arrayBrands))
			return;
		foreach($arrayBrands as $NewBrands)
		{
			$model = Mage::getModel('navigationlayered/attributesvalue')->load($NewBrands['option_id']);
			if($model->getData())
			{
				$attributes = Mage::getModel('navigationlayered/attributes')->getCollection();
				$attributes->addFieldToFilter('attribute_id',array('neq'=>$NewBrands['attribute_id']));
				$data = array();
				foreach($attributes as $attribute)
				{
					$data[$attribute->getId()]['include_page'] 	= 1;
					$data[$attribute->getId()]['meta_robots'] 	= 1;
				}
				$model->setBrandsData($data);
				Mage::getResourceModel('navigationlayered/brands')->saveBrandAttributes($model);
			}
		}
	}
	public function getProductCollection($attribute = false, $value = false, $category = false)
	{
			if(!$category)
				$category = Mage::registry('current_category');
			if(!$category || !$attribute)
				return false;
			$catId = $category->getId();
			$entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
			$modelName  = Mage::helper('navigationlayered')->isVersionLessThan(1, 4) ? 'catalog/entity_attribute' : 'catalog/resource_eav_attribute';
			if($attribute->getAttributeCode())
			{	
				$attribute  = Mage::getModel($modelName) 
							->loadByCode($entityTypeId, $attribute->getAttributeCode());
			}
			else
			{
				$brand_code = Mage::getModel('navigationlayered/brands')->getCollection()->getJoinAttribute()->getFirstItem();
				$attribute  = Mage::getModel($modelName) 
								->loadByCode($entityTypeId, $brand_code->getAttributeCode());
			}	
			
			if (!$attribute->getId()){
				return false;
			} 
			//1.3 only
			if (!$attribute->getSourceModel()){
				$attribute->setSourceModel('eav/entity_attribute_source_table'); 
			}
			
			$options = $attribute->getFrontend()->getSelectOptions();
			array_shift($options);
			
			$filter = new Varien_Object();
			// important when used at category pages
			$layer = Mage::getModel('catalog/layer')
					->setCurrentCategory($catId);
			
			$filter->setLayer($layer);
			$filter->setStoreId(Mage::app()->getStore()->getId());
			$filter->setAttributeModel($attribute);
			$optionsCount = array();
			if (Mage::helper('navigationlayered')->isVersionLessThan(1, 4)){
				$category   = Mage::getModel('catalog/category')->load($catId);
				$collection = $category->getProductCollection();
				Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
				Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
				
				$optionsCount = Mage::getSingleton('catalogindex/attribute')
					->getCount($attribute, $collection->getSelect());
			}
			else {
				if(!isset($this->allOptionsCount) || !$this->allOptionsCount)
					$this->allOptionsCount = Mage::getResourceModel('catalog/layer_filter_attribute')->getCount($filter);
				if($value) Mage::getResourceModel('catalog/layer_filter_attribute')->applyFilterToCollection($filter, $value);
			}
		return $filter->getLayer()->getProductCollection();
	}
	public function getOptionsCount()
	{
		return $this->allOptionsCount;
	}
	public function getProductCollectionByCategory($category = false)
	{
			if(!$category)
				return false;
			$catId = $category->getId();
			$filter = new Varien_Object();
			// important when used at category pages
			$layer = Mage::getModel('catalog/layer')
					->setCurrentCategory($catId);
			$filter->setLayer($layer);
			$filter->setStoreId(Mage::app()->getStore()->getId());
		return $filter->getLayer()->getProductCollection();
	}
	public function getChildCategory($categoryId)
	{
		$category = Mage::getModel('catalog/category')->load($categoryId); 
		if(!$category->getId()) return;
		$entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
		$modelName  = Mage::helper('navigationlayered')->isVersionLessThan(1, 4) ? 'catalog/entity_attribute' : 'catalog/resource_eav_attribute';
		$brand_code = Mage::getModel('navigationlayered/brands')->getCollection()->getJoinAttribute()->getFirstItem();
		$attribute  = Mage::getModel($modelName)->loadByCode($entityTypeId, $brand_code->getAttributeCode());
		
		// important when used at category pages
		$layer = Mage::getModel('catalog/layer')
			->setCurrentCategory($category->getId());
		Mage::app()->getLayout()->createBlock('catalog/layer_filter_attribute')->setLayer($layer)->setAttributeModel($attribute)->init();
		$discountBlock = Mage::app()->getLayout()->createBlock('navigationlayered/catalog_layer_filter_discount')
                    ->setLayer($layer)
                    ->init(); 
		$categoryHtml = Mage::app()->getLayout()->createBlock('catalog/layer_filter_category')->setLayer($layer)->init()->setTemplate('amz_navigationlayered/sub_category.phtml');
		return $categoryHtml->toHtml();
	}
	public function getResizedUrl($imgName,$x,$y=NULL, $imgPath = 'attributes'){
 
        /**
         * Path with Directory Seperator
         */
		$pathImageName = explode('/',$imgName);
		if(count($pathImageName) > 1)
			$imgName = $pathImageName[count($pathImageName)-1];
		
        $imgPath=str_replace("/",DS,$imgPath);
 
        /**
         * Absolute full path of Image
         */
        $imgPathFull=Mage::getBaseDir("media").DS.$imgPath.DS.$imgName;
	//	die($imgPathFull);
        /**
         * If Y is not set set it to as X
         */
        $widht=$x;
        $y?$height=$y:$height=$x;
 
        /**
         * Resize folder is widthXheight
         */
        $resizeFolder=$widht."X".$height;
 
        /**
         * Image resized path will then be
         */
        $imageResizedPath=Mage::getBaseDir("media").DS.$imgPath.DS.$resizeFolder.DS.$imgName;
 
        /**
         * First check in cache i.e image resized path
         * If not in cache then create image of the width=X and height = Y
         */
        if (!file_exists($imageResizedPath)&& file_exists($imgPathFull)) :
			//die('cccccccc');
            $imageObj = new Varien_Image($imgPathFull);
			$imageObj->constrainOnly(TRUE);
			$imageObj->keepFrame(TRUE);
			$imageObj->keepTransparency(TRUE);
            $imageObj->keepAspectRatio(TRUE);
			$imageObj->backgroundColor(array(255,255,255));
            $imageObj->setWatermarkImageOpacity(0.1);
			$imageObj->resize($widht,$height);
            $imageObj->save($imageResizedPath);
        endif;
 
        /**
         * Else image is in cache replace the Image Path with / for http path.
         */
        $imgUrl=str_replace(DS,"/",$imgPath);
 
        /**
         * Return full http path of the image
         */
        return Mage::getBaseUrl("media").$imgUrl."/".$resizeFolder."/".$imgName;
    }
	public function getStockLabelProduct($_product)
	{
		$stock = $_product->getStockItem();
		$manageStock = $stock->getManageStock();
		$qty = $stock->getQty();
		$LabelInStock = Mage::getStoreConfig('cataloginventory/stocklabel/manageinstock', $_product->getStockId());
		if($manageStock)
			return '<span class="stock-label">'. $this->__('Availability:') . '</span><span> ' . $LabelInStock .'</span>';
		elseif(!$manageStock && $qty == 0)
			return '<span class="stock-label">'. $this->__('Availability:') . '</span><span> ' . Mage::getStoreConfig('cataloginventory/stocklabel/instock', $_product->getStockId()) .'</span>';
		else
			return '<span class="stock-label">'. $this->__('Lieferzeit:') . '</span><span> ' . Mage::getStoreConfig('cataloginventory/stocklabel/everytime', $_product->getStockId()) .'</span>';
	}
	public function getMoreColors($_product)
	{
		$sku = $_product->getSku();
		$colors = array('white', 'red', 'yellow', 'white_yellow', 'white_red');
		$pathList = Mage::getBaseDir('media') . DS . 'productlist' . DS;
		$moreColor = array();
		foreach($colors as $color)
		{
			$nameImg = strtolower($sku. '_' . $color . '.jpg');
			if(file_exists($pathList.$nameImg))
			{
				$_product->setMoreImage($nameImg);
				$moreColor[$color] = (String)Mage::helper('catalog/image')->init($_product, 'small_image')->resize(190,285);
			}
		}
		$_product->setMoreImage(false);
		if(count($moreColor) < 2)
			return array();
		return $moreColor;
	}
	public function getMoreColorsDiamond($_product)
	{
		$sku = $_product->getSku();
		$colors = array('white', 'red', 'yellow');
		$diamond = Mage::getStoreConfig('navigationlayered/general/default_diamond');
		if(!$this->defaultDiamond)
		{
			$diamonds = Mage::app()->getRequest()->getParam('customoption');
			if($diamonds && isset($diamonds['diamond']) && $diamonds['diamond'])
				$diamond = $diamonds['diamond'];
			if($defaultDiamond = Mage::helper('navigationlayered/url')->getValueCustomOpFromRequest($diamond, 'navigationlayered/general/diamonds', true))
			{
				$dm_array = explode('-', strtolower($defaultDiamond));
				$this->defaultDiamond = $dm_array[0];
			}else
				$this->defaultDiamond = 'diamond';
		}		
		$pathList = Mage::getBaseDir('media') . DS . 'productlist' . DS;
		$moreColor = array();
		foreach($colors as $color)
		{
			$nameImg = strtolower($sku. '_' . $color . '_' . $this->defaultDiamond . '.jpg');
			if(file_exists($pathList.$nameImg))
			{
				$_product->setMoreImage($nameImg);
				$moreColor[$color] = (String)Mage::helper('catalog/image')->init($_product, 'small_image')->resize(190,285);
			}
		}
		$_product->setMoreImage(false);
		if(count($moreColor) < 2)
			return array();
		return $moreColor;
	}
}