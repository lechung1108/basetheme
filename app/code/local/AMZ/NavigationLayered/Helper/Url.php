<?php

class AMZ_NavigationLayered_Helper_Url extends Mage_Core_Helper_Abstract
{
    private $_options = null;
    private $_attributes = null;
	private $_sufficCustomOption = 'ct_';
	public function getAttributesConfig()
	{
		if (is_null($this->_attributes))
        {
            $hash = array();
            $attributes = Mage::getModel('catalog/layer')->getFilterableAttributes();
			foreach($attributes as $attribute)
			{
				$hash[$attribute->getId()] = $attribute;
			}
			$this->_attributes = $hash;
		}
		return $this->_attributes;
	}
    public function getCanonicalUrl()
    {
        $key           = Mage::getStoreConfig('navigationlayered/seo/key');
        $canonicalType = Mage::getStoreConfig('navigationlayered/seo/canonical');
        $isSeo         = Mage::getStoreConfig('navigationlayered/seo/urls');
        
        if (0 == $canonicalType || !$isSeo){ // show just shopby
            return Mage::getBaseUrl() . $key;
        }
        elseif (1 == $canonicalType){ // as is
            return Mage::helper('core/url')->getCurrentUrl();        
        }
        
        // show firts attribute value as canonical         
        $url = Mage::helper('core/url')->getCurrentUrl();
        // possible values
        // shopby
        // shopby/atrr_name_value1-value2/attr2_value3.html
        // shopby/value_name1/value_name2.html 
        // shopby/apple.html        
        $parts = explode($key, $url, 2);
		$attributes = '';
		if(array_key_exists("1", $parts)) {
			$attributes = trim($parts[1], '/');
		}
        
        // we should look for first "-" or first "/"
        $pos  = max(0, strpos($attributes, '-'));
        $pos2 = max(0, strpos($attributes, '/'));
        if ($pos && $pos2)
            $pos = min($pos, $pos2);
        else
            $pos = max($pos, $pos2);
        
        if ($pos){
            $url = Mage::getBaseUrl() . $key . '/' . substr($attributes, 0, $pos); 
            $suffix     = Mage::getStoreConfig('catalog/seo/category_url_suffix'); 
            if ($suffix){
                $url .= $suffix; 
            }            
        }
        return $url;      
    }
    
    //optimized version of the getFullUrl
    public function getOptionUrl($attrCode, $optLabel, $optId, $optObject = false, $category = false, $brand = false)// finish
    {
		$brandKey =  Mage::getStoreConfig('navigationlayered/general/brandkey');
		if($optObject != false && $optObject->getCanonicalUrl() != null && $optObject->getCanonicalUrl()== false)
			return '';
        $url = Mage::getBaseUrl();
		if($category)
		{
			$url = $category->getUrl();
			$rootId = (int) Mage::app()->getStore()->getRootCategoryId();
			if($rootId == $category->getId())
				$url = Mage::getBaseUrl();
		}
        if (Mage::getStoreConfig('navigationlayered/seo/urls')){
			if(!$category && $brandKey)
				$url .= $brandKey . '/';
            if (!Mage::getStoreConfig('navigationlayered/seo/hide_attributes')){
                $url .= $attrCode . '-';
            }
            $url .= $this->createKey($optLabel);
            $url .= Mage::getStoreConfig('catalog/seo/category_url_suffix');
			
        }
        else {
            $url .= '?' . $attrCode . '=' . $optId; 
        }		
        return $url;      
    }    
    
    public function getFullUrl($query=array(), $clear=false, $cat = null)
    {
		$brandKey =  Mage::getStoreConfig('navigationlayered/general/brandkey');
        $url = '';
        
        $cat    = $cat ? $cat : Mage::registry('current_category');
        $rootId = (int) Mage::app()->getStore()->getRootCategoryId();
		$mod  = Mage::app()->getRequest()->getModuleName();
        $isSearch = in_array(Mage::app()->getRequest()->getModuleName(), array('sqli_singlesearchresult', 'catalogsearch'));
        $isNewOrSale = (('catalognew' == $mod) || ('catalogsale' == $mod));
        
        $reservedKey = Mage::getStoreConfig('navigationlayered/seo/key');
        
        $base = Mage::getBaseUrl();
        
        if ($isSearch){
            $url = $base . 'catalogsearch/result/';     
        }
        elseif ($isNewOrSale) {
            $url = $base . $mod . '/'; 
            if ($cat)
                $query['cat'] = $cat->getId();  
        }
        elseif (!$cat) { // homepage, 
            $url = $base . $reservedKey . '/';    
        }
        elseif ($cat->getId() == $rootId || $cat->getDisplayMode() == 'PAGE') {
            $url = $base; 
        }
        else { // we have a valid category
            $url = $cat->getUrl();
        }
		if($cat and $cat->getId() == "2") {
			$url = str_replace("//?","/". $brandKey ."/?",$url);
			$url = $base.$brandKey.'/';
		}
        $query = array_merge(Mage::app()->getRequest()->getQuery(), $query);
        $params = array();
        //remove nulls and empty vals 
        foreach ($query as $k => $v){
            if ($v){
                //sort values to avoid duplicate content
                if (!is_array($v) && strpos($v, ',')){
                    $v = explode(',', $v);
                    sort($v);
                    $v = implode(',', $v);
                }
                $params[$k] = $v;
            }
        }
        // sort attribute names to avoid duplicate content
        //ksort($params);
		
        if ($isSearch) { // leave as it was before
            if ($params && !$clear)
                $url .=  $this->_buildQuery($params);
            
            if ($clear)
                $url .= '?q=' . urlencode($params['q']);            
        } 
        else {
            if (!$clear){
                $query = $params;
                $attrPart = array();
                // 2) add attributes as keys, not as ids
                if (Mage::getStoreConfig('navigationlayered/seo/urls')){
                    $query = array();
                    $options = $this->getAllFilterableOptionsAsHash();
					$arrayOptions = array();
					$i=1;
                    foreach ($params as $attrCode => $ids)
                    {
                        if (isset($options[$attrCode])){ // it is filterable attribute
							$weight = $options[$attrCode]['weight'] + 10;
							if(isset($attrPart[$weight]))
							{
								$weight = $weight + $i;
								$i++;
							}
                            $attrPart[$weight] = $this->_formatAttributePart($attrCode, $ids);
                        }
                        else {
							if($attrCode == 'discount')
							{	
								$attrPart[100] = $this->_formatAttributePart($attrCode, $ids);
								continue;
							}
                            $query[$attrCode] = $ids; // it is pager or smth else
                        }
                    }
                }
                if ($attrPart){
                    //remove category suffix if any
                    $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                    if($suffix != "/")
					{
						$url    = $this->rtrim($url, $suffix); //str_replace($suffix, '', $url);
						$url    = rtrim($url, '/'); //str_replace($suffix, '', $url);
						//add identificator for router
						ksort($attrPart);
						if (!strpos($url, $reservedKey))
							$url .= '/'; //.$reservedKey.'/';
						// add attributes and options
					} 
					$pathSff = implode('',$attrPart);
                    $url .= $pathSff;
                    // add suffix back
                    $url = rtrim($url, '/') . $suffix;
                }
                
                // add other params as query string if any
				
                if ($query){
                    $url .=  $this->_buildQuery($query);
                }
            } else {
				if($cat->getId() == 2) {
					$url = $base . $brandKey. "/";
				} else {
					$url = $cat->getUrl();
				}
			}
			
					if($url == $base."/") {
						$url = $base . $brandKey . "/";
					}
        }
        return $this->checkSlashUrl($url); 
    }
	public function checkSlashUrl($url)
	{
		/* $http = explode('://', $url);
		if(isset($http[1]))
		{
			$http[1] = str_replace('//', '/', $http[1]);
		}else
		{
			$http[0] = str_replace('//', '/', $http[0]);
		}
		$url = implode('://', $http); */
		return $url;
	}
    public function getFullCatUrl($query=array(), $clear=false, $cat = null)
    {
		$brandKey =  Mage::getStoreConfig('navigationlayered/general/brandkey');
        $url = '';
        $cat    = $cat ? $cat : Mage::registry('current_category');
        $rootId = (int) Mage::app()->getStore()->getRootCategoryId();
        $mod  = Mage::app()->getRequest()->getModuleName();
        $isSearch = in_array(Mage::app()->getRequest()->getModuleName(), array('sqli_singlesearchresult', 'catalogsearch'));
        $isNewOrSale = (('catalognew' == $mod) || ('catalogsale' == $mod));
        $reservedKey = Mage::getStoreConfig('navigationlayered/seo/key');        
        $base = Mage::getBaseUrl();
        if ($isSearch){
            $url = $base . 'catalogsearch/result/';     
        }
        elseif (!$cat) { // homepage, 
            $url = $base . $reservedKey . '/';    
        }
        else { // we have a valid category
            $url = $cat->getUrl();
        }
		if($cat and $cat->getId() == "2") {
			$url = str_replace("//?","/". $brandKey ."/?",$url);
			$url = $base.$brandKey.'/';
		}
        $query = array_merge(Mage::app()->getRequest()->getQuery(), $query);
        $params = array();
        //remove nulls and empty vals 
        foreach ($query as $k => $v){
            if ($v){
                //sort values to avoid duplicate content
                if (is_string($v) && strpos($v, ',')){
                    $v = explode(',', $v);
                    sort($v);
                    $v = implode(',', $v);
                }
                $params[$k] = $v;
            }
        }
        // sort attribute names to avoid duplicate content
        // ksort($params);
        if ($isSearch) { // leave as it was before
            if ($params && !$clear)
                $url .=  $this->_buildQuery($params);
            
            if ($clear)
                $url .= '?q=' . urlencode($params['q']);            
        } 
        else {
            if (!$clear){
                $query = $params;
                $attrPart = array();
                // 2) add attributes as keys, not as ids
                if (Mage::getStoreConfig('navigationlayered/seo/urls')){
                    $query = array();
                    $options = $this->getAllFilterableOptionsAsHash();
					$arrayOptions = array();
					$i=1;
                    foreach ($params as $attrCode => $ids)
                    {
                        if (isset($options[$attrCode])){ // it is filterable attribute
							$weight = $options[$attrCode]['weight'] + 10;
							if(isset($attrPart[$weight]))
							{
								$weight = $weight + $i;
								$i++;
							}
                            $attrPart[$weight] = $this->_formatAttributePart($attrCode, $ids);
                        }
                        else {
							if($attrCode == 'customoption')
							{	
								if(is_array($ids))
								{
									foreach($ids as $id)
									{
										$attrPart[] = $this->_sufficCustomOption . $this->createKey($id) . '/';
									}
								}else
								{
									$attrPart[] = $this->_sufficCustomOption . $this->createKey($ids) . '/';
								}
								continue;
							}
							continue;
                            $query[$attrCode] = $ids; // it is pager or smth else
                        }
                    }
                }
                if ($attrPart){
                    //remove category suffix if any
                    $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                    if($suffix != "/")
					{
						$url    = $this->rtrim($url, $suffix); //str_replace($suffix, '', $url);
						$url    = rtrim($url, '/');
						//add identificator for router
						ksort($attrPart);
						if (!strpos($url, $reservedKey))
							$url .= '/'; //.$reservedKey.'/';
						// add attributes and options
					} 
					$pathSff = implode('',$attrPart);
                    $url .= $pathSff;
                    // add suffix back
                    $url = rtrim($url, '/') . $suffix;
                }
                // add other params as query string if any
				
                if ($query){
                    $url .=  $this->_buildQuery($query);
                }
            } else {
				if($cat->getId() == 2) {
					$url = $base . $brandKey . "/";
				} else {
					$url = $cat->getUrl();
				}
			}
			
					if($url == $base."/") {
						$url = $base . $brandKey . "/";
					}
        }
        return $url;
    }
    public function saveParams($request)
    {
		$currentParams = Mage::registry('navigationlayered_current_params'); 
		$category = Mage::registry('current_category');
        if (!$currentParams){
            return true;        
        }
        $options = $this->getAllFilterableOptionsAsHash();// return array('attribute_code' => array( 'option_title' => 'id')); in object catalog/layer
        if (!$options){
            return true;
        }
        $hideAttributeNames = Mage::getStoreConfig('navigationlayered/seo/hide_attributes');
        foreach ($currentParams as $params){
            $attrCode = '';
            $params   = explode('-', $params);
            foreach($params as $param)
			{
				$attrCode = false;
				if($category && $category->getCustomViewLayout() && ($this->CheckIsCustomOption($param) || $this->CheckIsDiamondOption($param)))
				{
					$query = array();
					$cutomOptions = $request->getParam('customoption');
					$this->CheckIsCustomOption($param) ? $cutomOptions['alloy'] = $this->CheckIsCustomOption($param) : '';   
					$this->CheckIsDiamondOption($param) ? $cutomOptions['diamond'] = $this->CheckIsDiamondOption($param) : '';
					$query['customoption'] = $cutomOptions;
					$request->setQuery($query);
					continue;
				}
				if ($hideAttributeNames && !$this->isDecimal($param)){
					$attrCode = $this->_getAttributeCodeByOptionKey($param, $options);
				}
				else {
					$attrCode = $param;
					array_shift($params); // remove first element  
					  if ($attrCode && isset($options[$attrCode])){
						$query = array();
						if ($this->isDecimal($attrCode)){
							$v = is_array($params[0]) ? null : $params[0]; // just in case 
							$query[$attrCode] = $v;    
						}
						else {
							$ids = $this->_convertOptionKeysToIds($params, $options[$attrCode]);
							$ids = $ids ? join(',', $ids) : $request->getParam($attrCode);  // fix for store changing 
							$v = is_array($ids) ? null : $ids; // just in case 
							$query[$attrCode] = $v;                    
						}
						$request->setQuery($query);
					}
					else { // we have undefined string 
						if ($attrCode == 'discount' && $this->isDecimal($attrCode)){
							$v = is_array($params[0]) ? null : $params[0]; // just in case 
							$query[$attrCode] = $v;    
							$request->setQuery($query);
						}else
						{
							return false;
						}
					}
					break;
				}
				//die($attrCode);
				if ($attrCode && isset($options[$attrCode])){
					$query = array();
					if ($this->isDecimal($attrCode)){
						$v = is_array($params[0]) ? null : $params[0]; // just in case 
						$query[$attrCode] = $v;    
					}
					else {
						$ids = $this->_convertOptionKeysToIds($params, $options[$attrCode]);
						$ids = $ids ? join(',', $ids) : $request->getParam($attrCode);  // fix for store changing 
						$v = is_array($ids) ? null : $ids; // just in case 
						$query[$attrCode] = $v;                    
					}
					$request->setQuery($query);
				}
				else { // we have undefined string 
					return false;
				}
			}
        }
      // print_r($request->getQuery());die;
        return true;
        
    }
    
    public function isDecimal($attrCode)
    {
        return in_array($attrCode, array('special_price','price','discount'));
    }
    
    public function getQuery()
    {
        $q = Mage::app()->getRequest()->getQuery();
        if ($q) {
            $q =  $this->_buildQuery($q);
        }
        else {
            $q = '';
        }
        
        return $q;
    }
    
    public function createKey($optionLabel)
    {
		if(is_array($optionLabel))
			$optionLabel = '';
        $key = Mage::helper('catalog/product_url')->format($optionLabel);
        $key = preg_replace('/[^0-9a-z,]+/i', '_', $key);
        $key = strtolower($key);
        $key = trim($key, '_-');

        return $key;
    } 

    public function getCategoryUrl($cat)
    {
        $pager = Mage::getBlockSingleton('page/html_pager')->getPageVarName();
        return $this->getFullUrl(array($pager => null), false, $cat);
    } 
    
    public function getAllFilterableOptionsAsHash()
    {
        if (is_null($this->_options))
        {
            $hash = array();
            $attributes = $this->getAttributesConfig();
			$attributesNav = Mage::getModel('navigationlayered/attributes')->getCollection();
			$prefixs = $weights = array();
			foreach($attributesNav as $att)
			{
				$weights[$att->getAttributeCode()] = $att->getWeight();
				$prefixs[$att->getAttributeCode()] = $att->getPrefix();
			}
			// echo $attributes->getSelect();
			// die;
            foreach ($attributes as $a){
                $code       = $a->getAttributeCode();
				if(isset($weights[$code]))
					$weight 	= $weights[$code];
				else
					$weight = 10;
				$prefix = isset($prefixs[$code]) ? $prefixs[$code] : '';
                $hash[$code] = array();
				$hash[$code]['weight'] = $weight;
				$hash[$code]['prefix'] = $prefix;
                foreach ($a->getSource()->getAllOptions() as $o){
                    if ($o['value']){ // skip first empty
                        $hash[$code][$this->createKey($hash[$code]['prefix'] . $o['label'])] = $o['value'];
                    }
                }
            }
            $this->_options = $hash;
        }
        
        return $this->_options;
    }
    
    private function _convertIdToKeys($options, $ids)
    {
        $options = array_flip($options);
        
        $keys = array();
        foreach (explode(',', $ids) as $optionId){
            if (isset($options[$optionId])){
                $keys[] = $options[$optionId];
            }
        }
        return join('-', $keys);
    } 
    
    private function _formatAttributePart($attrCode, $ids)
    {
        if ($this->isDecimal($attrCode)){
            return $attrCode . '-' . $ids . '/'; // always show price and other decimal attributes
        }

        $options = $this->getAllFilterableOptionsAsHash();
        $part    = $this->_convertIdToKeys($options[$attrCode], $ids); 
        
        if (!$part){
            return '';
        }
        
        $hideAttributeNames = Mage::getStoreConfig('navigationlayered/seo/hide_attributes');
        $part =  $hideAttributeNames ? $part : ($attrCode . '-' . $part);
        $part .=  '/';
        
        return $part;
    } 
    
    private function _getAttributeCodeByOptionKey($key, $optionsHash)
    {
        if (!$key) {
            return false;
        }
        
        foreach ($optionsHash as $code => $values){
            if (isset($values[$key])){
                return $code;
            }
        }
        
        return false;      
    }
    private function _buildQuery($query)
	{
		if(is_array($query) && isset($query['SID']))
			unset($query['SID']);
		if(is_array($query) && isset($query['cat']) && $query['cat'] == 'showall')
			unset($query['cat']);
		$url = '';
		if($query)
			$url = '?' . http_build_query($query);
		return $url;
	}
    private function _convertOptionKeysToIds($keys, $values)
    {
        $ids = array();
        foreach ($keys as $k){
            if (isset($values[$k])){
                $ids[] = $values[$k];
            }
        }
                
        return $ids;
    }
    public function addParamateUrl($url, $paramater)
	{
		if(count(explode('?',$url)) > 1)
			$url = $url . '&' . $paramater;
		else
			$url = $url . '?' . $paramater;
		return $url;
	}
	public function removeParamaterUrl($url, $paramater)
	{
		if(count(explode('&',$url )) > 1)
		{
			if(count(explode('?' . $paramater, $url)) > 1)
				$url = str_replace($paramater . '&', '', $url);
			else
				$url = str_replace('&'.$paramater , '', $url);
		}
		else
			$url = str_replace('?' . $paramater, '', $url);
		return $url;
	}
    public function CheckIsCustomOption($param)
	{
		$param = str_replace($this->_sufficCustomOption,'', $param);
		$colour = Mage::getStoreConfig('navigationlayered/general/custom_colour'); 
		$colour = json_decode($colour);
		if(!is_array($colour) && !is_object($colour))
			return false;
		foreach($colour as $customOptions)
		{
			foreach($customOptions as $key=>$value)
			{
				if($param == $this->createKey($key))
					return $key;
			}
		}
	}
	public function getValueCustomOpFromRequest($param, $type = 'navigationlayered/general/custom_colour', $returnkey = false)
	{
		$colour = Mage::getStoreConfig($type); 
		$colour = json_decode($colour);
		$param = $this->createKey($param);
		$param = str_replace($this->_sufficCustomOption,'', $param);
		if(!is_array($colour) && !is_object($colour))
			return false;
		foreach($colour as $customOptions)
		{
			foreach($customOptions as $key=>$value)
			{
				if($param == $this->createKey($key))
				{
					if(!$returnkey)
						return $value;
					else
						return $key;
				}
			}
		}
		return false;
	}
	public function CheckIsDiamondOption($param)
	{
		$param = str_replace($this->_sufficCustomOption,'', $param);
		$diamonds = Mage::getStoreConfig('navigationlayered/general/diamonds'); 
		$diamonds = json_decode($diamonds);
		if(!is_array($diamonds) && !is_object($diamonds))
			return false;
		foreach($diamonds as $customOptions)
		{
			foreach($customOptions as $key=>$value)
			{
				if($param == $this->createKey($key))
					return $key;
			}
		}
	}
	public function getFullUrlCustomOption($query=array(), $clear=false, $cat = null)
    {
		$brandKey =  Mage::getStoreConfig('navigationlayered/general/brandkey');
        $url = '';
        
        $cat    = $cat ? $cat : Mage::registry('current_category');
        $rootId = (int) Mage::app()->getStore()->getRootCategoryId();
		$mod  = Mage::app()->getRequest()->getModuleName();
        $isSearch = in_array(Mage::app()->getRequest()->getModuleName(), array('sqli_singlesearchresult', 'catalogsearch'));
        $isNewOrSale = (('catalognew' == $mod) || ('catalogsale' == $mod));
        
        $reservedKey = Mage::getStoreConfig('navigationlayered/seo/key');
        
        $base = Mage::getBaseUrl();
		if ($isNewOrSale) {
            $url = $base . $mod . '/'; 
            if ($cat)
                $query['cat'] = $cat->getId();  
        }
        elseif (!$cat) { // homepage, 
            $url = $base . $reservedKey . '/';    
        }
        elseif ($cat->getId() == $rootId || $cat->getDisplayMode() == 'PAGE') {
            $url = $base; 
        }
        else { // we have a valid category
            $url = $cat->getUrl();
        }
		if($cat and $cat->getId() == "2") {
			$url = str_replace("//?","/". $brandKey ."/?",$url);
		}
		$request = Mage::app()->getRequest()->getQuery();
		if(isset($request['customoption']) && is_array($query['customoption']))
		{
			$query['customoption'] = array_merge($request['customoption'], $query['customoption']);
			ksort($query['customoption']);
			$query = array_merge($request, $query);
		}
		else
			$query = array_merge($request, $query);
		//print_r($query);
        $params = array();
        //remove nulls and empty vals 
        foreach ($query as $k => $v){
            if ($v){
                //sort values to avoid duplicate content
                if (is_string($v) && strpos($v, ',')){
                    $v = explode(',', $v);
                    sort($v);
                    $v = implode(',', $v);
                }
				if($k == 'price')
					continue;
                $params[$k] = $v;
            }
        }
        // sort attribute names to avoid duplicate content
        //ksort($params);
		
        if ($isSearch) { // leave as it was before
            if ($params && !$clear)
                $url .=  $this->_buildQuery($params);
            
            if ($clear)
                $url .= '?q=' . urlencode($params['q']);            
        } 
        else {
            if (!$clear){
                $query = $params;
                $attrPart = array();
                // 2) add attributes as keys, not as ids
                if (Mage::getStoreConfig('navigationlayered/seo/urls')){
                    $query = array();
                    $options = $this->getAllFilterableOptionsAsHash();
					$arrayOptions = array();
					$i=1;
                    foreach ($params as $attrCode => $ids)
                    {
                        if (isset($options[$attrCode])){ // it is filterable attribute
							$weight = $options[$attrCode]['weight'] + 10;
							if(isset($attrPart[$weight]))
							{
								$weight = $weight + $i;
								$i++;
							}
                            $attrPart[$weight] = $this->_formatAttributePart($attrCode, $ids);
                        }
                        else {
							if($attrCode == 'customoption')
							{	
								if(is_array($ids))
								{
									foreach($ids as $key => $id)
									{
										if($id)
											$attrPart[] = $this->_sufficCustomOption . $this->createKey($id) . '/';
									}
								}else
								{
									$attrPart[] = $this->_sufficCustomOption . $this->createKey($ids) . '/';
								}
								continue;
							}
                            $query[$attrCode] = $ids; // it is pager or smth else
                        }
                    }
                }
                if ($attrPart){
                    //remove category suffix if any
                    $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                    if($suffix != "/")
					{
						$url    = $this->rtrim($url, $suffix); //str_replace($suffix, '', $url);
						$url    = rtrim($url, '/');
						//add identificator for router
						ksort($attrPart);
						if (!strpos($url, $reservedKey))
							$url .= '/'; //.$reservedKey.'/';
						// add attributes and options
					} 
					$pathSff = implode('',$attrPart);
                    $url .= $pathSff;
                    // add suffix back
                    $url = rtrim($url, '/') . $suffix;
                }
                
                // add other params as query string if any
				
                if ($query){
                    $url .=  $this->_buildQuery($query);
                }
            } else {
				if($cat->getId() == 2) {
					$url = $base . $brandKey. "/";
				} else {
					$url = $cat->getUrl();
				}
			}
			
					if($url == $base."/") {
						$url = $base . $brandKey . "/";
					}
        }
        return $this->checkSlashUrl($url); 
    }
	public function rtrim($string, $trim)
	{
		if(strlen($trim) < 2)
			return rtrim($string, $trim);
		if(substr($string, strlen($string) - strlen($trim), strlen($trim)) == $trim)
		{
			return substr($string, 0, strlen($string) - strlen($trim));
		}
		return $string;
	}
}