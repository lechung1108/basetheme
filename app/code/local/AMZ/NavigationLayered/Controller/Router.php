<?php

class AMZ_NavigationLayered_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
	
    public function match(Zend_Controller_Request_Http $request) 
    {	
        if (Mage::app()->getStore()->isAdmin()) {
            return false;
        }
		$helper = Mage::helper('navigationlayered/url');
		$brandKey =  Mage::getStoreConfig('navigationlayered/general/brandkey');
        $pageId = $request->getPathInfo();
        // remove suffix if any
        $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
        //$pageId = str_replace($suffix, '', $pageId);
        
        //remove trailing slash if any
        $pageId = $helper->rtrim($pageId, $suffix);
        $reservedKey = Mage::getStoreConfig('navigationlayered/seo/key');
        //check if we have reserved word in the url
		
		
        if (false === strpos($pageId, $reservedKey)) {
			//action in here Dovantuan
			if(strpos($pageId, "/")) {
				list($cat, $params) = explode("/", $pageId, 2);
			} else {
				//action in here
				$cat = $pageId;
			}
			$model = Mage::getModel('catalog/category');
			$ids = $model->getCollection()->addAttributeToSelect(array('*'))->addIsActiveFilter();
			$pageId = ltrim($pageId, '/');
			if ($ids) {
				// print_r($ids);die;
				$leavecat = 0;
				$urlPath = '';
				foreach ($ids as $cur) {
					//$cur = $model->load($id);
					$arrayPath = $cur->getData('url_path') ? explode(str_replace($suffix, '', $cur->getData('url_path')) , $pageId, 2) : array();
					if($urlPath = $cur->getData('url_path'))
					{
						$urlPath = $helper->rtrim($urlPath, $suffix);
						$arrayPath = explode($urlPath, $pageId, 2);
					}else continue;
					if(count($arrayPath) > 1)
					{	
						if($arrayPath[0])
							continue;
						if(count(explode( '/', $cur->getData('url_path'))) >= count(explode( '/', $urlPath)))
						{
							$urlPath = $cur->getData('url_path');
							$cat =  $urlPath;
							$leavecat = 1;
							$params = explode("/", $arrayPath[1] ,100);
						}
					}
				}
				if($leavecat != 1) {
					$cat = "";
					$pageId = str_replace($brandKey,'', $pageId);
					$params = explode("/",$pageId,100);
					//return false;
				}
			}
        } else {
			list($cat, $params) = explode($reservedKey, $pageId, 2);
		}
		$newparams = array();
		if(isset($params) && is_array($params)) {
			foreach($params as $index => $data) {
				if($data != "") {
					$newparams[] = $data;
				}
			}
		}
		$params = $newparams;
        // remember for futire use in the helper
        if ($params){
            Mage::register('navigationlayered_current_params', $params);
        }  
		$cat = trim($cat, '/');
        if ($cat){ // normal category
            // if somebody has old urls in the cache.
			// die(Mage::getStoreConfig('navigationlayered/seo/urls'));
            if (!Mage::getStoreConfig('navigationlayered/seo/urls'))
                return false;
            $urlRewrite = Mage::getModel('core/url_rewrite')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->loadByRequestPath($cat); 
            if (!$urlRewrite->getId()){
                return false;
            }
			$realModule = 'AMZ_NavigationLayered';
            $request->setPathInfo($reservedKey);
            $request->setModuleName('navigationlayered');
            $request->setRouteName('navigationlayered');  
            $request->setControllerName('index');
            $request->setActionName('view'); 
			$request->setParam('id', $urlRewrite->getCategoryId());
            $request->setControllerModule($realModule);
            
            $file = Mage::getModuleDir('controllers', $realModule) . DS . 'IndexController.php';
            include $file;
            
            //compatibility with 1.3
            $class = $realModule . '_IndexController';
            $controllerInstance = new $class($request, $this->getFront()->getResponse()); 
                          
            $request->setDispatched(true);
            $controllerInstance->dispatch('view');   
        }
        else { // root category
            $realModule = 'AMZ_NavigationLayered';
            $request->setPathInfo($reservedKey);
            $request->setModuleName('navigationlayered');
            $request->setRouteName('navigationlayered');  
            $request->setControllerName('index');
            $request->setActionName('index'); 
            $request->setControllerModule($realModule);
            
            $file = Mage::getModuleDir('controllers', $realModule) . DS . 'IndexController.php';
            include $file;
            
            //compatibility with 1.3
            $class = $realModule . '_IndexController';
            $controllerInstance = new $class($request, $this->getFront()->getResponse()); 
                          
            $request->setDispatched(true);
            $controllerInstance->dispatch('index');
        }
        return true;
    }
}