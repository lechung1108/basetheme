<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesattributes')}` ADD `collapse` int(11) NOT NULL default '0' AFTER `meta_robots`");
$installer->endSetup(); 