<?php
$installer = $this;
$installer->startSetup();
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('amz_attributes')};
CREATE TABLE {$this->getTable('amz_attributes')} (
  `attribute_id` int(11) unsigned NOT NULL auto_increment,
  `attribute_code` varchar(255) NOT NULL default '',
  `display_type` int(5) NOT NULL default '0',
  `show_in_list` smallint(4) NOT NULL default '1',
  `show_in_view` smallint(4) NOT NULL default '1',
  `weight` int(10) unsigned NOT NULL default '10',
  `scroll_height` int NOT NULL DEFAULT '100',
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS {$this->getTable('amz_attributes_entity')};
CREATE TABLE {$this->getTable('amz_attributes_entity')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `entity_id` int(11) NOT NULL default '0',
  `store_id` int(11) NULL default '0',
  `frontend_label` varchar(255) NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


 DROP TABLE IF EXISTS {$this->getTable('amz_attributes_value')};
 CREATE TABLE {$this->getTable('amz_attributes_value')} (
  `option_id` int(11) NOT NULL default '0',
  `attribute_id` int(11) NOT NULL default '0',
  `navigation_icon` varchar(255) NOT NULL default '',
  `listing_icon` varchar(255) NOT NULL default '',
  `detail_icon` varchar(255) NOT NULL default '',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 DROP TABLE IF EXISTS {$this->getTable('amz_attributes_value_entity')};
 CREATE TABLE {$this->getTable('amz_attributes_value_entity')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`entity_id` int(11) NOT NULL default '0',
	`store_id` int(11) NULL default '0',
	`title` varchar(255) NOT NULL default '',
	`meta_title` varchar(255) NOT NULL default '',
	`meta_description` text NOT NULL default '',
	`description` text NOT NULL default '',
	`cms_block` varchar(255) NULL default '',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('amz_brands')};
CREATE TABLE {$this->getTable('amz_brands')} (
  `brand_id`  int(11) NOT NULL default '0',
  `attribute_id` int(11) NOT NULL default '0',
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS {$this->getTable('amz_categories_attributes')};
CREATE TABLE {$this->getTable('amz_categories_attributes')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`attribute_id` int(11) NOT NULL default '0',
	`category_id` int(11) NOT NULL default '0',
	`featured_category` smallint(4) NOT NULL default '1',
	`canonical_url` smallint(4) NOT NULL default '0',
	`multiple_selections` smallint(4) NOT NULL default '0',
	`filter_visible` smallint(4) NOT NULL default '1',
	`meta_robots` smallint(4) NOT NULL default '0',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS {$this->getTable('amz_categories_attributes_entity')};
CREATE TABLE {$this->getTable('amz_categories_attributes_entity')} (
	`ids` int(11) unsigned NOT NULL auto_increment,
	`entity_id` int(11) NOT NULL default '0',
	`store_id` int(11) NULL default '0',
	`cms_block` varchar(255) NOT NULL default '',
	`meta_title` varchar(255) NOT NULL default '',
	`meta_description` text NOT NULL default '',
	`description` text NOT NULL default '',
	`image` varchar(255) NOT NULL default '',
	PRIMARY KEY (`ids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('amz_categories_options')};
CREATE TABLE {$this->getTable('amz_categories_options')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`option_id` int(11) NOT NULL default '0',
	`group_id` int(11) NOT NULL default '0',
	`category_id` int(11) NOT NULL default '0',
	`featured_category` smallint(4) NOT NULL default '1',
	`canonical_url` smallint(4) NOT NULL default '0',
	`multiple_selections` smallint(4) NOT NULL default '0',
	`filter_visible` smallint(4) NOT NULL default '1',
	`meta_robots` smallint(4) NOT NULL default '0',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS {$this->getTable('amz_categories_options_entity')};
CREATE TABLE {$this->getTable('amz_categories_options_entity')} (
	`ids` int(11) unsigned NOT NULL auto_increment,
	`entity_id` int(11) NOT NULL default '0',
	`store_id` int(11) NULL default '0',
	`cms_block` varchar(255) NOT NULL default '',
	`meta_title` varchar(255) NOT NULL default '',
	`meta_description` text NOT NULL default '',
	`description` text NOT NULL default '',
	`image` varchar(255) NOT NULL default '',
	PRIMARY KEY (`ids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS {$this->getTable('amz_categories_brands')};

DROP TABLE IF EXISTS {$this->getTable('amz_brands_attributes')};
CREATE TABLE {$this->getTable('amz_brands_attributes')} (
	`id` int(11) unsigned NOT NULL auto_increment,
	`brand_id` int(11) NOT NULL default '0',
	`attribute_id` int(11) NOT NULL default '0',
	`include_page` smallint(4) NOT NULL default '1',
	`meta_robots` smallint(4) NOT NULL default '0',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"
);
$installer->endSetup(); 