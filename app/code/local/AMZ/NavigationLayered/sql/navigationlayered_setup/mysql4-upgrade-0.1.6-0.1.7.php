<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/attributes')}` ADD `prefix` varchar(11) NULL default '' AFTER `weight`");
$installer->endSetup(); 