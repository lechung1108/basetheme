<?php

$installer = $this;

$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` ADD `slider_id` varchar(255) NOT NULL default '' AFTER `meta_robots`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` ADD `slider_num_product` varchar(255) NOT NULL default '' AFTER `meta_robots`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` ADD `slider_status` SMALLINT(4) NOT NULL default '0' AFTER `meta_robots`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` ADD `slider_line_display` varchar(255) NOT NULL default '' AFTER `meta_robots`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` ADD `slider_align` varchar(255) NOT NULL default '' AFTER `meta_robots`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` ADD `popup_slide` varchar(255) NOT NULL default '' AFTER `meta_robots`");
$installer->endSetup();