<?php

$installer = $this;

$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` DROP `slider_id`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` DROP `slider_num_product`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` DROP `slider_status`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` DROP `slider_line_display`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` DROP `slider_align`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesoptions')}` DROP `popup_slide`");
// add column to table store
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/catoptionstore')}` ADD `slider_id` varchar(255) NOT NULL default '' AFTER `image`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/catoptionstore')}` ADD `slider_num_product` varchar(255) NOT NULL default '' AFTER `image`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/catoptionstore')}` ADD `slider_status` SMALLINT(4) NOT NULL default '0' AFTER `image`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/catoptionstore')}` ADD `slider_line_display` varchar(255) NOT NULL default '' AFTER `image`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/catoptionstore')}` ADD `slider_align` varchar(255) NOT NULL default '' AFTER `image`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/catoptionstore')}` ADD `popup_slide` varchar(255) NOT NULL default '' AFTER `image`");


$installer->endSetup();