<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesattributes')}` CHANGE  `featured_category`  `featured_category` SMALLINT( 4 ) NOT NULL DEFAULT  '0'");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesattributes')}` CHANGE  `filter_visible`  `filter_visible` SMALLINT( 4 ) NOT NULL DEFAULT  '0'");
$installer->run("ALTER TABLE  `{$installer->getTable('navigationlayered/categoriesattributes')}` ADD UNIQUE (`attribute_id` ,`category_id`);");
$installer->run("ALTER TABLE  `{$installer->getTable('navigationlayered/categoriesoptions')}` ADD UNIQUE (`option_id` ,`category_id`);");
$installer->run("ALTER TABLE  `{$installer->getTable('navigationlayered/optionstore')}` ADD UNIQUE (`entity_id` ,`store_id`);");
$installer->run("ALTER TABLE  `{$installer->getTable('navigationlayered/attstore')}` ADD UNIQUE (`entity_id` ,`store_id`);");
$installer->run("ALTER TABLE  `{$installer->getTable('navigationlayered/catattstore')}` ADD UNIQUE (`entity_id` ,`store_id`);");
$installer->run("ALTER TABLE  `{$installer->getTable('navigationlayered/catoptionstore')}` ADD UNIQUE (`entity_id` ,`store_id`);");
$installer->endSetup(); 