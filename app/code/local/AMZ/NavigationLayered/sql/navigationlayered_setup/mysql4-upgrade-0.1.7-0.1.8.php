<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/categoriesattributes')}` ADD `show_above_toolbar` int(11) NOT NULL default '1' AFTER `meta_robots`");
$installer->endSetup(); 