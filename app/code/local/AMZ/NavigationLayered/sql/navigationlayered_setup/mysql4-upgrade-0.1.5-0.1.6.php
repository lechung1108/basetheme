<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/brands')}` ADD `parent_brand` int(11) NULL AFTER `attribute_id`");
$installer->endSetup(); 