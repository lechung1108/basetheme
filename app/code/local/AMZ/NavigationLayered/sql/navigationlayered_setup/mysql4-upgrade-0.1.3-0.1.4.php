<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/attributesvalue')}` DROP `base_url`");
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/optionstore')}` ADD `base_url` varchar(255) NOT NULL default '' AFTER `cms_block`");
$installer->endSetup(); 