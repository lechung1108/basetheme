<?php

$installer = $this;

$installer->startSetup();
$installer->run("ALTER TABLE `{$installer->getTable('navigationlayered/attributesvalue')}` ADD `base_url` varchar(255) NOT NULL default '' AFTER `detail_icon`");
$installer->endSetup(); 