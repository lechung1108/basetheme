<?php
class AMZ_NavigationLayered_Block_Canonical extends Mage_Core_Block_Template
{
   protected function _prepareLayout()
    {	
		$head = $this->getLayout()->getBlock('head');
        if ($head && Mage::registry('canonical_newurl')){
			$url = Mage::getSingleton('catalog/layer')->getCurrentCategory()->getUrl();
		}
		return parent::_prepareLayout();
	}
}