<?php
  
class AMZ_NavigationLayered_Block_List extends Mage_Core_Block_Template
{
    private $items = array();
    
    protected function _prepareLayout()
    {
        $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
        $modelName  = Mage::helper('navigationlayered')->isVersionLessThan(1, 4) ? 'catalog/entity_attribute' : 'catalog/resource_eav_attribute';
		if($this->getAttributeCode())
		{	
			$attribute  = Mage::getModel($modelName) 
						->loadByCode($entityTypeId, $this->getAttributeCode());
		}
		else
		{
			$brand_code = Mage::getModel('navigationlayered/brands')->getCollection()->getJoinAttribute()->getFirstItem();
			$attribute  = Mage::getModel($modelName) 
							->loadByCode($entityTypeId, $brand_code->getAttributeCode());
		}
        
        if (!$attribute->getId()){
            return parent::_prepareLayout();
        }
          
        //1.3 only
        if (!$attribute->getSourceModel()){
            $attribute->setSourceModel('eav/entity_attribute_source_table'); 
        }
        
        $options = $attribute->getFrontend()->getSelectOptions();
        array_shift($options);
        
        $filter = new Varien_Object();
        // important when used at category pages
        $layer = Mage::getModel('catalog/layer')
            ->setCurrentCategory(Mage::app()->getStore()->getRootCategoryId());
        
        $filter->setLayer($layer);
        $filter->setStoreId(Mage::app()->getStore()->getId());
        $filter->setAttributeModel($attribute);
        
        $optionsCount = array();
        if (Mage::helper('navigationlayered')->isVersionLessThan(1, 4)){
            $category   = Mage::getModel('catalog/category')->load(Mage::app()->getStore()->getRootCategoryId());
            $collection = $category->getProductCollection();
            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
            
            $optionsCount = Mage::getSingleton('catalogindex/attribute')
                ->getCount($attribute, $collection->getSelect());
        }
        else {
            $optionsCount = Mage::getResourceModel('catalog/layer_filter_attribute')->getCount($filter);
        }
        
        usort($options, array($this, '_sortByName'));
                 
        // add images
        $ids = array();
        foreach ($options as $opt){
            $ids[] = $opt['value']; 
        }
        $collection = Mage::getResourceModel('navigationlayered/attributesvalue_collection')
			->addStoreFilter()
            ->addFieldToFilter('option_id', array('in'=>$ids))
            ->load();
       $objects = $images = array();
        foreach ($collection as $value){
            $images[$value->getOptionId()] = $value->getListingIcon() ? Mage::getBaseUrl('media') . 'attributes/' . $value->getListingIcon() : ''; 
			$objects[$value->getOptionId()] = $value;
        }
        // end add images        
            
        $c = 0;
        $letters = array();
        $hlp    = Mage::helper('navigationlayered/url');
        foreach ($options as $opt){
            // CBG
			//if (!empty($optionsCount[$opt['value']])){
                //$img = Mage::getResourceModel('navigationlayered/attributesvalue_collection')->addStoreFilter()->addFieldToFilter('option_id', $opt['value'])->load()->getData();
				$opt['image'] = isset($images[$opt['value']]) ?  $images[$opt['value']] : '';
				if(in_array($opt['value'], $optionsCount)) {
					$opt['cnt'] = $optionsCount[$opt['value']];
				}
                $opt['url'] = $hlp->getOptionUrl($attribute->getAttributeCode(), $opt['label'], $opt['value'], false, false, true);
                if(isset($objects[$opt['value']]))
				{
					$optObject =  $objects[$opt['value']];
					if($storeCode = $optObject->getBaseUrl())
					{
						$storeUrl = Mage::helper('navigationlayered')->getStoreDetail($storeCode)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
						$opt['url'] = str_replace(Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK), $storeUrl, $opt['url']);
					}
				}
                $i = strtoupper(substr($opt['label'], 0, 1));
                
                if (!isset($letters[$i]['items'])){
                    $letters[$i]['items'] = array();
                }
                    
                $letters[$i]['items'][] = $opt;
               
                if (!isset($letters[$i]['count'])){
                    $letters[$i]['count'] = 0;
                }
                    
                $letters[$i]['count']++;
                
                ++$c;
            //}
        }
        
        if (!$letters){
            return parent::_prepareLayout();
        }
        
        $itemsPerColumn = ceil(($c + sizeof($letters)) / max(1, abs(intVal($this->getColumns()))));

        $col = 0; // current column 
        $num = 0; // current number of items in column
        foreach ($letters as $letter => $items){
            $this->items[$col][$letter] = $items['items'];
            $num += $items['count'];
            $num++;
            if ($num >= $itemsPerColumn){
                $num = 0;
                $col++;
            }
        }
        
        return parent::_prepareLayout();
    }
    
    public function getItems()
    {
        return $this->items;
    }
    
    public function _sortByName($a, $b)
    {
        return strcmp($a['label'], $b['label']);
    }
	public function getBrandCollection()
	{ 
		$brands = Mage::getModel('navigationlayered/brands')->getCollection()->getParentsCollection()->getJoinOptions();
		return $brands;
	}
}