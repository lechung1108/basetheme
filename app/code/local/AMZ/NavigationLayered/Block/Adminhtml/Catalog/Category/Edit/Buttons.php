<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Enterprise
 * @package     Enterprise_CatalogEvent
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Catalog Events edit form select categories
 *
 * @category   Enterprise
 * @package    Enterprise_CatalogEvent
 */

class AMZ_NavigationLayered_Block_Adminhtml_Catalog_Category_Edit_Buttons extends Mage_Adminhtml_Block_Catalog_Category_Abstract
{
    /**
     * Retrieve category event
     *
     * @return Enterprise_CatalogEvent_Model_Category
     */
    public function getEvent()
    {
        if (!$this->hasData('event')) {
            $collection = Mage::getModel('catalog/category')->getCollection();

            $event = $collection->getFirstItem();
            $this->setData('event', $event);
        }

        return $this->getData('event');
    }

    /**
     * Add buttons on category edit page
     *
     * @return Enterprise_CatalogEvent_Block_Adminhtml_Catalog_Category_Buttons
     */
    public function addButtons()
    {
		$category = Mage::registry('current_category');
		if($category -> getLevel() >= 2)
		{
			$url = $this->helper('adminhtml')->getUrl('adminhtml/navigationlayered_category/edit', array(
				'category_id' => $this->getCategoryId()
			));
			$this->getParentBlock()->getChild('form')
				->addAdditionalButton('setting', array(
				'label' => $this->helper('navigationlayered')->__('Change setting attributes'),
				'class' => 'change',
				'onclick' => 'setLocation(\''. $url .'\')'
			));
        }
		/* elseif($category->getId() == 2)
		{
			$url = $this->helper('adminhtml')->getUrl('adminhtml/navigationlayered_category/brandDescription', array(
				'category_id' => $this->getCategoryId(),
			));
			$this->getParentBlock()->getChild('form')
				->addAdditionalButton('setting', array(
				'label' => $this->helper('navigationlayered')->__('Add Brand Des'),
				'class' => 'add',
				'onclick' => 'setLocation(\''. $url .'\')'
			));
		}  */
        return $this;
    }
}
