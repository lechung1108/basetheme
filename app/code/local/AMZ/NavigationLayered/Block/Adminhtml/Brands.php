<?php
class AMZ_NavigationLayered_Block_Adminhtml_Brands extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_brands';
    $this->_blockGroup = 'navigationlayered';
    $this->_headerText = Mage::helper('navigationlayered')->__('Brands Manager');   
    parent::__construct();
	$this->setTemplate('amz_navigationlayered/brands/brands.phtml');
  }
  /**
     * Prepare button and grid
     *
     * @return AMZ_NavigationLayered_Block_Adminhtml_Collars
     */
    protected function _prepareLayout()
    {
        $this->setChild('add_new_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('navigationlayered')->__('Change Attribute Brands'),
                    'onclick'   => "setLocation('".$this->getUrl('*/*/selectAttBrands')."')",
                    'class'   => 'change'
                    ))
                );
		$this->setChild('load_new',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('navigationlayered')->__('Load brands'),
                    'onclick'   => "setLocation('".$this->getUrl('*/*/loadBrands')."')",
                    'class'   => 'change'
                    ))
                );
		$this->setChild('load_new_attribute',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('navigationlayered')->__('Load Attributes'),
                    'onclick'   => "setLocation('".$this->getUrl('*/*/loadAttributes')."')",
                    'class'   => 'change'
                    ))
                );
		 if (!Mage::app()->isSingleStoreMode()) {
            $this->setChild('store_switcher',
                $this->getLayout()->createBlock('adminhtml/store_switcher')
                    ->setUseConfirm(false)
                    ->setSwitchUrl($this->getUrl('*/*/*', array('store'=>null)))
            );
        }
        $this->setChild('grid', $this->getLayout()->createBlock('navigationlayered/adminhtml_brands_grid', 'brands.grid'));
        return parent::_prepareLayout();
    }
	
    public function getAddNewButtonHtml()
    {
        return $this->getChildHtml('add_new_button');
    }
   

    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

    public function getStoreSwitcherHtml()
    {
        return $this->getChildHtml('store_switcher');
    }
	public function getButtonLoadNew()
	{
		 return $this->getChildHtml('load_new');
	}
	public function getButtonLoadNewAtt()
	{
		 return $this->getChildHtml('load_new_attribute');
	}
	public function getWeightUrl()
	{
		return $this->getUrl('*/*/changeWeight', array('_current'=>true));
	}
}