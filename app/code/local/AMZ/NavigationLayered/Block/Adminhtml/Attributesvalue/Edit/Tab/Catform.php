<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to buttonize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml catalog product downloadable items tab and form
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Block_Adminhtml_Attributesvalue_Edit_Tab_Catform extends Mage_Adminhtml_Block_Template
{

    /**
     * Reference to product objects that is being edited
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product = null;

    protected $_config = null;
    protected $_brandId = '607';

    public function __construct()
    {
        parent::__construct();
//        $this->setSkipGenerateContent(true);
    }
	public function getFormText($id, $name, $value ='')
	{
		$form = new Varien_Data_Form();
		$form->addField($id, 'text', array(
			'name' => $name,
			'value'		=> $value
			));
		return $form->getHtml();
	}
	public function getFormTextArea($id, $name, $value ='')
	{
		$form = new Varien_Data_Form();
		$form->addField($id, 'textarea', array(
			'name' =>  $name,
			'style'     => 'width:280px; height:100px;',
			'value'		=> $value
			));
		return $form->getHtml();
	}
	public function getFormFile($id, $name)
	{
		$form = new Varien_Data_Form();
		
		$form->addField($id, 'file', array(
				'required'  => false,
				'name'      => $name,
				));
		return $form->getHtml();
	}
	public function getAttributeCats()
    {
        return Mage::registry('option_cats');
    }
	public function getNodeDel($idCat, $name, $value = '')
	{
		$path = Mage::getBaseUrl('media') . 'attributes/';
		if($value != '')
		{
			return '<a onclick="imagePreview(\'view'.$idCat.'\'); return false;" href="'.$path.$value.'"><img class="view_image" id ="view'.$idCat.'"width="50px" src="'.$path.$value.'"/></a><input type="checkbox" name="'.$name.'[checkbox]"/><input type="hidden" value="'.$value.'" name="'.$name.'[hidden]"/><span> Delete</span>';
		}
		return '';
	}
	public function featuredCategorySelect($idCat, $value)
    {
		$options = array(
						array('value'=>'1','label'=>$this->__('Yes')),
						array('value'=>'0','label'=>$this->__('No'))
					);
		
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][featured_category]')
            ->setId('featured_category' . $idCat)
            ->setOptions($options)
            ->setValue($value);
        return $select->getHtml();
    }
	public function canonicalUrlSelect($idCat, $value)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][canonical_url]')
            ->setId('canonical_url' . $idCat)
            ->setOptions($options)
            ->setValue($value);
        return $select->getHtml();
    }
	public function multipleSelectionSelect($idCat, $value)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][multiple_selections]')
            ->setId('multiple_selections' . $idCat)
            ->setOptions($options)
            ->setValue($value);
        return $select->getHtml();
    }
	public function filterVisibleSelect($idCat, $value)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][filter_visible]')
            ->setId('filter_visible ' . $idCat)
            ->setOptions($options)
            ->setValue($value);
        return $select->getHtml();
    }
	public function getMetaRobotsSelect($idCat, $value)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('NOINDEX,FOLLOW')),
						array('value'=>'1','label'=>$this->__('INDEX,FOLLOW'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][meta_robots]')
            ->setId('meta_robots' . $idCat)
            ->setOptions($options)
            ->setValue($value);
        return $select->getHtml();
    }
	public function getSlideStatus($idCat, $value)
	{
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][slider_status]')
            ->setId('slider_status' . $idCat)
            ->setOptions($options)
            ->setValue($value);
        return $select->getHtml();
	}
	protected function _toHtml()
    {
        Mage::dispatchEvent('adminhtml_block_html_before', array('block' => $this));
    //    return parent::_toHtml();
    	$catOption = $this->getAttributeCats();
		if(!$catOption)
			return '';
		$html = '<div class="fieldset">
								<div class="grid">
									<table class="form-list">
										<tr>
											<td class="label">'.$this->__('Cms Block').'</td>
											<td class="value">
												'.$this->getFormText("cms_block".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][cms_block]", $catOption->getCmsBlock()).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Meta title').'</td>
											<td class="value">
												'.$this->getFormText("meta_title".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][meta_title]", $catOption->getMetaTitle()).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Meta Description').'</td>
											<td class="value">
												'.$this->getFormTextArea("meta_description".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][meta_description]", $catOption->getMetaDescription()).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Description').'</td>
											<td class="value">
												'.$this->getFormTextArea("description".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][description]", $catOption->getDescription()).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Image').'</td>
												<td class="value">
													'.$this->getFormFile("image".$catOption->getCategoryId(),"cat_image_".$catOption->getCategoryId()) . $this->getNodeDel($catOption->getCategoryId(), "cat[".$catOption->getCategoryId()."][del_image]", $catOption->getImage()).'    
												</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Featured Category").'</td>
											<td class="value">'.$this->featuredCategorySelect($catOption->getCategoryId(), $catOption->getFeaturedCategory()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Canonical Url").'</td>
											<td class="value">'.$this->canonicalUrlSelect($catOption->getCategoryId(), $catOption->getCanonicalUrl()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Multiple Selection").'</td>
											<td class="value">'.$this->multipleSelectionSelect($catOption->getCategoryId(), $catOption->getMultipleSelections()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Filter Visible").'</td>
											<td class="value">'.$this->filterVisibleSelect($catOption->getCategoryId(), $catOption->getFilterVisible()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Meta robots").'</td>
											<td class="value">'.$this->getMetaRobotsSelect($catOption->getCategoryId(), $catOption->getMetaRobots()).'</td>
										</tr>
									</table>
									<table class="form-list">
										<tr>
											<th>
												'. $this->__("Setting slide show") .'
											</th>
										</tr>
										<tr>
											<td class="label">'. $this->__('Slide Ids') .'</td>
											<td class="value">'. $this->getFormText("slider_id".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][slider_id]" , $catOption->getSliderId()) .'</td>
										</tr>
										<tr>
											<td class="label">'. $this->__('Popup slide') .'</td>
											<td class="value">'. $this->getFormText("popup_slide".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][popup_slide]" ,$catOption->getPopupSlide()) .'</td>
										</tr>
										<tr>
											<td class="label">'. $this->__('Line display') .'</td>
											<td class="value">'. $this->getFormText("slider_line_display".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][slider_line_display]" ,$catOption->getSliderLineDisplay()) .'</td>
										</tr>
										<tr>
											<td class="label">'. $this->__('Number product') .'</td>
											<td class="value">'. $this->getFormText("slider_num_product".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][slider_num_product]" ,$catOption->getSliderNumProduct()) .'</td>
										</tr>
										<tr>
											<td class="label">'. $this->__('Slide align') .'</td>
											<td class="value">'. $this->getFormText("slider_align".$catOption->getCategoryId(),"cat[".$catOption->getCategoryId()."][slider_align]" ,$catOption->getSliderAlign()) .'</td>
										</tr>
										<tr>
											<td class="label">'. $this->__('Slide status') .'</td>
											<td class="value">'. $this->getSlideStatus($catOption->getCategoryId(), $catOption->getSliderStatus()) .'</td>
										</tr>
									</table>
								</div>
							</div>';
		return $html;
	}
}
