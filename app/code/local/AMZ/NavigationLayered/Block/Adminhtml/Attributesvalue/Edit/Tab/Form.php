<?php

class AMZ_NavigationLayered_Block_Adminhtml_Attributesvalue_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	
  protected function _prepareForm()
  {
	    $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/saveOption'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
				)
			);
		$Option =Mage::registry('option_data');
		$this->setForm($form);
		$helper = Mage::helper('navigationlayered');
		$path = Mage::getBaseUrl('media') . 'attributes/';
		$fieldset = $form->addFieldset('option_form', array('legend'=>$helper->__('Item information')));
		$fieldset->addField('title', 'text', array(
			'name' => 'title',
			'label' => Mage::helper('navigationlayered')->__('Title'),
			'required'  => true,
			// 'disabled'	=>true
		));
		$fieldset->addField('meta_title', 'text', array(
			'name' => 'meta_title',
			'label' => Mage::helper('navigationlayered')->__('Meta title'),
			'required'  => false
		));
		
		$fieldset->addField('meta_description', 'textarea', array(
				'name' => 'meta_description',
				'label' => Mage::helper('navigationlayered')->__('Meta Description'),
				'style'     => 'width:280px; height:100px;'
			));
		$fieldset->addField('description', 'textarea', array(
				'name' => 'description',
				'label' => Mage::helper('navigationlayered')->__('Description'),
				'style'     => 'width:280px; height:100px;'
			));
		//print_r($this->getRequest()->getControllerName());die;
		if($this->getRequest()->getControllerName() == 'adminhtml_brands')
		{
			$fieldset->addField('parent_brand', 'select', 
					array( 
						'label' => Mage::helper('catalog')->__('Brand parent'), 
						'name' => 'parent_brand', 
						'required' => false ,
						'values' => $this->getBrandsArray(), 
						'note'	=> Mage::helper('navigationlayered')->__('View this brand on store')
					)); 
			$fieldset->addField('base_url', 'select', 
					array( 
						'label' => Mage::helper('catalog')->__('Store Name'), 
						'name' => 'base_url', 
						'required' => false ,
						'values' => $this->getStoresArray(), 
						'note'	=> Mage::helper('navigationlayered')->__('View this brand on store')
					)); 
			
			$fieldset->addField('cms_block', 'text', array(
			'name' => 'cms_block',
			'label' => Mage::helper('navigationlayered')->__('Cms Block'),
			'required'  => false,
			// 'disabled'	=>true
			));
			$fieldset->addField('listing_icon', 'file', array(
				'label'     => $helper->__('Listing Image'),
				'required'  => false,
				'name'      => 'listing_icon',
			));
			$fieldset->addField('note_listing_icon', 'note', array(
				'note'	=>$Option->getListingIcon()?'<a onclick="imagePreview(\'listing_icon_view\'); return false;" href="'.$path.$Option->getListingIcon().'"><img id ="listing_icon_view"width="50px" src="'.$path.$Option->getListingIcon().'"/></a><input type="checkbox" name="delete_listing_icon"/><span> Delete</span>':'',
			));
			$fieldset->addField('detail_icon', 'file', array(
					'label'     => $helper->__('Detail Image'),
					'required'  => false,
					'name'      => 'detail_icon',
					));
			$fieldset->addField('note_detail_icon', 'note', array(
					'note'	=>$Option->getDetailIcon()?'<a onclick="imagePreview(\'detail_icon_view\'); return false;" href="'.$path.$Option->getDetailIcon().'"><img id ="detail_icon_view"width="50px" src="'.$path.$Option->getDetailIcon().'"/></a><input type="checkbox" name="delete_detail_icon"/><span> Delete</span>':'',
				));
		}
		else
		{
			$fieldset->addField('base_url', 'hidden', 
					array( 
						'name' => 'base_url', 
						'required' => false 
					));
			$fieldset->addField('listing_icon', 'file', array(
					'label'     => $helper->__('Listing Icon'),
					'required'  => false,
					'name'      => 'listing_icon',
				));
			$fieldset->addField('note_listing_icon', 'note', array(
					'note'	=>$Option->getListingIcon()?'<a onclick="imagePreview(\'listing_icon_view\'); return false;" href="'.$path.$Option->getListingIcon().'"><img id ="listing_icon_view"width="50px" src="'.$path.$Option->getListingIcon().'"/></a><input type="checkbox" name="delete_listing_icon"/><span> Delete</span>':'',
				));
			$fieldset->addField('navigation_icon', 'file', array(
					'label'     => $helper->__('Navigation Icon'),
					'required'  => false,
					'name'      => 'navigation_icon',
				));
			$fieldset->addField('note_navigation_icon', 'note', array(
					'note'	=>$Option->getNavigationIcon()?'<a onclick="imagePreview(\'navigation_icon_view\'); return false;" href="'.$path.$Option->getNavigationIcon().'"><img id ="navigation_icon_view"width="50px" src="'.$path.$Option->getNavigationIcon().'"/></a><input type="checkbox" name="delete_navigation_icon"/><span> Delete</span>':'',
				));	
			$fieldset->addField('detail_icon', 'file', array(
					'label'     => $helper->__('Detail Icon'),
					'required'  => false,
					'name'      => 'detail_icon',
					));
			$fieldset->addField('note_detail_icon', 'note', array(
					'note'	=>$Option->getDetailIcon()?'<a onclick="imagePreview(\'detail_icon_view\'); return false;" href="'.$path.$Option->getDetailIcon().'"><img id ="detail_icon_view"width="50px" src="'.$path.$Option->getDetailIcon().'"/></a><input type="checkbox" name="delete_detail_icon"/><span> Delete</span>':'',
				));
		}
		// $fieldset->addField('event_product', 'hidden', array(
				// 'name'      => 'event_product',
				// 'label'     => '',
				// 'values'    => ''
		// ));
      if ( Mage::getSingleton('adminhtml/session')->getOptionData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getOptionData());
          Mage::getSingleton('adminhtml/session')->setOptionData(null);
      } elseif ( Mage::registry('option_data') ) {
          $form->setValues(Mage::registry('option_data')->getData());
      }
      return parent::_prepareForm();
  }
   public function getWebsites()
    {
        $websites = Mage::app()->getWebsites();
        return $websites;
    }
	 public function getStores($group)
    {
        if (!$group instanceof Mage_Core_Model_Store_Group) {
            $group = Mage::app()->getGroup($group);
        }
        $stores = $group->getStores();
        return $stores;
    }
  public function getStoresArray()
  {
		$options = array();        
        $options[] = array(
	            'label' => 'All Stores Views',
	            'value' => null,
	        );
		 if ($websites = $this->getWebsites()):
			 foreach ($websites as $website):
				$options[$website->getId()+1] = array(
					'label' => $website->getName(),
					'value' => null
				);
				foreach ($website->getGroups() as $group):
					$options[$website->getId()+1]['value'][$group->getId()] = array(
						'label' => '&nbsp;&nbsp;' . $group->getName(),
						'value' => null
					);
					foreach ($this->getStores($group) as $store):
						$options[$website->getId()+1]['value'][$group->getId()]['value'][$store->getId()] = array(
							'label' => $store->getName(),
							'value' => $store->getCode()
						);
					endforeach;
				endforeach;
			 endforeach;
		 endif;
		
        return $options;
  }
  public function getBrandsArray()
  {
		$options = array();        
        $options[] = array(
	            'label' => $this->__('-------non-------'),
	            'value' => '',
	        );
		$collection = Mage::getModel('navigationlayered/brands')->getCollection()->getJoinOptions()->addFieldToFilter('brand_id', array('neq'=>$this->getRequest()->getParam('id')));
		
		foreach($collection as $brand)
		{
			$options[$brand->getBrandId()] = array(
						'label' => $brand->getTitle(),
						'value' => $brand->getBrandId()
						);
		}
        return $options;
  }
}