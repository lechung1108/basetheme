<?php
class AMZ_NavigationLayered_Block_Adminhtml_Brands_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
		parent::__construct();
		$this->setId('brandsGrid');
		$this->setDefaultSort('brand_id');
		$this->setDefaultDir('ASC');
		// $this->setUseAjax(true);
		$this->setSaveParametersInSession(true);
  }
  protected function _getStore()
  {
	  $storeId = (int) $this->getRequest()->getParam('store', 0);
	  return Mage::app()->getStore($storeId);
  }
  protected function _prepareCollection()
  {
      $collection = Mage::getModel('navigationlayered/brands')->getCollection()->getJoinOptions();
	  $store = $this->_getStore();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
    
	  $this->addColumn('brand_id', array(
          'header'  => Mage::helper('navigationlayered')->__('ID'),
          'align'   =>'left',
          'width'   => '10px',
		  'index'	=>'brand_id',
		));
		$this->addColumn('title', array(
          'header'  => Mage::helper('navigationlayered')->__('Name'),
          'align'   =>'left',
          'width'   => '150px',
		  'index'	=>'title',
          'filter_index' =>'store.title',
		));
		$this->addColumn('meta_title', array(
          'header'  => Mage::helper('navigationlayered')->__('Meta title'),
          'align'   =>'left',
		  'index'	=>'meta_title',
		  'filter_index' =>'store.meta_title',
		));
		$this->addColumn('meta_description', array(
          'header'  => Mage::helper('navigationlayered')->__('Meta Des'),
          'align'   =>'left',
		  'index'	=>'meta_description',
		  'filter_index' =>'store.meta_description',
		));
		$this->addColumn('description', array(
          'header'  => Mage::helper('navigationlayered')->__('Description'),
          'align'   =>'left',
		  'index'	=>'description',
		  'filter_index' =>'store.description',
		));
	
		
	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('navigationlayered')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

		$this->addExportType('*/*/exportCsv', Mage::helper('navigationlayered')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('navigationlayered')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    // protected function _prepareMassaction()
    // {
        // $this->setMassactionIdField('brand_id');
        // $this->getMassactionBlock()->setFormFieldName('navigationlayered');

        // $this->getMassactionBlock()->addItem('delete', array(
             // 'label'    => Mage::helper('navigationlayered')->__('Delete'),
             // 'url'      => $this->getUrl('*/*/massDelete'),
             // 'confirm'  => Mage::helper('navigationlayered')->__('Are you sure?')
        // ));
        // return $this;
    // }
	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
	
}