<?php

class AMZ_NavigationLayered_Block_Adminhtml_Brands_Changeatt_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'brand_id';
        $this->_blockGroup = 'navigationlayered';
        $this->_controller = 'adminhtml_brands_changeatt';
        
       // $this->_updateButton('save', 'label', Mage::helper('navigationlayered')->__('Save Item'));
		 
        //$this->_updateButton('delete', 'label', Mage::helper('navigationlayered')->__('Delete Item'));
		$this->_removeButton('save');
		$this->_removeButton('delete');
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('models_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'attributes_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'attributes_content');
                }
            }

            function changeAttribute(value){
				if(confirm('If you change any your setting will lost. You sure?'))
				{
					setLocation('".$this->getUrl('*/*/changeAttBrands')."id/' + value);
				}
				else
				{
					setLocation('".$this->getUrl('*/*/')."');
				}
				$('page:main-container').remove();
            }
        ";
    }
	
	protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
	
    public function getHeaderText()
    {
        return Mage::helper('navigationlayered')->__('Select Attribute Brand');
    }
}