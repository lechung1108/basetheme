<?php

class AMZ_NavigationLayered_Block_Adminhtml_Brands_Changeatt_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('brands_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('navigationlayered')->__('Setting'));
  }
	
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('navigationlayered')->__('Select Attribute'),
          'title'     => Mage::helper('navigationlayered')->__('Select Attribute'),
          'content'   => $this->getLayout()->createBlock('navigationlayered/adminhtml_brands_changeatt_edit_tab_form')->toHtml(),
      ));
	return parent::_beforeToHtml();
  }
}