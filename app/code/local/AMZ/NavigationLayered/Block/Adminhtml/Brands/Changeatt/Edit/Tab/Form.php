<?php

class AMZ_NavigationLayered_Block_Adminhtml_Brands_Changeatt_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	
  protected function _prepareForm()
  {
	    $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/saveSelect'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
				)
			);
		$Option =Mage::registry('option_data');
		$this->setForm($form);
		$helper = Mage::helper('navigationlayered');
		$path = Mage::getBaseUrl('media') . 'attributes/';
		$fieldset = $form->addFieldset('option_form', array('legend'=>$helper->__('Attribute')));
		$sets = Mage::getModel('navigationlayered/attributes')->getCollection();
		$array = array();
		$array[0]['value'] = '';
		$array[0]['label'] = '';
		$i=1; 
		foreach($sets as $att)
		{
			$array[$i]['value'] = $att->getId();
			$array[$i]['label'] = $att->getAttributeCode();
			$i++;
		}
		$fieldset->addField('attribute_id', 'select', 
					array( 
						'label' => Mage::helper('catalog')->__('Attribute'), 
						'name' => 'attribute_id', 
						'required' => true,
						'class' => 'required-entry', 
						'values' => $array, 
						'onchange' => 'changeAttribute(this.value)'
					)); 
		
      if ( Mage::getSingleton('adminhtml/session')->getOptionData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getOptionData());
          Mage::getSingleton('adminhtml/session')->setOptionData(null);
      } else {
			$brand = Mage::getModel('navigationlayered/brands')->getCollection()->getFirstItem();
			$form->setValues(array('attribute_id' => $brand->getAttributeId()));
      }
      return parent::_prepareForm();
  }
}