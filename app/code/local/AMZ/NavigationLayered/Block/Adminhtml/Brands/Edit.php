<?php

class AMZ_NavigationLayered_Block_Adminhtml_Brands_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'brand_id';
        $this->_blockGroup = 'navigationlayered';
        $this->_controller = 'adminhtml_brands';
        
        $this->_updateButton('save', 'label', Mage::helper('navigationlayered')->__('Save Item'));
        //$this->_updateButton('delete', 'label', Mage::helper('navigationlayered')->__('Delete Item'));
		$this->_removeButton('reset');
		$this->_removeButton('delete');
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('models_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'attributes_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'attributes_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
	
	protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
	
    public function getHeaderText()
    {
        if( Mage::registry('option_data') && Mage::registry('option_data')->getId() ) {
           $store = $this->_getStore();
			if(Mage::registry('option_data')->getTitle()){
				return Mage::helper('navigationlayered')->__("Edit %s",$this->htmlEscape(Mage::registry('option_data')->getTitle()));
			}else{
				 if ($store->getId()) {
					return Mage::helper('navigationlayered')->__("Add data in %s",$store->getName());
				 }
			}
        } else {
            return Mage::helper('navigationlayered')->__('');
        }
    }
}