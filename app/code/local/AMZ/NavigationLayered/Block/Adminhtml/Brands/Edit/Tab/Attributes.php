<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to buttonize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml catalog product downloadable items tab and form
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Block_Adminhtml_Brands_Edit_Tab_Attributes extends Mage_Adminhtml_Block_Widget implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Reference to product objects that is being edited
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product = null;

    protected $_config = null;

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amz_navigationlayered/attributes/categories.phtml');
    }


    /**
     * Check is readonly block
     *
     * @return boolean
     */
    public function isReadonly()
    {
       return false;
    }

    /**
     * Retrieve product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getBrandAttributes()
    {
		return Mage::registry('brand_attributes');
    }
	
    /**
     * Get tab label
     *
     * @return string
     */
   
    /**
     * Get tab title
     *
     * @return string
     */
   
    /**
     * Check if tab can be displayed
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Check if tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
	public function getTabLabel()
    {
        return $this->__('Event Information');
    }

    /**
     * Get tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Event Information');
    }
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        return parent::_toHtml();
    }
	
	
	// Begin many function Add element
	public function getFormText($id, $name, $value ='')
	{
		$form = new Varien_Data_Form();
		$form->addField($id, 'text', array(
			'name' => $name,
			'value'		=> $value
			));
		return $form->getHtml();
	}
	public function getFormTextArea($id, $name, $value ='')
	{
		$form = new Varien_Data_Form();
		$form->addField($id, 'textarea', array(
			'name' =>  $name,
			'style'     => 'width:280px; height:100px;',
			'value'		=> $value
			));
		return $form->getHtml();
	}
	public function getFormFile($id, $name)
	{
		$form = new Varien_Data_Form();
		
		$form->addField($id, 'file', array(
				'required'  => false,
				'name'      => $name,
				));
		return $form->getHtml();
	}
	public function getNodeDel($idCat, $name)
	{
		$path = Mage::getBaseUrl('media') . 'attributes/';
		if($this->getImage($idCat) != '')
		{
			$value = $this->getImage($idCat);
			return '<a onclick="imagePreview(\'view'.$idCat.'\'); return false;" href="'.$path.$value.'"><img class="view_image" id ="view'.$idCat.'"width="50px" src="'.$path.$value.'"/></a><input type="checkbox" name="'.$name.'[checkbox]"/><input type="hidden" value="'.$value.'" name="'.$name.'[hidden]"/><span> Delete</span>';
		}
		return '';
	}
	//End function add new element.
	public function getIncludePageHtml($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][include_page]')
            ->setId('include_page' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueIncludePage($idCat));
        return $select->getHtml();
    }
	public function getValueIncludePage($idCat)
	{
		foreach($this->getBrandAttributes() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getIncludePage();
			}
		}
		return 1;
	}
	public function getMetaRobotsSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('NOINDEX,FOLLOW')),
						array('value'=>'1','label'=>$this->__('INDEX,FOLLOW'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][meta_robots]')
            ->setId('meta_robots' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueMetaRobots($idCat));
        return $select->getHtml();
    }
	public function getValueMetaRobots($idCat)
	{
		foreach($this->getBrandAttributes() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getMetaRobots();
			}
		}
		return 0;
	}
	
	
	public function getAttributeHtml($attribute)
	{
		$htmlLi = '<dl class="accordion" id="categories">
						<dt class="label_active" id="dt-buttons"><a class="title" href="#">'.$this->escapeHtml($attribute->getFrontendLabel()).'</a>
						</dt>
						<dd class="" id="dd-buttons">
							<div class="fieldset">
								<div class="grid">
									<table class="form-list">
										<tr>
											<td class="label">'.$this->__("Include Page").'</td>
											<td class="value">'.$this->getIncludePageHtml($attribute->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Meta robots").'</td>
											<td class="value">'.$this->getMetaRobotsSelect($attribute->getId()).'</td>
										</tr>
									</table>
								</div>
							</div>
						</dd>
					</dl>';
        $html[] = $htmlLi;
		$html = implode("\n", $html);
        return $html;
	}
	// End function get valua Cat attribute
	
	public function renderCategoriesMenuHtml($level = 0, $outermostItemClass = '', $childrenWrapClass = '')
    {
		$html = '';
		$attributes = Mage::getModel('navigationlayered/attributes')->getCollection();
		$attributes->addStoreFilter()->addFieldToFilter('attribute_id',array('neq'=>Mage::registry('brand_data')->getAttributeId()));
		foreach ($attributes as $_attribute) {
           $html .= $this->getAttributeHtml($_attribute);
        }
		
        return $html;
    }
}

