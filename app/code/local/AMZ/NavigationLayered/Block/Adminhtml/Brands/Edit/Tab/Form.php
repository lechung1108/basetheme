<?php

class AMZ_NavigationLayered_Block_Adminhtml_Brands_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	
  protected function _prepareForm()
  {
		$form = new Varien_Data_Form(array(
			'id' => 'edit_form',
			'action' => $this->getUrl('*/*/saveOption'),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
				)
			);
		$Option =Mage::registry('brand_data');
		$this->setForm($form);
		$helper = Mage::helper('navigationlayered');
		$path = Mage::getBaseUrl('media') . 'attributes/';
		$fieldset = $form->addFieldset('option_form', array('legend'=>$helper->__('Item information')));
		$fieldset->addField('title', 'text', array(
			'name' => 'title',
			'label' => Mage::helper('navigationlayered')->__('Title'),
			'required'  => true,
			// 'disabled'	=>true
		));
		$fieldset->addField('cms_block', 'text', array(
			'name' => 'cms_block',
			'label' => Mage::helper('navigationlayered')->__('Cms Block'),
			'required'  => false,
			// 'disabled'	=>true
		));
		$fieldset->addField('meta_title', 'text', array(
			'name' => 'meta_title',
			'label' => Mage::helper('navigationlayered')->__('Meta title'),
			'required'  => false
		));
		$fieldset->addField('meta_description', 'textarea', array(
				'name' => 'meta_description',
				'label' => Mage::helper('navigationlayered')->__('Meta Description'),
				'style'     => 'width:280px; height:100px;',
			));
		$fieldset->addField('short_description', 'textarea', array(
				'name' => 'short_description',
				'label' => Mage::helper('navigationlayered')->__('Short Description'),
				'style'     => 'width:280px; height:100px;',
			));
		$fieldset->addField('main_description', 'textarea', array(
				'name' => 'main_description',
				'label' => Mage::helper('navigationlayered')->__('Main Description'),
				'style'     => 'width:280px; height:100px;',
			));
		$fieldset->addField('image', 'file', array(
				'label'     => $helper->__('Logo Brand'),
				'required'  => false,
				'name'      => 'image',
			));
		$fieldset->addField('note_image', 'note', array(
				'note'	=>$Option->getImage()?'<a onclick="imagePreview(\'image_view\'); return false;" href="'.$path.$Option->getImage().'"><img id ="image_view"width="50px" src="'.$path.$Option->getImage().'"/></a><input type="checkbox" name="delete_image"/><span> Delete</span>':'',
			));
		// $fieldset->addField('event_product', 'hidden', array(
				// 'name'      => 'event_product',
				// 'label'     => '',
				// 'values'    => ''
		// ));
	  if ( Mage::getSingleton('adminhtml/session')->getOptionData() )
	  {
		  $form->setValues(Mage::getSingleton('adminhtml/session')->getOptionData());
		  Mage::getSingleton('adminhtml/session')->setOptionData(null);
	  } elseif ( Mage::registry('brand_data') ) {
		  $form->setValues(Mage::registry('brand_data')->getData());
	  }
	  return parent::_prepareForm();
  }
	public function getBrandsToSelect()
	{
		
	}
}