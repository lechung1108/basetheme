<?php

class AMZ_NavigationLayered_Block_Adminhtml_Brands_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('brands_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('navigationlayered')->__('Setting'));
  }
	
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('navigationlayered')->__('Information'),
          'title'     => Mage::helper('navigationlayered')->__('Information'),
          'content'   => $this->getLayout()->createBlock('navigationlayered/adminhtml_attributesvalue_edit_tab_form')->toHtml(),
      ));
	   $this->addTab('categories', array(
                'label'     => Mage::helper('catalog')->__('Setting With Attributes'),
                'url'       => $this->getUrl('*/*/categories', array('_current' => true)),
                'class'     => 'ajax',
				));
      return parent::_beforeToHtml();
  }
}