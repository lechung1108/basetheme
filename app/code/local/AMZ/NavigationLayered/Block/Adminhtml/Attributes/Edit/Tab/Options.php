<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product in category grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Block_Adminhtml_Attributes_Edit_Tab_Options extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('amz_attributes_value');
        $this->setDefaultSort('id');
        $this->setUseAjax(true);
    }
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in category flag
        
        parent::_addColumnFilterToCollection($column);
        return $this;
    }
	protected function _getStore()
	  {
		  $storeId = (int) $this->getRequest()->getParam('store', 0);
		  return Mage::app()->getStore($storeId);
	  }
    protected function _prepareCollection()
    {
		$store = $this->_getStore();
        if($id = $this->getRequest()->getParam('id'))
		{
			$collection = Mage::getModel('navigationlayered/attributesvalue')->getCollection()->addFieldToFilter('main_table.attribute_id', $id);
			$collection->addStoreFilter();
			$collection->getJoinAttributes();
			$this->setCollection($collection);
        }
		else 
		{
			$collection = Mage::getModel('navigationlayered/attributesvalue')->getCollection();
			$collection->addStoreFilter();
			$this->setCollection($collection);
		}
		
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
            
        $this->addColumn('option_id', array(
            'header'    => Mage::helper('catalog')->__('Option Id'),
            'sortable'  => true,
            'width'     => '60',
            'index'     => 'option_id'
        ));
        $this->addColumn('title', array(
            'header'    => Mage::helper('catalog')->__('Title'),
            'index'     => 'title',
			'filter_index' => 'store.title'
        ));
		$this->addColumn('meta_title', array(
            'header'    => Mage::helper('catalog')->__('Meta title'),
            'index'     => 'meta_title',
			'filter_index' => 'store.meta_title'
        ));
        $this->addColumn('meta_description', array(
            'header'    => Mage::helper('catalog')->__('Meta Description'),
            'index'     => 'meta_description',
			'filter_index' => 'store.meta_description'
        ));
        $this->addColumn('description', array(
            'header'    => Mage::helper('catalog')->__('Description'),
            'index'     => 'description',
			'filter_index' => 'store.description'
        ));
		 $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('catalog')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('catalog')->__('Edit Option'),
                        'url'       => array('base'=> '*/*/editOption'),
                        'field'     => 'option_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
        
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/gridOptions', array('_current'=>true));
    }

    

}

