<?php

class AMZ_NavigationLayered_Block_Adminhtml_Attributes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('attributesGrid');
      $this->setDefaultSort('attribute_id');
      $this->setDefaultDir('ASC');
	//  $this->setUseAjax(true);
      $this->setSaveParametersInSession(true);
  }
  protected function _getStore()
  {
	  $storeId = (int) $this->getRequest()->getParam('store', 0);
	  return Mage::app()->getStore($storeId);
  }
  protected function _prepareCollection()
  {
      $collection = Mage::getModel('navigationlayered/attributes')->getCollection();
	  $store = $this->_getStore();
	  $collection->addStoreFilter();
	  //echo $collection->getSelect();die;
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
    
	  $this->addColumn('attribute_id', array(
          'header'  => Mage::helper('navigationlayered')->__('ID'),
          'align'   =>'right',
          'width'   => '50px',
		  'index'	=>'attribute_id',
		));
		$this->addColumn('weight', array(
            'header'    => Mage::helper('catalog')->__('Weight'),
            'width'     => '1',
            'type'      => 'number',
            'index'     => 'weight',
            'editable'  => true
            //'renderer'  => 'adminhtml/widget_grid_column_renderer_input'
        ));
		$this->addColumn('prefix', array(
            'header'    => Mage::helper('catalog')->__('Prefix'),
            'width'     => '1',
            'index'     => 'prefix',
        ));
      $this->addColumn('attribute_code', array(
          'header'  => Mage::helper('navigationlayered')->__('Attributes code'),
          'align'   =>'left',
		   'width'   => '150px',
		  'index'	=>'attribute_code',
		));
		$this->addColumn('frontend_label', array(
          'header'    => Mage::helper('navigationlayered')->__('Frontend Label'),
          'align'     =>'left',
          'index'     =>'frontend_label',
          'filter_index'     =>'store.frontend_label',
		));
		$this->addColumn('display_type', array(
            'header'    => Mage::helper('navigationlayered')->__('Display Type'),
            'align'     =>'left',
            'index'     => 'display_type',
			'type'      => 'select',
			'options'   => array(
			    0 => Mage::helper('navigationlayered')->__('Labels Only'),
			    1 => Mage::helper('navigationlayered')->__('Images Only'),
			    2 => Mage::helper('navigationlayered')->__('Images and Labels'),
			    3 => Mage::helper('navigationlayered')->__('Drop-down List'),
			    4 => Mage::helper('navigationlayered')->__('Labels in 2 columns'),
		    ),
            'editable'	=> true,
            'name'		=> 'display_type'
        ));
	  /*
	  <select class=" select" name="display_type" id="display_type">
<option selected="selected" value="0">Labels Only</option>
<option value="1">Images Only</option>
<option value="2">Images and Labels</option>
<option value="3">Drop-down List</option>
<option value="4">Labels in 2 columns</option>
</select>
      $this->addColumn('content', array(
			'header'    => Mage::helper('navigationlayered')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

		$this->addExportType('*/*/exportCsv', Mage::helper('navigationlayered')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('navigationlayered')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('attribute_id');
        $this->getMassactionBlock()->setFormFieldName('navigationlayered');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('navigationlayered')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('navigationlayered')->__('Are you sure?')
        ));
		
        return $this;
    }

	public function getRowUrl($row)
	{
	  return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
	public function getWeightUrl()
	{
		return $this->getUrl('*/*/changeWeight', array('_current'=>true));
	}
}