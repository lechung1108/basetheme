<?php

class AMZ_NavigationLayered_Block_Adminhtml_Attributes_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	
	
  protected function _prepareForm()
  {
	    $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
				)
			);
		$Option =Mage::registry('attribute_data');
		$this->setForm($form);
		$helper = Mage::helper('navigationlayered');
		$path = Mage::getBaseUrl('media') . 'attributes/';
		$fieldset = $form->addFieldset('attribute_form', array('legend'=>$helper->__('Item information')));
		$array = array(
					array(
						'value'	=>0,
						'label'	=> Mage::helper('navigationlayered')->__('Labels Only')),
					array(
						'value'	=>1 ,
						'label'	=> Mage::helper('navigationlayered')->__('Images Only')),
					array(
						'value'	=>2 ,
						'label'	=> Mage::helper('navigationlayered')->__('Images and Labels')),
					array(
						'value'	=>3 ,
						'label'	=> Mage::helper('navigationlayered')->__('Drop-down List')),
					array(
						'value'	=>4 ,
						'label'	=> Mage::helper('navigationlayered')->__('Labels in 2 columns'))
				);
		$fieldset->addField('frontend_label', 'text', array(
			'name' => 'frontend_label',
			'label' => Mage::helper('navigationlayered')->__('Frontend Label'),
			'required'  => false
		));
		$fieldset->addField('display_type', 'select', 
					array( 
						'label' => Mage::helper('catalog')->__('Display type'), 
						'name' => 'display_type', 
						'required' => true,
						'class' => 'required-entry', 
						'values' => $array, 
					)); 
		$fieldset->addField('show_in_list', 'select', 
					array( 
						'label' => Mage::helper('catalog')->__('Show in list'), 
						'name' => 'show_in_list', 
						'required' => true,
						'class' => 'required-entry', 
						'values' => array(
											array('value'=>0,'label'=>$helper->__('No')), 
											array('value'=>1,'label'=>$helper->__('Yes'))
										), 
					)); 
		$fieldset->addField('show_in_view', 'select', 
					array( 
						'label' => Mage::helper('catalog')->__('Show in view'), 
						'name' => 'show_in_view', 
						'required' => true,
						'class' => 'required-entry', 
						'values' => array(
											array('value'=>0,'label'=>$helper->__('No')), 
											array('value'=>1,'label'=>$helper->__('Yes'))
										), 
					));
		$fieldset->addField('weight', 'text', array(
			'name' => 'weight',
			'label' => Mage::helper('navigationlayered')->__('Weight'),
			'required'  => false
		));
		$fieldset->addField('prefix', 'text', array(
			'name' => 'prefix',
			'label' => Mage::helper('navigationlayered')->__('Prefix'),
			'required'  => false
		));
		$fieldset->addField('scroll_height', 'text', array(
			'name' => 'scroll_height',
			'label' => Mage::helper('navigationlayered')->__('Height scroll'),
			'required'  => false
		));
		$fieldset->addField('note_navigation_icon', 'note', array(
					'note'	=>$this->__('if field is text or less than 65 => height = 65')
				));	
		
		
		
      if ( Mage::getSingleton('adminhtml/session')->getOptionData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getOptionData());
          Mage::getSingleton('adminhtml/session')->setOptionData(null);
      } elseif ( Mage::registry('attribute_data') ) {
          $form->setValues(Mage::registry('attribute_data')->getData());
      }
      return parent::_prepareForm();
  }
}