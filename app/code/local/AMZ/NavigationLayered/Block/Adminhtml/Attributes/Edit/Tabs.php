<?php

class AMZ_NavigationLayered_Block_Adminhtml_Attributes_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('attributes_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('navigationlayered')->__('General Information'));
  }
	
  protected function _beforeToHtml()
  {
		$this->addTab('Setting attributes', array(
                'label'     => Mage::helper('catalog')->__('Setting'),
                'title'       =>  Mage::helper('catalog')->__('Setting'),
                'content'   => $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_edit_tab_form')->toHtml(),
            ));
		$this->addTab('options', array(
                'label'     => Mage::helper('catalog')->__('Options'),
                'title'       =>  Mage::helper('catalog')->__('Options'),
                'content'   => $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_edit_tab_options')->toHtml(),
            ));
		$this->addTab('categories', array(
                'label'     => Mage::helper('catalog')->__('Setting Category'),
                'url'       => $this->getUrl('*/*/categoriesAttribute', array('_current' => true)),
                'class'     => 'ajax',
				));
      return parent::_beforeToHtml();
  }
}