<?php

class AMZ_NavigationLayered_Block_Adminhtml_Attributes_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'attribute_id';
        $this->_blockGroup = 'navigationlayered';
        $this->_controller = 'adminhtml_attributes';
        
        $this->_updateButton('save', 'label', Mage::helper('navigationlayered')->__('Save Item'));
		 
        //$this->_updateButton('delete', 'label', Mage::helper('navigationlayered')->__('Delete Item'));
		$this->_removeButton('reset');
		$this->_removeButton('delete');
		$url = $this->helper('adminhtml')->getUrl('adminhtml/navigationlayered_attributes/loaddefaultcatoption', array(
					'attid' => $this->getRequest()->getParam('id')
				));
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('models_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'attributes_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'attributes_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
	
	protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
	
    public function getHeaderText()
    {
        if( Mage::registry('attribute_data') && Mage::registry('attribute_data')->getId() ) {
           $store = $this->_getStore();
			if(Mage::registry('attribute_data')->getFrontendLabel()){
				return Mage::helper('navigationlayered')->__("Setting Attributes %s",$this->htmlEscape(Mage::registry('attribute_data')->getFrontendLabel()));
			}else{
				 if ($store->getId()) {
					return Mage::helper('navigationlayered')->__("Add data in %s",$store->getName());
				 }
			}
        } else {
            return Mage::helper('navigationlayered')->__('');
        }
    }
}