<?php

class AMZ_NavigationLayered_Block_Adminhtml_Category_Options_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('attributes_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('navigationlayered')->__('General'));
  }
	
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('navigationlayered')->__('Setting'),
          'title'     => Mage::helper('navigationlayered')->__('Setting'),
          'content'   => $this->getLayout()->createBlock('navigationlayered/adminhtml_category_options_edit_tab_options')->toHtml(),
      ));
	  // $tab=	$this->addTab('options', array(
                // 'label'     => Mage::helper('catalog')->__('Options'),
                // 'title'       =>  Mage::helper('catalog')->__('Options'),
                // 'content'   => $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_edit_tab_options')->toHtml(),
            // ));
      return parent::_beforeToHtml();
  }
}