<?php

class AMZ_NavigationLayered_Block_Adminhtml_Category_Options_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'category_id';
        $this->_blockGroup = 'navigationlayered';
        $this->_controller = 'adminhtml_category_options';
        $this->_updateButton('save', 'label', Mage::helper('navigationlayered')->__('Save setting'));
		 $this->_removeButton('reset');
		$this->_updateButton('back', 'onclick','setLocation(\''.$this->getUrl('*/*/edit',array('category_id'=>$this->getRequest()->getParam('category_id'))).'\');');
        

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('models_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'attributes_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'attributes_content');
                }
            }
		";
    }
	
	protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
	
    public function getHeaderText()
    {
        if( Mage::registry('catAttributes_data') && Mage::registry('catAttributes_data')->getId() ) {
			
			
        } else {
            return Mage::helper('navigationlayered')->__('Setting Category ID('.$this->getRequest()->getParam('category_id').')');
        }
    }
}