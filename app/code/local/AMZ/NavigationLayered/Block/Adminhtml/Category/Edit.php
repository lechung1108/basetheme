<?php

class AMZ_NavigationLayered_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'category_id';
        $this->_blockGroup = 'navigationlayered';
        $this->_controller = 'adminhtml_category';
        $this->_updateButton('save', 'label', Mage::helper('navigationlayered')->__('Save setting'));
        $this->_removeButton('reset');
		$this->_updateButton('back', 'label',Mage::helper('navigationlayered')->__('Back'));
		$this->_updateButton('back', 'onclick','setLocation(\''.$this->getUrl('adminhtml/catalog_category/index',array('id'=>$this->getRequest()->getParam('category_id'))).'\');');
		// $this->_addButton('load', array(
            // 'label'     => Mage::helper('adminhtml')->__('Load'),
            // 'onclick'   => 'editForm.submit(\''.$this->getUrl('*/*/loadGrid',array('category_id'=>$this->getRequest()->getParam('category_id'))).'\');',
            // 'class'     => 'save',
        // ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('models_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'attributes_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'attributes_content');
                }
            }
		";
    }
	
	protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
	
    public function getHeaderText()
    {
        if( Mage::registry('attributes_data') && Mage::registry('attributes_data')->getId() ) {
           $store = $this->_getStore();
			if(Mage::registry('attributes_data')->getTitle()){
				return Mage::helper('navigationlayered')->__("Edit %s",$this->htmlEscape(Mage::registry('attributes_data')->getTitle()));
			}else{
				 if ($store->getId()) {
					return Mage::helper('navigationlayered')->__("Add data in %s",$store->getName());
				 }
			}
        } else {
            return Mage::helper('navigationlayered')->__('Setting Category ID('.$this->getRequest()->getParam('category_id').')');
        }
    }
}