<?php

class AMZ_NavigationLayered_Block_Adminhtml_Category_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('attributes_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('navigationlayered')->__('General Information'));
  }
	
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('navigationlayered')->__('Setting with attributes'),
          'title'     => Mage::helper('navigationlayered')->__('Setting with attributes'),
          'content'   => $this->getLayout()->createBlock('navigationlayered/adminhtml_category_edit_tab_attributes')->toHtml(),
      ));
	  // $this->addTab('categories', array(
                // 'label'     => Mage::helper('catalog')->__('Brands'),
                // 'url'       => $this->getUrl('*/*/brands', array('_current' => true)),
                // 'class'     => 'ajax',
				// ));
      return parent::_beforeToHtml();
  }
}