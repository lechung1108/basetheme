<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to buttonize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml catalog product downloadable items tab and form
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Block_Adminhtml_Category_Edit_Tab_Attributes extends Mage_Adminhtml_Block_Widget implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Reference to product objects that is being edited
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product = null;

    protected $_config = null;
	protected $_brandId = '607';
    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amz_navigationlayered/attributes/categories.phtml');
    }


    /**
     * Check is readonly block
     *
     * @return boolean
     */
    public function isReadonly()
    {
       return false;
    }

    /**
     * Retrieve product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getAttributeCats()
    {
		return Mage::registry('attribute_cats');
    }
	
    /**
     * Get tab label
     *
     * @return string
     */
   
    /**
     * Get tab title
     *
     * @return string
     */
   
    /**
     * Check if tab can be displayed
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Check if tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
	public function getTabLabel()
    {
        return $this->__('Event Information');
    }

    /**
     * Get tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Event Information');
    }
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        return parent::_toHtml();
    }
	public function getCatOptionsAttributes()
	{
		$categoryId = $this->getRequest()->getParam('category_id');
		$category = Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->addFieldToFilter('category_id', $categoryId)->setPageSize(1);
		if($category->count() < 1)
			return Mage::getModel('navigationlayered/attributes')->getCollection()->addStoreFilter();
		return false;
	}
	public function getUrlAddDefaultOptions()
	{
		return $this->getUrl('*/*/addDefaultOptions',array('category_id'=>$this->getRequest()->getParam('category_id')));
	}
	// Begin many function Add element
	public function getFormText($id, $name, $value ='')
	{
		$form = new Varien_Data_Form();
		$form->addField($id, 'text', array(
			'name' => $name,
			'value'		=> $value
			));
		return $form->getHtml();
	}
	public function getFormTextArea($id, $name, $value ='')
	{
		$form = new Varien_Data_Form();
		$form->addField($id, 'textarea', array(
			'name' =>  $name,
			'style'     => 'width:280px; height:100px;',
			'value'		=> $value
			));
		return $form->getHtml();
	}
	public function getFormFile($id, $name)
	{
		$form = new Varien_Data_Form();
		
		$form->addField($id, 'file', array(
				'required'  => false,
				'name'      => $name,
				));
		return $form->getHtml();
	}
	public function getNodeDel($idCat, $name)
	{
		$path = Mage::getBaseUrl('media') . 'attributes/';
		if($this->getImage($idCat) != '')
		{
			$value = $this->getImage($idCat);
			return '<a onclick="imagePreview(\'view'.$idCat.'\'); return false;" href="'.$path.$value.'"><img class="view_image" id ="view'.$idCat.'"width="50px" src="'.$path.$value.'"/></a><input type="checkbox" name="'.$name.'[checkbox]"/><input type="hidden" value="'.$value.'" name="'.$name.'[hidden]"/><span> Delete</span>';
		}
		return '';
	}
	//End function add new element.
	public function getCategories()
	{
		$categories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addIsActiveFilter()
                    ->addAttributeToFilter('level',2);
		return $categories;
	}
	public function featuredCategorySelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
		
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][featured_category]')
            ->setId('featured_category' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueFeature($idCat));
        return $select->getHtml();
    }
	public function getValueFeature($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getFeaturedCategory();
			}
		}
		return 1;
	}
	public function canonicalUrlSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][canonical_url]')
            ->setId('canonical_url' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueCanonical($idCat));
        return $select->getHtml();
    }
	public function getValueCanonical($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getCanonicalUrl();
			}
		}
		return 1;
	}
	public function multipleSelectionSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][multiple_selections]')
            ->setId('multiple_selections' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueMultiple($idCat));
        return $select->getHtml();
    }
	public function getValueMultiple($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getMultipleSelections();
			}
		}
		return 0;
	}public function filterVisibleSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][filter_visible]')
            ->setId('filter_visible ' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueFilter($idCat));
        return $select->getHtml();
    }
	public function getValueFilter($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getFilterVisible();
			}
		}
		return 1;
	}
	
	// Begin get value of Cat attribute
	public function getMetaTitle($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getMetaTitle();
			}
		}
		return '';
	}
	public function getDescription($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getDescription();
			}
		}
		return '';
	}
	public function getMetaDescription($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getMetaDescription();
			}
		}
		return '';
	}
	public function getImage($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getImage();
			}
		}
		return '';
	}
	public function getCmsBlock($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getCmsBlock();
			}
		}
		return '';
	}
	public function getMetaRobotsSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('NOINDEX,FOLLOW')),
						array('value'=>'1','label'=>$this->__('INDEX,FOLLOW'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][meta_robots]')
            ->setId('meta_robots' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueMetaRobots($idCat));
        return $select->getHtml();
    }
	public function getValueMetaRobots($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getMetaRobots();
			}
		}
		return 1;
	}
	public function getCollapseSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][collapse]')
            ->setId('collapse' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueCollapse($idCat));
        return $select->getHtml();
    }
	public function getValueCollapse($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getCollapse();
			}
		}
		return 0;
	}
	public function getShowAboveToolbarseSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('cat[' . $idCat .'][show_above_toolbar]')
            ->setId('show_above_toolbar' . $idCat)
            ->setOptions($options)
            ->setValue($this->getShowAboveToolbar($idCat));
        return $select->getHtml();
    }
	public function getShowAboveToolbar($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getAttributeId() == $idCat)
			{
				return $object->getShowAboveToolbar();
			}
		}
		return 0;
	}
	public function getButtonEditOpions($groupId)
	{
		return '<button onclick="setLocation(\''.$this->getUrl('*/*/editOption', array('category_id'=>$this->getRequest()->getParam('category_id'), 'catatt_id' => $groupId )).'\');" class="scalable buttonlinkOptions" type="button"><span>'.$this->__("Edit with options.").'</span></button>';
	}
	public function getAttributeHtml($attribute)
	{
		$htmlLi = '<dl class="accordion cat_attributes" id="categories">
						<dt class="label_active" id="dt-buttons"><a class="title" href="#">'.$this->escapeHtml($attribute->getFrontendLabel()).'</a>
							'.$this->getButtonEditOpions($attribute->getId()).'
						</dt>
						<dd class="" id="dd-buttons">
							<div class="fieldset">
								<div class="grid">
									<table class="form-list">
										<tr>
											<td class="label">'.$this->__('Cms Block').'</td>
											<td class="value">
												'.$this->getFormText("cms_block".$attribute->getId(),"cat[".$attribute->getId()."][cms_block]", $this->getCmsBlock($attribute->getId())).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Meta title').'</td>
											<td class="value">
												'.$this->getFormText("meta_title".$attribute->getId(),"cat[".$attribute->getId()."][meta_title]", $this->getMetaTitle($attribute->getId())).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Meta Description').'</td>
											<td class="value">
												'.$this->getFormTextArea("meta_description".$attribute->getId(),"cat[".$attribute->getId()."][meta_description]", $this->getMetaDescription($attribute->getId())).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Description').'</td>
											<td class="value">
												'.$this->getFormTextArea("description".$attribute->getId(),"cat[".$attribute->getId()."][description]", $this->getDescription($attribute->getId())).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Image').'</td>
												<td class="value">
													'.$this->getFormFile("image".$attribute->getId(),"cat_image_".$attribute->getId()) . $this->getNodeDel($attribute->getId(), "cat[".$attribute->getId()."][del_image]").'    
												</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Featured Category").'</td>
											<td class="value">'.$this->featuredCategorySelect($attribute->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Canonical Url").'</td>
											<td class="value">'.$this->canonicalUrlSelect($attribute->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Multiple Selection").'</td>
											<td class="value">'.$this->multipleSelectionSelect($attribute->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Filter Visible").'</td>
											<td class="value">'.$this->filterVisibleSelect($attribute->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Meta robots").'</td>
											<td class="value">'.$this->getMetaRobotsSelect($attribute->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Show above toolbar").'</td>
											<td class="value">'.$this->getShowAboveToolbarseSelect($attribute->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Collapsed").'</td>
											<td class="value">'.$this->getCollapseSelect($attribute->getId()).'</td>
										</tr>
									</table>
								</div>
							</div>
						</dd>
					</dl>';
        $html[] = $htmlLi;
		$html = implode("\n", $html);
        return $html;
	}
	// End function get valua Cat attribute
	public function renderCategoriesMenuHtml($level = 0, $outermostItemClass = '', $childrenWrapClass = '')
    {
		$html = '';
		$attributes = Mage::getModel('navigationlayered/attributes')->getCollection()->addStoreFilter();
        foreach ($attributes as $_attribute) {
           $html .= $this->getAttributeHtml($_attribute);
        }
		
        return $html;
    }
}

