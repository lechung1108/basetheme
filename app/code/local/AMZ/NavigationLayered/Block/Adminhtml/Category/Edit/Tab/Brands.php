<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to buttonize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml catalog product downloadable items tab and form
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Block_Adminhtml_Category_Edit_Tab_Brands extends Mage_Adminhtml_Block_Widget implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Reference to product objects that is being edited
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product = null;

    protected $_config = null;

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amz_navigationlayered/attributes/brands.phtml');
    }


    /**
     * Check is readonly block
     *
     * @return boolean
     */
    public function isReadonly()
    {
       return false;
    }

    /**
     * Retrieve product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getBrandsCat()
    {
		return Mage::registry('brands_cat');
    }
	
    /**
     * Get tab label
     *
     * @return string
     */
   
    /**
     * Get tab title
     *
     * @return string
     */
   
    /**
     * Check if tab can be displayed
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Check if tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
	public function getTabLabel()
    {
        return $this->__('Event Information');
    }

    /**
     * Get tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Event Information');
    }
    /**
     * Render block HTML
     *
     * @return string
     */
      protected function _toHtml()
    {
        return parent::_toHtml();
    }
	
	
	// Begin many function Add element
	public function getFormText($id, $name, $value ='')
	{
		$form = new Varien_Data_Form();
		$form->addField($id, 'text', array(
			'name' => $name,
			'value'		=> $value
			));
		return $form->getHtml();
	}
	public function getFormTextArea($id, $name, $value ='')
	{
		$form = new Varien_Data_Form();
		$form->addField($id, 'textarea', array(
			'name' =>  $name,
			'style'     => 'width:280px; height:100px;',
			'value'		=> $value
			));
		return $form->getHtml();
	}
	public function getFormFile($id, $name)
	{
		$form = new Varien_Data_Form();
		
		$form->addField($id, 'file', array(
				'required'  => false,
				'name'      => $name,
				));
		return $form->getHtml();
	}
	public function getNodeDel($idCat, $name)
	{
		$path = Mage::getBaseUrl('media') . 'attributes/';
		if($this->getImage($idCat) != '')
		{
			$value = $this->getImage($idCat);
			return '<a onclick="imagePreview(\'view'.$idCat.'\'); return false;" href="'.$path.$value.'"><img class="view_image" id ="view'.$idCat.'"width="50px" src="'.$path.$value.'"/></a><input type="checkbox" name="'.$name.'[checkbox]"/><input type="hidden" value="'.$value.'" name="'.$name.'[hidden]"/><span> Delete</span>';
		}
		return '';
	}
	//End function add new element.
	public function getCategories()
	{
		$categories = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addIsActiveFilter()
                    ->addAttributeToFilter('level',2);
		return $categories;
	}
	public function featuredCategorySelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
		
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('brands[' . $idCat .'][featured_category]')
            ->setId('featured_category' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueFeature($idCat));
        return $select->getHtml();
    }
	public function getValueFeature($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getFeaturedCategory();
			}
		}
		return 0;
	}
	public function canonicalUrlSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('brands[' . $idCat .'][canonical_url]')
            ->setId('canonical_url' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueCanonical($idCat));
        return $select->getHtml();
    }
	public function getValueCanonical($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getCanonicalUrl();
			}
		}
		return 0;
	}
	public function multipleSelectionSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('brands[' . $idCat .'][multiple_selections]')
            ->setId('multiple_selections' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueMultiple($idCat));
        return $select->getHtml();
    }
	public function getValueMultiple($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getMultipleSelections();
			}
		}
		return 0;
	}public function filterVisibleSelect($idCat)
    {
		$options = array(
						array('value'=>'0','label'=>$this->__('No')),
						array('value'=>'1','label'=>$this->__('Yes'))
						);
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setName('brands[' . $idCat .'][filter_visible]')
            ->setId('filter_visible ' . $idCat)
            ->setOptions($options)
            ->setValue($this->getValueFilter($idCat));
        return $select->getHtml();
    }
	public function getValueFilter($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getFilterVisible();
			}
		}
		return 0;
	}
	
	// Begin get value of Cat attribute
	public function getMetaTitle($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getMetaTitle();
			}
		}
		return '';
	}
	public function getDescription($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getDescription();
			}
		}
		return '';
	}
	public function getMetaDescription($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getMetaDescription();
			}
		}
		return '';
	}
	public function getImage($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getImage();
			}
		}
		return '';
	}
	public function getCmsBlock($idCat)
	{
		foreach($this->getAttributeCats() as $object)
		{
			if($object->getOptionId() == $idCat)
			{
				return $object->getCmsBlock();
			}
		}
		return '';
	}
	public function getAttributeHtml($option)
	{
		$htmlLi = '<dl class="accordion brands" id="brands">
						<dt class="" id="dt-buttons"><a class="title" href="#">'.$this->escapeHtml($option->getTitle()).'</a>
						</dt>
						<dd class="" id="dd-buttons">
							<div class="fieldset">
								<div class="grid">
									<table class="form-list">
										<tr>
											<td class="label">'.$this->__('Cms Block').'</td>
											<td class="value">
												'.$this->getFormText("cms_block".$option->getId(),"brands[".$option->getId()."][cms_block]", $this->getCmsBlock($option->getId())).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Meta title').'</td>
											<td class="value">
												'.$this->getFormText("meta_title".$option->getId(),"brands[".$option->getId()."][meta_title]", $this->getMetaTitle($option->getId())).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Meta Description').'</td>
											<td class="value">
												'.$this->getFormTextArea("meta_description".$option->getId(),"brands[".$option->getId()."][meta_description]", $this->getMetaDescription($option->getId())).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Description').'</td>
											<td class="value">
												'.$this->getFormTextArea("description".$option->getId(),"brands[".$option->getId()."][description]", $this->getDescription($option->getId())).'           
											</td>
										</tr>
										<tr>
											<td class="label">'.$this->__('Image').'</td>
												<td class="value">
													'.$this->getFormFile("image".$option->getId(),"brands_image_".$option->getId()) . $this->getNodeDel($option->getId(), "brands[".$option->getId()."][del_image]").'    
												</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Featured Category").'</td>
											<td class="value">'.$this->featuredCategorySelect($option->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Canonical Url").'</td>
											<td class="value">'.$this->canonicalUrlSelect($option->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Multiple Selection").'</td>
											<td class="value">'.$this->multipleSelectionSelect($option->getId()).'</td>
										</tr>
										<tr>
											<td class="label">'.$this->__("Filter Visible").'</td>
											<td class="value">'.$this->filterVisibleSelect($option->getId()).'</td>
										</tr>
									</table>
								</div>
							</div>
						</dd>
					</dl>';
        $html[] = $htmlLi;
		$html = implode("\n", $html);
        return $html;
	}
	// End function get valua Cat attribute
	public function renderCategoriesMenuHtml($level = 0, $outermostItemClass = '', $childrenWrapClass = '')
    {
		$attributeId = $this->getRequest()->getParam('catatt_id');
		$html = '<div class="content-header"><h4>'.$this->__('All Brands').'</h4></div>';
		$brands = Mage::getModel('navigationlayered/brands')->getCollection()->getJoinOptions();
        foreach ($brands as $_brand) {
           $html .= $this->getAttributeHtml($_brand);
        }
		
        return $html;
    }
}

