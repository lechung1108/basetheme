<?php
class AMZ_NavigationLayered_Block_Adminhtml_Attributes extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_attributes';
    $this->_blockGroup = 'navigationlayered';
    $this->_headerText = Mage::helper('navigationlayered')->__('Attributes Manager');
    $this->_addButtonLabel = Mage::helper('navigationlayered')->__('Add Item');
    parent::__construct();
	$this->setTemplate('amz_navigationlayered/attributes/attributes.phtml');
  }
  /**
     * Prepare button and grid
     *
     * @return AMZ_NavigationLayered_Block_Adminhtml_Collars
     */
    protected function _prepareLayout()
    {
        $this->setChild('add_new_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('navigationlayered')->__('Load'),
                    'onclick'   => "setLocation('".$this->getUrl('*/*/loadattributes')."')",
                    'class'   => 'add'
                    ))
                );
		$this->setChild('load_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('navigationlayered')->__('Load Default with Category'),
                    'onclick'   => "loadDefaultSetting('".$this->getUrl('*/*/loaddefaultsetting')."');", 
                    'class'   => 'load'
                    ))
                );
		$this->setChild('load_cat_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('navigationlayered')->__('Add Products to Root Cat'),
                    'onclick'   => "loadDefaultSetting('".$this->getUrl('*/*/addAllProductsToRootCat')."');", 
                    'class'   => 'load'
                    ))
                );
		 if (!Mage::app()->isSingleStoreMode()) {
            $this->setChild('store_switcher',
                $this->getLayout()->createBlock('adminhtml/store_switcher')
                    ->setUseConfirm(false)
                    ->setSwitchUrl($this->getUrl('*/*/*', array('store'=>null)))
            );
        }
        $this->setChild('grid', $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_grid', 'attributes.grid'));
        return parent::_prepareLayout();
    }
	
    public function getAddNewButtonHtml()
    {
        return $this->getChildHtml('add_new_button');
    }
    public function getLoadButtonHtml()
    {
        return $this->getChildHtml('load_button');
    }
    public function getLoadCatButtonHtml()
    {
        return $this->getChildHtml('load_cat_button');
    }

    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

    public function getStoreSwitcherHtml()
    {
        return $this->getChildHtml('store_switcher');
    }
	public function getWeightUrl()
	{
		return $this->getUrl('*/*/changeWeight', array('_current'=>true));
	}
}