<?php
$GLOBALS['_DOJO'] = true;
class AMZ_NavigationLayered_Block_Treecat extends Mage_Catalog_Block_Navigation
{
    /**
     * Render category to html
     *
     * @param Mage_Catalog_Model_Category $category
     * @param int Nesting level number
     * @param boolean Whether ot not this item is last, affects list item class
     * @param boolean Whether ot not this item is first, affects list item class
     * @param boolean Whether ot not this item is outermost, affects list item class
     * @param string Extra class of outermost list items
     * @param string If specified wraps children list in div with this class
     * @param boolean Whether ot not to add on* attributes to list item
     * @return string
    */
	protected $hasItemShowAll = false;
	public function getCatNavigationHtml()
	{
		$currentCat = Mage::registry('current_category');
		$html = '';
		$allCats = array();
		$currentCat ? $currentSex = $currentCat->getAttributeSex() : $currentSex = 'null';
		$arrayAttributeSex = $this->getAttributeSexToArray();
		$collection = Mage::getModel('catalog/category')->getCollection()
											->addAttributeToSelect(array('*'))
											->addAttributeToSort('level','asc')
											->addAttributeToSort('position','asc')
											->addAttributeToFilter('level', array('gt'=>1))
											->addAttributeToFilter('is_active',1);
		
		foreach($collection as $category)
		{
			if($category->getLevel() ==2)$parents[] = $category;
			else $allCats[$category->getId()] = $category;
		}
		$html = '';
		$currentSex = explode(',', $currentSex);
		if($currentCat && $currentCat->getAttributeSex() == 'null')
			$currentSex = array();
		foreach($parents as $parent)
		{
			$html .=  $this->_renderHtml($allCats, $parent, $arrayAttributeSex, $currentSex);
		}
		return $html;
	}
	public function getAttributeSexToArray()
	{
		$attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_category','attribute_sex');
		$attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;
		$attribute_options_model->setAttribute($attribute);
		$options = $attribute_options_model->getAllOptions(false);
		$arrayIds = array();
		foreach($options as $option)
		{
			if(strtolower($option['label']) == 'men' || strtolower($option['label']) == 'women' || strtolower($option['label']) == 'kids')
			{
				$arrayIds[] = $option['value'];
			}
		}
		return $arrayIds;	
	}
	protected function _renderHtml($collection, $category, $arrayAttributeSex = array() ,$currentSex = array(), $level=0, $currentCatId = false)
	{
		if(!$currentCatId && Mage::registry('current_category'))
			$currentCatId = Mage::registry('current_category')->getId();
		$curCat = $this->getCurrentCategory();
		if( !array_intersect(explode(',',$category->getAttributeSex()),$currentSex) && $currentSex && $arrayAttributeSex)
		{
			if(!$this->isCategoryActive($category) && array_intersect($currentSex, $arrayAttributeSex))
				return '';
		}
		if (!$category->getIsActive()){
            return '';
        }
		if(!$category->getIncludeInMenu())
		{
			return '';
		}
		$html = array();
		if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = explode(',',$category->getChildren());
            $childrenCount = count($children);
        } else {
            $children = explode(',',$category->getChildren());
            $childrenCount = count($children);
        }
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        foreach (explode(',',$category->getChildren()) as $ch) {
			if($childrenCount != '1') {
				if (isset($collection[$ch]) && $collection[$ch]->getIsActive()) {
					$activeChildren[] = $collection[$ch];
				}
			}
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 1);
		
		$classes = array();
        $classes[] = 'level' . $level;
        $classes[] = 'nav-' . $this->_getItemPosition($level);
		if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
            $classes[] = 'open';
			if($curCat->getId() == $category->getId())
				$classes[] = 'current_active';
			if ($hasActiveChildren) 
				$classes[] = 'parent_active';
        }
		
		if(!$category->getProductCount())
		{
			$classes[] = 'no-product';
		}
		if ($hasActiveChildren) {
            $classes[] = 'parent';
        }
		$attributes = array();
        if (count($classes) > 0) {
            $attributes['class'] = implode(' ', $classes);
        }
		$htmlLi = '<dt';
        foreach ($attributes as $attrName => $attrValue) {
            $htmlLi .= ' ' . $attrName . '="' . str_replace('"', '\"', $attrValue) . '"';
        }
        $htmlLi .= '>';
        $html[] = $htmlLi;
        $html[] = '<a href="'.$this->getCategoryUrl($category).'">';
        $html[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        $html[] = '</a>';
        $html[] = '</dt>';

        // render children
        $htmlChildren = '';
		if($category->getChildren() && $this->isCategoryActive($category))
		{	
			$children = explode(',',$category->getChildren());
			$arrayValue = array();
			foreach($children as $child)
			{
				if(isset($collection[$child]))
				{
					$childObject = $collection[$child];
					if(isset($arrayValue[$childObject->getPosition()]))  $arrayValue[$childObject->getPosition().$childObject->getId()] = $childObject;
					else $arrayValue[$childObject->getPosition()] = $childObject;
				}
			}
			
			ksort($arrayValue);
			foreach($arrayValue as $childCat)
			{
				$htmlChildren .= $this->_renderHtml($collection, $childCat, $arrayAttributeSex ,$currentSex , ($level + 1), $currentCatId);
			}
			if(!$this->hasItemShowAll && $category->getId() != $currentCatId)
			{
				$htmlChildren .= $this->getShowAll($category, $level+1);
				$this->hasItemShowAll = true;
			}
		}
		$html[] = '<dd class="'.$attributes['class'].'">';
		if (!empty($htmlChildren)) {
			$html[] = '<dl class="level' . ($level+1) . '">';
            $html[] = $htmlChildren;
            $html[] = '</dl>';
         }
        $html[] = '</dd>';

        //$html[] = '</dl>';

        $html = implode("\n", $html);
		return $html;
	}
	protected function getShowAll($category, $level)
	{
		if (!$category->getIsActive()){
            return '';
        }
		if(!$category->getIncludeInMenu())
		{
			return '';
		}
		$html = array();
		$classes = array();
        $classes[] = 'level' . $level;
        $classes[] = 'cat_showall';
        $classes[] = 'nav-' . $this->_getItemPosition($level);
		if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
            $classes[] = 'open';
			if($this->getRequest()->getParam('cat'))
				$classes[] = 'current_active';
        }
		if(!$category->getProductCount())
		{
			$classes[] = 'no-product';
		}
		$attributes = array();
        if (count($classes) > 0) {
            $attributes['class'] = implode(' ', $classes);
        }
		$htmlLi = '<dt';
        foreach ($attributes as $attrName => $attrValue) {
            $htmlLi .= ' ' . $attrName . '="' . str_replace('"', '\"', $attrValue) . '"';
        }
        $htmlLi .= '>';
        $html[] = $htmlLi;
        $html[] = '<a href="'.$this->getCategoryUrl($category).'">';
        // $html[] = '<span>' . $this->__($this->escapeHtml('...ALLE anzeigen')) . '</span>';
        $html[] = '</a>';
        $html[] = '</dt>';
		$html[] = '<dd class="'.$attributes['class'].'">';
        $html[] = '</dd>';
		$html = implode("\n", $html);
		return $html;
	}
    protected function _renderCatNavigationTreeHtml($category, $arrayAttributeSex, $curAttSex = array())
    {
		$level = 0; $isLast = false; $isFirst = false;
        $isOutermost = false; $outermostItemClass = ''; $childrenWrapClass = ''; $noEventAttributes = true;
		$htmlChildrenFirst = "";
		
		if($category->getAttributeSex())
		{
			// die($category->getAttributeSex());
		}
		if($category->getAttributeSex() && !array_intersect(explode(',',$category->getAttributeSex()),$curAttSex) && !array_intersect(explode(',',$category->getAttributeSex()),$arrayAttributeSex))
		{
			return '';
		}
        if (!$category->getIsActive()){
            return '';
        }
        $html = array();

        // get all children
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        foreach ($children as $child) {
			if($childrenCount != '1') {
				if ($child->getIsActive()) {
					$activeChildren[] = $child;
				}
			}
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 1);

        // prepare list item html classes
        $classes = array();
        $classes[] = 'level' . $level;
        $classes[] = 'nav-' . $this->_getItemPosition($level);
        $linkClass = '';
        if ($isOutermost && $outermostItemClass) {
            $classes[] = $outermostItemClass;
            $linkClass = ' class="'.$outermostItemClass.'"';
        }
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
            $classes[] = 'open';
        }
        if ($isFirst) {
            $classes[] = 'first';
        }
        if ($isLast) {
            $classes[] = 'last';
        }
        if ($hasActiveChildren) {
            $classes[] = 'parent';
        }

        // prepare list item attributes
        $attributes = array();
        if (count($classes) > 0) {
            $attributes['class'] = implode(' ', $classes);
        }

        // assemble list item with attributes
        $htmlLi = '<dt';
        foreach ($attributes as $attrName => $attrValue) {
            $htmlLi .= ' ' . $attrName . '="' . str_replace('"', '\"', $attrValue) . '"';
        }
        $htmlLi .= '>';
        $html[] = $htmlLi;
        if($this->alllink || !$hasActiveChildren)
        $html[] = '<a href="'.$this->getCategoryUrl($category).'"'.$linkClass.'>';
        $html[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        if($this->alllink || !$hasActiveChildren)
        $html[] = '</a>';
        
        $html[] = '</dt>';

        // render children
        $htmlChildren = '';
        $j = 0;
        foreach ($activeChildren as $child) {
			if(Mage::registry('current_category') and preg_match("/".$child->getId()."/", Mage::registry('current_category')->getData('path')) and $category->getId() == 119) {
				$htmlChildrenFirst .= $this->_renderCatNavigationTreeHtml($child,$arrayAttributeSex, $curAttSex);
			} else {
				if(Mage::registry('current_category') and Mage::registry('current_category')->getId() != $child->getParentId()) {
					$htmlChildren .= $this->_renderCatNavigationTreeHtml($child, $arrayAttributeSex, $curAttSex);
				}
			}
			$j++;
        }
        $html[] = '<dd class="'.$attributes['class'].'">';
        if (!empty($htmlChildren)) {
            if ($childrenWrapClass) {
                $html[] = '<div class="' . $childrenWrapClass . '">';
            }
            $html[] = '<dl class="level' . ($level+1) . '">';
			if($htmlChildrenFirst) {
				$html[] = $htmlChildrenFirst;
			}
            $html[] = $htmlChildren;
            $html[] = '</dl>';
            if ($childrenWrapClass) {
                $html[] = '</div>';
            }
        }
        $html[] = '</dd>';

        //$html[] = '</dl>';

        $html = implode("\n", $html);
        return $html;
    }
	public function getSexNavHtml(){
		$currentCat = Mage::registry('current_category');
		
		$html = '';
		$allCats = array();
		if(!$currentCat)
			return false;
		$currentCat ? $currentSex = $currentCat->getAttributeSex() : $currentSex = 'null';
		$arrayAttributeSex = $this->getAttributeSexToArray();
		$collection = Mage::getModel('catalog/category')->getCollection()
											->addAttributeToSelect(array('*'))
											->addAttributeToSort('level','asc')
											->addAttributeToSort('position','asc')
											->addAttributeToFilter('level', array('gt'=>1))
											->addAttributeToFilter('include_in_menu', 1)
											->addAttributeToFilter('is_active',1);
		
		$html = '';
		
		foreach($collection as $category)
		{
			
			if($category->getLevel() ==2 && $category->getAttributeSex() && $category->getAttributeSex() != 'null' && array_intersect(explode(',',$category->getAttributeSex()), $arrayAttributeSex))  
			{
				$html .=  '<li class="';
				if($currentSex != 'null' && array_intersect(explode(',',$currentSex),  $arrayAttributeSex))
				{
					if(!array_intersect(explode(',',$category->getAttributeSex()),explode(',',$currentSex)))
						$html .= 'disable';
					else
						$html .= 'active';
					
				}
				else
					$html .= 'nornal';
				$href = $this->getCategoryUrl($category);//Mage::helper('navigationlayered/url')->getFullCatUrl(array(), false, $category)	
				$html .= ' '. str_replace(' ','_',$category->getName()) .'">';	
				$html .= '<a href="'. $href .'">'. $category->getName(). '</a>';
				$html .= '</li>';
			}
			else
			{
				$allCats[$category->getId()] = $category;
			}
		}  
		
		$this->CatCollection = $allCats;		
		return $html;
	}
	
	public function getSubCurrentCategory()
	{
		$currentCat = Mage::registry('current_category');
		if(!$currentCat) return false;
		$html = '';
		if($currentCat->getId() == 2)
			return '';
		$ParentCat = $this->getParentCat($currentCat);
		return $this->_renderHtml($this->CatCollection, $ParentCat);
		print_r($ParentCat->getName());die;
		
		if($currentCat->getChildren() && $this->isCategoryActive($currentCat))
		{	
			$children = explode(',',$currentCat->getChildren());
			$arrayValue = array();
			foreach($children as $child)
			{
				if(isset($this->CatCollection[$child]))
				{
					$childObject = $this->CatCollection[$child];
					if(isset($arrayValue[$childObject->getPosition()]))  $arrayValue[$childObject->getPosition().$childObject->getId()] = $childObject;
					else $arrayValue[$childObject->getPosition()] = $childObject;
				}
			}
			
			ksort($arrayValue);
			if(count($arrayValue) < 1)
				return '';
			foreach($arrayValue as $category)
			{
				// $url = Mage::helper('navigationlayered/url')->getFullCatUrl(array(), false, $category) ;
				// if($currentCat->getId() != '2')
					$url = $this->getCategoryUrl($category);
				$html .= '<li class="sub_cat"><a href="'. $url .'">'.$category->getName() .'</a></li>';
			}
		}
		return $html;
		
	}
	public function getSubTitleCat(){
		$currentCat = Mage::registry('current_category');
		if(!$currentCat) return false;
		if($currentCat->getId() == 2)
		return '';
		// if($brand = Mage::registry('current_brand'))
		// {
			// if($currentCat->getId() == 2)
			// return $brand->getTitle();
		// }
		return $this->getParentCat($currentCat)->getName();
	}
	public function getOtherCategory()
	{
		$currentCat = Mage::registry('current_category');
		if(!$currentCat) return false;
		$currentParent = $this->getParentCat($currentCat); //$currentCat->getParentCategory();
		$parenCat = $currentParent->getParentCategory();
		$html = '';
		if($parenCat->getChildren() && $this->isCategoryActive($parenCat))
		{	
			$children = explode(',',$parenCat->getChildren());
			$arrayValue = array();
			foreach($children as $child)
			{
				if(isset($this->CatCollection[$child]))
				{
					$childObject = $this->CatCollection[$child];
					if($currentParent->getId() == $childObject ->getId())
						continue;
					if(isset($arrayValue[$childObject->getPosition()]))  $arrayValue[$childObject->getPosition().$childObject->getId()] = $childObject;
					else $arrayValue[$childObject->getPosition()] = $childObject;
				}
			}
			
			ksort($arrayValue);
			if(count($arrayValue) < 1)
				return '';
			
			foreach($arrayValue as $category)
			{
				$url = Mage::helper('navigationlayered/url')->getFullCatUrl(array(), false, $category) ;
				if($currentCat->getId() != '2')
				$url = $this->getCategoryUrl($category);
				$html .= '<li class="sub_cat"><a href="'. $url .'">'.$category->getName() .'</a></li>';
			}
		}
		return $html;
	}
	public function getParentCat($category)
	{
		if(isset($this->CatCollection[$category->getParentId()]))
			$category = $this->getParentCat($this->CatCollection[$category->getParentId()]);
		return $category;
	}
}
?>