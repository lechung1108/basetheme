<?php
  
class AMZ_NavigationLayered_Block_Featured extends Mage_Core_Block_Template
{
    public function getItems()
    {
        $items = array();
        // get filter ID by attribute code 
        $id = Mage::getResourceModel('navigationlayered/attributes')->getCollection()->addStoreFilter()->addFieldToFilter('attribute_code', $this->getAttributeCode())->getFirtsItem()->getId();
         
        if ($id){
            $items = Mage::getResourceModel('navigationlayered/attributesvalue_collection')
                ->addFieldToFilter('attribute_id', $id); 
                
            if ($this->getRandom()){
                $items->setOrder('rand()');
            } 
            else {
                $items->setOrder('value', 'asc');    
                $items->setOrder('title', 'asc');    
            }  
             
            if ($this->getLimit()){
                $items->setPageSize(intVal($this->getLimit()));
            }   
                
            $hlp = Mage::helper('navigationlayered/url');
            $base = Mage::getBaseUrl('media') . 'attributes/';
            foreach ($items as $item){
                $item->setDetailIcon($base . $item->getDetailIcon());   
                
                $attrCode = $this->getAttributeCode();
                $optLabel = $item->getValue() ? $item->getValue() : $item->getTitle();
                $optId    = $item->getOptionId();
                $item->setUrl($hlp->getOptionUrl($attrCode, $optLabel, $optId));   
            }
        }
        return $items;
    }

}