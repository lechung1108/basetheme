<?php
class AMZ_NavigationLayered_Block_Abstract extends Mage_Core_Block_Template
{
	protected $_selectDefaultTemplate = 'navigationlayered/select.phtml';
	protected $_name = '';
	
	protected function _getStore()
    {
		return Mage::app()->getStore()->getId();
    }
	
	public function getObjectName()
	{
	
	}
	
	/*
	* Get item image url
	**/
	public function getImageUrl($item)
	{
		$imageUrl = Mage::getBaseUrl('media').'none.png';
		if($item->getImage()){
			$imageUrl = Mage::getBaseUrl('media').$item->getImage();
		}
		return $imageUrl;
	}
	
	/*
	* Get item price
	**/
	public function getItemPrice($item)
	{
		return  Mage::app()->getStore()->formatPrice(Mage::app()->getStore()->convertPrice($item->getPrice()));
	}
	
	/*
	* To select Html
	*/
	public function getItemToSelect($collection, $value=null)
	{
		$results = array();
		foreach($collection as $item){
			$results[$item->getId()] = $item->getTitle();
		}
		 $select = $this->getLayout()->createBlock('core/html_select')
            ->setName($this->_name)
            ->setId($this->_name)
            ->setClass('validate-select')
            ->setValue($value)
            ->setOptions($results);
        return $select->getHtml();
	}
	
}