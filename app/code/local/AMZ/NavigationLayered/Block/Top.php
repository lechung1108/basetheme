<?php

class AMZ_NavigationLayered_Block_Top extends Mage_Core_Block_Template
{
    private $options = array();
	private $brand = false;
    
    private function trim($str)
    {
        $str = strip_tags($str);  
        $str = str_replace('"', '', $str); 
        return trim($str, " -");
    }
	protected function _construct()
    {
		parent::_construct();
        $this->addData(array(
            'cache_lifetime'    => false,
            'cache_tags'        => array(
        	    Mage_Catalog_Model_Product::CACHE_TAG,
        	    Mage_Catalog_Model_Category::CACHE_TAG,
        	    Mage_Cms_Model_Block::CACHE_TAG, 
                Mage_Catalog_Model_Category::CACHE_TAG, 
                Mage_Core_Model_Store_Group::CACHE_TAG
            ),
        ));
    }
    
    /**
     * Retrieve Key for caching block content
     *
     * @return string
     */
    public function getCacheKey()
    {
        return $this->generateBlockName(array('block', 'AMZ_NavigationLayered_Block_Top', md5($this->helper('core/url')->getCurrentUrl())))
        	. '_' . Mage::app()->getStore()->getId()
            . '_' . Mage::getSingleton('customer/session')->getCustomerGroupId()
            . '_' . md5($this->getTemplate());
    }
	public function generateBlockName($array = array())
    {
        $name = implode('_', $array);
        while(in_array($name, array_keys($this->getLayout()->getAllBlocks()))) {
            $name .= rand(1,9);
        }        
        return $name; 
    }
    public function renderHtml($collection, $category)
	{
		$html = '<a href="">'.$category->getName().'</a><br>';
		if($category->getChildren())
		{	
			$children = explode(',',$category->getChildren());
			$arrayValue = array();
			foreach($children as $child)
			{
				if(isset($collection[$child]))
				{
					
					$childObject = $collection[$child];
					if(isset($arrayValue[$childObject->getPosition()]))  $arrayValue[$childObject->getPosition().$childObject->getId()] = $childObject;
					else $arrayValue[$childObject->getPosition()] = $childObject;
				}
			}
			
			ksort($arrayValue);
			foreach($arrayValue as $childCat)
			{
				$html .= $this->renderHtml($collection, $childCat);
			}
		}
		
		return $html;
	}
    protected function _prepareLayout()
    {	
		$head_data = array();
		$category = Mage::registry('current_category');
		if(!$category)
		 return parent::_prepareLayout();
		$hasCanonical = !Mage::helper('navigationlayered')->isVersionLessThan(1, 4);
        $isNavigationlayered   = in_array(Mage::app()->getRequest()->getModuleName(), array(Mage::getStoreConfig('navigationlayered/seo/key'), 'navigationlayered'));
        if ($hasCanonical && $isNavigationlayered){
            $url = Mage::getSingleton('catalog/layer')->getCurrentCategory()->getUrl();
            
            $head = $this->getLayout()->getBlock('head');
            //remove canonical URL for the root category for 1.4+
            $head->removeItem('link_rel', $url);
            //$head->addLinkRel('canonical', Mage::helper('navigationlayered/url')->getCanonicalUrl());
        }  
        $filters = 	Mage::getResourceModel('navigationlayered/attributes_collection')
					->addStoreFilter()
					->getSettingWithCatCollection($category->getId()); 
        $hash = array();
        foreach ($filters as $f){
            $code = $f->getAttributeCode();
            $vals = Mage::helper('navigationlayered')->getRequestValues($code);
			if ($vals){
				$abc[] = $vals;
                foreach($vals as $v)
                    $hash[$v] = true;
            }
        }
       
        if (!$hash)
		{
			$this->setSlideArray($category);
            return parent::_prepareLayout();  
		}
		//if(!Mage::registry('cat_hadfilter')) Mage::register('cat_hadfilter', true);
		$options = Mage::getResourceModel('navigationlayered/attributesvalue_collection')
				->addStoreFilter()
				->getByOptionInArray(array_keys($hash))
				->getSettingWithCatCollection($category->getId())
				->getJoinAttributes()
				->getJoinBrands()
				->getJoinSettingAttributesCat($category->getId())
				->setOrder('weight','asc');
		//echo $options->getSelect();die;
        $cnt = $options->count();
		if (!$cnt)
            return parent::_prepareLayout(); 
            
        //some of the options value have wrong value; 
        if ($cnt && $cnt < count($hash)){
            return parent::_prepareLayout(); 
            // or make 404 ?
        }            
              
        // sort options by attribute ids and add "show_on_list" property
		$attIds = array();
		$brandId = '';
		$hashoption = array();
        foreach ($options as $opt){ 
            $id = $opt->getOptionId();
            $opt->setShowOnList($hash[$id]);
            $hashoption[$id] = clone $opt; 
        }
		$hash = $hashoption;
		
        // unset "fake"  options (not object)
        foreach ($hash as $id => $opt){
            if (!is_object($opt))
                unset($hash[$id]);     
        }    
        if (!$hash)
            return parent::_prepareLayout();        


        $head = $this->getLayout()->getBlock('head');
        if ($head){
			$headTitle = $head->getTitle();
			$headDescr = $head->getDescription();
			$title = '';
			$descr = '';
            // trim prefix if any
            $prefix = Mage::getStoreConfig('design/head/title_prefix');
            $prefix = htmlspecialchars(html_entity_decode(trim($prefix), ENT_QUOTES, 'UTF-8'));            
            if ($prefix){
                $title = substr($title, strlen($prefix));
            }

            $titleSeparator = Mage::getStoreConfig('navigationlayered/general/title_separator');
            $descrSeparator = Mage::getStoreConfig('navigationlayered/general/descr_separator');
			$counter = 0;
            foreach ($hash as $opt) {
				$attributeOptions = Mage::getResourceModel('eav/entity_attribute_option_collection')
                                    ->setPositionOrder('asc')
                                    ->setAttributeFilter(Mage::getModel('eav/entity_attribute_option')->load($opt->getId())->getData('attribute_id'))
                                    ->setStoreFilter()
                                    ->load()
									->getData();
				foreach($attributeOptions as $attrdata){
					if($attrdata['option_id'] == $opt->getId()) {
						$opt->setCanonical($attrdata['value']);
						$opt->setTitle($attrdata['value']);
					}
				}
				if($opt->getIsBrand())
				{
					if(!Mage::registry('current_brand'))Mage::register('current_brand', $opt); 
				}
				if($category->getId() == 2)
				{
					if($opt->getIsBrand())
					{
						$brandId = $opt->getId();
						$BrandAtts = $opt;
						if(!Mage::registry('current_brand'))Mage::register('current_brand', $opt);
					}
					else
					{
						$attIds[] = $opt->getAttributeId();
					}
				}
				$counter++;
            }
			
			if($category->getId() != 2) {
				$label = '';
				$newUrl = $category->getUrl();
				$url = Mage::getSingleton('catalog/layer')->getCurrentCategory()->getUrl();
				$head->removeItem('link_rel', $url);
				$first = true;
				$brand = false;
				foreach ($hash as $opt) {
					if($opt->getIsBrand())
						$brand = $opt;
					if($opt->getCatMetaTitle())
						$head_data['retitle_cat'] = true;
					$first == true ? (($opt->getMetaRobots() != null && $opt->getMetaRobots() == true)? $head_data['robots']='INDEX,FOLLOW' : (($opt->getMetaRobots() != null && $opt->getMetaRobots() == false)? $head_data['robots']='NOINDEX,FOLLOW' : (($opt->getAttCatMetaRobots() == true) ? $head_data['robots']='INDEX,FOLLOW' : $head_data['robots']='NOINDEX,FOLLOW'))):'';
					$newUrl .= $opt->getCanonicalUrl() ? str_replace("katalog/","",str_replace(Mage::getBaseUrl(), "", Mage::helper('navigationlayered/url')->getOptionUrl(Mage::getModel('eav/config')->getAttribute('catalog_product', $opt->getAttributeId())->getData('attribute_code'), $opt->getData('canonical'), $opt->getData('option_id'),$opt))) : '';
					$label =  $opt->getCatMetaTitle()?$opt->getCatMetaTitle(): $opt->getTitle();
					$discription = $opt->getCatMetaDescription()?$opt->getCatMetaDescription():($opt->getMetaDescription()?$opt->getMetaDescription():($opt->getMetaTitle()?$opt->getMetaTitle():$opt->getTitle()));
					$title .= $label . $titleSeparator;
					$descr .= $discription . $descrSeparator;
					$first = false;
				}
				if($brand)
					$descr = $brand->getTitle() . $titleSeparator;
				//$head->addLinkRel('canonical', $newUrl); // Older
				$head_data['canonical'] = $newUrl;
				$head_data['title'] = Mage::helper('navigationlayered')->__($title);
				$head_data['descr'] = Mage::helper('navigationlayered')->__($descr);
				//$title .= $headTitle;
				//$descr .= $headDescr;
			} else {
				$i=0;
				$newUrl = Mage::getBaseUrl();
				$url = Mage::getBaseUrl() . Mage::getStoreConfig('navigationlayered/seo/key');
				$head->removeItem('link_rel', $url);
				isset($BrandAtts) ? (($BrandAtts ->getMetaRobots() != null && $BrandAtts->getMetaRobots() == true) ? $head_data['robots']='INDEX,FOLLOW' : $head_data['robots'] = 'NOINDEX,FOLLOW'): '' ;
				foreach ($hash as $opt) {
					$data['attribute_id'] = Mage::getModel('eav/entity_attribute_option')->load($opt->getId())->getData('attribute_id');
					if($data['attribute_id'] == $opt->getAttributeId()) {	
						$i++;
						
						$newUrl .= $opt->getCanonicalUrl() ? str_replace("katalog/","",str_replace(Mage::getBaseUrl(), "", Mage::helper('navigationlayered/url')->getOptionUrl(Mage::getModel('eav/config')->getAttribute('catalog_product', $opt->getAttributeId())->getData('attribute_code'), $opt->getData('canonical'), $opt->getData('option_id'),$opt))) : '' ;
						$label =  $opt->getCatMetaTitle()?$opt->getCatMetaTitle():($opt->getMetaTitle()?$opt->getMetaTitle():$opt->getTitle());
						$discription = $opt->getCatMetaDescription()?$opt->getCatMetaDescription():($opt->getMetaDescription()?$opt->getMetaDescription():($opt->getMetaTitle()?$opt->getMetaTitle():$opt->getTitle()));
						$title .= $label;
						$title .= $i < count($hash) ? $titleSeparator : '';
						$descr .= $discription;
						$descr .= $i < count($hash) ? $descrSeparator : '';
					}
				}
				//$head->addLinkRel('canonical', $newUrl);
				if($opt && $opt->getIsBrand() && Mage::getStoreConfig('brandmeta/brand/enabled'))
				{
					$opt->setCategory($category);
					$tplTitle = Mage::getStoreConfig('brandmeta/brand/meta_title');
					if(!$opt->getMetaTitle() && $tplTitle)
					{
						$opt->setBrand($opt->getTitle());
						$title = Mage::helper('ammeta')->parse($opt, $tplTitle);
					}
					$tplDescription = Mage::getStoreConfig('brandmeta/brand/meta_description');
					if(!$opt->getMetaDescription() && $tplDescription)
					{
						$opt->setBrand($opt->getTitle());
						$descr = Mage::helper('ammeta')->parse($opt, $tplDescription);
					}
					$tplKeyWord = Mage::getStoreConfig('brandmeta/brand/meta_keywords');
					$head = $this->getLayout()->getBlock('head');
					if($tplKeyWord && $head)
					{
						$opt->setBrand($opt->getTitle());
						$keywords = Mage::helper('ammeta')->parse($opt, $tplKeyWord);
						$head->setKeywords($keywords);
					}
				}
				$head_data['canonical'] = $newUrl;
				$head_data['title'] = Mage::helper('navigationlayered')->__($title);
				$head_data['descr'] = Mage::helper('navigationlayered')->__($descr);
				if($title == "") {
					$title = $headTitle;
					$descr = $headDescr;
				}
			}
			$suffix = Mage::getStoreConfig('design/head/title_suffix');            
            $suffix = htmlspecialchars(html_entity_decode(trim($suffix), ENT_QUOTES, 'UTF-8'));            
            if ($suffix){
                $title = str_replace($suffix, "", $title);
            }
			$title = ltrim($title, " ");
			$descr = ltrim($descr, " ");
            //$head->setTitle(Mage::helper('navigationlayered')->__($title));
            //$head->setDescription(Mage::helper('navigationlayered')->__($descr)); 
        }
		//print_r($newUrl);die;
		if(!Mage::registry('head_data')) Mage::register('head_data', $head_data);
        $this->options = $hash;
        $options =  $this->getOptions();
		Mage::unregister('cat_options');
		Mage::register('cat_options', $options);
        return parent::_prepareLayout();
    }
    
    public function getOptions()
    {
        $res = array();
		$category = Mage::registry('current_category');
		$hadFilter = Mage::registry('cat_hadfilter');
		$options = $this->options;
		if($hadFilter && $this->options)
		{
			$option = array_shift($options);
			//print_r($option);die;
			$this->setSlideArray($option);
		}
		
        foreach ($this->options as $opt){
				if (!$opt->getFeaturedCategory() && !$opt->getCatAttFeaturedCategory() && $category->getId() != 2)
					continue;
				else if($opt->getCatAttFeaturedCategory() == false && $opt->getCatAttFeaturedCategory() != null && $category->getId() != 2)
					continue;
					
				$item = array();
				$item['title'] = $this->htmlEscape($opt->getTitle());
				$item['descr'] = $opt->getDescription();
				$item['cms_block'] = '';
				if($category->getId() == 2)
				{
					$blockName = $opt->getCmsBlock();
				}
				else
				{
					$blockName = $opt->getCatCmsBlock();
				}
				if ($blockName) {
					if($this->getLayout()
						->createBlock('cms/block')
						->setBlockId($blockName)
						->toHtml())
					$item['cms_block'] = $this->getLayout()
						->createBlock('cms/block')
						->setBlockId($blockName)
						->toHtml();
				}
				
				$item['image'] = '';
				if ($opt->getListingIcon())
					$item['image'] = Mage::getBaseUrl('media') . '/attributes/' . $opt->getListingIcon();
				$item['brand_att'] = $this->brand;
				$item['object'] = $opt;
				$res[] = $item;
        }
        return $res;
    }
	public function setSlideArray($category = false)
	{
		$slide = $this->getSlideArray($category);
		Mage::unregister('slider_custom');
		Mage::register('slider_custom', $slide);
	}
	private function cvStrToArray($string, $key = '|')
	{
		return explode($key, $string);
	}
	public function getSlideArray($_category = false)
	{
		if(!$_category)
			$_category = Mage::registry('current_category');
		if(!$_category->getSliderStatus())
			return false;
		$slideId = $_category->getSliderId(); // Line display slider in setting.
		$lineDisplaySlide = $_category->getSliderLineDisplay(); // Line display slider in setting.
		$NoProductOnSlide = $_category->getSliderNumProduct();	// No products in slider cleared.
		$popupSlide = $_category->getPopupSlide();	// popup slide.
		$slideAlign = $_category->getSliderAlign();	// slide align
		$array = array();
		if(!$slideId) return '';
		foreach($this->cvStrToArray($slideId) as $key=>$id)
		{
			$array[$key]['slideId'] = $id;
			$array[$key]['lineDisplay'] = 0;
			foreach($this->cvStrToArray($lineDisplaySlide) as $keys => $line)
			{
				if($key == $keys)
				{
					$array[$key]['lineDisplay'] = $line;
					break;
				}
			}
			$array[$key]['productCleared'] = 2;
			foreach($this->cvStrToArray($NoProductOnSlide) as $numkey => $number)
			{
				if($key == $numkey)
				{
					$array[$key]['productCleared'] = $number;
					break;
				}
			}
			foreach($this->cvStrToArray($popupSlide) as $popkey => $popup)
			{
				if($key == $popkey)
				{
					$array[$key]['popupSlide'] = $popup;
					break;
				}
			}
			foreach($this->cvStrToArray($slideAlign) as $alignkey => $align)
			{
				if($key == $alignkey)
				{
					$array[$key]['slideAlign'] = $align;
					break;
				}
			}
		}
		return $array;
	}
}