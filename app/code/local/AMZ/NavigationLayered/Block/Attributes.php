<?php
class AMZ_NavigationLayered_Block_Attributes extends AMZ_NavigationLayered_Block_Abstract
{
	protected $_name = 'attributes';
	
	protected function _construct() {
		parent::_construct();
	}
	
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
	
    public function _getCollection()
    {				
		$collection = Mage::getModel('navigationlayered/attributes')->getCollection();
		$store = $this->_getStore();
		$collection->addStoreFilter($store)
		->addFieldToFilter('status',1);
		return $collection;
    }
}