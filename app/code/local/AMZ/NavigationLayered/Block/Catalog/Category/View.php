<?php
class AMZ_NavigationLayered_Block_Catalog_Category_View extends Mage_Catalog_Block_Category_View
{
	public function getTitleSeparator($store = null)
    {
        $separator = (string)Mage::getStoreConfig('catalog/seo/title_separator', $store);
        return ' ' . $separator . ' ';
    }
	protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $category = $this->getCurrentCategory();
            if (!$title = $category->getMetaTitle()) {
                $path  = Mage::helper('catalog')->getBreadcrumbPath();
				$i = 0;
				foreach ($path as $name => $breadcrumb) {
					$titles[] = $breadcrumb['label'];
				}
				for($j = count($titles)-1; $j >= 0; $j--)
				{
					$title[$j] =  $titles[$j];
					if($i++ == 1)
						break;
				}
				ksort($title);
				$headBlock->setTitle(join($this->getTitleSeparator(), array_reverse($title)));
            }
        }
        return $this;
    }
    public function getSubCategory($_category)
	{
		$childCat = $_category->getChildrenCategories();
		$html = '';
		$first='subcat_first';
		$i=0;
		$class='';
		$limit = 4;
		if($this->getColumnCount()){
			$limit = $this->getColumnCount();
			if($this->getColumnCount() == 4){
				$class = 'fcs';
			}
		}
		if($limit == 4){
			$class = 'fcs';
		}
		foreach($childCat as $cat)
		{
			$html .= $this->getLayout()->createBlock('navigationlayered/catalog_product_listcat')->setCategory($cat)->toHtml();
		}
		return $html;
	}
	public function getSubCategoryRing($_category)
	{
		$childCat = $_category->getChildrenCategories();
		$html = '';
		$first='subcat_first';
		$i=0;
		foreach($childCat as $cat)
		{
			$collection = Mage::Helper('navigationlayered')->getProductCollectionByCategory($cat);
			$collection->addAttributeToSelect('created_at');
			$collection->getSelect()->order('created_at'); //uncomment to get products in random order      
			$collection->setPageSize(5); 
			if(count($collection) < 1) continue;
			$html .= '<div class="wrapper_subcat '. $first .'">
							<div class="subcat_top">';
			if($i++== -1){
				$html .= '<h1 class="title_subcat">'.$cat->getName().'</h1>';
			}else{
				$html .= '<h2 class="title_subcat">'.$cat->getName().'</h2>';
			}
			$html .= '<a class="right cat_link_top" href="'. $cat->getUrl() .'"><span><span>'. $this->__("Zeige alle") .'</span></span></a>
					</div>';
			if(!empty($collection))
			{
				$html .= '<div class="category-products"> <ol class="products-grid subcat_product ';
				if(count($collection) < 5) $html .= 'last-no-full';
				$html .= '">';
				$html .= $this->getLayout()->createBlock('navigationlayered/catalog_subcategory_product')->setProductsCollection($collection)->setTemplate('amz_navigationlayered/subcat/list_ring.phtml')->toHtml();
				$html .= '</ol></div>';
			}else
			{
				$html .= 'No products exists';
			}
			$html .= '</div>';
			$first = '';
		}
		return $html;
	}
}