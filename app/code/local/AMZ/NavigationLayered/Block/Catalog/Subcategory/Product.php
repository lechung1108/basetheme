<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AMZ_NavigationLayered_Block_Catalog_Subcategory_Product extends Mage_Catalog_Block_Product_List
{
    public function getCatProducts($collection)
	{
		$_helper = $this->helper('catalog/output');
		$html = '';
		$i = 1;
		$total = count($collection);
		foreach ($collection as $_product):
			($i == 1)? $class = 'first' : (($i == $total) ? $class = '' : $class = '');
			$html .= '<li class="item '. $class .'" id="'. $_product->getId() .'">'.'
			<div class="mx_control quickview-button quick_view" id="qv-'. $_product->getId() .'">
				<span class="right_quickview">'. $this->__('Schnellansich') .'</span>
			</div>
			<input type="hidden" id="quickview-content-'. $_product->getId() .'" />
			<a href="'. $_product->getProductUrl() .'" title="'. $this->stripTags($this->getImageLabel($_product, 'small_image'), null, true) .'" class="product-image">
				<img class="lazy" long-desc="'. $this->helper('catalog/image')->init($_product, 'small_image')->resize(158). '" src="'. $this->getSkinUrl('images/lazy_image_loader/loader.gif') .'" alt="'. $this->stripTags($this->getImageLabel($_product, 'small_image'), null, true).'" />
			</a>';
			$html .= '<div class="product_info">
						<h2 class="product-name"><a href="'. $_product->getProductUrl() .'" title="'. $this->stripTags($_product->getName(), null, true) .'">'. $_helper->productAttribute($_product, $_product->getName(), 'name') .'</a></h2>';
			$html .= '<div class="price-box">';
			if($_product->getPrice() == $_product->getFinalPrice())
				$html .= '<span class="">'. $this->helper('core')->currency($_product->getPrice()) . '</span>';
			else
			{
				$percent = round(($_product->getFinalPrice() / $_product->getPrice() * 100) - 100,0);
				$html .='<span class="old-price">'. $this->helper('core')->currency($_product->getPrice()) . '</span>';
				$html .='<span class="special-price">'. $this->helper('core')->currency($_product->getFinalPrice()) . '</span>';
				$html .='<span class="percent_sale">'. $percent . $this->__('%') . '</span>';
				$html .='<div class="is_sale_msg">'. $this->__('Sale') .'</div>';
			}
			if((int)strtotime($_product->getCreatedAt()."+2 month") > (int)Mage::getModel('core/date')->timestamp(time()))
			{
				$html .='<div class="is_new_msg">'.$this->__('Neu!').'</div>';
			}
			
			$html .='</div>';
			//$html .= $this->getPriceHtml($_product, true);
			// if($_product->getRatingSummary()): 
				$html .= $this->getReviewsSummaryHtml($_product, 'short');
            // endif;
			$html .= '<div class="actions">';
			if($_product->isSaleable()):
				//$html .= '<button type="button" title="'.$this->__("Add to Cart") .'" class="button btn-cart" onclick="setLocation(\''. $this->getAddToCartUrl($_product) . '\')"><span><span>'.$this->__('Add to Cart') .'</span></span></button>';
			else: 
				 // $html .= '<p class="availability out-of-stock"><span>'. $this->__('Out of stock') .'</span></p>';  
			 endif; 
				/* $html .= '<ul class="add-to-links">';
				 if ($this->helper('wishlist')->isAllow()) : 
					$html .= '<li><a href="'. $this->helper('wishlist')->getAddUrl($_product) .'" class="link-wishlist">'. $this->__('Add to Wishlist') .'</a></li>';
				 endif; 
				 if($_compareUrl=$this->getAddToCompareUrl($_product)): 
					$html .= '<li><a href="'.$_compareUrl .'" class="link-compare">'. $this->__('Add to Compare') .'</a></li>';
				 endif; 
				$html .= '</ul>'; */
				if($_compareUrl=$this->getAddToCompareUrl($_product)): 
					$html .= '<div class="add-to-compare"><a href="'.$_compareUrl .'" class="link-compare"><span>'. $this->__('In die engere Auswahl') .'</span></a></div>';
				 endif; 
			
			$html .= '</div></div><div class="shadown_product" title="'. $this->stripTags($this->getImageLabel($_product, 'small_image'), null, true) .'" onclick="setLocation(\''. $_product->getProductUrl() .'\');"></div></li>';
			$i++;
		endforeach;
		 /*---------------------------Quick View--------------------------*/
			$javascript = 'quickviewIds.push(';
			$collection->setPage(0,999999);
			foreach($collection->getData() as $product){
				$javascript .= $product['entity_id'].',';
				//echo '<input type="hidden" id="quickview-content-'.$product['entity_id'].'" />';
			}
			$javascript = trim($javascript,',');
			$javascript .= ');';
		$html .= '<script type="text/javascript">
					'.$javascript.'
				</script>';
		/*-------------------------End Quick View------------------------*/
		return $html;
	}
}
?>