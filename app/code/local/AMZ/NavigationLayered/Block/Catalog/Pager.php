<?php
 
class AMZ_NavigationLayered_Block_Catalog_Pager extends Mage_Page_Block_Html_Pager
{
    public function getPagerUrl($params=array())
    {
        return $this->getParentBlock()->getPagerUrl($params);
    }
	 protected function _initFrame()
    {
        if (!$this->isFrameInitialized()) {
			
            $start = 0;
            $end = 0;
            $collection = $this->getCollection();
            if ($collection->getLastPageNumber() <= $this->getFrameLength()) {
                $start = 1;
                $end = $collection->getLastPageNumber();
				if($slideshow = $this->hadSlideShow())
						$end = $slideshow;
            }
            else {
				
                $half = ceil($this->getFrameLength() / 2);
                if ($collection->getCurPage() >= $half && $collection->getCurPage() <= $collection->getLastPageNumber() - $half) {
                    $start  = ($collection->getCurPage() - $half) + 1;
                    $end = ($start + $this->getFrameLength()) - 1;
                }
                elseif ($collection->getCurPage() < $half) {
					
                    $start  = 1;
                    $end = $this->getFrameLength();
                }
                elseif ($collection->getCurPage() > ($collection->getLastPageNumber() - $half)) {
					
                    $end = $collection->getLastPageNumber();
                    $start  = $end - $this->getFrameLength() + 1;
					if($slideshow = $this->hadSlideShow())
						$end = $slideshow;
                }
            }
            $this->_frameStart = $start;
			$this->_frameEnd = $end;
			
            $this->_setFrameInitialized(true);
        }

        return $this;
    }
	public function getLastPageNum()
    {
		if($slideshow = $this->hadSlideShow())
			return $slideshow;
        return $this->getCollection()->getLastPageNumber();
    }
	public function getLastPageUrl()
    {
		if($slideshow = $this->hadSlideShow())
			return $this->getPageUrl($slideshow);
        return $this->getPageUrl($this->getCollection()->getLastPageNumber());
    }
	public function hadSlideShow()
	{
		$_category = Mage::registry('current_category');
			//$_option = Mage::registry('current_option');
		$Slider = Mage::registry('slider_custom');
		//print_r($Slider);die;
		$ItemsOnRows = Mage::getStoreConfig('navigationlayered/slider/itemonrow');//5;
		$limit = $this->getLimit();
		($limit % $ItemsOnRows == 0)? $rowsOnPage = ceil($limit/$ItemsOnRows) : $rowsOnPage = (int)($limit/$ItemsOnRows) + 1; // $total  rows display in one page.
		if($_category && $Slider)
		{
			$totalNumProduct = 0;
			foreach($Slider as $slide)
			{
				$totalNumProduct = $totalNumProduct + (int)$slide['productCleared'];
			}
			$totalNum = $this->getTotalNum();
			$totalNum = $totalNum + $totalNumProduct; //30/9 3 
			($totalNum % $limit == 0) ? $lastpage = (int)($totalNum / $limit) : $lastpage = (int)($totalNum / $limit) + 1;
			return $lastpage;
		}
		return '';
	}
}