<?php
if ('true' == (string)Mage::getConfig()->getNode('modules/Amasty_Sorting/active')){
    class AMZ_NavigationLayered_Block_Catalog_Product_List_Abstract extends Amasty_Sorting_Block_Catalog_Product_List_Toolbar {}
} 
else {
    class AMZ_NavigationLayered_Block_Catalog_Product_List_Abstract extends Mage_Catalog_Block_Product_List_Toolbar {}
}