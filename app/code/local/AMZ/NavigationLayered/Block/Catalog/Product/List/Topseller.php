<?php
class AMZ_NavigationLayered_Block_Catalog_Product_List_Topseller extends Mage_Catalog_Block_Product_Abstract
{
	protected $_productCollection;
	
	protected function _construct()
    {
        parent::_construct();

        $this->_initBlocks();
    }

    /**
     * Initialize blocks names
     */
    protected function _initBlocks()
    {
        $this->_stateBlockName              = 'catalog/layer_state';
        $this->_categoryBlockName           = 'catalog/layer_filter_category';
        $this->_attributeFilterBlockName    = 'catalog/layer_filter_attribute';
        $this->_priceFilterBlockName        = 'catalog/layer_filter_price';
        $this->_decimalFilterBlockName      = 'catalog/layer_filter_decimal';
    }    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
			$filterableAttributes = $this->_getFilterableAttributes();
			foreach ($filterableAttributes as $attribute) {
				if ($attribute->getAttributeCode() == 'price') {
					$filterBlockName = $this->_priceFilterBlockName;
				} elseif ($attribute->getBackendType() == 'decimal') {
					$filterBlockName = $this->_decimalFilterBlockName;
				} else {
					$filterBlockName = $this->_attributeFilterBlockName;
				}
				$this->getLayout()->createBlock($filterBlockName)
						->setLayer($layer)
						->setAttributeModel($attribute)
						->init();
			}
			$layer->apply();
            $this->_productCollection = $layer->getProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());
			
            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        } 
        return $this->_productCollection;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
	protected function _getFilterableAttributes()
    {
        $attributes = $this->getData('_filterable_attributes');
        if (is_null($attributes)) {
            $attributes = $this->getLayer()->getFilterableAttributes();
            $this->setData('_filterable_attributes', $attributes);
        }

        return $attributes;
    }
    public function getLayer()
    {
        return Mage::getModel('catalog/layer');
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection()
    {
		$limit = 16;
		if($this->getLimitProduct())
		$limit = $this->getLimitProduct();
		$collection = $this->_getProductCollection();
		if($collection ->getSize() > 100 && (!$this->getRequest()->getParam('p') || $this->getRequest()->getParam('p') < 2))
		{
			Mage::getSingleton('amsorting/method_bestselling')->apply($collection, 'desc');
			$collection->getSelect()->limit($limit);
			return $collection ;
		}
		return array();
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
	protected function _getConfig()
    {
        return Mage::getSingleton('catalog/config');
    }

    /**
     * Prepare Sort By fields from Category Data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Block_Product_List
     */
    public function prepareSortableFieldsByCategory($category) {
        if (!$this->getAvailableOrders()) {
            $this->setAvailableOrders($category->getAvailableSortByOptions());
        }
        $availableOrders = $this->getAvailableOrders();
        if (!$this->getSortBy()) {
            if ($categorySortBy = $category->getDefaultSortBy()) {
                if (!$availableOrders) {
                    $availableOrders = $this->_getConfig()->getAttributeUsedForSortByArray();
                }
                if (isset($availableOrders[$categorySortBy])) {
                    $this->setSortBy($categorySortBy);
                }
            }
        }

        return $this;
    }
	
}