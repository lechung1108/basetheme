<?php
class AMZ_NavigationLayered_Block_Catalog_Product_List_Toolbar extends AMZ_NavigationLayered_Block_Catalog_Product_List_Abstract
{
	public function setCollection($collection)
    {
		parent::setCollection($collection);
		$this->_collection->addAttributeToSelect('created_at');
		$limit = (int)$this->getLimit();
		$this->_collection->setCurPage($this->getCurrentPage());
		$_category = Mage::registry('current_category');
		$ItemsOnRows = Mage::getStoreConfig('navigationlayered/slider/itemonrow');//5;
		$currentPage 	  = $this->getCurrentPage(); 
		$Slider = Mage::registry('slider_custom');
		if ($limit) {
			if($Slider)
			{
				// $total  rows display in one page.
				($limit % $ItemsOnRows == 0)? $rowsOnPage = ceil($limit/$ItemsOnRows) : $rowsOnPage = (int)($limit/$ItemsOnRows) + 1;
				
				$Slider = Mage::registry('slider_custom'); // aray slide
				foreach($Slider as $key => $slide)
				{
					($slide['lineDisplay'] % $rowsOnPage == 0) ? $ShowOnPage = (int) ($slide['lineDisplay'] / $rowsOnPage)  : $ShowOnPage = (int) ($slide['lineDisplay'] / $rowsOnPage) + 1 ;
					$slide['pageDisplay'] = $ShowOnPage;
					($slide['lineDisplay'] % $rowsOnPage == 0) ? $slide['lineDisplay'] = $rowsOnPage : $slide['lineDisplay'] = ($slide['lineDisplay'] % $rowsOnPage) ;
					$pageSlide[$ShowOnPage][$slide['lineDisplay']] = $slide;
				}
				//$lineDisplayOnPage = $lineDisplaySlide % $rowsOnPage;
				$totalItemBefore = $totalItemsCurrent = 0;
				foreach($pageSlide as $key => $item)
				{
					if($key < $currentPage)
					{
						foreach($item as $slide)
						{
							$totalItemBefore = $totalItemBefore + (int)$slide['productCleared'];
						}
					}
					if($key == $currentPage)
					{
						foreach($item as $slide)
						{
							$totalItemsCurrent = $totalItemsCurrent + (int)$slide['productCleared'];
						}
					}
				}
				
				if(!Mage::registry('had_slider') && isset($pageSlide[$currentPage])) Mage::register('had_slider', $pageSlide[$currentPage]);
					$this->_collection->getSelect()->limit($limit - $totalItemsCurrent, $limit * ($currentPage - 1) - $totalItemBefore);
				
			}
			else
			{
				$this->_collection->setPageSize($limit);
			}
		}
		
        if ($this->getCurrentOrder()) {
            //$this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
        }
        return $this;
    }
	public function getLimit()
    {
        $limit = $this->_getData('_current_limit');
		if ($limit) {
            return $limit;
        }

        $limits = $this->getAvailableLimit();
        $defaultLimit = $this->getDefaultPerPageValue();
        if (!$defaultLimit || !isset($limits[$defaultLimit])) {
            $keys = array_keys($limits);
            $defaultLimit = $keys[0];
        }

        $limit = $this->getRequest()->getParam($this->getLimitVarName());
        if ($limit && isset($limits[$limit])) {
            if ($limit == $defaultLimit) {
                Mage::getSingleton('catalog/session')->unsLimitPage();
            } else {
                $this->_memorizeParam('limit_page', $limit);
            }
        } else {
            $limit = Mage::getSingleton('catalog/session')->getLimitPage();
        }
        if (!$limit || !isset($limits[$limit])) {
            $limit = $defaultLimit;
        }

        $this->setData('_current_limit', $limit);
        return $limit;
    }
    public function getPagerUrl($params=array())
    {
        if ($this->skip())
            return parent::getPagerUrl($params);
            
        return Mage::helper('navigationlayered/url')->getFullUrl($params);
    } 
    
    public function getPagerHtml()
    {
        if ($this->skip())
            return parent::getPagerHtml();
            
        $alias = 'product_list_toolbar_pager';
        $oldPager   = $this->getChild($alias);
        if ($oldPager instanceof Varien_Object){
            $newPager = $this->getLayout()->createBlock('navigationlayered/catalog_pager')
                ->setArea('frontend')
                ->setTemplate($oldPager->getTemplate());
           //@todo assign other params if needed     
                
            $newPager->assign('_type', 'html')
                     ->assign('_section', 'body');
                     
            $this->setChild($alias, $newPager);
        }
        return parent::getPagerHtml();
    }
    
    private function skip()
    {
        $r = Mage::app()->getRequest();
        // todo add more conditions if needed   
        if (in_array($r->getModuleName(), array('supermenu', 'supermenuadmin', 'catalogsearch','tag', 'catalogsale','catalognew')))
            return true;
            
        return false;
    }
	
}