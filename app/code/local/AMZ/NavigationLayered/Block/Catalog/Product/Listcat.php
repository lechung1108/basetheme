<?php
 
class AMZ_NavigationLayered_Block_Catalog_Product_Listcat extends Mage_Catalog_Block_Product_List
{
    protected $_filterBlocks = null;
    public static $_filters = null;
	 /**
     * State block name
     *
     * @var string
     */
    protected $_stateBlockName;
    protected $_layer;

    /**
     * Category Block Name
     *
     * @var string
     */
    protected $_categoryBlockName;

    /**
     * Attribute Filter Block Name
     *
     * @var string
     */
    protected $_attributeFilterBlockName;

    /**
     * Price Filter Block Name
     *
     * @var string
     */
    protected $_priceFilterBlockName;

    /**
     * Decimal Filter Block Name
     *
     * @var string
     */
    protected $_decimalFilterBlockName;

    /**
     * Internal constructor
     */

    /**
     * Initialize blocks names
     */  
    protected function _isCacheActive()
    {
		/* if there are any messages dont read from cache to show them */
        if (Mage::getSingleton('core/session')->getMessages(true)->count() > 0) {
            return false;
        }
        return true;

    }

    public function getCacheLifetime()
    {
        if ($this->_isCacheActive()) {
            return false;
        }
    }

    public function getCacheKey()
    {
        if (!$this->_isCacheActive()) {
            parent::getCacheKey();
        }
		if(!$this->getCategory())
			$this->_category = Mage::getSingleton('catalog/layer')->getCurrentCategory();
		else
			$this->_category = $this->getCategory();
        $_page = $this->getPage();

        $toolbar = new Mage_Catalog_Block_Product_List_Toolbar();
		$isHttps = '';
		if(Mage::getModel('core/store')->isCurrentlySecure())
			$isHttps = '_https';
        $cacheKey = 'ProductList_'.
			/* Cat for http and https */
			$isHttps.
            /* Create different caches for different categories */
            $this->_category->getId().'_'.
            /* ... orders */
            $toolbar->getCurrentOrder().'_'.
            /* ... direction */
            $toolbar->getCurrentDirection().'_'.
            /* ... mode */
            $toolbar->getCurrentMode().'_'.
            /* ... page */
            $toolbar->getCurrentPage().'_'.
            /* ... items per page */
            $toolbar->getLimit().'_'.
            /* ... stores */
            Mage::App()->getStore()->getCode().'_'.
            /* ... currency */
            Mage::App()->getStore()->getCurrentCurrencyCode().'_'.
            /* ... customer groups */
           /*  $_customer->getGroupId().'_'.
            $_taxRateRequest->getCountryId()."_".
            $_taxRateRequest->getRegionId()."_".
            $_taxRateRequest->getPostcode()."_".
            $_taxRateRequest->getCustomerClassId()."_". */
            /* ... tags */
            Mage::registry('current_tag').'_'.
            '';
        /* ... layern navigation + search */
        foreach (Mage::app()->getRequest()->getParams() as $key=>$value) {
			if(!is_string($value) && !is_array($value))
				continue;
			if(!is_string($value))
				$value = implode('_',$value);
            $cacheKey .= $key.'-'.$value.'_';
        }
        return $cacheKey;
    }


    public function getCacheTags()
    {
        if (!$this->_isCacheActive()) {
            return parent::getCacheTags();
        }
        $cacheTags = array(
            Mage_Catalog_Model_Category::CACHE_TAG,
            Mage_Catalog_Model_Category::CACHE_TAG.'_'.$this->_category->getId()
        );
        foreach ($this->_getProductCollection() as $_product) {
            $cacheTags[] = Mage_Catalog_Model_Product::CACHE_TAG."_".$_product->getId();
        }
        return $cacheTags;

    }
    protected function _initBlocks()
    {
        $this->_stateBlockName              = 'catalog/layer_state';
        $this->_categoryBlockName           = 'catalog/layer_filter_category';
        $this->_attributeFilterBlockName    = 'catalog/layer_filter_attribute';
        $this->_priceFilterBlockName        = 'catalog/layer_filter_price';
        $this->_decimalFilterBlockName      = 'catalog/layer_filter_decimal';
    }
    protected function _construct()
    {
		parent::_construct();
		$this->setTemplate('amz_navigationlayered/catalog/product/list.phtml');
		$this->_initBlocks();
		$this->_productCollection = null;
        $this->addData(array(
            'cache_lifetime'    => false,
            'cache_tags'        => array(
        	    Mage_Catalog_Model_Product::CACHE_TAG,
        	    Mage_Catalog_Model_Category::CACHE_TAG,
        	    Mage_Cms_Model_Block::CACHE_TAG, 
                Mage_Catalog_Model_Category::CACHE_TAG, 
                Mage_Core_Model_Store_Group::CACHE_TAG
            ),
        ));
    }
    
    /**
     * Retrieve Key for caching block content
     *
     * @return string
     */
	public function generateBlockName($array = array())
    {
        $name = implode('_', $array);
        while(in_array($name, array_keys($this->getLayout()->getAllBlocks()))) {
            $name .= rand(1,9);
        }        
        return $name; 
    }
	protected function _getFilterableAttributes()
    {
        $attributes = $this->getData('_filterable_attributes');
        if (is_null($attributes)) {
            $attributes = $this->getLayer()->getFilterableAttributes();
            $this->setData('_filterable_attributes', $attributes);
        }

        return $attributes;
    }
	public function getFilters()
    {
        $filters = array();
        if ($categoryFilter = $this->_getCategoryFilter()) {
            $filters[] = $categoryFilter;
        }

        $filterableAttributes = $this->_getFilterableAttributes();
        foreach ($filterableAttributes as $attribute) {
            $filters[] = $this->getChild($attribute->getAttributeCode() . '_filter');
        }
		$attrIds = array();

        // option ids
        $ids = array();
        $category = Mage::registry('current_category');
		$catId = $this->getCategory()->getId();
        foreach ($filters as $f){
            if ($f->getItemsCount() && ($f instanceof Mage_Catalog_Block_Layer_Filter_Attribute)){
                $items = $f->getItems();
                foreach ($items as $item){  
                    $vals =  explode(',', $item->getValue() ? $item->getValue() : $item->getOptionId());
                    foreach ($vals as $v) 
                        $ids[] = $v;
                }
                $attrIds[] = $items[0]->getFilter()->getAttributeModel()->getId();
            }
        }
        
        $attrCollection = Mage::getModel('navigationlayered/attributes')
			->getCollection()
			->addStoreFilter()
			->getByAttributeInArray($attrIds)
			->getSettingWithCatCollection($catId);
         //echo $attrCollection->getSelect();die;        
        $attributes = array();
        foreach ($attrCollection as $row){
            $attributes[$row->getAttributeId()] = $row;
        }
        
        // images of filter values 
        $optionsCollection = Mage::getModel('navigationlayered/attributesvalue')
								->getCollection()
								->addStoreFilter()
								->getByOptionInArray($ids)
								->getSettingWithCatCollection($catId); 	
		// echo $optionsCollection->getSelect();die;
        foreach ($optionsCollection as $row){
            $options[$row->getId()] = array('img'=>$row->getNavigationIcon(), 'descr'=>$row->getDescription(), 'option'=>$row);
        }
		foreach ($filters as $f){
            if ($f->getItemsCount() && $f instanceof Mage_Catalog_Block_Layer_Filter_Attribute){
                $items = $f->getItems();
               
                //add selected and image properties for all items              
                $attributeCode = $items[0]->getFilter()->getAttributeModel()->getAttributeCode();
                $selectedValues = Mage::app()->getRequest()->getParam($attributeCode);
                if ($selectedValues){
                    $selectedValues = explode(',', $selectedValues);
                    $f->setHasSelection(true);
                }
                else {
                    $selectedValues = array();
                }
                    
                foreach ($items as $item){ 
                    $optId = $item->getOptionId();
                    if (!empty($options[$optId]['img'])){
                        $item->setImage($options[$optId]['img']);
                    }
                    if (!empty($options[$optId]['descr'])){
                        $item->setDescr($options[$optId]['descr']);
                    }
                    if(!empty($options[$optId]['option']))
					{
						 $item->setOption($options[$optId]['option']);
					}
                    
                    $item->setIsSelected(in_array($optId, $selectedValues));
                }    
				$attributeId  = $items[0]->getFilter()->getAttributeModel()->getId();
                if (isset($attributes[$attributeId])){
                    $a = $attributes[$attributeId];
                    $f->setDisplayType($a->getDisplayType());
                    $f->setScrollHeight($a->getScrollHeight());
					$f->setFrontendLabel($a->getFrontendLabel());
                    $f->setMultipleSelections($a->getMultipleSelections());
					$f->setAttributeId($attributeId);
					//if (Mage::getStoreConfig('navigationlayered/general/enable_collapsing')){  $f->setCollapsed($a->getCollapse());                    }
					$f->setShowAboveToolbar($a->getShowAboveToolbar());
					$f->setCollapsed($a->getCollapse());
					$f->setFilterVisible(true);
                    $f->setSingleChoice($a->getSingleChoice());
                }
			}
		}
        return $filters;
    }
	public function getLayer()
	{
		return $this->getData('layer');
	}
	 protected function _getCategoryFilter()
    {
        return $this->getChild('category_filter');
    }
	protected function _getPriceFilter()
    {
        return $this->getChild('_price_filter');
    }
	protected function _getProductCollection()
    {
		$layer = Mage::getModel('catalog/layer');
		$category = $this->getCategory();
		$layer->setCurrentCategory($category->getId());
		$stateBlock = $this->getLayout()->createBlock($this->_stateBlockName)
			->setLayer($layer);

		/* $categoryBlock = $this->getLayout()->createBlock($this->_categoryBlockName)
			->setLayer($layer)
			->init(); */
		$this->setData('layer', $layer);
		$this->setChild('layer_state', $stateBlock);
		$this->setChild('category_filter', $categoryBlock);

		$filterableAttributes = $this->_getFilterableAttributes();
		foreach ($filterableAttributes as $attribute) {
			if ($attribute->getAttributeCode() == 'price') {
				$filterBlockName = $this->_priceFilterBlockName;
			} elseif ($attribute->getBackendType() == 'decimal') {
				$filterBlockName = $this->_decimalFilterBlockName;
			} else {
				$filterBlockName = $this->_attributeFilterBlockName;
			}

			$this->setChild($attribute->getAttributeCode() . '_filter',
				$this->getLayout()->createBlock($filterBlockName)
					->setLayer($layer)
					->setAttributeModel($attribute)
					->init());
		}
		/* $category = $this->getCategory();
		$layer->setCurrentCategory($category); */
		$layer->apply();
		//echo $layer->getProductCollection()->getSelect();die;
        return $layer->getProductCollection();
    }
	
	public function getAttributeIdBrand()
	{
		$BrandAtts 	= Mage::getModel('navigationlayered/brands')->getCollection();
		return $BrandAtts->getFirstItem()->getAttributeId();
	}
}