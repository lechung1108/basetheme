<?php
class AMZ_NavigationLayered_Block_Catalog_Product_View_Attributes extends Mage_Catalog_Block_Product_View_Attributes
{
	protected function _construct()
    {
		parent::_construct();
        $this->addData(array(
            'cache_lifetime'    => false,
            'cache_tags'        => array(
        	    Mage_Catalog_Model_Product::CACHE_TAG,
        	    Mage_Catalog_Model_Category::CACHE_TAG,
        	    Mage_Cms_Model_Block::CACHE_TAG, 
                Mage_Catalog_Model_Category::CACHE_TAG, 
                Mage_Core_Model_Store_Group::CACHE_TAG
            ),
        ));
    }
	public function getCacheKey()
    {
		$storeId = Mage::app()->getStore()->getId();
		$product = $this->getRequest()->getParam('id');
		$isHttps = '';
		if(Mage::getModel('core/store')->isCurrentlySecure())
			$isHttps = 'https_';
        return md5($isHttps . 'catalog_product_view_id_' . $product . '_' . $storeId);
    }
	protected function _prepareLayout()
	{
		
		if(!Mage::registry('amz_options_data'))
		{	
			$product = Mage::registry('product');
			$optionIds = array();
			foreach($product->getAttributes() as $attribute)
			{
				if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), array())) {
					$optionIds[] = $product->getData($attribute->getAttributeCode());
				}
				
			}
			$AttributeModel = Mage::getModel('navigationlayered/attributesvalue')->getCollection()
							->addStoreFilter()
							->getByOptionInArray($optionIds)
							->getJoinBrands()
							->getJoinAttributes();
			Mage::register('amz_options_data', $AttributeModel);
		}
		return parent::_prepareLayout();
	}
    public function getAdditionalData(array $excludeAttr = array())
    {	 
        $data = parent::getAdditionalData($excludeAttr);

		$product = $this->getProduct();
		
		$AttributeModel =  Mage::registry('amz_options_data');
		
		$hash = array();
		foreach($AttributeModel as $attribute)
		{
			$hash[$attribute->getAttributeCode()] = $attribute;
		}
		foreach($data as $key => $value)
		{
			if(isset($hash[$value['code']]))
			{
				$value['attribute'] = $hash[$value['code']];
			}
			$data[$key] = $value;
		}
		return $data;
	}
	    
}
