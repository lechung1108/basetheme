<?php
class AMZ_NavigationLayered_Block_Catalog_Product_Head extends Mage_Catalog_Block_Product_View
{
   protected function _prepareLayout()
    {	
		$head = $this->getLayout()->getBlock('head');
        if ($head && Mage::registry('product')){
			if($this->helper('core/url')->getCurrentUrl() != $this->getProductUrl(Mage::registry('product')))
				$head->setRobots('NOINDEX,FOLLOW');
		}
		if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
			$breadcrumbsBlock->setTemplate('amz_navigationlayered/catalog/product/breadcrumbs.phtml');
		}
		return parent::_prepareLayout();
	}
}