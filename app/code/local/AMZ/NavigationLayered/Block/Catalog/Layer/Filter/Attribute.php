<?php

class AMZ_NavigationLayered_Block_Catalog_Layer_Filter_Attribute extends Mage_Catalog_Block_Layer_Filter_Attribute
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amz_navigationlayered/attribute.phtml');
    }
    private $urlRemovesAttribue = array();
    public function getItemsAsArray()
    {
        $items = array(); 
		$catId = $this->getLayer()->getCurrentCategory()->getId();
        foreach (parent::getItems() as $itemObject){
			if($this->getHasSelection() && $itemObject->getIsSelected())
				continue;
            $item = array();
            $item['url']   = $this->htmlEscape($itemObject->getUrl());
            if (!$this->getMultipleSelections()){ /** sinse @version 1.3.0 */
                $query = array(
                    $this->getRequestValue() => $itemObject->getIsSelected() ? null : $itemObject->getOptionId(),
                    Mage::getBlockSingleton('page/html_pager')->getPageVarName() => null 
                ); 
                $item['url'] = Mage::helper('navigationlayered/url')->getFullUrl($query);            
            }
			//$item['url'] = Mage::helper('navigationlayered')->removeCatShowAll($item['url']);// = str_replace('?cat=showall','', $item['url']);
			//$item['url'] = str_replace('&cat=showall','', $item['url']);
            $item['label'] = $itemObject->getLabel();
            $item['descr'] = $itemObject->getDescr();
			$item['option'] = $itemObject->getOption();
            $item['count'] = '';
            if (!$this->getHideCounts())
                $item['count']  = ' (' . $itemObject->getCount() . ')';
            
            $item['image'] = '';
			//$item['image'] = Mage::getBaseUrl('media') . 'attributes/small405.png';
			if ($itemObject->getImage()){
                $item['image'] = Mage::getBaseUrl('media') . 'attributes/' . $itemObject->getImage();
            }

            $item['css'] = 'navigationlayered-attr';
            if ($itemObject->getIsSelected()){
				$item['selected'] = true;
                $item['css'] .= '-selected';
                if (3 == $this->getDisplayType()) //dropdown
                    $item['css'] = 'selected';
            }
            if( Mage::getStoreConfig('navigationlayered/general/show_all_filter'))
				$items[$itemObject->getOptionId()] = $item;
			else
				$items[$itemObject->getOptionId()] = $item;
        }
         if( Mage::getStoreConfig('navigationlayered/general/show_all_filter') && $this->getAttributeId())
		 {
			$optionsCollection = Mage::getModel('navigationlayered/attributesvalue')
								->getCollection()
								->addStoreFilter()
								->getByAttributeId($this->getAttributeId())
								->getSettingWithCatCollection($catId);
			foreach($optionsCollection as $option)
			{
				if(!isset($items[$option->getOptionId()]))
				{
					 $query = array(
							$this->getRequestValue() => $itemObject->getIsSelected() ? null : $option->getOptionId(),
							Mage::getBlockSingleton('page/html_pager')->getPageVarName() => null 
						); 
					$op['url'] = Mage::helper('navigationlayered/url')->getFullUrl($query);
					$op['url'] = Mage::helper('navigationlayered')->removeCatShowAll($op['url']);
					$op['label'] = $option->getTitle();
					$op['descr'] = $option->getDescription();
					$op['option'] = $option;
					$op['count'] = '';
					$op['image'] = '';
					$op['disable'] = true;
					$op['css'] = 'navigationlayered-attr disable';
					$items[$option->getOptionId()]= $op;
				}
			}
		 }
        $sortBy = $this->getSortBy();
        $functions = array(1 => '_sortByName', 2 => '_sortByCounts');
        if (isset($functions[$sortBy])){
            usort($items, array($this, $functions[$sortBy]));
        }
        
        // add less/more
        $max = $this->getMaxOptions();
        $i   = 0;
        foreach ($items as $k=>$item){
            $style = '';
            if ($max && (++$i > $max)){
                $style = ' class="hiddenopt navigationlayered-attr-' . $this->getRequestValue() . '"'; 
            }
            $items[$k]['style'] = $style;
        }
        $this->setShowLessMore($max && ($i > $max));
        return $items;
    }
    
    public function _sortByName($a, $b)
    {
        return strcmp($a['label'], $b['label']);
    }
    
    public function _sortByCounts($a, $b)
    {
        if (empty($a['count']))
            return false;

        return ((int)substr($a['count'], 2, -1) < (int)substr($b['count'], 2, -1));
    }
    
    public function getRequestValue()
    {
        return $this->_filter->getAttributeModel()->getAttributeCode();
    }
    
    public function getItemsCount()
     {
        $cnt     = parent::getItemsCount();
        $showAll = !Mage::getStoreConfig('navigationlayered/general/hide_one_value'); 
        return ($cnt > 1 || $showAll) ? $cnt : 0;
     }
     
    public function getRemoveUrl()
    {
        $query = array(
            $this->getRequestValue() => null,
            Mage::getBlockSingleton('page/html_pager')->getPageVarName() => null // exclude current page from urls
        );
        
        $url = Mage::helper('navigationlayered/url')->getFullUrl($query);
        return $url;        
    }
}