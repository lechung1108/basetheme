<?php
 
class AMZ_NavigationLayered_Block_Catalog_Layer_View extends AMZ_NavigationLayered_Block_Catalog_Layer_Abstract
{
    protected $_filterBlocks = null;
    public static $_filters = null;
	
    protected function _construct()
    {
		parent::_construct();
        $this->addData(array(
            'cache_lifetime'    => false,
            'cache_tags'        => array(
        	    Mage_Catalog_Model_Product::CACHE_TAG,
        	    Mage_Catalog_Model_Category::CACHE_TAG,
        	    Mage_Cms_Model_Block::CACHE_TAG, 
                Mage_Catalog_Model_Category::CACHE_TAG, 
                Mage_Core_Model_Store_Group::CACHE_TAG
            ),
        ));
    }
    
    /**
     * Retrieve Key for caching block content
     *
     * @return string
     */
    public function getCacheKey()
    {
        return $this->generateBlockName(array('name', 'block', md5($this->helper('core/url')->getCurrentUrl())))
        	. '_' . Mage::app()->getStore()->getId()
            /* . '_' . Mage::getSingleton('customer/session')->getCustomerGroupId() */
            . '_' . md5($this->getTemplate());
    }
	public function generateBlockName($array = array())
    {
        $name = implode('_', $array);
        while(in_array($name, array_keys($this->getLayout()->getAllBlocks()))) {
            $name .= rand(1,9);
        }        
        return $name; 
    }
    public function getFilters()
    {
        if (!is_null($this->_filterBlocks)){
            return $this->_filterBlocks;
        }
        
        //attributes ids
        $attrIds = array();

        // option ids
        $ids = array();
        $category = Mage::registry('current_category');
		$catId = $this->getLayer()->getCurrentCategory()->getId();
        $filters = parent::getFilters();
        
        // remove some filtes for the home page
        $exclude = Mage::getStoreConfig('navigationlayered/general/exclude');
        if ('/' == Mage::app()->getRequest()->getRequestString() && $exclude){
            $exclude = explode(',', preg_replace('/[^a-zA-Z0-9_\-,]+/','', $exclude));
            $filters = $this->excludeFilters($filters, $exclude);
        }
        $filters[] = $this->getChild('layer_discount');
        foreach ($filters as $f){
            if ($f->getItemsCount() && ($f instanceof Mage_Catalog_Block_Layer_Filter_Attribute)){
                $items = $f->getItems();
                foreach ($items as $item){  
                    $vals =  explode(',', $item->getValue() ? $item->getValue() : $item->getOptionId());
                    foreach ($vals as $v) 
                        $ids[] = $v;
                }
                $attrIds[] = $items[0]->getFilter()->getAttributeModel()->getId();
            }
        }
        
        //additional filter properties
		($category)? $idCat = $category->getId() : $idCat = '2';
        $attrCollection = Mage::getModel('navigationlayered/attributes')
			->getCollection()
			->addStoreFilter()
			->getByAttributeInArray($attrIds)
			->getSettingWithCatCollection($catId);
         //echo $attrCollection->getSelect();die;        
        $attributes = array();
        foreach ($attrCollection as $row){
            $attributes[$row->getAttributeId()] = $row;
        }
        
        // images of filter values 
        $optionsCollection = Mage::getModel('navigationlayered/attributesvalue')
								->getCollection()
								->addStoreFilter()
								->getByOptionInArray($ids)
								->getSettingWithCatCollection($catId); 	
		// echo $optionsCollection->getSelect();die;
        foreach ($optionsCollection as $row){
            $options[$row->getId()] = array('img'=>$row->getNavigationIcon(), 'descr'=>$row->getDescription(), 'option'=>$row);
        }
		
        
        $exclude = array();
        
        // update filters with new properties
		if($brandId = Mage::registry('current_brand'))
		{
			$BrandAtts 	= Mage::getModel('navigationlayered/brandsattributes')
						->getCollection()
						->addFieldToFilter('main_table.brand_id',$brandId->getOptionId());
			$BrandSetAtt = array();
			foreach ($BrandAtts as $brand)
			{
				$BrandSetAtt[$brand->getAttributeId()] = $brand;
			}
		}
        foreach ($filters as $f){
            if ($f->getItemsCount() && $f instanceof Mage_Catalog_Block_Layer_Filter_Attribute){
                $items = $f->getItems();
               
                //add selected and image properties for all items              
                $attributeCode = $items[0]->getFilter()->getAttributeModel()->getAttributeCode();
                $selectedValues = Mage::app()->getRequest()->getParam($attributeCode);
                if ($selectedValues){
                    $selectedValues = explode(',', $selectedValues);
                    $f->setHasSelection(true);
                }
                else {
                    $selectedValues = array();
                }
                    
                foreach ($items as $item){ 
                    $optId = $item->getOptionId();
                    if (!empty($options[$optId]['img'])){
                        $item->setImage($options[$optId]['img']);
                    }
                    if (!empty($options[$optId]['descr'])){
                        $item->setDescr($options[$optId]['descr']);
                    }
                    if(!empty($options[$optId]['option']))
					{
						 $item->setOption($options[$optId]['option']);
					}
                    
                    $item->setIsSelected(in_array($optId, $selectedValues));
                }                    
                
                $attributeId  = $items[0]->getFilter()->getAttributeModel()->getId();
                if (isset($attributes[$attributeId])){
                    $a = $attributes[$attributeId];
                    $f->setDisplayType($a->getDisplayType());
                    $f->setScrollHeight($a->getScrollHeight());
					$f->setFrontendLabel($a->getFrontendLabel());
                    $f->setMultipleSelections($a->getMultipleSelections());
					$f->setAttributeId($attributeId);
					//if (Mage::getStoreConfig('navigationlayered/general/enable_collapsing')){  $f->setCollapsed($a->getCollapse());                    }
					$f->setShowAboveToolbar($a->getShowAboveToolbar());
					$f->setCollapsed($a->getCollapse());
					if($catId == 2)
					{
						$f->setFilterVisible(true); 
						if(isset($BrandSetAtt[$attributeId]))
						{
							$att = $BrandSetAtt[$attributeId];
						}
						else
						{
							if($brandId && $a->getAttributeId() == $brandId->getAttributeId())
							{
								$f->setIsBrandPage(true);
							}
						}
					}
					else
					{
						if($brandId && $a->getAttributeId() == $brandId->getAttributeId())
						{
							$f->setIsBrandPage(true);
						}
						$f->setFilterVisible($a->getFilterVisible());
					}
                    $f->setSingleChoice($a->getSingleChoice());
                    
                    $cats = trim($a->getExcludeFrom());
                    if ($cats){
                        if (in_array($catId, explode(',', $cats))){
                            $exclude[] = $attributeCode;
                        }
                    }
                } //if attibute
                
            }// if count items and attribute
            elseif ($f instanceof Mage_Catalog_Block_Layer_Filter_Category){
                $f->setDisplayType(Mage::getStoreConfig('navigationlayered/general/categories_type'));
                $f->setScrollHeight(Mage::getStoreConfig('navigationlayered/general/categories_scroll'));
                $f->setTemplate('amz_navigationlayered/category.phtml'); 
                $f->setHasSelection($catId != $this->getLayer()->getCurrentStore()->getRootCategoryId());
				$f->setFilterVisible(true);
                if (Mage::getStoreConfig('navigationlayered/general/enable_collapsing'))
                    $f->setCollapsed(Mage::getStoreConfig('navigationlayered/general/categories_collapsed'));
            }
            elseif ($f instanceof Mage_Catalog_Block_Layer_Filter_Price){
                $f->setDisplayType(Mage::getStoreConfig('navigationlayered/general/price_type'));
                $f->setScrollHeight(Mage::getStoreConfig('navigationlayered/general/price_scroll'));
                $f->setTemplate('amz_navigationlayered/price.phtml');
                $f->setHasSelection(Mage::app()->getRequest()->getParam('price'));
				$f->setFilterVisible(true);
                if (Mage::getStoreConfig('navigationlayered/general/enable_collapsing'))
                    $f->setCollapsed(Mage::getStoreConfig('navigationlayered/general/price_collapsed'));
            }
			elseif ($f instanceof AMZ_NavigationLayered_Block_Catalog_Layer_Filter_Discount){
				$f->setDisplayType(Mage::getStoreConfig('navigationlayered/general/price_type'));
                $f->setScrollHeight(Mage::getStoreConfig('navigationlayered/general/price_scroll'));
				$f->setHasSelection(Mage::app()->getRequest()->getParam('discount'));
				$f->setFilterVisible(true);
                if (Mage::getStoreConfig('navigationlayered/general/enable_collapsing'))
                    $f->setCollapsed(Mage::getStoreConfig('navigationlayered/general/price_collapsed'));
            }
        }
        // print_r($abc);die;
        // 1.2.7 exclude some filters from the selected categories
        $filters = $this->excludeFilters($filters, $exclude);
        $this->_filterBlocks = $filters;
		self::$_filters = $filters;
        return $filters;       
    }
    
    public function getStateHtml()
    {
        $this->getChild('layer_state')->setTemplate('amz_navigationlayered/state.phtml');
        return $this->getChildHtml('layer_state');
    }   
    
    protected function excludeFilters($filters, $exclude)
    {
        $new = array();
        foreach ($filters as $f){
            $code = substr($f->getData('type'), 1+strrpos($f->getData('type'), '_'));
            if ($f->getAttributeModel()){
                $code = $f->getAttributeModel()->getAttributeCode();
            }
            
            if (in_array($code, $exclude)){
                 continue;
            } 
             
            $new[] = $f;          
        }
        return $new;
    }
    
    protected function _afterToHtml($html)
    {
        $html = parent::_afterToHtml($html);
        
        //to make js and css work for 1.3 also
        $html = str_replace('narrow-by', 'narrow-by block-layered-nav', $html);
        
        $collapsing = intval(Mage::getStoreConfig('navigationlayered/general/enable_collapsing'));
        $html .= '<script type="text/javascript">navigationlayered_start('.$collapsing.')</script>';
        
        // we don't want to move this into the template as there are different 
        // teplates for different themes
		if(self::$_filters != null && count(self::$_filters) > 0):
			foreach (self::$_filters as $f){
				if ($f->getCollapsed() && !$f->getHasSelection()){
					$name = $this->__($f->getName());
					$html = str_replace('<dt>'.$name, '<dt class="navigationlayered-collapsed">'.$name, $html);
				}
			}
		endif;
        return $html;
    } 
    
    protected function _prepareLayout()
    {
		$category = Mage::registry('current_category');
		$options = Mage::registry('cat_options');
        if (!Mage::getStoreConfigFlag('customer/startup/redirect_dashboard')) { 
            $url = Mage::helper('navigationlayered/url')->getFullUrl($_GET);
            Mage::getSingleton('customer/session')
                ->setBeforeAuthUrl($url);           
        }       
        $head = $this->getLayout()->getBlock('head');
		$headData = Mage::registry('head_data');
		//print_r($headData);die;
         if ($head && $headData){
			$headTitle = $head->getTitle();
			$headDescr = $head->getDescription();
			$suffix = Mage::getStoreConfig('design/head/title_suffix');            
            $suffix = htmlspecialchars(html_entity_decode(trim($suffix), ENT_QUOTES, 'UTF-8'));   
			$title = $headData['title'];
			$descr = $headData['descr'];
            if ($suffix){
                $title = str_replace($suffix, "", $title);
            }
			if($category->getId() != 2 && !isset($headData['retitle_cat']))
			{
				$title .= $category->getMetaTitle();
				$descr .= $headDescr;
			}
			if(isset($headData['robots']))
			{
				$head->setRobots($headData['robots']);
			}
			$url = Mage::getSingleton('catalog/layer')->getCurrentCategory()->getUrl();
			if(isset($headData['canonical']))
			{
				$head->removeItem('link_rel', $url);	
				$head->addLinkRel('canonical', $headData['canonical']);
			}
			$head->setTitle(Mage::helper('navigationlayered')->__($title));
            $head->setDescription(Mage::helper('navigationlayered')->__($descr)); 
		}
		if($this->getLayout()->getBlock('breadcrumbs')):
			if($options)
			{
				foreach($options as $option)
				{
					$opt = $option['object'];
					$this->getLayout()->getBlock('breadcrumbs')->addCrumb($opt->getTitle(),
							array('label'=>Mage::helper('catalogsearch')->__($opt->getTitle()),
								'title'=>Mage::helper('catalogsearch')->__($opt->getTitle()),
								'link'=>'')
							);
				}
			}
		endif;
		$discountBlock = $this->getLayout()->createBlock('navigationlayered/catalog_layer_filter_discount')
                    ->setLayer($this->getLayer())
                    ->init(); 
		$this->setChild('layer_discount', $discountBlock);
        return parent::_prepareLayout();
		$this->getLayer()->getProductCollection()->getSelect();die;
     } 
   
}