<?php
 
class AMZ_NavigationLayered_Block_Catalog_Layer_Viewsecond extends AMZ_NavigationLayered_Block_Catalog_Layer_View
{
    public function getFilters()
    {
		if(self::$_filters)
			return  self::$_filters;   
		return array();
    }
    
    public function getStateHtml()
    {
		$stateBlock = $this->getLayout()->createBlock($this->_stateBlockName)
            ->setLayer($this->getLayer());
		$this->setChild('layer_state', $stateBlock);
        $this->getChild('layer_state')->setTemplate('amz_navigationlayered/state.phtml');
        return $this->getChildHtml('layer_state');
    }
     protected function _prepareLayout()
    {
		return $this;
	}
	public function getCacheKey()
    {
        $_taxRateRequest = Mage::getModel('tax/calculation')->getRateRequest();
        $_customer = Mage::getSingleton('customer/session')->getCustomer();
        $this->_category = Mage::getSingleton('catalog/layer')->getCurrentCategory();
        $_page = $this->getPage();

        $toolbar = new Mage_Catalog_Block_Product_List_Toolbar();
		$isHttps = '';
		if(Mage::getModel('core/store')->isCurrentlySecure())
			$isHttps = '_https';
        $cacheKey = 'layerviewsecond_'.
			/* Cat for http and https */
			$isHttps.
            /* Create different caches for different categories */
            $this->_category->getId().'_'.
            /* ... orders */
            $toolbar->getCurrentOrder().'_'.
            /* ... direction */
            $toolbar->getCurrentDirection().'_'.
            /* ... mode */
            $toolbar->getCurrentMode().'_'.
            /* ... page */
            $toolbar->getCurrentPage().'_'.
            /* ... items per page */
            $toolbar->getLimit().'_'.
            /* ... stores */
            Mage::App()->getStore()->getCode().'_'.
            /* ... currency */
            Mage::App()->getStore()->getCurrentCurrencyCode().'_'.
            /* ... customer groups */
            /* $_customer->getGroupId().'_'.
            $_taxRateRequest->getCountryId()."_".
            $_taxRateRequest->getRegionId()."_".
            $_taxRateRequest->getPostcode()."_".
            $_taxRateRequest->getCustomerClassId()."_". */
            /* ... tags */
            Mage::registry('current_tag').'_'.
            '';
        /* ... layern navigation + search */
        foreach (Mage::app()->getRequest()->getParams() as $key=>$value) {
			if(is_array($value) || is_object($value))
				$value = implode('_', $value);
            $cacheKey .= $key.'-'.$value.'_';
        }
        return $cacheKey;
    }
}