<?php

class AMZ_NavigationLayered_IndexController extends Mage_Core_Controller_Front_Action
{
	protected $_noRoute = false;
	public function setNoRoute()
	{
		$this->_noRoute = true;
		return $this;
	}
    public function indexAction()
    {	
        // init category
        $categoryId = (int) Mage::app()->getStore()->getRootCategoryId();
        if (!$categoryId) {
            $this->_forward('noRoute');
        }
        $category = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($categoryId);
            
        Mage::register('current_category', $category); 
        Mage::getSingleton('catalog/session')->setLastVisitedCategoryId($category->getId());  
          
        // need to prepare layer params
        try {
            Mage::dispatchEvent('catalog_controller_category_init_after', 
                array('category' => $category, 'controller_action' => $this));
				
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return;
        } 
        // observer can change value
        if (!$category->getId()){
            $this->_forward('noRoute'); 
            return;
        }     
        $this->loadLayout();
		if($brand = Mage::registry('current_brand'))
		{
			if(Mage::helper('navigationlayered')->getStoreDetail($brand->getBaseUrl())->getCode() != Mage::app()->getStore()->getCode())
			{
				$url = Mage::helper('core/url')->getCurrentUrl();
				$url = str_replace(Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK), Mage::helper('navigationlayered')->getStoreDetail($brand->getBaseUrl())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK), $url);// . $brand->get
				if(substr($url,-1) != '/')
					$url = $url.'/';
				$this->getResponse()->setRedirect($url, 301);
				// header("HTTP/1.1 301 Moved Permanently");
				// header("Location: ". $url ."");
			}
		}
        $this->renderLayout();
    }
	public function forwardNoroute($name = 'noRoute')
	{
		$this->_forward($name);
	}
	public function viewAction()
	{
		$categoryId = (int) Mage::app()->getStore()->getRootCategoryId();
		if($catId = $this->getRequest()->getParam('id'))
			$categoryId = $catId;
		
        if (!$categoryId) {
            $this->_forward('noRoute');
        }
        $category = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($categoryId);
        Mage::register('current_category', $category); 
        Mage::getSingleton('catalog/session')->setLastVisitedCategoryId($category->getId());  
          
        // need to prepare layer params
        try {
            Mage::dispatchEvent('catalog_controller_category_init_after', 
                array('category' => $category, 'controller_action' => $this));
				
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return;
        } 
		
        // observer can change value
        if (!$category->getId()){
            $this->_forward('noRoute'); 
            return;
        }       
		$design = Mage::getSingleton('catalog/design');
		$settings = $design->getDesignSettings($category);

		// apply custom design
		if ($settings->getCustomDesign()) {
			$design->applyCustomDesign($settings->getCustomDesign());
		}

		Mage::getSingleton('catalog/session')->setLastViewedCategoryId($category->getId());

		$update = $this->getLayout()->getUpdate();
		$update->addHandle('default');

		if (!$category->hasChildren()) {
			$update->addHandle('catalog_category_layered_nochildren');
		}
		if($this->_noRoute)
		{
			$this->_forward('noRoute'); 
            return;
		}
		$this->addActionLayoutHandles();
		$update->addHandle('CATEGORY_' . $category->getId());
		$this->loadLayoutUpdates();
		// apply custom layout update once layout is loaded
		if ($layoutUpdates = $settings->getLayoutUpdates()) {
			if (is_array($layoutUpdates)) {
				foreach($layoutUpdates as $layoutUpdate) {
					$update->addUpdate($layoutUpdate);
				}
			}
		}
		$this->generateLayoutXml()->generateLayoutBlocks();
		if($this->_noRoute)
		{
			$update->resetHandles();
			$this->getResponse()->setRedirect(Mage::getUrl('cms/index/noRoute'),302); 
            return false;
		}
		// apply custom layout (page) template once the blocks are generated
		if ($settings->getPageLayout()) {
			$this->getLayout()->helper('page/layout')->applyTemplate($settings->getPageLayout());
		}
		if ($root = $this->getLayout()->getBlock('root')) {
			$root->addBodyClass('categorypath-' . $category->getUrlPath())
				->addBodyClass('category-' . $category->getUrlKey());
		}
		
		$this->_initLayoutMessages('catalog/session');
		$this->_initLayoutMessages('checkout/session');
		$this->renderLayout();
	}
	public function addSessionColourAction()
	{
		$session=Mage::getSingleton('core/session');
		if($colour = $this->getRequest()->getParam('colour'))
			$session->setColour($colour);
		return;
	}
	public function changeUrlKeyProductAction()
	{
		
		$stores = Mage::app()->getStores();
		$urlSuffix = Mage::getStoreConfig('catalog/seo/product_url_suffix');
		//$limitStart = 
		//Mage::getSingleton('catalog/product_action')->updateAttributes(array(1), array('url_key' => 'tuanhatay'), 1);
		$defaultStore = Mage::app()->getStore(Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID); 
		$start = 1;
		$end = 100;
		$html = 'Product same url key:';
		if($this->getRequest()->getParam('start'))
			$start = $this->getRequest()->getParam('start');
		if($this->getRequest()->getParam('end'))
			$end = $this->getRequest()->getParam('end');
		foreach($stores as $store)
		{
			$arrayUrlKey = array();
			$collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect(array('*'))
			->addStoreFilter($store)
			->addAttributeToSort('url_key','ASC')
			->addAttributeToSort('url_path','DESC')
			->setPage($start, $end);
			$preProduct = '';
			$i = 1;
			$html .= '</br>store: ' . $store->getName() . '</br>';
			foreach($collection as $product)
			{
				$urlKey = $product->getUrlKey();
				$urlPath = $product->getUrlPath();
				$explodeUrlKey = explode(' ',$urlKey);
				if(count($explodeUrlKey) > 1)
				{
					$newUrlKey = str_replace(' ','-', $urlKey);
					$newUrlKey = strtolower($newUrlKey);
					$urlPath = $newUrlKey . $urlSuffix;
					$html .= 'space url '. $product->getId() . ', ';
					$content = array('store'=>$store->getName(),'url_key' => $newUrlKey, 'product_old' => array('id'=>$product->getId(), 'url_key'=>$product->getUrlKey(), 'url_path'=>$product->getUrlPath()));
					Mage::log($content, null, 'changeUrlKey.log');
					Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('url_key' =>$newUrlKey , 'url_path' => $urlPath), $store->getId());
				}
				if(!$preProduct)
				{	$preProduct = $product; continue; }
				if($preProduct->getUrlKey() == $product->getUrlKey())
				{
					if($urlKey . $urlSuffix != $urlPath)
					{
						$sku = str_replace(' ','-',$product->getSku());
						$keyUrl = $urlKey . '-sku' .$sku;
						$keyUrl = strtolower($keyUrl);
						$pathUrl = $keyUrl . $urlSuffix;
						$content = array('store'=>$store->getName(),'url_key' => $keyUrl, 'url_path' => $pathUrl ,'product_old' => array('id'=>$product->getId(), 'url_key'=>$product->getUrlKey(), 'url_path'=>$product->getUrlPath()));
						Mage::log($content, null, 'changeUrlKey.log');
						$html .= $product->getId() . ' , ';
						Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('url_key' =>$keyUrl , 'url_path' => $pathUrl), $store->getId());
					}
				}
				$preProduct = $product;
				$i++;
			}
		}
		echo $html;  
		echo '</br>';
		echo 'Please, view file var/log/changeUrlKey.log to have result; Reindex Catalog product flat.';
		die();
	}
	public function changeUrlKeyProductOnStoreAction()
	{
		
		$urlSuffix = Mage::getStoreConfig('catalog/seo/product_url_suffix');
		//$limitStart = 
		//Mage::getSingleton('catalog/product_action')->updateAttributes(array(1), array('url_key' => 'tuanhatay'), 1);
		$store = Mage::app()->getStore(Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID); 
		if($storeId = $this->getRequest()->getParam('storeid'))
			$store = Mage::app()->getStore($storeId);
		$start = 1;
		$end = 100;
		if($this->getRequest()->getParam('start'))
			$start = $this->getRequest()->getParam('start');
		if($this->getRequest()->getParam('end'))
			$end = $this->getRequest()->getParam('end');
		$collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect(array('*'))
			->addStoreFilter($store)
			->addAttributeToSort('url_key','ASC')
			->addAttributeToSort('url_path','DESC')
			->setPage($start, $end);
		//echo $collection->getSelect();die;
		$preProduct = '';
		foreach($collection as $product)
		{
			if(!$preProduct)
			{	$preProduct = $product; continue; }
			if($preProduct->getUrlKey() == $product->getUrlKey())
			{
				$urlKey = $product->getUrlKey();
				$urlPath = $product->getUrlPath();
				if($urlKey . $urlSuffix != $urlPath)
				{
					$keyUrl = $urlKey . '-sku' .$product->getSku();
					$pathUrl = $keyUrl . $urlSuffix;
					$content = array('url_key' => $keyUrl, 'url_path' => $pathUrl ,$product->getData());
					Mage::log($content, null, 'changeUrlKey.log');
					//Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('url_key' =>$keyUrl , 'url_path' => $pathUrl), $store->getId());
				}
				echo $product->getUrlKey();
				echo '</br>';
				echo '</br>';
			}
			$preProduct = $product;
		}
		echo 'Please, view file var/log/changeUrlKey.log to have result; Reindex Catalog product flat.';
		die();
	}
}