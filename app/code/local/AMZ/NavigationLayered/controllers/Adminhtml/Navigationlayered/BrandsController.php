<?php 
class AMZ_NavigationLayered_Adminhtml_Navigationlayered_BrandsController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('navigationlayered/brands')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Brands Manager'), Mage::helper('adminhtml')->__('Brands Manager'));
		return $this;
	}   
	protected function _getStore()
	{
		return $this->getRequest()->getParam('store') ? $this->getRequest()->getParam('store') : '0';
	}
	public function indexAction()
	{
		$this	->_initAction()
				->renderLayout();
	}
	public function editAction()
	{
		if($id = $this->getRequest()->getParam('id'))
		{
			$model = Mage::getModel('navigationlayered/attributesvalue')->load($id);
			$brandModel = Mage::getModel('navigationlayered/brands')->load($this->getRequest()->getParam('id'));
			$model->setParentBrand($brandModel->getParentBrand());
			Mage::register('option_data', $model);
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			$this->loadLayout();
			$this->_setActiveMenu('navigationlayered/attributesvalue');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brands Manager'), Mage::helper('adminhtml')->__('Brands Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Brands Manager'), Mage::helper('adminhtml')->__('Brands Manager'));
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock('navigationlayered/adminhtml_Brands_edit'))
					->_addLeft($this->getLayout()->createBlock('navigationlayered/adminhtml_Brands_edit_tabs'));

			$this->renderLayout();
		}
		else
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	public function categoriesAction()
	{
		$id = $this->getRequest()->getParam('id');
		$brandAtt = Mage::getModel('navigationlayered/brandsattributes')->getCollection()->getByBrandId($id);
		$model = Mage::getModel('navigationlayered/attributesvalue')->load($id);
		Mage::register('brand_data', $model);
		Mage::register('brand_attributes', $brandAtt);
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('navigationlayered/adminhtml_brands_edit_tab_attributes', 'categories.grid')
					->toHtml()
        );
	}
	
	public function saveAction()
	{
		if($data = $this->getRequest()->getPost()) {
			$brandModel = Mage::getModel('navigationlayered/brands')->load($this->getRequest()->getParam('id'));
			try{
				$brandModel->setParentBrand($data['parent_brand']);
				$brandModel->save();
			}catch(Exception $e)
			{ 
			}
			$model = Mage::getModel('navigationlayered/attributesvalue')->load($this->getRequest()->getParam('id'));
			$path = Mage::getBaseDir('media') . DS . 'attributes' . DS;
			if(isset($_FILES['listing_icon']['name']) && $_FILES['listing_icon']['name'] != '') {
				try {	
						$uploader = new Varien_File_Uploader('listing_icon');
						$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$uploader->save($path, $_FILES['listing_icon']['name'] );
						$data['listing_icon'] = $_FILES['listing_icon']['name'];
				}
				catch (Exception $e) {
		        }
			}
			if(isset($_FILES['detail_icon']['name']) && $_FILES['detail_icon']['name'] != '') {
				try {	
						$uploader = new Varien_File_Uploader('detail_icon');
						$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$uploader->save($path, $_FILES['detail_icon']['name'] );
						$data['detail_icon'] = $_FILES['detail_icon']['name'];
				}
				catch (Exception $e) {
		        }
			}
	  		if(isset($data['delete_listing_icon']) && $data['delete_listing_icon']){
				if(file_exists($path.$model->getListingIcon()))
					unlink($path.$model->getListingIcon());
				$data['listing_icon']='';
			}
			if(isset($data['delete_detail_icon']) && $data['delete_detail_icon']){
				if(file_exists($path.$model->getDetailIcon()))
					unlink($path.$model->getDetailIcon());
				$data['detail_icon']='';
			}	
			
			$attributeId = $model->getAttributeId();
			if(isset($data['base_url'])	&& $data['base_url'])
				$model->setBaseUrl($data['base_url']);
			$model->setData($data)
					->setId($this->getRequest()->getParam('id'));
			try {
				$model->setAttributeId($attributeId);
				$model->save();
				 if(isset($data['cat']))
				{
					$model->setBrandsData($data['cat']);
					Mage::getResourceModel('navigationlayered/brands')->saveBrandAttributes($model);
				}
				$brandCats = Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->addStoreFilter(0)->getByCategoryId(2)->getByOptionId($this->getRequest()->getParam('id'));
				if(count($brandCats)<1)
				{
					$data['cat'][2] = array('featured_category'=> 1,
											'multiple_selections'=> 0,
											'filter_visible'=>1,
											'canonical_url'=>1,
											'meta_robots'=>1,
											'cms_block'=>'',
											'meta_title'=>'',
											'meta_description' => '',
											'description' => '',
											'slider_id' => '',
											'slider_num_product' => '',
											'slider_status' => 0,
											'slider_line_display' => '',
											'slider_align' => '',
											'popup_slide'	=> ''
										);
					$model->setCatsData($data['cat']);
					Mage::getResourceModel('navigationlayered/attributesvalue')->saveOptionCats($model);
				}
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId(),'store'=> $this->_getStore()));
					return;
				}
				$this->_redirect('*/*/edit', array('id'=>$model->getId(),'store'=> $this->_getStore()));
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),'store'=> $this->_getStore()));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	public function selectAttBrandsAction()
	{
		$this->loadLayout();
		$this->_setActiveMenu('navigationlayered/attributesvalue');
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->_addContent($this->getLayout()->createBlock('navigationlayered/adminhtml_brands_changeatt_edit'))
				->_addLeft($this->getLayout()->createBlock('navigationlayered/adminhtml_brands_changeatt_edit_tabs'));
		$this->renderLayout();
	}
	public function changeAttBrandsAction()
	{
		if($id = $this->getRequest()->getParam('id'))
		{
			Mage::getResourceModel('navigationlayered/brands')->ChangeAttBrands($id);
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Brands was successfully changed'));
			Mage::getSingleton('adminhtml/session')->setFormData(false);
		}
		$this->_redirect('*/*/');
		return;
	}
	public function loadAttributesAction()
	{
		try {
            Mage::getResourceModel('navigationlayered/attributes')->createAttributes();
            $msg = Mage::helper('navigationlayered')->__('Attribute and options have been loaded! Click Load brands for new values, please .');
            Mage::getSingleton('adminhtml/session')->addSuccess($msg);
	    }
	    catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());    
		}
        $this->_redirect('*/*/');
	}
	public function loadBrandsAction()
	{	
		$item = Mage::getModel('navigationlayered/brands')->getCollection()->getFirstItem();
		if($item->getId())
		{
			try {
				Mage::getResourceModel('navigationlayered/attributes')->createAttributes();
				//$msg = Mage::helper('navigationlayered')->__('Filters and their options have been loaded');
				//Mage::getSingleton('adminhtml/session')->addSuccess($msg);
				Mage::getResourceModel('navigationlayered/brands')->loadBrands($item);
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Brands was successfully loaded'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());    
			}
		}
		else
		{
			$this->_redirect('*/*/selectAttBrands');
			return;
		}
		$this->_redirect('*/*/');
		return;
	}
	 public function exportCsvAction()
    {
        $fileName   = 'navigationlayered.csv';
        $content    = $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'brands.xml';
        $content    = $this->getLayout()->createBlock('navigationlayered/adminhtml_brands_grid')
					->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
?>