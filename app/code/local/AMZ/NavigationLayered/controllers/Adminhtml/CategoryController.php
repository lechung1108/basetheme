<?php
class AMZ_NavigationLayered_Adminhtml_CategoryController extends Mage_Adminhtml_Controller_Action
{
	protected function _getStore()
	{
		return $this->getRequest()->getParam('store') ? $this->getRequest()->getParam('store') : '0';
	}
	protected function _iniCategory()
	{
		$id = $this->getRequest()->getParam('category_id');
		if ($id) {
            $category = Mage::getModel('catalog/category')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($id);
            if ($category->getId()) {
                return $category;
            }
        }
        return false;
	}
	
	public function editAction()
	{	
		if(!$this->_iniCategory())
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Category is not loaded!'));
			$this->_redirect('adminhtml/catalog_category/index', array('store'=> $store));
			return;
		}
		$id = $this->getRequest()->getParam('category_id');
		$catAtt =  Mage::getModel('navigationlayered/categoriesattributes')->getCollection()->addStoreFilter()->getByCategoryId($id);
		Mage::register('attribute_cats', $catAtt);
		if($id)
		{
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			$this->loadLayout();
			$this->_setActiveMenu('navigationlayered/attributes');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Manager'), Mage::helper('adminhtml')->__('Category Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Manager'), Mage::helper('adminhtml')->__('Category Manager'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('navigationlayered/adminhtml_category_edit'))
				->_addLeft($this->getLayout()->createBlock('navigationlayered/adminhtml_category_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Item does not exist'));
			$this->_redirect();
		}
	}
	public function loadGridAction()
	{
		try {
            Mage::getResourceModel('navigationlayered/categoriesattributes')->loadSettingByCatId($this->getRequest()->getParam('category_id'));
            $msg = Mage::helper('navigationlayered')->__('Setting with attributes have been loaded');
            Mage::getSingleton('adminhtml/session')->addSuccess($msg);
	    }
	    catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());    
		}
        $this->_redirect('*/*/edit',array('category_id' => $this->getRequest()->getParam('category_id')));
		return;
	}
	public function gridAttributesAction()
	{
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('navigationlayered/adminhtml_category_edit_tab_attributes', 'attribute.grid')
                ->toHtml()
        );
	}
	public function gridOptionsAction()
	{
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('navigationlayered/adminhtml_category_Options_edit_tab_options', 'options.grid')
					->toHtml()
        );
	}
	public function saveAttributesAction()
	{
		if ($data = $this->getRequest()->getPost()) {
			$model = Mage::getModel('catalog/category')->load($this->getRequest()->getParam('category_id'));	
			if($model)
			{
				try {
						$model->setCatsData($data['cat']);
						Mage::getResourceModel('navigationlayered/attributes')->saveCatAttributes($model);
						if(isset($data['brand']))
						{
							$model->setCatBrands($data['brand']);
							Mage::getResourceModel('navigationlayered/brands')->saveCatBrands($model);
						}
						Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('navigationlayered')->__('Item was successfully saved'));
						Mage::getSingleton('adminhtml/session')->setFormData(false);
					$this->_redirect('*/*/edit', array('category_id' => $model->getId(),'store'=> $this->_getStore()));
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setFormData($data);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'), 'store'=> $this->_getStore()));
					return;
				}
			}
			
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Unable to find item to save'));
		$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),  'store'=> $this->_getStore()));
	}
	public function editOptionAction()
	{
		if(!$this->_iniCategory())
		{
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Category is not loaded!'));
			$this->_redirect('adminhtml/catalog_category/index', array('store'=> $store));
			return;
		}
		$id = $this->getRequest()->getParam('category_id');
		$groupId = $this->getRequest()->getParam('catatt_id');
		$catAtt =  Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->addStoreFilter()->getByAttributeId($groupId)->getByCategoryId($id);
		Mage::register('attribute_cats', $catAtt);
		if($id && $groupId)
		{
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			$this->loadLayout();
			$this->_setActiveMenu('navigationlayered/attributes');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Manager'), Mage::helper('adminhtml')->__('Category Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Manager'), Mage::helper('adminhtml')->__('Category Manager'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('navigationlayered/adminhtml_category_options_edit'))
				->_addLeft($this->getLayout()->createBlock('navigationlayered/adminhtml_category_options_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	public function loadGridOptionsAction()
	{
		try {
			$model = Mage::getModel('navigationlayered/categoriesattributes')->load($this->getRequest()->getParam('catatt_id'));
			if(!$model)return;
            Mage::getResourceModel('navigationlayered/categoriesoptions')->loadSettingByCatId($this->getRequest()->getParam('category_id'), $model->getAttributeId());
            $msg = Mage::helper('navigationlayered')->__('Setting with Options have been loaded');
            Mage::getSingleton('adminhtml/session')->addSuccess($msg);
	    }
	    catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());    
		}
        $this->_redirect('*/*/editOption',array('category_id' => $this->getRequest()->getParam('category_id'),'catatt_id'=>$this->getRequest()->getParam('catatt_id'),  'store'=> $this->_getStore()));
		return;
	}
	public function addDefaultOptionsAction()
	{
		$attributeId = $this->getRequest()->getPost('attribute_id');
		if($catId = $this->getRequest()->getParam('category_id'))
		{
			try {
				$categoryId = $this->getRequest()->getParam('category_id');
				$options = Mage::getModel('navigationlayered/attributesvalue')->getCollection()->addStoreFilter()->getByAttributeId($attributeId)->setOrder('option_id', 'ASC');
				$dataCatOpts = array();
				foreach ($options as $option) {
				   $dataCatOpts[$option->getId()] = array('featured_category'=>1,
														'canonical_url'=>0,
														'multiple_selections'=>0,
														'filter_visible'=>1,
														'meta_robots'=>0,
														'cms_block'=>'',
														'meta_title'=>'',
														'meta_description'=>'',
														'description'=>''
													);
				}
				$model = Mage::getModel('navigationlayered/attributes')->load($attributeId);
				$model->setCategoryId($catId);
				$model->setCatsData($dataCatOpts);
				Mage::getResourceModel('navigationlayered/attributesvalue')->saveCatOptions($model);
				return;
			} catch (Exception $e) {
				return;
			}
		}
	}
	public function saveOptionsAction()
	{
		if($data = $this->getRequest()->getPost())
		{
			if($catId = $this->getRequest()->getParam('category_id'))
			{
				try {
					$categoryId = $this->getRequest()->getParam('category_id');
					$model = Mage::getModel('navigationlayered/attributes')->load($this->getRequest()->getParam('catatt_id'));
					$model->setCategoryId($catId);
					$model->setCatsData($data['cat']);
					Mage::getResourceModel('navigationlayered/attributesvalue')->saveCatOptions($model);
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Setting was successfully saved'));
					$this->_redirect('*/*/editOption',array('category_id'=>$categoryId, 'catatt_id' => $this->getRequest()->getParam('catatt_id'), 'store'=> $this->_getStore()));
					//check if create new
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setRecruitmentData($this->getRequest()->getPost());
					$this->_redirect('*/*/edit',array('category_id',$this->getRequest()->getParam('category_id'), 'store'=> $this->_getStore()));
					return;
				}
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	public function brandsAction()
	{
		$brand = Mage::getModel('navigationlayered/brands')->getCollection()->getFirstItem();
		Mage::register('brands_cat', Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->getByAttributeId($brand->getAttributeId()));
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('navigationlayered/adminhtml_category_edit_tab_brands', 'brands.grid')
					->toHtml()
        );
	}
	public function brandDescriptionAction()
	{
		$brand = Mage::getModel('navigationlayered/brands')->getCollection()->getFirstItem();
		$id = $this->getRequest()->getParam('category_id');
		$groupId = $brand->getAttributeId();
		$catAtt =  Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->addStoreFilter()->getByAttributeId($groupId)->getByCategoryId($id);
		Mage::register('brand_cats', $catAtt);
		Mage::register('attribute_brand', $brand);
		if($id && $groupId)
		{
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			$this->loadLayout();
			$this->_setActiveMenu('navigationlayered/attributes');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Manager'), Mage::helper('adminhtml')->__('Category Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Manager'), Mage::helper('adminhtml')->__('Category Manager'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('navigationlayered/adminhtml_category_brands_edit'))
					->_addLeft($this->getLayout()->createBlock('navigationlayered/adminhtml_category_brands_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	public function saveBrandsAction()
	{
		$brand = Mage::getModel('navigationlayered/brands')->getCollection()->getFirstItem();
		if($data = $this->getRequest()->getPost() )
		{
			if($this->getRequest()->getParam('category_id') && $brand)
			{
				$catId = $this->getRequest()->getParam('category_id');
				try {
					$categoryId = $this->getRequest()->getParam('category_id');
					$model = Mage::getModel('navigationlayered/attributes')->load($brand->getAttributeId());
					
					$model->setCategoryId($catId);
					$model->setCatsData($data['cat']);
					Mage::getResourceModel('navigationlayered/attributesvalue')->saveCatBrands($model);
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Setting was successfully saved'));
					$this->_redirect('*/*/brandDescription',array('category_id'=>$categoryId, 'store'=> $this->_getStore()));
					//check if create new
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setRecruitmentData($this->getRequest()->getPost());
					$this->_redirect('*/*/edit',array('category_id',$this->getRequest()->getParam('category_id'), 'store'=> $this->_getStore()));
					return;
				}
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
}
?>