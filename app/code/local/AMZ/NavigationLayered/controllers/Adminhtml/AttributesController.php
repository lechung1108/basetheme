<?php

class AMZ_NavigationLayered_Adminhtml_AttributesController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('navigationlayered/attributes')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Attributes Manager'), Mage::helper('adminhtml')->__('Attributes Manager'));
		
		return $this;
	}   
	protected function _getStore()
	{
		return $this->getRequest()->getParam('store') ? $this->getRequest()->getParam('store') : '0';
	}
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	public function changeWeightAction()
	{
		$data = $this->getRequest()->getPost();
		
		$attribute = Mage::getModel('navigationlayered/attributes')->load($data['attribute_id']);
		if($attribute->getData())
		{	$attribute->setData($data);
			$attribute->save();
			die($attribute->getWeight());
		}
	}
	public function loadAttributesAction()
	{
		try {
            Mage::getResourceModel('navigationlayered/attributes')->createAttributes();
            $msg = Mage::helper('navigationlayered')->__('Filters and their options have been loaded');
            Mage::getSingleton('adminhtml/session')->addSuccess($msg);
	    }
	    catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());    
		}
        $this->_redirect('*/*/');
	}
	public function editAction()
	{
		$id = $this->getRequest()->getParam('id');
		$CAttribute = Mage::getModel('navigationlayered/categoriesattributes')->getCollection()->getByAttributeId($id);
		Mage::register('attribute_cats', $CAttribute);
		Mage::register('attribute_data', Mage::getModel('navigationlayered/attributes')->load($id));
		if ($id != 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			$this->loadLayout();
			$this->_setActiveMenu('navigationlayered/attributes');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Sale Event Manager'), Mage::helper('adminhtml')->__('Sale Event Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Sale Event News'), Mage::helper('adminhtml')->__('Sale Event News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_edit'))
				->_addLeft($this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	public function categoriesAttributeAction()
	{
		$id = $this->getRequest()->getParam('id');
		$CAttribute = Mage::getModel('navigationlayered/categoriesattributes')->getCollection()->getByAttributeId($id)->addStoreFilter($this->_getStore());
		//echo $CAttribute->getSelect();die;
		Mage::register('attribute_cats', $CAttribute);
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_edit_tab_categories', 'categories.grid')
					->toHtml()
        );
	}
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('navigationlayered/attributes');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),  'store'=> $this->_getStore()));
			}
		}
		$this->_redirect('*/*/');
	}
	public function gridOptionsAction()
	{
		 $this->getResponse()->setBody(
            $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_edit_tab_options', 'attribute.options.grid')
                ->toHtml()
        );
	}
	public function editOptionAction()
	{
		$id     = $this->getRequest()->getParam('option_id');
		$option = Mage::getModel('navigationlayered/attributesvalue')->load($id);
		
		Mage::register('option_data', $option);
		if ($id != 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$option->setData($data);
			}
			$this->loadLayout();
			$this->_setActiveMenu('navigationlayered/attributesvalue');

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('navigationlayered/adminhtml_attributesvalue_edit'))
					->_addLeft($this->getLayout()->createBlock('navigationlayered/adminhtml_attributesvalue_edit_tabs'));
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	public function categoriesOptionAction()
	{
		$id     = $this->getRequest()->getParam('option_id');
		$optionCats = Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->addStoreFilter()->getByOptionId($id);
		Mage::register('option_cats', $optionCats);
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('navigationlayered/adminhtml_attributesvalue_edit_tab_categories', 'categories.grid')
					->toHtml()
        );
	}
	public function getFormCatOptionAction()
	{
		$id     = $this->getRequest()->getParam('option_id');
		$categoryId = $this->getRequest()->getParam('categoryId');
		$optionCats = Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->addStoreFilter()->getByOptionId($id)->getByCategoryId($categoryId);
		$option = $optionCats->getFirstItem();
		$option->setCategoryId($categoryId);
		Mage::register('option_cats', $option);
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('navigationlayered/adminhtml_attributesvalue_edit_tab_catform', 'catform.form')
					->toHtml()
        );
							
	}
	public function loadOptionDefaultAction()
	{
		$optionId = $this->getRequest()->getParam('option_id');
		$option = Mage::getModel('navigationlayered/attributesvalue')->load($optionId);
		$brand = 0;
		if(!$option->getData())
			$this->_redirect('*/*/editoption', array('option_id'=>$optionId));
		$attributeId = $option->getAttributeId();
		if($attributeId == 607)
			$brand = 1;
		$catOptions = Mage::getModel('navigationlayered/categoriesoptions')->getCollection()->addStoreFilter()->getByOptionId($optionId);
		$arrayCatOptions = array();
		foreach($catOptions as $catOpt)
		{
			$arrayCatOptions[$catOpt->getCategoryId()] = 1;
		}
		$categories = Mage::getModel('catalog/category')->getCollection()->addIsActiveFilter()->addAttributeToFilter('level',array('gteq'=>1));
		$model = Mage::getModel('navigationlayered/attributes')->load($attributeId);
		foreach($categories as $category)
		{
			if(!isset($arrayCatOptions[$category->getId()]))
			{
				$dataCatOpts[$option->getId()] = array('featured_category'=>1,
												'canonical_url'=>$brand,
												'multiple_selections'=>0,
												'filter_visible'=>1,
												'meta_robots'=>$brand,
												'cms_block'=>'',
												'meta_title'=>'',
												'meta_description'=>'',
												'description'=>''
											);
				$model->setCategoryId($category->getId());
				$model->setCatsData($dataCatOpts);
				Mage::getResourceModel('navigationlayered/attributesvalue')->saveCatOptions($model);
			}
		}
		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Item was successfully loaded'));
		$this->_redirect('*/*/editoption', array('option_id'=>$optionId));
	}
	public function saveAction()
	{
		//print_r($this->getRequest()->getParams());die;
		$storeId = $this->_getStore();
		if ($data = $this->getRequest()->getPost()){
			
			$model = Mage::getModel('navigationlayered/attributes')->load($this->getRequest()->getParam('id'));	
			$model	->setData($data)
					->setId($this->getRequest()->getParam('id'))
					->setStoreId($storeId )
					->save();
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Item was successfully saved'));
			Mage::getSingleton('adminhtml/session')->setFormData(false);
			if(isset($data['cat']))
			{
				try {
						$model->setCatsData($data['cat']);
						$model->setCatsFileData($_FILES);
						Mage::getResourceModel('navigationlayered/attributes')->saveAttributeCats($model);
						if ($this->getRequest()->getParam('back')) {
							$this->_redirect('*/*/edit', array('id' => $model->getId(),'store' => $storeId));
							return;
						}
					$this->_redirect('*/*/');
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setFormData($data);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'), 'store'=> $this->_getStore()));
					return;
				}
			}
			else
			{
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),'store' => $storeId));
				return;
			}
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	public function saveOptionAction()
	{
		$storeId = $this->_getStore();
		if($data = $this->getRequest()->getPost()) {
			$path = Mage::getBaseDir('media') . DS . 'attributes' . DS;
			$model = Mage::getModel('navigationlayered/attributesvalue')->load($this->getRequest()->getParam('id'));	
			if(isset($_FILES['navigation_icon']['name']) && $_FILES['navigation_icon']['name'] != '') {
				try {	
						$uploader = new Varien_File_Uploader('navigation_icon');
						$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$uploader->save($path, $_FILES['navigation_icon']['name'] );
						$data['navigation_icon'] = $_FILES['navigation_icon']['name'];
				}
				catch (Exception $e) {
		        }
			}
			//print_r($data);die;
			if(isset($_FILES['listing_icon']['name']) && $_FILES['listing_icon']['name'] != '') {
				try {	
						$uploader = new Varien_File_Uploader('listing_icon');
						$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$uploader->save($path, $_FILES['listing_icon']['name'] );
						$data['listing_icon'] = $_FILES['listing_icon']['name'];
				}
				catch (Exception $e) {
		        }
			}
			if(isset($_FILES['detail_icon']['name']) && $_FILES['detail_icon']['name'] != '') {
				try {	
						$uploader = new Varien_File_Uploader('detail_icon');
						$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						$uploader->save($path, $_FILES['detail_icon']['name'] );
						$data['detail_icon'] = $_FILES['detail_icon']['name'];
				}
				catch (Exception $e) {
		        }
			}
	  		if(isset($data['delete_navigation_icon']) && $data['delete_navigation_icon']){
				$data['navigation_icon']='';
				if(file_exists($path.$model->getNavigationIcon()))
					unlink($path.$model->getNavigationIcon());
			}	
			if(isset($data['delete_listing_icon']) && $data['delete_listing_icon']){
				$data['listing_icon']='';
				if(file_exists($path.$model->getListingIcon()))
					unlink($path.$model->getListingIcon());
			}	
			if(isset($data['delete_detail_icon']) && $data['delete_detail_icon']){
				$data['detail_icon']='';
				if(file_exists($path.$model->getDetailIcon()))
					unlink($path.$model->getDetailIcon());
			}	
			$attributeId = $model->getAttributeId();
			$model->setData($data)
					->setId($this->getRequest()->getParam('id'));
			
			try {
				$model->setAttributeId($attributeId);
				$model->save();
				if(isset($data['cat']))
				{
					$model->setCatsData($data['cat']);
					Mage::getResourceModel('navigationlayered/attributesvalue')->saveOptionCats($model);
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/editOption', array('option_id' => $model->getId(), 'store'=>$storeId));
					return;
				}
				$this->_redirect('*/*/editOption', array('option_id'=>$model->getId(), 'store'=>$storeId));
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/editOption', array('option_id' => $this->getRequest()->getParam('id'), 'store'=>$storeId));
                return;
            }
			
        }
		
		
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	public function loadDefaultSettingAction()
	{
		$storeId = $this->_getStore();
		try{
				$attributes = Mage::getModel('navigationlayered/attributes')->getCollection();
				foreach($attributes as $attribute)
				{
					Mage::getResourceModel('navigationlayered/attributesvalue')->LoadDefaultOptionCats($attribute);
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Default setting category with option was loaded!'));
				$this->_redirect('*/*/', array('store'=>$storeId));
				return;
			}
			catch(Exception $e)
			{
				 Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                 $this->_redirect('*/*/', array('store'=>$storeId));
				return;
			}
	}
	public function loadDefaultCatOptionAction()
	{
		$storeId = $this->_getStore();
		$id = $this->getRequest()->getParam('attid');
		$model = Mage::getModel('navigationlayered/attributes')->load($id);	
		if($model->getData())
		{
			try{
				Mage::getResourceModel('navigationlayered/attributesvalue')->LoadDefaultOptionCats($model);
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('catalog')->__('Default setting category with option was loaded!'));
				$this->_redirect('*/*/edit', array('id' => $id, 'store'=>$storeId));
				return;
			}
			catch(Exception $e)
			{
				 Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                 $this->_redirect('*/*/edit', array('id' => $id, 'store'=>$storeId));
				return;
			}
		}
		//die();
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('navigationlayered')->__('Unable to find item to load'));
        $this->_redirect('*/*/');
	}
    public function massDeleteAction() {
        $navigationlayeredIds = $this->getRequest()->getParam('navigationlayered');
        if(!is_array($navigationlayeredIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($navigationlayeredIds as $navigationlayeredId) {
                    $navigationlayered = Mage::getModel('navigationlayered/attributes')->load($navigationlayeredId);
                    $navigationlayered->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($navigationlayeredIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $navigationlayeredIds = $this->getRequest()->getParam('navigationlayered');
        if(!is_array($navigationlayeredIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($navigationlayeredIds as $navigationlayeredId) {
                    $navigationlayered = Mage::getSingleton('navigationlayered/attributes')
                        ->load($navigationlayeredId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($navigationlayeredIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'navigationlayered.csv';
        $content    = $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'attributes.xml';
        $content    = $this->getLayout()->createBlock('navigationlayered/adminhtml_attributes_grid')
					->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}