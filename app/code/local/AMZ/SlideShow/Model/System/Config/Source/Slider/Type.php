<?php

class AMZ_SlideShow_Model_System_Config_Source_Slider_Type
{
    public function toOptionArray()
    {
        return array(
			array('value' => 'block',	'label' => Mage::helper('amz_slideshow')->__('Block Slider')),
			array('value' => 'product',	'label' => Mage::helper('amz_slideshow')->__('Product Slider'))
        );
    }
}
