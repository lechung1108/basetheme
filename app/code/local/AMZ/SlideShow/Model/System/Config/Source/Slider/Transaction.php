<?php

class AMZ_SlideShow_Model_System_Config_Source_Slider_Transaction
{
    public function toOptionArray()
    {
        return array(
			array('value' => 'slide',	'label' => Mage::helper('amz_slideshow')->__('Slide')),
			array('value' => 'fade',	'label' => Mage::helper('amz_slideshow')->__('Fade'))
        );
    }
}
