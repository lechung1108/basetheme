<?php

class AMZ_SlideShow_Model_System_Config_Source_Block_Plugin_Type
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'revolution',    'label' => Mage::helper('amz_slideshow')->__('Revolution Slider')),
            array('value' => 'sequence',    'label' => Mage::helper('amz_slideshow')->__('Sequence Slider')),
			array('value' => 'fraction',    'label' => Mage::helper('amz_slideshow')->__('Fraction Slider'))
        );
    }
}
