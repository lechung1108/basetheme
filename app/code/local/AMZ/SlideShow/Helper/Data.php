<?php

class AMZ_SlideShow_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getConfig($option)
    {
        return Mage::getStoreConfig('amz_slideshow/' . $option);
    }
    
    public function getJsPluginFile() {
        if ($this->getConfig('general/type') == 'block') {
            switch ($this->getConfig('general/block_plugin')) {
                case 'fraction': // Fraction Slider
                    return 'amzthemes/slideshow/jquery.fractionslider.min.js';
                case 'sequence': // Sequence Modern Slide In
                    return 'amzthemes/slideshow/jquery.sequence-min.js';
                default: // Revolution Slider
                    return 'amzthemes/slideshow/jquery.themepunch.revolution.min.js';
            }
        }
        // Box Slider
        return 'amzthemes/slideshow/jquery.bxslider.min.js';
    }
    
    public function getCssPluginFile() {
        if ($this->getConfig('general/type') == 'block') {
            switch ($this->getConfig('general/block_plugin')) {
                case 'fraction': // Fraction Slider
                    return 'amzthemes/slideshow/fractionslider.css';
                case 'sequence': // Sequence Modern Slide In
                    return 'amzthemes/slideshow/sequencejs.css';
                default: // Revolution Slider
                    return 'amzthemes/slideshow/revolution.css';
            }
        }
        // Box Slider
        return 'amzthemes/slideshow/bxslider.css';
    }
    
    public function getResponsiveCssPluginFile() {
        if (!$this->getConfig('general/active_responsive'))
            return 'css/empty.css';
            
        if ($this->getConfig('general/type') == 'block') {
            switch ($this->getConfig('general/block_plugin')) {
                case 'fraction': // Fraction Slider
                    return 'amzthemes/slideshow/fractionslider-responsive.css';
                case 'sequence': // Sequence Modern Slide In
                    return 'amzthemes/slideshow/sequencejs-responsive.css';
                default: // Revolution Slider
                    return 'amzthemes/slideshow/revolution-responsive.css';
            }
        }
        // Box Slider
        return 'amzthemes/slideshow/bxslider-responsive.css';
    }
}
