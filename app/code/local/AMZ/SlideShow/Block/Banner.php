<?php
class AMZ_SlideShow_Block_Banner extends Mage_Core_Block_Template
{	
	public function getBannerIds()
	{
		$blockIdsString = Mage::helper('amz_slideshow')->getConfig('banner/blocks');
		$blockIds = explode(",", str_replace(" ", "", $blockIdsString));
		return $blockIds;
	}
    
    public function getBannerCount()
    {
        return Mage::helper('amz_slideshow')->getConfig('banner/count');
    }
}