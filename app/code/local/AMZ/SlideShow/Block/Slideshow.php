<?php
class AMZ_SlideShow_Block_Slideshow extends Mage_Core_Block_Template
{	
	public function getBlockIds()
	{
		$blockIdsString = Mage::helper('amz_slideshow')->getConfig('general/blocks');
		$blockIds = explode(",", str_replace(" ", "", $blockIdsString));
		return $blockIds;
	}
	
	public function getProductSkus()
	{
		$productSkuString = Mage::helper('amz_slideshow')->getConfig('general/products');
		$productSkus = explode(",", $productSkuString);
        return $productSkus;
	}
}