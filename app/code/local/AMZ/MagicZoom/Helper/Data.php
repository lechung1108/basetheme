<?php
/**
 * Magic-Zoom Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_MagicZoom
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0
 * @link       http://www.amzthemes.com
 */

class AMZ_MagicZoom_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param string $node
     * @return mixed
     */
    public function getConfigData($node)
    {
        return Mage::getStoreConfig(sprintf('catalog/magiczoom/%s', $node));
    }

    /**
     * @param string $node
     * @return bool
     */
    public function getConfigFlag($node)
    {
        return (bool) Mage::getStoreConfig(sprintf('catalog/magiczoom/%s', $node));
    }
}
