<?php
/**
 * Magic-Zoom Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_MagicZoom
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0
 * @link       http://www.amzthemes.com
 */

class AMZ_MagicZoom_Model_System_Config_Source_Position
{
    /** @var AMZ_MagicZoom_Helper_Data */
    protected $_helper;

    /**
     * @return AMZ_MagicZoom_Helper_Data
     */
    public function getHelper()
    {
        if (is_null($this->_helper)) {
            $this->_helper = Mage::helper('amz_magiczoom');
        }
        return $this->_helper;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $helper = $this->getHelper();
        return array(
            'right'  => $helper->__('Right'),
            'left'   => $helper->__('Left'),
            'top'    => $helper->__('Top'),
            'bottom' => $helper->__('Bottom'),
            'inside' => $helper->__('Inside')
        );
    }
}
