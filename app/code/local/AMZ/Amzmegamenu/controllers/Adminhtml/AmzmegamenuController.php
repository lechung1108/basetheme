<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */ 
class AMZ_Amzmegamenu_Adminhtml_AmzmegamenuController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction()
	{
	    
		
		$this->loadLayout()->_setActiveMenu('amzmegamenu/items')->_addBreadcrumb(Mage::helper('adminhtml')->__('Menu Manager'),
		Mage::helper('adminhtml')->__('Menu Manager'));
		
		return $this;
	}
	public function indexAction() {
	  
		$this->_initAction();
		$this->renderLayout();
	}
	public function groupAction(){
		$this->_initAction();
		$this->renderLayout();
	}
	
	public function editAction()
	{
		$Id = $this->getRequest()->getParam('id');
		
		$menuModel = Mage::getModel('amzmegamenu/amzmegamenu')->load($Id);
	
		if ($menuModel->getId() || $Id == 0) {
		    
		    if($Id == 0){
			   $menuModel->setData("menutype",2);
			   $menuModel->setData("mega_subcontent",1);
			   $menuModel->setData("mega_group",0);
			   $menuModel->setData("mega_cols",1);
			   $menuModel->setData("showtitle",1);
			}
            
			Mage::register('amzmegamenu_data', $menuModel);

			$this->loadLayout();
			$this->_setActiveMenu('amzmegamenu/items');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Menu Manager'), Mage::helper('adminhtml')->__('Menu Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Menu Description'), Mage::helper('adminhtml')->__('Menu Description'));
			
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			
			$this->_addContent($this->getLayout()->createBlock('amzmegamenu/adminhtml_amzmegamenu_edit'))
			->_addLeft($this->getLayout()->createBlock('amzmegamenu/adminhtml_amzmegamenu_edit_tabs'));
			
		    $this->renderLayout();
			
		} else {
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('amzmegamenu')->__('Menu Item does not exist'));
		    $this->_redirect('*/*/');
		}
	}

	public function newAction()
	{
	    $this->_forward('edit');
	}


    public function editgroupAction()
	{
		$Id = $this->getRequest()->getParam('id');
		
		$groupModel = Mage::getModel('amzmegamenu/amzmegamenugroup')->load($Id);
	
		if ($groupModel->getId() || $Id == 0) {
			Mage::register('amzmegamenugroup_data', $groupModel);
			$this->loadLayout();
			$this->_setActiveMenu('amzmegamenu/group');
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Menu Group Configuration'), Mage::helper('adminhtml')->__('Menu Group Configuration'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Menu Group Configuration'), Mage::helper('adminhtml')->__('Menu Group Configuration'));
			
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			
			$this->_addContent($this->getLayout()->createBlock('amzmegamenu/adminhtml_amzmegamenugroup_edit'))
			->_addLeft($this->getLayout()->createBlock('amzmegamenu/adminhtml_amzmegamenugroup_edit_tabs'));
			
		    $this->renderLayout();
			
		} else {
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('amzmegamenu')->__('Menu Group does not exist'));
		    $this->_redirect('*/*/');
		}
	}


	public function newgroupAction()
	{
	    $this->_forward('editgroup');
	}
	protected function getNextorder($where){
	
	    $resource = Mage::getSingleton('core/resource');
		$read= $resource->getConnection('core_read');
		$menutable = $resource->getTableName('amzmegamenu');
		$sqlquery = 'SELECT MAX(ordering) From '.$menutable.' where '.$where;
		$rows = $read->fetchAll($sqlquery);
		if($rows){
		  return $rows[0]['MAX(ordering)'] + 1;
		}
		
	}
	
	public function saveAction()
	{
		if ( $this->getRequest()->getPost() ) {
			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
				try {
					/* Starting upload */
					$uploader = new Varien_File_Uploader('image');
					// Any extention would work
					$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					// Set the file upload mode
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS ."amzmegamenu".DS;
					$uploader->save($path, $_FILES['image']['name'] );
				} catch (Exception $e) {
				}
				//this way the name is saved in DB
				$image = "amzmegamenu/".$_FILES['image']['name'];
			}
			try {
				$postData = $this->getRequest()->getPost();
				if(isset($postData['image']['delete']) && $postData['image']['delete']==1) {
					$path = Mage::getBaseDir('media') .DS. $postData['image']['value'];
					$path= preg_replace("/\//", "\\", $path);
					unlink($path);
					$postData['image']="";
				}else{
					if (isset($image)) $postData['image']= $image;
					else unset($postData['image']);
				}
				$resource = Mage::getSingleton('core/resource');
		        $read= $resource->getConnection('core_read');
				$menutable = $resource->getTableName('amzmegamenu');
				if($postData["menualias"] && $postData["menualias"] !== ""){
					$query = 'SELECT tblmega.menu_id'
						. ' FROM '.$menutable
						. ' as tblmega WHERE tblmega.menualias = "'.$postData["menualias"]
						. '" and tblmega.menu_id != "'.$this->getRequest()->getParam('id')
						.'"  ORDER BY tblmega.menu_id';
					
					$rows = $read->fetchAll($query);
	                
	                if(count($rows)){
	                     throw new Exception('The alias already used by another menu item.');
	                }
                }
				
				$postData['link'] = str_replace ( Mage::getBaseUrl() , "" , $postData['link'] );
                $postData['category'] = str_replace ( Mage::getBaseUrl() , "" , $postData['category'] );
                $postData['cms'] = str_replace ( Mage::getBaseUrl() , "" , $postData['cms'] );
				
				$menuModel = Mage::getModel('amzmegamenu/amzmegamenu');
				$helper = Mage::helper('amzmegamenu');
				if( $this->getRequest()->getParam('id') <= 0 ) {
				   $menuModel->setCreatedTime(Mage::getSingleton('core/date')->gmtDate());
				   $where = " status >= 0 AND parent = ".(int) $postData['parent'];
				   $postData['ordering'] = $this->getNextorder($where);
				}
				$menuModel
				->addData($postData)
				->setUpdateTime(Mage::getSingleton('core/date')->gmtDate())
				->setId($this->getRequest()->getParam('id'))
				->save();
				
				$helper->reorder(' parent= '.(int)$postData['parent']);
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Menu Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setAmzMegamenuData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $menuModel->getId()));
					return;
				}
				$this->_redirect('*/*/index/groupid/'.$postData['menugroup']);
			return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setAmzMegamenuData($this->getRequest()->getPost());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		$this->_redirect('*/*/');
	}
    
    public function savegroupAction()
	{
		if ( $this->getRequest()->getPost() ) {
			try {
				$postData = $this->getRequest()->getPost();

				
				$groupModel = Mage::getModel('amzmegamenu/amzmegamenugroup');
				$helper = Mage::helper('amzmegamenu');
				if( $this->getRequest()->getParam('id') <= 0 ) {
				   $groupModel->setCreatedTime(Mage::getSingleton('core/date')->gmtDate());
				}
				$oldstoreid =  $groupModel->load($this->getRequest()->getParam('id'))->getstoreid();

                $resource = Mage::getSingleton('core/resource');
		        $read= $resource->getConnection('core_read');
		        $write = $resource->getConnection('core_write');
          
                //remove assigning this menu group for the old store
                if($oldstoreid !== $postData['storeid']){
					$storegrouptable = $resource->getTableName('amzmegamenu_store_menugroup');
	                $query = 'Delete '
							. ' FROM '.$storegrouptable
							. '  WHERE store_id = "'.$oldstoreid
							. '" and menugroupid = "'.$this->getRequest()->getParam('id').'"';
							
					$write->query($query);
                }
                $query = 'SELECT store_id'
						. ' FROM '.$resource->getTableName('amzmegamenu_store_menugroup')
						. '  WHERE store_id = "'.$oldstoreid
						.'"  ORDER BY store_id';                  
                $storegroups = $read->fetchRow($query);
               
                  
                $query = 'SELECT id,storeid'
						. ' FROM '.$resource->getTableName('amzmegamenu_types')
						. '  WHERE storeid = "'.$oldstoreid
						.'"  ORDER BY storeid';
				$groupstores = $read->fetchRow($query);
                
                
                if(empty($storegroups) and $groupstores){
                    $adds =  array();
                    $adds["store_id"] = $groupstores["storeid"];
                    $adds["menugroupid"] = $groupstores["id"];
                    $write->insert($resource->getTableName('amzmegamenu_store_menugroup'),$adds);
                    $write->commit(); 
                } 

				$groupModel
				->addData($postData)
				->setUpdateTime(Mage::getSingleton('core/date')->gmtDate())
				->setId($this->getRequest()->getParam('id'))
				->save();
				
				//check to see if there are any menu group associated with this store
                $storegroupModel =  Mage::getModel('amzmegamenu/amzmegamenustoregroup');
                $storecollection = $storegroupModel->getCollection()->addFieldToFilter("store_id",array('eq' => $postData['storeid']));
                if(!count($storecollection)){
                	$storegroupModel->setstore_id($postData['storeid'])
                                    ->setmenugroupid($groupModel->getId())
                                    ->save();
                }


				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Menu Group was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setAmzMegamenugroupData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/editgroup', array('id' => $groupModel->getId()));
					return;
				}
				$this->_redirect('*/*/group');
			return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setAmzMegamenugroupData($this->getRequest()->getPost());
				$this->_redirect('*/*/editgroup', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		$this->_redirect('*/*/');
	}


	public function deleteAction()
	{
			if( $this->getRequest()->getParam('id') > 0 ) {
				try {

					$this->deleteitems($this->getRequest()->getParam('id'));
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Brand was successfully deleted'));
					$groupid = $this->getRequest()->getParam('group') ;
					$this->_redirect('*/*/index', array('groupid' => $groupid));
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				}
			}
			
	}

	public function deletegroupAction()
	{

			if( $this->getRequest()->getParam('id') > 0 ) {
				$groupid = $this->getRequest()->getParam('id');
                $storeid = $this->getRequest()->getParam('storeid');
                $resource = Mage::getSingleton('core/resource');
		        $read= $resource->getConnection('core_read');
		        $write = $resource->getConnection('core_write');

				try {
					$groupModel = Mage::getModel('amzmegamenu/amzmegamenugroup');
					$groupModel->setId($groupid)->delete();
                    $storegrouptable = $resource->getTableName('amzmegamenu_store_menugroup');
	                $query = 'Delete '
							. ' FROM '.$storegrouptable
							. '  WHERE menugroupid = "'.$groupid.'"';
							
					$write->query($query);
            		
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The menu group was successfully deleted'));
					$this->_forward('group');
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					$this->_redirect('*/*/editgroup', array('id' => $this->getRequest()->getParam('id')));
				}

			}
			
	}
	
	protected function deleteitems($id){
	       if($id){
		      $menuModel = Mage::getModel('amzmegamenu/amzmegamenu');
			  $menuModel->setId($id)->delete();
			  $collections = $menuModel->getCollection()->addFilter("parent",$id,"eq");
			  foreach($collections as $collection){
			     $this->deleteitems($collection->menu_id);
			  }
		   } 
	}
	
	
	
	public function ajaxAction(){

		$groupid = $this->getRequest()->getParam('menugroup');
		$activecat = $this->getRequest()->getParam('activecat');
		$activecms = $this->getRequest()->getParam('activecms');
        $helper = Mage::helper('amzmegamenu'); 

        $groupModel = Mage::getModel('amzmegamenu/amzmegamenugroup')->load($groupid);
        $storeid = $groupModel->getData("storeid");
		$store = Mage::app()->getStore($storeid);
		//The root category id of this store 
	    $parent = $store->getRootCategoryId();
	    /**
		 * Check if parent node of the store still exists
		 */
		 $category = Mage::getModel('catalog/category');
		 /* @var $category Mage_Catalog_Model_Category */
		 if (!$category->checkId($parent)) {
			return array();
		 }

		 $recursionLevel  = max(0, $store->getConfig('catalog/navigation/max_depth'));
		 $storeCategories = $category->getCategories($parent, $recursionLevel, false, true, true);
		
		 $storeCategories =  $storeCategories->load()->addAttributeToSelect("*");
		 
		 //categories list
		 $catlist = $helper->getoutputList($parent,$storeCategories,"name","entity_id","parent_id");
		 $clist = array();
		 foreach($catlist as $id => $cat){
		    $category = Mage::getModel('catalog/category')->load($id);
			$url =  str_replace ( Mage::getBaseUrl() , "" , $category->getUrl() );
		    $clist[$url] = $cat; 
		 }
		 $response = array();
		 $categories = '<select class=" select" name="category" id="category">';

		 foreach($clist as $url => $catname){
		 	if($activecat == $url){
               $categories .= '<option selected value="'.$url.'">'.$catname.'</option>';
		 	}else{
		 	   $categories .= '<option value="'.$url.'">'.$catname.'</option>';
		 	}   
		 }
		 $categories .= '</select>';
		 $response["category"] = $categories;

         //cmspages list 
		 $cmspages = array();
		 $cmspages = '<select class=" select" name="cms" id="cms">';
         if(Mage::getStoreConfig("web/secure/use_in_adminhtml")){
                    $baseurl = Mage::getStoreConfig("web/secure/base_url");
				 }else{
                    $baseurl = Mage::getBaseUrl();  
		 }
	     if(!strpos($baseurl,"index.php")) $baseurl .= "index.php/";
		 foreach($helper->getListcms($storeid) as $page){
		    $url =  $baseurl.$page;
		    if($page == $activecms){
               $cmspages .= '<option selected value="'.$url.'">'.$page.'</option>';	  
		    }else{
		       $cmspages .= '<option value="'.$url.'">'.$page.'</option>';	
		    }
			
		 } 
		 
		 $cmspages .= "</select>";    
		 $response["cmspage"] = $cmspages; 

		 //get collections of all menu item filter by the requested menu group
         if($this->getRequest()->getParam('menuid')){
         	$menumodle = Mage::getModel('amzmegamenu/amzmegamenu')->load($this->getRequest()->getParam('menuid'));
         	$parentid = $menumodle->getData("parent");
            $collections = Mage::getModel('amzmegamenu/amzmegamenu')->getCollection()->addFieldToFilter("menu_id",array("neq" => $this->getRequest()->getParam('menuid')))
                                                                                   ->addFieldToFilter("menugroup",array('eq' => $groupid ));  
		 } else{                                                                    
           $collections = Mage::getModel('amzmegamenu/amzmegamenu')->getCollection()->addFieldToFilter("menugroup",array('eq' => $groupid ));
		 }
         
         $parent = '<select class=" select" name="parent" id="parent">';
		 //Get the parent list		  
		 $parents = $helper->getoutputList(0,$collections,"title","menu_id","parent",true);
		 foreach($parents as $paid => $palabel){
		 	$parent .= '<option value="'.$paid;
		 	if($paid == $parentid){
		 		$parent .= '" selected="selected"';
		 	}else{
		 		$parent .= '" ';
		 	}
		 	$parent .= ' >'.$palabel.'</option>';
		 }
		 $parent .= '</select>';
         $response['parent'] = $parent; 

		 echo json_encode($response);

	}
	
	public function massDuplicateAction() {
		$menuIds = $this->getRequest()->getParam('amzmegamenu');
		if(!is_array($menuIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
		} else {
			try {
				$menuModel = Mage::getModel('amzmegamenu/amzmegamenu');
				foreach($menuIds as $menuId){
					$menuModel->load($menuId);
					$data= $menuModel->getData();
					if(!isset($groupid)) $groupid= $data['menugroup'];
					unset($data['menu_id']);
					unset($data['menualias']);
					$data['parent']='0';
					$menuModel->setData($data);
					$menuModel->setCreatedTime(now())->setUpdateTime(now());
					$menuModel->save();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
				Mage::helper('adminhtml')->__(
				'Total of %d record(s) were successfully duplicate', count($menuIds)
				)
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index', array('groupid' => $groupid));
	} 
	public function massDeleteAction() {
		$menuIds = $this->getRequest()->getParam('amzmegamenu');
		if(!is_array($menuIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
		} else {
			try {
				$menuModel = Mage::getModel('amzmegamenu/amzmegamenu');
				foreach($menuIds as $menuId){
					if(!isset($groupid)){
						$menuModel->load($menuId);
						$groupid= $menuModel->getData('menugroup');	
					} 
					$this->deleteitems($menuId);
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
				Mage::helper('adminhtml')->__(
				'Total of %d record(s) were successfully delete', count($menuIds)
				)
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index', array('groupid' => $groupid));
} 
	public function massStatusAction() {
		$menuIds = $this->getRequest()->getParam('amzmegamenu');
		if(!is_array($menuIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
		} else {
			try {
				$menuModel = Mage::getModel('amzmegamenu/amzmegamenu');
				foreach ($menuIds as $menuId) {
                    $menuModel->load($menuId);
                    if(!isset($groupid)){
						$groupid= $menuModel->getData('menugroup');	
					} 
                    $menuModel->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
				Mage::getSingleton('adminhtml/session')->addSuccess(
				Mage::helper('adminhtml')->__(
				'Total of %d record(s) were successfully updated', count($menuIds)
				)
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index', array('groupid' => $groupid));
	}
	public function massDuplicateGroupAction() {
		$groupIds = $this->getRequest()->getParam('amzmegamenu');
		$to_groupId = $this->getRequest()->getParam('duplicate_to');
		if(!is_array($groupIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
		} else {
			try {
				foreach($groupIds as $groupId){
					$this->findDuplicate(0,$groupId,0,$to_groupId);
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
				Mage::helper('adminhtml')->__(
				'Total of %d record(s) were successfully duplicate', count($menuIds)
				)
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/group');
	}
	private function findDuplicate($parent,$idgroup,$parentNew,$idgroupNew){
		$menuModel = Mage::getModel('amzmegamenu/amzmegamenu');
		$collections = $menuModel->getCollection()
						->addFieldToFilter("menugroup",array('eq'=>$idgroup))
						->addFieldToFilter("parent",array('eq'=>$parent));
		foreach ($collections as $collection){
			$data= $collection->getData();
			$id= $data['menu_id'];
			$idNew= $this->addDuplicate($data, $parentNew, $idgroupNew);
			$this->findDuplicate($id, $idgroup,$idNew,$idgroupNew);
		}
	}
	private function addDuplicate($data,$parentNew,$idgroupNew){
		$menuModel = Mage::getModel('amzmegamenu/amzmegamenu');
		unset($data['menu_id']);
		unset($data['menualias']);
		$data['menugroup']= $idgroupNew;
		$data['parent']= $parentNew;
		$menuModel->setData($data);
		$menuModel->setCreatedTime(now())->setUpdateTime(now());
		$menuModel->save();
		return $menuModel->getId();
	}
} 