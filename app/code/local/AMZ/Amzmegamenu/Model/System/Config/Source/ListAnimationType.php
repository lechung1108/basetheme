<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */


class AMZ_Amzmegamenu_Model_System_Config_Source_ListAnimationType
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'none', 'label'=>Mage::helper('amzmegamenu')->__('None')),
            array('value'=>'jaws', 'label'=>Mage::helper('amzmegamenu')->__('Fade')),
			array('value'=>'fence', 'label'=>Mage::helper('amzmegamenu')->__('fence')),
			array('value'=>'venitian', 'label'=>Mage::helper('amzmegamenu')->__('venitian')),
			array('value'=>'fly', 'label'=>Mage::helper('amzmegamenu')->__('fly')),
			array('value'=>'papercut', 'label'=>Mage::helper('amzmegamenu')->__('papercut')),
			array('value'=>'fan', 'label'=>Mage::helper('amzmegamenu')->__('fan')),
			array('value'=>'wave', 'label'=>Mage::helper('amzmegamenu')->__('wave')),
			array('value'=>'helix', 'label'=>Mage::helper('amzmegamenu')->__('helix')),
			array('value'=>'pop', 'label'=>Mage::helper('amzmegamenu')->__('pop')),
			array('value'=>'linear', 'label'=>Mage::helper('amzmegamenu')->__('linear')),
			array('value'=>'bounce', 'label'=>Mage::helper('amzmegamenu')->__('bounce')),
			array('value'=>'winding', 'label'=>Mage::helper('amzmegamenu')->__('winding')),
			array('value'=>'shield', 'label'=>Mage::helper('amzmegamenu')->__('shield'))
        );
    }    
}
