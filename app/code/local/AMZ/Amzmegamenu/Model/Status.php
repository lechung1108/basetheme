<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */
       class AMZ_Amzmegamenu_Model_Status extends Varien_Object
       {
	        const STATUS_ENABLED = 1;
            const STATUS_DISABLED = 0;
			static public function getOptionArray()
			{
				return array(
				self::STATUS_ENABLED => Mage::helper('amzmegamenu')->__
				('Enabled'),
				self::STATUS_DISABLED => Mage::helper('amzmegamenu')->__
				('Disabled')
				);
			}
       }