<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */
    class AMZ_Amzmegamenu_Model_Observer
	{

          public function prefarestoreForm($block){
            if(Mage::registry('store_type') == 'store'){
          	   $form = $block["block"]->getForm();
               $storeid = Mage::app()->getRequest()->getParam("store_id");

               $collections = Mage::getModel('amzmegamenu/amzmegamenugroup')->getCollection()->addFieldToFilter("storeid",array("eq" => $storeid))->setOrder("id", "DESC");
               $listgroup = array();

               foreach ($collections as $collection) {
                  $listgroup[$collection->id] = $collection->title;
               }
               
               //add the menugoup field
               if(!empty($listgroup)){
                   
                   $resource = Mage::getSingleton('core/resource');
                   $read= $resource->getConnection('core_read');
                   $menutable = $resource->getTableName('amzmegamenu_store_menugroup');
                   $query = 'SELECT menugroupid '
                    . ' FROM '.$menutable
                    . ' WHERE store_id = '.(int) $storeid
                  
                    . ' ORDER BY id';
                   $rows = $read->fetchRow($query);
                   $fieldset = $form->addFieldset('amzmegamenu_fieldset', array(
                       'legend' => Mage::helper('core')->__('Amz megamenu Information')
                   ));

                  //print_r($listgroup);die();
                   $fieldset->addField('menugroup', 'select', array(
                      'name'      => 'menugroup',
                      'label'     => Mage::helper('amzmegamenu')->__('Menu Group'),
                      'no_span'   => true,
                      'values'     => $listgroup
                   ));
                   $menugroup = $form->getElement("menugroup");
                   $menugroup->setValue($rows['menugroupid']);
                   if($rows['menugroupid']){
                      
                      $form->setValue("menugroup",$rows['menugroupid']);
                   }
                   $block["block"]->setForm($form);
                
               }

               
            }  
              
          }   


          public function storeedit($store){

              if(Mage::app()->getRequest()->isPost() && $postData = Mage::app()->getRequest()->getPost() ){
                  if($postData['store_type'] == 'store'){
                    
                	   $storegroupmodel = Mage::getModel('amzmegamenu/amzmegamenustoregroup');
                     $storecollection = Mage::getModel('amzmegamenu/amzmegamenustoregroup')->getCollection()->addFieldToFilter("store_id",array("eq" => $postData['store']['store_id']));

                     foreach($storecollection as $collection){
                       $id = $collection->id;
                       break;
                     }
                     if($id){
                        $storegroupmodel->load($id);
                     }
                   
                    
                    $save['menugroupid'] = $postData['menugroup'];
                    $save['store_id'] = $postData['store']['store_id'];
                    
                    $storegroupmodel->setData('menugroupid',$postData['menugroup']);
                    $storegroupmodel->setData('store_id',$postData['store']['store_id']);
                    $storegroupmodel->save(); 
                  }
              }
          }

	}




?>