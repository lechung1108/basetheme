<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */

  class AMZ_Amzmegamenu_Block_Adminhtml_Amzmegamenu_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
  {
		public function __construct()
		{
			parent::__construct();
				$this->_objectId = 'id';
				$this->_blockGroup = 'amzmegamenu';
				$this->_controller = 'adminhtml_amzmegamenu';
				$this->_updateButton('save', 'label', Mage::helper('amzmegamenu')->__('Save Menu Item'));
				$this->_updateButton('delete', 'label', Mage::helper('amzmegamenu')->__('Delete Menu Item'));
				$this->_updateButton('back', array('label' => Mage::helper('amzmegamenu')->__('back to group')));
				$this->_addButton('saveandcontinue', array(
					'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
					'onclick'   => 'saveAndContinueEdit()',
					'class'     => 'save',
				), -100);
                
                //The root category id of default store 
	            $parent = Mage::app()->getWebsite(true)->getDefaultStore()->getRootCategoryId(); 
                $helper = Mage::helper('amzmegamenu');
                /**
				 * Check if parent node of the store still exists
				 */
				 $category = Mage::getModel('catalog/category');
				 /* @var $category Mage_Catalog_Model_Category */
				 if ($category->checkId($parent)) {
					
					 $recursionLevel  = max(0, (int) Mage::app()->getWebsite(true)->getDefaultStore()->getConfig('catalog/navigation/max_depth'));
					 $storeCategories = $category->getCategories($parent, $recursionLevel, false, true, true);
					
					 $storeCategories =  $storeCategories->load()->addAttributeToSelect("*");
					 
					 //categories list
					 $catlist = $helper->getoutputList($parent,$storeCategories,"name","entity_id","parent_id");
					 $clist = "var mycats = new Array();  \n";

					 foreach($catlist as $id => $cat){
					    $category = Mage::getModel('catalog/category')->load($id);
					 	$url =  $category->getUrl();
					  
					    $clist .=  "mycats['".$url."']  = ".$id."\n";
					 }
				 }
				 
				 if(Mage::getStoreConfig("web/secure/use_in_adminhtml")){
                    $baseurl = Mage::getStoreConfig("web/secure/base_url");
				 }else{
                    $baseurl = Mage::getBaseUrl();  
				 }
				 if(!strpos($baseurl,"index.php")) $baseurl .= "index.php/";
				 $this->getUrl('*/*/index', array('groupid' => $this->getRequest()->getParam('id')));
		      

				$this->_formScripts[] = "
				      var baseurl = '".$baseurl."';
				     ".$clist.";
				      function saveAndContinueEdit(){
                         editForm.submit($('edit_form').action+'back/edit/');
                      }
                    
					  function updateurl(url){
					     url = url.replace(baseurl,'');
						 $('url').setValue(url);      
					  }
                      
                      function updatecatid(id){
                      	$('catid').setValue(id);
                      }

				      function getStatics()
					  {
						 var Form = document.forms.edit_form;
						
						 var statics = '';
						 var x = 0;
				         var first = 1;
						 for (x=0;x<Form.staticblocks.length;x++)
						 {
						    
							if (Form.staticblocks[x].selected)
							{
							 if(first) {
							    first = 0;
							 }else{
							    statics = statics + ',';
							 }
							 statics = statics  + Form.staticblocks[x].value ;
							}
						 }
					
						 return statics;
					  }
					  
                      function setStatics(exstring)
					  {
					   
						 var Form = document.forms.edit_form;
						 var y = 0;
						 var arr = exstring.split(',');
						
						 for(y=0;y<$('staticblocks').length;y++)
						 {
						      arr.each(function(value){
							     if($('staticblocks')[y].value == value)  $('staticblocks')[y].selected = true
							  })
						 }
						 
						 return true;
					  }
 
				   
				      var first = 1;
				      $('menugroup').observe('change',function(){
					           
						    	group = $('menugroup');
                                category = $('category');
                                cms = $('cms');
                                order = $('parent');
                                groupurl = baseurl+'/amzmegamenu/adminhtml_amzmegamenu/ajax?menugroup='+group.getValue()+'&menuid='+$('menu_id').getValue();
                                groupurl = groupurl+'&activecat='+category.getValue();
                                var cmstext = cms.selectedIndex >= 0 ? cms.options[cms.selectedIndex].innerHTML : undefined;
                             	groupurl = groupurl+'&activecms='+cmstext;
                             	
                             	new Ajax.Request(groupurl, {
								  onSuccess: function(response) {
								    // Handle the response content...
                                    var response = response.responseText.evalJSON();
								   
								    parentcategory = category.up('td');
								    parentcms = cms.up('td');
								    parentorder = order.up('td');
                                    Element.remove(category);
                                    Element.remove(cms);
                                    Element.remove(order);
                                    parentcategory.insert(response.category);
                                    parentcms.insert(response.cmspage);
                                    parentorder.insert(response.parent);
                                    //rebind change event for the category select
                                      
								       $('category').observe('change',function(){
							            $('link').setValue($('category').getValue());
							            updatecatid(mycats[$('link').getValue()]);
							          	updateurl($('link').getValue());
					                   });
                                   
                                       $('cms').observe('change',function(){
					                       $('link').setValue($('cms').getValue());
								           updateurl($('link').getValue());
					                   });
								    },
								    'Content-type':'application/json'
								});
										   
					  });
			          $('menutype').observe('change',function(){
					           
						    	type = $('menutype');
								if(first){
								   first = 0;
								}else{
								   $('link').value = '';
								}
								$('link').setStyle({'opacity':0.3});
						        if($(type).getValue() == 2){
									  $('category').up('tr').setStyle({'display':'none'});
									  $('cms').up('tr').setStyle({'display':'none'});
									  $('link').removeAttribute('readonly');
									  $('link').setStyle({'opacity':1});
							    }else if($(type).getValue() == 0){
								       $('cms').up('tr').setStyle({'display':'none'});
									   $('category').up('tr').setStyle({'display':''});
									   $('category').simulate('change');
									   $('link').setAttribute('readonly','true');
								}else if($(type).getValue() == 1){
								        $('category').up('tr').setStyle({'display':'none'});
									    $('cms').up('tr').setStyle({'display':''});
										 $('cms').simulate('change');
										$('link').setAttribute('readonly','true');
								}	
										   
					   });
					   $('category').observe('change',function(){
					            $('link').setValue($('category').getValue());
					            updatecatid(mycats[$('link').getValue()]);
					          	updateurl($('link').getValue());
					   });
					   $('menugroup').simulate('change');
					   $('cms').observe('change',function(){
					            $('link').setValue($('cms').getValue());
								updateurl($('link').getValue());
					   });
				       $('menutype').simulate('change');
					   
					   $('staticblocks').observe('change',function(){
					     	$('static_block').setValue(getStatics());
					   });
					 
					   setStatics($('static_block').getValue());
					   $$('.mega_subcontent').each(function(item){
					   
					        
					       $(item).observe('click',function(){
						      var itemvalue = $(item).getValue();  
						      if(itemvalue == 1){
							       $('staticblocks').up('tr').setStyle({'display':'none'});
								   $('contentxml').up('tr').setStyle({'display':'none'});
							  }else if(itemvalue == 2){
							       $('staticblocks').up('tr').setStyle({'display':'none'});
								   $('contentxml').up('tr').setStyle({'display':''});
							  }else{
							        $('staticblocks').up('tr').setStyle({'display':''});
								   $('contentxml').up('tr').setStyle({'display':'none'});
							  }
							  
						   });
						
						   if($(item).getValue() !== null){
						      $(item).simulate('click');
						   }
						   
					   })
                       
				    ";
			}
			
			public function getHeaderText()
			{
				if( Mage::registry('amzmegamenu_data') && Mage::registry('amzmegamenu_data')->getId() ) {
				   return Mage::helper('amzmegamenu')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('amzmegamenu_data')->getTitle()));
				} else {
				   return Mage::helper('amzmegamenu')->__('Add Menu Item');
			    }
			
		   }

		   public function getBackUrl()
           {
              
               if(Mage::registry('amzmegamenu_data')){
               	  $groupid = Mage::registry('amzmegamenu_data')->getData("menugroup");
               }
               if(!$groupid)  $groupid = Mage::app()->getRequest()->getParam('groupid');
               return $this->getUrl('*/*/index/groupid/'.$groupid);
           }
           public function getDeleteUrl()
           {
           	   if(Mage::registry('amzmegamenu_data')){
               	  $groupid = Mage::registry('amzmegamenu_data')->getData("menugroup");
               }
               if(!$groupid)  $groupid = Mage::app()->getRequest()->getParam('groupid');
               return $this->getUrl('*/*/delete', array($this->_objectId => $this->getRequest()->getParam($this->_objectId),"group" => $groupid));
           }
  }