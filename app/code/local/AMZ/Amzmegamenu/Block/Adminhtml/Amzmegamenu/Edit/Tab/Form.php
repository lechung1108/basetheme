<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */
	  
      class AMZ_Amzmegamenu_Block_Adminhtml_Amzmegamenu_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
	  {
	  
		protected function _prepareForm()
		{
		  
			 $form = new Varien_Data_Form(array(
                                      'id' => 'edit_form',
                                      'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                                      'method' => 'post',
        							  'enctype' => 'multipart/form-data'
                                   ));
			  
			 $collections = Mage::getModel('amzmegamenu/amzmegamenu')->getCollection()->addFieldToFilter("menu_id",array("neq" => $this->getRequest()->getParam('id')));
			 
			
			 $helper = Mage::helper('amzmegamenu');
	         
			 $statics =  Mage::getResourceModel('cms/block_collection')->load()->toOptionArray();
			 $menugroup = Mage::getModel('amzmegamenu/amzmegamenugroup')->getCollection()->toOptionArray();
			 //get out the orders array
			 if($this->getRequest()->getParam('id')) { 
			   $orders = $helper->getorders($this->getRequest()->getParam('id'));
			 }
			 //Get the menu items list		  
			 $parents = $helper->getoutputList(0,$collections,"title","menu_id","parent",true);
			 
			 //The root category id of default store 
	         $parent = Mage::app()->getWebsite(true)->getDefaultStore()->getRootCategoryId(); 
			 
	        /**
			 * Check if parent node of the store still exists
			 */
			 $category = Mage::getModel('catalog/category');
			 /* @var $category Mage_Catalog_Model_Category */
			 if (!$category->checkId($parent)) {
				
				return array();
			 }
	
			 $recursionLevel  = max(0, (int) Mage::app()->getWebsite(true)->getDefaultStore()->getConfig('catalog/navigation/max_depth'));
			 $storeCategories = $category->getCategories($parent, $recursionLevel, false, true, true);
			
			 $storeCategories =  $storeCategories->load()->addAttributeToSelect("*");
			 
			 //categories list
			 $catlist = $helper->getoutputList($parent,$storeCategories,"name","entity_id","parent_id");
			 $clist = array();
			 foreach($catlist as $id => $cat){
			    $category = Mage::getModel('catalog/category')->load($id);
				$url =  str_replace ( Mage::getBaseUrl() , "" , $category->getUrl() );
			    $clist[$url] = $cat; 
			 }

			 if(Mage::getStoreConfig("web/secure/use_in_adminhtml")){
                    $baseurl = Mage::getStoreConfig("web/secure/base_url");
				 }else{
                    $baseurl = Mage::getBaseUrl();  
			 }
			 if(!strpos($baseurl,"index.php")) $baseurl .= "index.php/";
			 //cmspages list 
			 $cmspages = array();
			 foreach($helper->getListcms() as $page){
				$cmspages[$page] = $page;
			 } 
			 
			  $this->setForm($form);
		      $fieldset = $form->addFieldset('amzmegamenu_form', array('legend'=>Mage::helper('amzmegamenu')->__('amzmegamenu information')));
			   
				$fieldset->addField('title', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Menu title'),
					'class' => 'required-entry',
					'required' => true,
					'name' => 'title',
				));
				$fieldset->addField('image', 'image', array(
						'label'     => Mage::helper('amzmegamenu')->__('Image File'),
						'required'  => false,
						'name'      => 'image',
				));
				$fieldset->addField('menualias', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Alias'),
					'required' => false,
					'name' => 'menualias',
				));
				
				$fieldset->addField('showtitle', 'radios', array(
					'label'     => Mage::helper('amzmegamenu')->__('Show title'),
					 'name'      => 'showtitle',
					 'values' => array(
										array('value'=>'1','label'=>'Yes'),
										array('value'=>'0','label'=>'No'),
								   ),
					 'disabled' => false,
					 'readonly' => false,
					 'tabindex' => 0
				));

				$fieldset->addField('menugroup', 'select', array(
						'label' => Mage::helper('amzmegamenu')->__('Display In'),
						'required' => true,
						'size' => 10,
						'name' => 'menugroup',
						'values' => $menugroup,
			    ));

				$fieldset->addField('status', 'select', array(
					'label' => Mage::helper('amzmegamenu')->__('Status'),
					'name' => 'status',
					'values' => array(
						array(
							'value' => 1,
							'label' => Mage::helper('amzmegamenu')->__('Active'),
						),
						array(
							'value' => 0,
							'label' => Mage::helper('amzmegamenu')->__('Inactive'),
							),
					),
				));
				if($this->getRequest()->getParam('id') ) {
					$fieldset->addField('ordering', 'select', array(
						'label' => Mage::helper('amzmegamenu')->__('Order'),
						'required' => false,
						'size' => 10,
						'name' => 'ordering',
						'values' => $orders,
					));
				}
				$fieldset->addField('parent', 'select', array(
					'label' => Mage::helper('amzmegamenu')->__('Parent'),
					'required' => false,
					'size' => 10,
					'name' => 'parent',
					'values' => $parents,
				));
				
				
				$fieldset->addField('menutype', 'select', array(
					'label' => Mage::helper('amzmegamenu')->__('Menu type'),
					'required' => false,
					'name' => 'menutype',
					'values' => array('0' => "Categories",'1' => "Cms pages",'2' => "Custom link"),
					'value' => 2,
				));
				
				$fieldset->addField('link', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Link'),
					'class' => 'required-entry',
					'required' => true,
					'name' => 'link',
				));
				$fieldset->addField('category', 'select', array(
					'label' => Mage::helper('amzmegamenu')->__('Select a category'),
					'required' => false,
					'size' => 10,
					'name' => 'category',
					'values' => $clist,
				));

				$fieldset->addField('shownumproduct', 'select', array(
					'label' => Mage::helper('amzmegamenu')->__('Show number products of Category'),
					'required' => false,
					'size' => 10,
					'name' => 'shownumproduct',
					'values' => array(
						array(
							'value' => 0,
							'label' => Mage::helper('amzmegamenu')->__('Use General Setting'),
						),
						array(
							'value' => 1,
							'label' => Mage::helper('amzmegamenu')->__('Enable'),
						),
						array(
							'value' => 2,
							'label' => Mage::helper('amzmegamenu')->__('Disable'),
							),
					),
				));
				
				$fieldset->addField('cms', 'select', array(
					'label' => Mage::helper('amzmegamenu')->__('Select a cms Page'),
					'required' => false,
					'size' => 10,
					'name' => 'cms',
					'values' => $cmspages,
				));
				$fieldset->addField('mega_cols', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Columns'),
					'required' => false,
					'width' => '100px',
					 'value' => '1',
					'name' => 'mega_cols',
				));
				
				$fieldset->addField('mega_group', 'radios', array(
					'label' => Mage::helper('amzmegamenu')->__('Group'),
					
					'required' => false,
					'name' => 'mega_group',
					'values' => array(
										array('value'=>'0','label'=>'No'),
										array('value'=>'1','label'=>'Yes'),
										
								   ),
				));
				$fieldset->addField('mega_width', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Submenu Width'),
					'required' => false,
					'name' => 'mega_width',
				));
				$fieldset->addField('mega_colw', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Submenu Column Width'),
					'required' => false,
					'name' => 'mega_colw',
				));
				$fieldset->addField('mega_colxw', 'textarea', array(
					'label' => Mage::helper('amzmegamenu')->__('Submenu Column[i] Width'),
					'required' => false,
					'style'     => 'width:300px; height:100px;',
					'name' => 'mega_colxw',
				));
				$fieldset->addField('mega_class', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Additional class'),
				    'required' => false,
					'name' => 'mega_class',
				));
				$fieldset->addField('browserNav', 'select', array(
					'label' => Mage::helper('amzmegamenu')->__('Target Window'),
					'required' => false,
					'size' => 10,
					'name' => 'browserNav',
					'values' =>  array(
										array('value'=>'0','label'=>'Parent'),
										array('value'=>'1','label'=>'New window With Navigation'),
										array('value'=>'2','label'=>'New without navigation'),
								   ),
				));
				
				$fieldset->addField('mega_subcontent', 'radios', array(
					'label'     => Mage::helper('amzmegamenu')->__('Submenu Content'),
					 'name'      => 'mega_subcontent',
					 'onclick' => "",
					 'onchange' => "",
					 'class'    => "mega_subcontent",
					 'value'  => '2',
					 'values' => array(
										array('value'=>'1','label'=>Mage::helper('amzmegamenu')->__('Child menu items'),'selected' => 'true'),
										array('value'=>'2','label'=>Mage::helper('amzmegamenu')->__('Custom xml block')),
										array('value'=>'3','label'=>Mage::helper('amzmegamenu')->__('Satic blocks')),
								   ),
					 'disabled' => false,
					 'readonly' => false,
					 'tabindex' => 0
				));
				
				$fieldset->addField('staticblocks', 'multiselect', array(
					'label' => Mage::helper('amzmegamenu')->__('Select Blocks'),
					'required' => false,
					'size' => 10,
					'name' => 'staticblocks',
					'values' => $statics,
				));
				
				$fieldset->addField('url', 'hidden', array(
					'required' => false,
					'size' => 10,
					'name' => 'url',
					
				));
                
                $fieldset->addField('catid', 'hidden', array(
					'required' => false,
					'size' => 10,
					'name' => 'catid',
					
				));

				$fieldset->addField('static_block', 'hidden', array(
					'required' => false,
					'size' => 10,
					'name' => 'static_block',
					
				));

				$fieldset->addField('menu_id', 'hidden', array(
					'required' => false,
					'size' => 10,
					'name' => 'menu_id',
					
				));

				$fieldset->addField('contentxml', 'editor', array(
				    'name'      => 'contentxml',
				    'label'     => Mage::helper('amzmegamenu')->__('Custom block xml'),
				     'title'     => Mage::helper('amzmegamenu')->__('Custom block xml'),
				     'style'     => 'width:400px; height:200px;',
				     'wysiwyg'   => false,
				     'required'  => false,
			     ));
				$fieldset->addField('description', 'editor', array(
						'label'     => Mage::helper('amzmegamenu')->__('Description'),
						'title'     => Mage::helper('amzmegamenu')->__('Description'),
						'style'     => 'width:400px; height:200px;',
						'required' => false,
						'wysiwyg'   => false,
						'name' => 'description',
				));

				// Append dependency javascript
				$this->setChild('form_after', $this->getLayout()
				    ->createBlock('adminhtml/widget_form_element_dependence')
				        ->addFieldMap('menutype', 'menutype')
				        ->addFieldMap('shownumproduct', 'shownumproduct')
				        ->addFieldDependence('shownumproduct', 'menutype', 0) // 2 = 'Specified'
				);
			
				if ( Mage::getSingleton('adminhtml/session')->getAmzMegamenuData() )
				{
				     
					$form->setValues(Mage::getSingleton('adminhtml/session')->getAmzMegamenuData());
					Mage::getSingleton('adminhtml/session')->setAmzMegamenuData(null);
				} elseif ( Mage::registry('amzmegamenu_data') ) { 
						
					$form->setValues(Mage::registry('amzmegamenu_data')->getData());
				}
				return parent::_prepareForm();
			}
			
}   