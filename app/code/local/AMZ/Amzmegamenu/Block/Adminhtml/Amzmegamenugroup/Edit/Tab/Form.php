<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */  
	  
      class AMZ_Amzmegamenu_Block_Adminhtml_Amzmegamenugroup_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
	  {
	  
		protected function _prepareForm()
		{
		  
			 $form = new Varien_Data_Form(array(
                                      'id' => 'edit_form',
                                      'action' => $this->getUrl('*/*/savegroup', array('id' => $this->getRequest()->getParam('id'))),
                                      'method' => 'post',
        							  'enctype' => 'multipart/form-data'
                                   ));
			
			 $helper = Mage::helper('amzmegamenu');
				 
			 $this->setForm($form);
		     $fieldset = $form->addFieldset('amzmegamenugroup_form', array('legend'=>Mage::helper('amzmegamenu')->__('amzmegamenu group information')));
			   
				$fieldset->addField('title', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Group title'),
					'class' => 'required-entry',
					'required' => true,
					'name' => 'title',
				));

                
                $fieldset->addField('menutype', 'text', array(
					'label' => Mage::helper('amzmegamenu')->__('Group Unique'),
					'class' => 'required-entry',
					'required' => true,
					'name' => 'menutype',
				));
                $stores = Mage::app()->getStores();

                $fieldset->addField('description', 'textarea', array(
					'label' => Mage::helper('amzmegamenu')->__('Group Description'),
					'name' => 'description',
				));
			
			    $fieldset->addField('storeid', 'select', array(
	                'name'      => 'storeid',
	                'label'     => Mage::helper('cms')->__('Store View'),
	                'title'     => Mage::helper('cms')->__('Store View'),
	                'required'  => true,
	                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, false),
	                'disabled'  => false
                ));
				if ( Mage::getSingleton('adminhtml/session')->getAmzMegamenugroupData() )
				{
				     
					$form->setValues(Mage::getSingleton('adminhtml/session')->getAmzMegamenugroupData());
					Mage::getSingleton('adminhtml/session')->setAmzMegamenugroupData(null);
				} elseif ( Mage::registry('amzmegamenugroup_data') ) { 
						
					$form->setValues(Mage::registry('amzmegamenugroup_data')->getData());
				}
				return parent::_prepareForm();
			}
			
}   