<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */  

  class AMZ_Amzmegamenu_Block_Adminhtml_Amzmegamenugroup_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
  {
			public function __construct()
			{
			parent::__construct();
				$this->_objectId = 'id';
				$this->_blockGroup = 'amzmegamenu';
				$this->_controller = 'adminhtml_amzmegamenugroup';
				$this->_updateButton('save', 'label', Mage::helper('amzmegamenu')->__('Save Menu Group'));
				$this->_updateButton('delete', 'label', Mage::helper('amzmegamenu')->__('Delete Menu Group'));
				$this->_addButton('saveandcontinue', array(
					'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
					'onclick'   => 'saveAndContinueEdit()',
					'class'     => 'save',
				), -100);
                $objId = $this->getRequest()->getParam($this->_objectId);

			    if (! empty($objId)) {
			        $this->_addButton('delete', array(
			            'label'     => Mage::helper('adminhtml')->__('Delete'),
			            'class'     => 'delete',
			            'onclick'   => 'deleteConfirm(\''. Mage::helper('adminhtml')->__('Are you sure you want to remove this menu group and menu items belong to it?')
			                .'\', \'' . $this->getDeleteUrl() . '\')',
			        ));
			    }


				$this->_formScripts[] = "
				      
				      function saveAndContinueEdit(){
                         editForm.submit($('edit_form').action+'back/editgroup/');
                      };

				    ";
			}
			
			public function getHeaderText()
			{
				if( Mage::registry('amzmegamenugroup_data') && Mage::registry('amzmegamenugroup_data')->getId() ) {
				   return Mage::helper('amzmegamenu')->__("Edit Group '%s'", $this->htmlEscape(Mage::registry('amzmegamenugroup_data')->getTitle()));
				} else {
				   return Mage::helper('amzmegamenu')->__('Add Menu Group');
			    }

			
		    }
		    public function getDeleteUrl()
            {
                  return $this->getUrl('*/*/deletegroup', array($this->_objectId => $this->getRequest()->getParam($this->_objectId)));
            }
  }