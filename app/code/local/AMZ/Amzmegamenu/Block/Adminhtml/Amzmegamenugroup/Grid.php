<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */
   class AMZ_Amzmegamenu_Block_Adminhtml_Amzmegamenugroup_Grid extends Mage_Adminhtml_Block_Widget_Grid
   {
       
		var $CurPage = 1;
		var $LastPage = 1;
		
		public function __construct()
		{
			parent::__construct();
			$this->setId('amzmegamenugroupGrid');
			$this->setDefaultSort('id');
			$this->setDefaultDir('ASC');
			$this->setSaveParametersInSession(true);
			
		}
		
		protected function _prepareCollection()
		{
		   
			$collections = Mage::getModel('amzmegamenu/amzmegamenugroup')->getCollection()->setOrder("id", "DESC");
			
			$this->setCollection($collections);
			parent::_prepareCollection();
			return $this;
		}
		
		protected function _prepareColumns()
		{
		    
			$this->addColumn('id', array(
				'header' => Mage::helper('amzmegamenu')->__('ID'),
				'align' =>'right',
				'width' => '50px',
				'index' => 'id',
			));
			$this->addColumn('title', array(
				'header' => Mage::helper('amzmegamenu')->__('Title'),
				'align' =>'left',
				'index' => 'title',
			));
		
			$this->addColumn('action',
				array(
				'header' => Mage::helper('amzmegamenu')->__('Action'),
				'width' => '100',
				'type' => 'action',
				'getter' => 'getId',
				'actions' => array(
				array(
				'caption' => Mage::helper('amzmegamenu')->__('Edit'),
				'url' => array('base'=> '*/*/editgroup'),
				'field' => 'id'
				)
				),
				'filter' => false,
				'sortable' => false,
				'index' => 'stores',
				'is_system' => true,
			));
			return parent::_prepareColumns();
		}
		public function getRowUrl($row)
		{
		    return $this->getUrl('*/*/index', array('groupid' => $row->getId()));
		}
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('menu_id');
			$this->getMassactionBlock()->setFormFieldName('amzmegamenu');
			$groupList = Mage::getSingleton('amzmegamenu/listmenugroup')->getOptionArray();
			$this->getMassactionBlock()->addItem('duplicate', array(
					'label'    => Mage::helper('amzmegamenu')->__('Duplicate'),
					'url'  => $this->getUrl('*/*/massDuplicateGroup', array('_current'=>true)),
					'additional' => array(
							'visibility' => array(
									'name' => 'duplicate_to',
									'type' => 'select',
									'class' => 'required-entry',
									'label' => Mage::helper('amzmegamenu')->__('Duplicate To'),
									'values' => $groupList
							)
					)
			));
			return $this;
		}
}