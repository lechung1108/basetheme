<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */ 
   class AMZ_Amzmegamenu_Block_Adminhtml_Amzmegamenu extends Mage_Adminhtml_Block_Widget_Grid_Container
   {
        public function __construct()
        {
			$this->_controller = 'adminhtml_amzmegamenu';
			$this->_blockGroup = 'amzmegamenu';
			$groupid = $this->getRequest()->getParam('groupid');
			$group = Mage::getModel('amzmegamenu/amzmegamenugroup')->load($groupid);
			$this->_headerText = Mage::helper('amzmegamenu')->__('Item Manager for '.$group->gettitle());
			$this->_addButtonLabel = Mage::helper('amzmegamenu')->__('Add Menu Item');
			
			parent::__construct();
        }

        public function getCreateUrl()
        {
          $groupid = $this->getRequest()->getParam('groupid');

          return $this->getUrl('*/*/new/',array("groupid" => $groupid));
        }
   }		
   
 