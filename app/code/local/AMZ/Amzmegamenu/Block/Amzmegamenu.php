<?php
/**
 * AMZ Mega-Menu Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.amzthemes.com/license.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    AMZ_Amzmegamenu
 * @author     AmzThemes <sales@amzthemes.com>
 * @copyright  2014 AMZ Solution
 * @license    http://www.amzthemes.com/license
 * @version    2.0.3
 * @link       http://www.amzthemes.com
 */  
    require_once "megamenu/mega.class.php";
    class AMZ_Amzmegamenu_Block_Amzmegamenu extends Mage_Page_Block_Html_Topmenu
	{
		
			var $children = null;
			var $items = null;
			var $menu = null;
			var $open = null;
			public function __construct($attributes = array()) {
			  
			   parent::__construct ();
              
              
			    $this->addData(array(
                   'cache_lifetime' => null,
               ));

			   $this->menu = new AMZMenuMega();
			   $this->params = new stdclass(); 
			   
			   $this->params->megamenu = 1;
			   $this->params->startlevel = 0;
			   $this->params->endlevel = 10;
			}
			
			public function _prepareLayout()
			{
			  
				$headBlock = $this->getLayout()->getBlock('head');
				return parent::_prepareLayout();
			}
            
			
			public function _toHtml(){
			  
			   if(!Mage::helper('amzmegamenu')->get('show')){
			     return parent::_toHtml();
			   }
			   $this->setTemplate("amzthemes/amzmegamenu/output.phtml");
			   $storeid = Mage::app()->getStore()->getStoreId();
			   $resource = Mage::getSingleton('core/resource');
               $read= $resource->getConnection('core_read');


               $menutable = $resource->getTableName('amzmegamenu_store_menugroup');
               $query = 'SELECT menugroupid '
                . ' FROM '.$menutable
                . ' WHERE store_id = '.(int) $storeid
              
                . ' ORDER BY id';
               $rows = $read->fetchRow($query); 

               if(!$rows["menugroupid"]){
               	 $rows["menugroupid"] = 0;
               }
               $collections = Mage::getModel('amzmegamenu/amzmegamenu')->getCollection()->setOrder("parent", "ASC")->setOrder("ordering","ASC")->addFilter("status",1,"eq")->addFilter("menugroup",$rows["menugroupid"]);

			   $tree = array();
			   foreach($collections as $collection){
			       $collection->tree = array();
				   $parent_tree  = array();
				   if(isset($tree[$collection->parent])){
				   	 $parent_tree = $tree[$collection->parent];
				   }
					//Create tree
				   array_push($parent_tree, $collection->menu_id);
				   $tree[$collection->menu_id] = $parent_tree;

				   $collection->tree   = $parent_tree;
			   }
			   $this->menu->getList($collections);
			   
			   ob_start();
                 $this->menu->genMenu();
                 $menuoutput = ob_get_contents();
                 $this->assign ( 'menuoutput', $menuoutput );
               ob_end_clean();
               return parent::_toHtml ();
			}
			
	 }

?>