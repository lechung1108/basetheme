<?php
$installer = $this;

$installer->startSetup();
	$installer->run("
	ALTER TABLE {$this->getTable('amzmegamenu')} ADD COLUMN `image` varchar(255) DEFAULT '';
	ALTER TABLE {$this->getTable('amzmegamenu')} ADD COLUMN `description` text DEFAULT ''
	");
$installer->endSetup();
