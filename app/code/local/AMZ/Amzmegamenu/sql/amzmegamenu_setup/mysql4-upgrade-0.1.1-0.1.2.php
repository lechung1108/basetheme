<?php
$installer = $this;

$installer->startSetup();
	$installer->run("
	ALTER TABLE {$this->getTable('amzmegamenu')} ADD COLUMN `shownumproduct` smallint(6) DEFAULT '0';
	");
$installer->endSetup();
