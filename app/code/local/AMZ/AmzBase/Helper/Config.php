<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://amzthemes.com/license  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     AMZThemes <sales@amzthemes.com>
 * @copyright  2014 AMZThemes
 * @license    http://amzthemes.com/license
 * @version    1.0.1
 * @link       http://amzthemes.com
 */

class AMZ_AmzBase_Helper_Config extends Mage_Core_Helper_Abstract{
	/** Extensions feed path */
    const EXTENSIONS_FEED_URL = 'http://amzthemes.com/feed';
	/** Updates Feed path */
    const UPDATES_FEED_URL = 'http://amzthemes.com/feed';
	    /** Store URL */
    const STORE_URL = 'http://amzthemes.com';

    /** Store response cache key*/
    const STORE_RESPONSE_CACHE_KEY = 'amz_store_response_cache_key';
	
	// Path and directory of the automatically generated CSS
    protected $_generatedCssFolder;
    protected $_generatedCssPath;
    protected $_generatedCssDir;
    
    public function __construct()
    {
        //Create paths
        $this->_generatedCssFolder = '_config/';
        $this->_generatedCssPath = 'frontend/default/default/' . $this->_generatedCssFolder;
        $this->_generatedCssDir = Mage::getBaseDir('skin') . '/' . $this->_generatedCssPath;
    }
    
    // Get automatically generated CSS directory
    public function getGeneratedCssDir()
    {
        return $this->_generatedCssDir;
    }

    // Get configuration css file path
    public function getCssFile()
    {
        return $this->_generatedCssFolder . 'store_' . Mage::app()->getStore()->getCode() . '.css';
    }
    
    // Get responsive css file path
    public  function getCssResponsiveFile() 
    {
        if (Mage::getStoreConfig('amzconfig/general/active_responsive'))
            return 'css/styles-amzbase-responsive.css';
        return 'css/empty.css';
    }  
    
    // Get skin responsive css file path
    public  function getCssSkinResponsiveFile() 
    {
        if (Mage::getStoreConfig('amzconfig/general/active_responsive'))
            return 'css/skin-responsive.css';
        return 'css/empty.css';
    } 
	
}
