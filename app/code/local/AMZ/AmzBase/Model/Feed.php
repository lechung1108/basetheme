<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://amzthemes.com/license  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     AMZThemes <sales@amzthemes.com>
 * @copyright  2014 AMZThemes
 * @license    http://amzthemes.com/license
 * @version    1.0.1
 * @link       http://amzthemes.com
 */

class AMZ_AmzBase_Model_Feed extends Mage_AdminNotification_Model_Feed
{
    const XML_USE_HTTPS_PATH    = 'amzbase/feed/use_https';
    const XML_FEED_URL_PATH     = 'amzbase/feed/url';
    const XML_FREQUENCY_PATH    = 'amzbase/feed/check_frequency';
	const XML_FREQUENCY_ENABLE    = 'amzbase/feed/enabled';
    const XML_LAST_UPDATE_PATH  = 'amzbase/feed/last_update';

	
	public static function check(){
		return Mage::getModel('amzbase/feed')->checkUpdate();
	}
	
    public function getFrequency(){
        return 86400 * 3600;
    }

    public function getLastUpdate(){
        return Mage::app()->loadCache('amzbase_notifications_lastcheck');
    }

    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), 'amzbase_notifications_lastcheck');
        return $this;
    }
    public function getFeedUrl(){
        if (is_null($this->_feedUrl)) {
            $this->_feedUrl = (Mage::getStoreConfigFlag(self::XML_USE_HTTPS_PATH) ? 'https://' : 'http://')
                . Mage::getStoreConfig(self::XML_FEED_URL_PATH);
        }
        return $this->_feedUrl;
    }

    public function checkUpdate(){
        if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
            return $this;
        }

        $feedData = array();

        $feedXml = $this->getFeedData();

        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            foreach ($feedXml->channel->item as $item) {
                $feedData[] = array(
                    'severity'      => (int)$item->severity ? (int)$item->severity : 3,
                    'date_added'    => $this->getDate((string)$item->pubDate),
                    'title'         => (string)$item->title,
                    'description'   => (string)$item->description,
                    'url'           => (string)$item->link,
                );
            }
            if ($feedData) {
                Mage::getModel('adminnotification/inbox')->parse(array_reverse($feedData));
            }

        }
        $this->setLastUpdate();

        return $this;
    }

}
