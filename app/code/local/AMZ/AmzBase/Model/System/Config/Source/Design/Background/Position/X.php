<?php

class AMZ_AmzBase_Model_System_Config_Source_Design_Background_Position_X
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'left',    'label' => Mage::helper('amzbase')->__('left')),
            array('value' => 'center',  'label' => Mage::helper('amzbase')->__('center')),
            array('value' => 'right',   'label' => Mage::helper('amzbase')->__('right'))
        );
    }
}
