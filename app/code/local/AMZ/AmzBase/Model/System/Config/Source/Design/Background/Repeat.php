<?php

class AMZ_AmzBase_Model_System_Config_Source_Design_Background_Repeat
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'no-repeat',   'label' => Mage::helper('amzbase')->__('no-repeat')),
            array('value' => 'repeat',      'label' => Mage::helper('amzbase')->__('repeat')),
            array('value' => 'repeat-x',    'label' => Mage::helper('amzbase')->__('repeat-x')),
            array('value' => 'repeat-y',    'label' => Mage::helper('amzbase')->__('repeat-y'))
        );
    }
}
