<?php

class AMZ_AmzBase_Model_System_Config_Source_Setting_Category_Align
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'left', 'label' => Mage::helper('amzbase')->__('Left')),
            array('value' => 'right', 'label' => Mage::helper('amzbase')->__('Right')),
            array('value' => 'center', 'label' => Mage::helper('amzbase')->__('Center'))
        );
    }
}