<?php

class AMZ_AmzBase_Model_System_Config_Source_Design_Background_Position_Y
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'top',     'label' => Mage::helper('amzbase')->__('top')),
            array('value' => 'center',  'label' => Mage::helper('amzbase')->__('center')),
            array('value' => 'bottom',  'label' => Mage::helper('amzbase')->__('bottom'))
        );
    }
}
