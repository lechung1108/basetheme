<?php

class AMZ_AmzBase_Model_System_Config_Source_Design_Background_Mode
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'image',     'label' => Mage::helper('amzbase')->__('Image')),
            array('value' => 'texture',   'label' => Mage::helper('amzbase')->__('Texture'))
        );
    }
}
