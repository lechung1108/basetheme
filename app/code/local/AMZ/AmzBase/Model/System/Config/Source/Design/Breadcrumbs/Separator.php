<?php

class AMZ_AmzBase_Model_System_Config_Source_Design_Breadcrumbs_Separator
{
    public function toOptionArray()
    {
		return array(
			array('value' => '|', 'label' => Mage::helper('amzbase')->__('|')),
			array('value' => '/', 'label' => Mage::helper('amzbase')->__('/')),
			array('value' => '&gt;', 'label' => Mage::helper('amzbase')->__('>'))
        );
    }
}