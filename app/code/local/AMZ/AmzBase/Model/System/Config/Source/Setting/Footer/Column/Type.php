<?php

class AMZ_AmzBase_Model_System_Config_Source_Setting_Footer_Column_Type
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'custom', 'label' => Mage::helper('amzbase')->__('Static Block')),
            array('value' => 'default', 'label' => Mage::helper('amzbase')->__('Default Links')),
            array('value' => 'twitter', 'label' => Mage::helper('amzbase')->__('Twitter Tweets')),
            array('value' => 'custom', 'label' => Mage::helper('amzbase')->__('Static Block'))
        );
    }
}