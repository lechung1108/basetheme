<?php

class AMZ_AmzBase_Model_System_Config_Source_Design_Icon_Type
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'white/', 'label' => Mage::helper('amzbase')->__('White')),
            array('value' => 'grey/', 'label' => Mage::helper('amzbase')->__('Grey')),
            array('value' => 'black/', 'label' => Mage::helper('amzbase')->__('Black'))
        );
    }
}