<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://amzthemes.com/license  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     AMZThemes <sales@amzthemes.com>
 * @copyright  2014 AMZThemes
 * @license    http://amzthemes.com/license
 * @version    1.0.1
 * @link       http://amzthemes.com
 */

class AMZ_AmzBase_Model_Feed_Extensions extends AMZ_AmzBase_Model_Feed_Abstract{
	
	 /**
     * Retrieve feed url
     *
     * @return string
     */
    public function getFeedUrl(){
		return AMZ_AmzBase_Helper_Config::EXTENSIONS_FEED_URL;
    }
	
	
	/**
	 * Checks feed
	 * @return 
	 */
	public function check(){
		if(!(Mage::app()->loadCache('amzbase_extensions_feed')) || (time()-Mage::app()->loadCache('amzbase_extensions_feed_lastcheck')) > 86400){
			$this->refresh();
		}
	}
	
	public function refresh(){
		$exts = array();
		try{
			$Node = $this->getFeedData();
			if(!$Node) return false;
			foreach($Node->children() as $ext){
				$exts[(string)$ext->name] = array(
					'display_name' => (string)$ext->display_name,
					'version' => (string)$ext->version,
					'url'		=> (string)$ext->url
				);
			}
			Mage::app()->saveCache(serialize($exts), 'amzbase_extensions_feed');
			Mage::app()->saveCache(time(), 'amzbase_extensions_feed_lastcheck');
			return true;
		}catch(Exception $E){
			return false;
		}
	}
}