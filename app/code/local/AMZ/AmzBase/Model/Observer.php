<?php
/**
 * Call actions after configuration is saved
 */
class AMZ_AmzBase_Model_Observer
{
    // After any system config is saved
    public function amzbase_controllerActionPostdispatchAdminhtmlSystemConfigSave()
    {
        $section = Mage::app()->getRequest()->getParam('section');
        
        $websiteCode = Mage::app()->getRequest()->getParam('website');
        $storeCode = Mage::app()->getRequest()->getParam('store');
        
        Mage::getSingleton('amzbase/config_generator')->generateCss($websiteCode, $storeCode);
    }
    
    // After store view is saved
    public function amzbase_storeEdit(Varien_Event_Observer $observer)
    {
        $store = $observer->getEvent()->getStore();
        $storeCode = $store->getCode();
        $websiteCode = $store->getWebsite()->getCode();
        
        Mage::getSingleton('amzbase/config_generator')->generateCss($websiteCode, $storeCode);
    }
}
