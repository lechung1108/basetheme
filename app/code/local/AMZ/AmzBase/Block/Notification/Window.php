<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://amzthemes.com/license  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     AMZThemes <sales@amzthemes.com>
 * @copyright  2014 AMZThemes
 * @license    http://amzthemes.com/license
 * @version    1.0.1
 * @link       http://amzthemes.com
 */
 
class AMZ_AmzBase_Block_Notification_Window extends Mage_Adminhtml_Block_Notification_Window{
	protected function _construct(){
        parent::_construct();

		if(!Mage::getStoreConfig('amzbase/install/run')){
       		$c = Mage::getModel('core/config_data');
			$c->setScope('default')
				->setPath('amzbase/install/run')
				->setValue(time())
				->save();
			$this->setHeaderText($this->__("AMZ Notifications Setup"));	
       		$this->setIsFirstRun(1);
			$this->setIsHtml(1);
       
	   }
    }
	
	protected function _toHtml(){
		 if($this->getIsHtml()){
		 	$this->setTemplate('amzthemes/amzbase/notification/window.phtml');
		 }
		 return parent::_toHtml();
	}
	public function presetFirstSetup(){
		
	}
	public function getNoticeMessageText(){
		return $this->getData('notice_message_text');
	}
	
	
}
