<?php
/**
 * Extensions Manager Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://amzthemes.com/license  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@amzthemes.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    ExtensionManager
 * @author     AMZThemes <sales@amzthemes.com>
 * @copyright  2014 AMZThemes
 * @license    http://amzthemes.com/license
 * @version    1.0.1
 * @link       http://amzthemes.com
 */
 
class AMZ_AmzBase_Block_Page_Html_Head extends Mage_Page_Block_Html_Head
{
	 /**
     * Initialize template
     *
     */
    protected function _construct()
    {
        $this->setTemplate('page/html/head.phtml');
		if(Mage::getStoreConfig('amzconfig/general/include_jquery')) {
			$this->_data['items']['js/amzthemes/amzbase/jquery-1.8.2.min.js'] = array(
				'type'   => 'js',
				'name'   => 'amzthemes/amzbase/jquery-1.8.2.min.js',
				'params' => '',
				'if'     => '',
				'cond'   => '',
			);
		}
		if(Mage::getStoreConfig('amzconfig/general/include_jquery_noconflict')) {
			$this->_data['items']['js/amzthemes/amzbase/no-conflict.js'] = array(
				'type'   => 'js',
				'name'   => 'amzthemes/amzbase/no-conflict.js',
				'params' => '',
				'if'     => '',
				'cond'   => '',
			);
		}
    }
	
}
