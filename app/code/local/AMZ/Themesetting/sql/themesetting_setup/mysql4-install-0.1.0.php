<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$installer->getTable('themesetting/themesetting')};

CREATE TABLE {$installer->getTable('themesetting/themesetting')} (
  `theme_id` int(11) unsigned NOT NULL auto_increment,
  `stores` varchar(255) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `layout` varchar(255) NOT NULL default '',
  `template` varchar(255) NOT NULL default '',
  `skin` varchar(255) NOT NULL default '',
  `status` tinyint(1) NOT NULL default '2',
  `description` text NULL,
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$installer->getTable('themesetting/themebrowser')};
CREATE TABLE {$installer->getTable('themesetting/themebrowser')} (
  `theme_browser_id` int(11) unsigned NOT NULL auto_increment,
  `theme_id` int(11) unsigned NOT NULL,
  `browser` varchar(255) NOT NULL default '',
  INDEX (`theme_id`),
  FOREIGN KEY (`theme_id`) REFERENCES {$installer->getTable('themesetting/themesetting')} (`theme_id`) ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY KEY (`theme_browser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$installer->getTable('themesetting/themeplatform')};
CREATE TABLE {$installer->getTable('themesetting/themeplatform')} (
  `theme_platform_id` int(11) unsigned NOT NULL auto_increment,
  `theme_id` int(11) unsigned NOT NULL,
  `platform` varchar(255) NOT NULL default '',
  INDEX (`theme_id`),
  FOREIGN KEY (`theme_id`) REFERENCES {$installer->getTable('themesetting/themesetting')} (`theme_id`) ON UPDATE CASCADE ON DELETE CASCADE,  
  PRIMARY KEY (`theme_platform_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$installer->getTable('themesetting/installer')};
CREATE TABLE {$installer->getTable('themesetting/installer')} (
  `installer_id` int(11) unsigned NOT NULL auto_increment,
  `theme_name` varchar(96) NOT NULL default '',
  `created_time` datetime NULL,
  PRIMARY KEY (`installer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup(); 