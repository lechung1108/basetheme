<?php

class AMZ_Themesetting_Adminhtml_ThemeinstallerController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('themesetting/install')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Theme Installation'), Mage::helper('adminhtml')->__('Theme Installation'));
		
		return $this;
	}  
	
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function runAction() {
		$session = Mage::getSingleton('adminhtml/session');
		if($data = $this->getRequest()->getPost()){
			try{
				$model = explode('_', $data['theme']);
				Mage::getModel('themesetting/template_'.$model[0])->install($data['theme']);
				$session->addSuccess(Mage::helper('adminhtml')->__('The Theme has been installed.'));
			}catch(Exception $e){
				$session->addError(Mage::helper('adminhtml')->__('There was an error occured, please re-install.'));
			}
		} else {
			$session->addError(Mage::helper('adminhtml')->__('Please select theme to install.'));
		}
        $this->_redirect('*/*/');
	}

}