<?php

class AMZ_Themesetting_Model_Mysql4_Themeplatform extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
		// Note that the theme_platform_id refers to the key field in your database table.
        $this->_init('themesetting/themeplatform', 'theme_platform_id');
    }
}