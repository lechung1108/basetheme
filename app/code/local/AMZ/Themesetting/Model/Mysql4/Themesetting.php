<?php

class AMZ_Themesetting_Model_Mysql4_Themesetting extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the theme_id refers to the key field in your database table.
        $this->_init('themesetting/themesetting', 'theme_id');
    }
	protected function _afterLoad(Mage_Core_Model_Abstract $object)
	{
		//Load Platform
	    $platforms = array();

        $themePlatforms = Mage::getResourceModel('themesetting/themeplatform_collection')
                ->addFieldToFilter('theme_id', $object->getId());
        if (count($themePlatforms))
            foreach ($themePlatforms as $themePlatform) {
                $platforms[] = $themePlatform->getPlatform();
            }
        $object->setPlatform(implode(',', $platforms));
		
        //Load Browser
		$browsers = array();

        $themeBrowsers = Mage::getResourceModel('themesetting/themebrowser_collection')
                ->addFieldToFilter('theme_id', $object->getId());
        if (count($themeBrowsers))
            foreach ($themeBrowsers as $themeBrowser) {
                $browsers[] = $themeBrowser->getBrowser();
            }
        $object->setBrowser(implode(',', $browsers));
		return parent::_afterLoad($object);
	}
	public function getInstalledThemes()
	{
		$db = $this->_getReadAdapter();
		$select = $db->select('*')
            ->from($this->getTable('themesetting/installer'));
		return $db->fetchAll($select);
	}
}