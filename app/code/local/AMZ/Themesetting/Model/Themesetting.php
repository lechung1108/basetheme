<?php

class AMZ_Themesetting_Model_Themesetting extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('themesetting/themesetting');
    }
	public function loadByBrowserAndStore($browser,$platform,$store){
        
         $collection = $this->getJoinedCollection()
                ->addFieldToFilter('browser', $browser)
                ->addFieldToFilter('platform', $platform)
                ->addFieldToFilter('status', 1)
                ->addStoresFilter($store)
                ->setOrder('theme_id', 'DESC')
        ;
         
        if ($collection->getSize())
            return $collection->getFirstItem();
        $collection = $this->getJoinedCollection()
                ->addFieldToFilter('browser', 'all')
                ->addFieldToFilter('platform', $platform)
                ->addFieldToFilter('status', 1)
                ->addStoresFilter($store)
                ->setOrder('theme_id', 'DESC');
        
        if ($collection->getSize())
            return $collection->getFirstItem();

        $collection = $this->getJoinedCollection()
                ->addFieldToFilter('browser', $browser)
                ->addFieldToFilter('platform', 'all')
                ->addFieldToFilter('status', 1)
                ->addStoresFilter($store)
                ->setOrder('theme_id', 'DESC');
        if ($collection->getSize())
            return $collection->getFirstItem();

        $collection = $this->getJoinedCollection()
                ->addFieldToFilter('browser', 'all')
                ->addFieldToFilter('platform', 'all')
                ->addFieldToFilter('status', 1)
                ->addStoresFilter($store)
                ->setOrder('theme_id', 'DESC');
       
        if ($collection->getSize())
            return $collection->getFirstItem();

        return $this->setId(null);
    }
    public function loadByBrowser($browser, $platform) {
        $collection = $this->getJoinedCollection()
                ->addFieldToFilter('browser', $browser)
                ->addFieldToFilter('platform', $platform)
                ->addFieldToFilter('status', 1)
                ->setOrder('theme_id', 'DESC')
        ;
        if ($collection->getSize())
            return $collection->getFirstItem();

        $collection = $this->getJoinedCollection()
                ->addFieldToFilter('browser', 'all')
                ->addFieldToFilter('platform', $platform)
                ->addFieldToFilter('status', 1)
                ->setOrder('theme_id', 'DESC');
        if ($collection->getSize())
            return $collection->getFirstItem();

        $collection = $this->getJoinedCollection()
                ->addFieldToFilter('browser', $browser)
                ->addFieldToFilter('platform', 'all')
                ->addFieldToFilter('status', 1)
                ->setOrder('theme_id', 'DESC');
        if ($collection->getSize())
            return $collection->getFirstItem();

        $collection = $this->getJoinedCollection()
                ->addFieldToFilter('browser', 'all')
                ->addFieldToFilter('platform', 'all')
                ->addFieldToFilter('status', 1)
                ->setOrder('theme_id', 'DESC');
        if ($collection->getSize())
            return $collection->getFirstItem();

        return $this->setId(null);
    }
	
    public function getJoinedCollection() {
        $resource = Mage::getSingleton('core/resource');
        $themeBrowserTable = $resource->getTableName('amz_theme_browser');
        $themePlatformTable = $resource->getTableName('amz_theme_platform');
        $collection = Mage::getResourceModel('themesetting/themesetting_collection');
        $collection->getSelect()
                ->join($themeBrowserTable, 'main_table.theme_id=' . $themeBrowserTable . '.theme_id', array('browser' => 'browser'))
                ->join($themePlatformTable, 'main_table.theme_id=' . $themePlatformTable . '.theme_id', array('platform' => 'platform'))
        ;
        return $collection;
    }

    public function addBrowser() {
        $themeBrowser = Mage::getModel('themesetting/themebrowser');
        $browsers = $this->getBrowser();
        if (isset($browsers[0]) && $browsers[0] == 'all') {
            $browsers = array_keys(Mage::helper('themesetting')->getBrowserList());
            $browsers = array('all');
        }
        if (count($browsers)) {
            foreach ($browsers as $browser) {
                $themeBrowser->loadByTheme($this->getId(), $browser);
                $themeBrowser->setThemeId($this->getId())
                        ->setBrowser($browser)
                        ->setStoreId($this->getStoreId())
                        ->save()
                        ->setId(null);
            }
        } else {
            $browsers = array(null);
        }
        $themeBrowsers = $themeBrowser->getCollection()
                ->addFieldToFilter('theme_id', $this->getId())
                ->addFieldToFilter('browser', array('nin' => $browsers))
        ;
        if (count($themeBrowsers)) {
            foreach ($themeBrowsers as $themeBrowser) {
                $themeBrowser->delete();
            }
        }
        return $this;
    }

    public function addPlatform() {
        $themePlatform = Mage::getModel('themesetting/themeplatform');
        $platforms = $this->getPlatform();
        if (isset($platforms[0]) && $platforms[0] == 'all') {
            $platforms = array_keys(Mage::helper('themesetting')->getPlatformList());
            $platforms = array('all');
        }
        if (count($platforms)) {
            foreach ($platforms as $platform) {
                $themePlatform->loadByTheme($this->getId(), $platform);
                $themePlatform->setThemeId($this->getId())
                        ->setPlatform($platform)
                        ->setStoreId($this->getStoreId())
                        ->save()
                        ->setId(null);
            }
        } else {
            $platforms = array(null);
        }
        $themePlatforms = $themePlatform->getCollection()
                ->addFieldToFilter('theme_id', $this->getId())
                ->addFieldToFilter('platform', array('nin' => $platforms))
        ;
        if (count($themePlatforms)) {
            foreach ($themePlatforms as $themePlatform) {
                $themePlatform->delete();
            }
        }
        return $this;
    }
}