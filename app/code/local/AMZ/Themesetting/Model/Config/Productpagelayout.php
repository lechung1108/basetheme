<?php 

class AMZ_Themesetting_Model_Config_Productpagelayout
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'productpage_small', 'label'=>Mage::helper('themesetting')->__('Small')),
            array('value'=>'productpage_medium', 'label'=>Mage::helper('themesetting')->__('Medium')),
            array('value'=>'productpage_large', 'label'=>Mage::helper('themesetting')->__('Large')),
            array('value'=>'productpage_extralarge', 'label'=>Mage::helper('themesetting')->__('Extra large'))
        );
    }

}