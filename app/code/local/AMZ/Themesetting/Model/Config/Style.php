<?php

class AMZ_Themesetting_Model_Config_Style
{
	/**
	 * fonts weight list
	 *
	 * @var string
	 */
	private $gfontsstyle = "inherit,initial,italic,normal,oblique";

    public function toOptionArray()
    {
	    $fontstyle = explode(',', $this->gfontsstyle);
	    $options = array();
	    foreach ($fontstyle as $f ){
		    $options[] = array(
			    'value' => $f,
			    'label' => $f,
		    );
	    }

        return $options;
    }

}
