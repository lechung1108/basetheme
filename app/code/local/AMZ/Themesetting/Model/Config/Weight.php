<?php

class AMZ_Themesetting_Model_Config_Weight
{
	/**
	 * fonts weight list
	 *
	 * @var string
	 */
	private $gfontsweight = "100,300,400,500,700,800,900";

    public function toOptionArray()
    {
	    $fontsweight = explode(',', $this->gfontsweight);
	    $options = array();
	    foreach ($fontsweight as $f ){
		    $options[] = array(
			    'value' => $f,
			    'label' => $f,
		    );
	    }

        return $options;
    }

}
