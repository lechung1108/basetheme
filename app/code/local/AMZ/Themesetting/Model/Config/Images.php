<?php

class AMZ_Themesetting_Model_Config_Images
{
	/**
	 * Images list
	 *
	 * @var string
	 */
	private $paternimages = "bg0,bg1,bg2,bg3,bg4,bg5,bg6,bg7,bg8,bg9,bg10,bg11,bg12,bkg_body";

    public function toOptionArray()
    {
	    $images = explode(',', $this->paternimages);
	    $options = array();
	    foreach ($images as $f ){
		    $options[] = array(
			    'value' => $f,
			    'label' => $f,
		    );
	    }

        return $options;
    }

}
