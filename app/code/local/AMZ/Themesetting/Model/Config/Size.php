<?php

class AMZ_Themesetting_Model_Config_Size
{
	/**
	 * fonts size list
	 *
	 * @var string
	 */
	private $gfontsize = "8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50";

    public function toOptionArray()
    {
	    $fontsize = explode(',', $this->gfontsize);
	    $options = array();
	    foreach ($fontsize as $f ){
		    $options[] = array(
			    'value' => $f,
			    'label' => $f,
		    );
	    }

        return $options;
    }

}
