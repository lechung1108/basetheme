<?php 

class AMZ_Themesetting_Model_Config_Layout
{
    public function toOptionArray()
    {
        return array(
            array('value'=>'sidebar_left', 'label'=>Mage::helper('themesetting')->__('Left')),
            array('value'=>'sidebar_right', 'label'=>Mage::helper('themesetting')->__('Right'))          
        );
    }

}