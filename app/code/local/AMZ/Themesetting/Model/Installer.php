<?php

class AMZ_Themesetting_Model_Installer extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
		$this->_init('themesetting/installer');
    }
	public function isInstalled()
	{
		$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
		return $installer->getConnection()->fetchRow("select * from " . $installer->getTable('themesetting/installer') . "");
	}
	public function install()
	{
		$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
		$installer->startSetup();
		if(!$this->isInstalled()){
			$now = Mage::app()->getLocale()->date()
				->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE)
				->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

			$installer->getConnection()->insert($installer->getTable('cms/block'), array(
				'title' => 'Bestseller',
				'identifier' => 'catalog_bestseller',
				'content' => '{{block type="themesetting/catalog_bestseller" name="catalog.bestseller" template="themesetting/bestseller.phtml"}}',
				'creation_time' => $now,
				'update_time' => $now,
				'is_active' => 1
			));

			$blockId = $installer->getConnection()->lastInsertId();
			$select = $installer->getConnection()->select()
				->from($installer->getTable('core/store'), array('block_id' => new Zend_Db_Expr($blockId), 'store_id'))
				->where('store_id > 0');

			$installer->run($select->insertFromSelect($installer->getTable('cms/block_store')), array('block_id', 'store_id'));
			
			$installer->setConfigData('design/package/name', 'default');
			
			$installer->setConfigData('design/theme/template', 'default');
			
			$installer->setConfigData('design/theme/skin', 'default');
			
			$installer->setConfigData('design/theme/layout', 'default');

			$installer->getConnection()->insert($installer->getTable('themesetting/installer'), array('created_time' => $now));
		}
		$installer->endSetup();
	}
}