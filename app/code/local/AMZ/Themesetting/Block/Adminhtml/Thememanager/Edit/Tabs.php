<?php

class AMZ_Themesetting_Block_Adminhtml_Thememanager_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('themesetting_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('themesetting')->__('Theme Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('themesetting')->__('Theme Information'),
          'title'     => Mage::helper('themesetting')->__('Theme Information'),
          'content'   => $this->getLayout()->createBlock('themesetting/adminhtml_thememanager_edit_tab_form')->toHtml(),
      ));
	  
      $this->addTab('browser_section', array(
          'label'     => Mage::helper('themesetting')->__('Browser Information'),
          'title'     => Mage::helper('themesetting')->__('Browser Information'),
          'content'   => $this->getLayout()->createBlock('themesetting/adminhtml_thememanager_edit_tab_browsers')->toHtml(),
      ));

      $this->addTab('platform_section', array(
          'label'     => Mage::helper('themesetting')->__('Platform Information'),
          'title'     => Mage::helper('themesetting')->__('Platform Information'),
          'content'   => $this->getLayout()->createBlock('themesetting/adminhtml_thememanager_edit_tab_platforms')->toHtml(),
      ));	  
     
      return parent::_beforeToHtml();
  }
}