<?php

class AMZ_Themesetting_Block_Adminhtml_Thememanager_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('thememanagerGrid');
      $this->setDefaultSort('theme_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('themesetting/themesetting')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('theme_id', array(
          'header'    => Mage::helper('themesetting')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'theme_id',
      ));

      $this->addColumn('title', array(
          'header'    => Mage::helper('themesetting')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));

      $this->addColumn('template', array(
          'header'    => Mage::helper('themesetting')->__('Template'),
          'align'     => 'left',
		  'type'      => 'options',
		  'options'   => Mage::helper('themesetting')->getTemplateList(),
          'index'     => 'template',
      ));	  
	  
      $this->addColumn('skin', array(
          'header'    => Mage::helper('themesetting')->__('Skin'),
          'align'     => 'left',
		  'type'      => 'options',
		  'options'   => Mage::helper('themesetting')->getSkinList(),
          'index'     => 'skin',
      ));	  	  
	  
      $this->addColumn('layout', array(
          'header'    => Mage::helper('themesetting')->__('Layout'),
          'align'     => 'left',
		  'type'      => 'options',
		  'options'   => Mage::helper('themesetting')->getLayoutList(),
          'index'     => 'layout',
      ));		  

      $this->addColumn('status', array(
          'header'    => Mage::helper('themesetting')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => Mage::helper('themesetting')->__('Enabled'),
              2 => Mage::helper('themesetting')->__('Disabled'),
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('themesetting')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('themesetting')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('themesetting')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('themesetting')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('theme_id');
        $this->getMassactionBlock()->setFormFieldName('themesetting');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('themesetting')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('themesetting')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('themesetting/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('themesetting')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('themesetting')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}