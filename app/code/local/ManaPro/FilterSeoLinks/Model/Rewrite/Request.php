<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Url rewrite request model
 *
 * @category Mage
 * @package Mage_Core
 * @author Magento Core Team <core@magentocommerce.com>
 */
class ManaPro_FilterSeoLinks_Model_Rewrite_Request extends Mage_Core_Model_Url_Rewrite_Request
{
	protected $_isLayer = false;
    public function rewrite()
    {
        if (!Mage::isInstalled()) {
            return false;
        }

        if (!$this->_request->isStraight()) {
            Varien_Profiler::start('mage::dispatch::db_url_rewrite');
            $this->_rewriteDb();
            Varien_Profiler::stop('mage::dispatch::db_url_rewrite');
        }

        Varien_Profiler::start('mage::dispatch::config_url_rewrite');
        $this->_rewriteConfig();
        Varien_Profiler::stop('mage::dispatch::config_url_rewrite');

        return true;
    }

    /**
     * Implement logic of custom rewrites
     *
     * @return bool
     */
	protected function _getRequestCases()
    {
        $pathInfo = $this->_request->getPathInfo();
        $requestPath = trim($pathInfo, '/');
        $origSlash = (substr($pathInfo, -1) == '/') ? '/' : '';
        // If there were final slash - add nothing to less priority paths. And vice versa.
        $altSlash = $origSlash ? '' : '/';

        $requestCases = array();
        // Query params in request, matching "path + query" has more priority
        $queryString = $this->_getQueryString();
        if ($queryString) {
            $requestCases[] = $requestPath . $origSlash . '?' . $queryString;
            $requestCases[] = $requestPath . $altSlash . '?' . $queryString;
        }
        $requestCases[] = $requestPath . $origSlash;
        $requestCases[] = $requestPath . $altSlash;
		// check condition
		$_core = Mage::helper(strtolower('Mana_Core'));
        $conditionalWord = $_core->getStoreConfig('mana_filters/seo/conditional_word');
        $categorySuffix = Mage::helper('catalog/category')->getCategoryUrlSuffix();
		if ($conditionalWord && (($conditionPos = strpos($requestPath, '/'.$conditionalWord.'/')) != false) &&
        	(!$categorySuffix || $categorySuffix == '/' || strrpos($requestPath, $categorySuffix) == strlen($requestPath) - strlen($categorySuffix))) 
        {
            Varien_Autoload::registerScope('catalog');
        	$this->_isLayer = substr($requestPath, 0, $conditionPos) . ($categorySuffix != '/' ? $categorySuffix : '');
	        $requestCases[] = $this->_isLayer . $origSlash;
	        $requestCases[] = $this->_isLayer . $altSlash;
        }
		//end
        return $requestCases;
    }
	protected function _addLayerQuery()
    {
		$categorySuffix = Mage::helper('catalog/category')->getCategoryUrlSuffix();
		$pathInfo = $this->_request->getPathInfo();
        $requestPath = trim($pathInfo, '/');
        $origSlash = (substr($pathInfo, -1) == '/') ? '/' : '';
        // If there were final slash - add nothing to less priority paths. And vice versa.
        $altSlash = $origSlash ? '' : '/';
		if ($this->_isLayer !== false && in_array($this->_rewrite->getRequestPath(), array($this->_isLayer . $origSlash, $this->_isLayer . $altSlash))
			&& $this->_rewrite->getCategoryId()) {
			if ($categorySuffix && $categorySuffix != '/') {
				$parameters = substr($requestPath, $conditionPos + strlen('/'.$conditionalWord.'/'), 
					strlen($requestPath) - strlen($categorySuffix) - $conditionPos - strlen('/'.$conditionalWord.'/'));
			}
			else {
				$parameters = substr($requestPath, $conditionPos + strlen('/'.$conditionalWord.'/'));
			}
			$_SERVER['QUERY_STRING'] = http_build_query($this->_getQueryParameters($parameters), '', '&');
		} 
    }
	protected function _getQueryParameters($parameters) {
    	$parameters = explode('/', $parameters);
    	$state = 0; // waiting for parameter
    	$urlValue = '';
    	$filterName = false;
    	$result = array();
        
        /* @var $_core Mana_Core_Helper_Data */ $_core = Mage::helper(strtolower('Mana_Core'));
		$showAllSuffix = $_core->getStoreConfig('mana_filters/seo/show_all_suffix');
		$showAllSeoSuffix = $showAllSuffix;

    	for ($parameterIndex = 0; $parameterIndex < count($parameters); ) {
    	    $parameter = $parameters[$parameterIndex];
    	    switch ($state) {
                case 0: // waiting for parameter
                    if ($showAllSuffix && strrpos($parameter, $showAllSeoSuffix) === strlen($parameter) - strlen($showAllSeoSuffix)) {
                        $state = 2; // show all suffix found
                    }
                    elseif ($filterName = $this->_getFilterName($parameter)) {
                        $urlValue = '';
                        $parameterIndex++;
                        $state = 1; // waiting for value
                    }
                    else {
                        $parameterIndex+=2;
                    }
                    break;

                case 1: // waiting for value
                    if (!$urlValue) {
                        $urlValue = $parameter;
                        $parameterIndex++;
                    }
                    elseif ($nextFilterName = $this->_getFilterName($parameter)) {
                        $values = array();
                        foreach (explode('_', $urlValue) as $value) {
                            $values[] = $this->_getFilterValue($filterName, $value);
                        }
                        $result[$filterName] = implode('_', $values);

                        $filterName = $nextFilterName;
                        $urlValue = '';
                        $parameterIndex++;
                    }
                    else {
                        $urlValue .= '/'.$parameter;
                        $parameterIndex++;
                    }
                    break;
                case 2: // show all suffix found
                    $requestVar = substr($parameter, 0, strlen($parameter) - strlen($showAllSeoSuffix));
                    $filterName = $this->_getFilterName($requestVar);
                    $result[$filterName . $showAllSuffix] = 1;

                    $parameterIndex++;
                    break;
            }
    	}
    	if ($state == 1 && $urlValue) {
            $values = array();
            foreach (explode('_', $urlValue) as $value) {
                $values[] = $this->_getFilterValue($filterName, $value);
            }
            $result[$filterName] = implode('_', $values);
        }
    	return $result;
    }
	protected function _getFilterName($seoName) {
		$vars = Mage::helper('manapro_filterseolinks')->getRewriteVars();
        if ($seoName == Mage::helper('manapro_filterseolinks')->getCategoryName()) {
    		return 'cat'; 
    	}
    	elseif(isset($vars[$seoName])) {
    		return $vars[$seoName];
    	}
    	else {
        	/* @var $_core Mana_Core_Helper_Data */ $_core = Mage::helper(strtolower('Mana_Core'));
    		$candidateNames = array($seoName, Mage::getStoreConfigFlag('mana_filters/seo/use_label') ? $_core->urlToLabel($seoName) : $_core->lowerCased($seoName));
			/*@var $resource ManaPro_FilterSeoLinks_Resource_Rewrite */ 
			$resource = Mage::getResourceModel('manapro_filterseolinks/rewrite');
    		return $resource->getFilterName($candidateNames);
    	}
    }
    
    protected function _getFilterValue($name, $seoValue) {
    	/*@var $resource ManaPro_FilterSeoLinks_Resource_Rewrite */ 
		$resource = Mage::getResourceModel('manapro_filterseolinks/rewrite');
		$vars = Mage::helper('manapro_filterseolinks')->getUrlVars();
    	if ($name == 'cat') {
    		return $resource->getCategoryValue($this->_rewrite, $seoValue);
    	}
    	elseif(isset($vars[$name])) {
    		return $seoValue;
    	}
    	else {
	    	/* @var $_core Mana_Core_Helper_Data */ $_core = Mage::helper(strtolower('Mana_Core'));
	    	$candidateValues = array($seoValue, $_core->urlToLabel($seoValue));
    		return $resource->getFilterValue($this, $name, $candidateValues);
    	}
    }
	
	// end 
    protected function _rewriteDb()
    {
		if (null === $this->_rewrite->getStoreId() || false === $this->_rewrite->getStoreId()) {
            $this->_rewrite->setStoreId($this->_app->getStore()->getId());
        }
		
        $requestCases = $this->_getRequestCases();
		
        $this->_rewrite->loadByRequestPath($requestCases);

        $fromStore = $this->_request->getQuery('___from_store');
        if (!$this->_rewrite->getId() && $fromStore) {
            $stores = $this->_app->getStores();
            if (!empty($stores[$fromStore])) {
                /** @var $store Mage_Core_Model_Store */
                $store = $stores[$fromStore];
                $fromStoreId = $store->getId();
            } else {
                return false;
            }

            $this->_rewrite->setStoreId($fromStoreId)->loadByRequestPath($requestCases);
            if (!$this->_rewrite->getId()) {
                return false;
            }

            // Load rewrite by id_path
            $currentStore = $this->_app->getStore();
            $this->_rewrite->setStoreId($currentStore->getId())->loadByIdPath($this->_rewrite->getIdPath());

            $this->_setStoreCodeCookie($currentStore->getCode());

            $targetUrl = $this->_request->getBaseUrl() . '/' . $this->_rewrite->getRequestPath();
            $this->_sendRedirectHeaders($targetUrl, true);
        }

        if (!$this->_rewrite->getId()) {
            return false;
        }
		$this->_addLayerQuery();
        $this->_request->setAlias(Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
            $this->_rewrite->getRequestPath());
        $this->_processRedirectOptions();

        return true;
    }
	

    /**
     * Process redirect (R) and permanent redirect (RP)
     *
     * @return Mage_Core_Model_Url_Rewrite_Request
     */
    protected function _processRedirectOptions()
    {
        $isPermanentRedirectOption = $this->_rewrite->hasOption('RP');

        $external = substr($this->_rewrite->getTargetPath(), 0, 6);
        if ($external === 'http:/' || $external === 'https:') {
            $destinationStoreCode = $this->_app->getStore($this->_rewrite->getStoreId())->getCode();
            $this->_setStoreCodeCookie($destinationStoreCode);
            $this->_sendRedirectHeaders($this->_rewrite->getTargetPath(), $isPermanentRedirectOption);
        }

        $targetUrl = $this->_request->getBaseUrl() . '/' . $this->_rewrite->getTargetPath();

        $storeCode = $this->_app->getStore()->getCode();
        if (Mage::getStoreConfig('web/url/use_store') && !empty($storeCode)) {
            $targetUrl = $this->_request->getBaseUrl() . '/' . $storeCode . '/' . $this->_rewrite->getTargetPath();
        }

        if ($this->_rewrite->hasOption('R') || $isPermanentRedirectOption) {
            $this->_sendRedirectHeaders($targetUrl, $isPermanentRedirectOption);
        }

        $queryString = $this->_getQueryString();
        if ($queryString) {
            $targetUrl .= '?' . $queryString;
        }

        $this->_request->setRequestUri($targetUrl);
        $this->_request->setPathInfo($this->_rewrite->getTargetPath());

        return $this;
    }
	
}
