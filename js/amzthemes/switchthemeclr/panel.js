
jQuery(function($) {

	// load google fonts
	var fontLoaded = {};
	$('#switchthemeclr_google_font').after('<p id="switchthemeclr_google_font_preview" style="font-size:20px;padding:10px 0;margin:0"></p>')
		.bind('change', function() {
			var font = $(this).val();
			if (typeof fontLoaded[font] == 'undefined') {
				$('head').append('<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family='+encodeURIComponent(font)+':400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic-ext,cyrillic,greek-ext,greek,vietnamese,latin-ext"></link>');
				$('#switchthemeclr_google_font_preview').html(font)
					.css('font-family', font);
			}
		});
	$('#switchthemeclr_google_font1').after('<p id="switchthemeclr_google_font_preview1" style="font-size:20px;padding:10px 0;margin:0"></p>')
		.bind('change', function() {
			var font = $(this).val();
			if (typeof fontLoaded[font] == 'undefined') {
				$('head').append('<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family='+encodeURIComponent(font)+':400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic-ext,cyrillic,greek-ext,greek,vietnamese,latin-ext"></link>');
				$('#switchthemeclr_google_font_preview1').html(font)
					.css('font-family', font);
			}
		});
	$('#switchthemeclr_google_font2').after('<p id="switchthemeclr_google_font_preview2" style="font-size:20px;padding:10px 0;margin:0"></p>')
		.bind('change', function() {
			var font = $(this).val();
			if (typeof fontLoaded[font] == 'undefined') {
				$('head').append('<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family='+encodeURIComponent(font)+':400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,cyrillic-ext,cyrillic,greek-ext,greek,vietnamese,latin-ext"></link>');
				$('#switchthemeclr_google_font_preview2').html(font)
					.css('font-family', font);
			}
		});	
});