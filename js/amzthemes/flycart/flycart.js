/**
 *Flycart Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.store.onlinebizsoft.com/license.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@onlinebizsoft.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    OnlineBiz_Flycart
 * @author     OnlineBiz <sales@onlinebizsoft.com>
 * @copyright  2007-2011 OnlineBiz
 * @license    http://www.store.onlinebizsoft.com/license.txt
 * @version    1.0.1
 * @link       http://www.store.onlinebizsoft.com
 */
var varCounter = 0;
var closePopup = function(){
    if(varCounter < 1) {
        varCounter++;
		if($('fly-cart-content'))
			new Effect.Fade($('fly-cart-content'));
    } else {
        clearInterval(closePopup);
		varCounter = 0;
    }
};
Event.observe(window, 'load', function() {
		FlycartCreate();	
	}
);

function FlycartCreate(){
	if(typeof(defaultConfig) == 'object'){
		ajaxcartConfig = new FlycartConfigClass(defaultConfig);
	}
}

FlycartConfigClass = Class.create();
FlycartConfigClass.prototype = {
	
	config: null,	
	qty_input: null,
	cart_qty: null,
	custom_cart: null,
	visible_top_cart: null,
	product_qty: null,
	overlay: null,
	loading: null,	
	js_scripts: null,
	configurable_qty: null,
	grouped_qty: null,
	add_to_cart_url: null,
	associated_products: null,
	slide_control: undefined,
	auto_hide_window: null,
	confirmation_type: null,
	addition_product_ids: null, 
	
	initialize: function(config){
		
		this.config = config;		
		if (this.config.enable != '1') return;
				
		this.add_to_cart_url = new Array();
		this.addition_product_ids = new Array();
		this.associated_products = {};
		
		this.qty_input = defaultQtyTemp;
		this.cart_qty = cartQtyTemp;
		this.product_qty = productQtyTemp;
		
		if($$('div.category-products').length > 0){
			if (typeof(flycart_associated_products) != 'undefined'){
				this.associated_products = flycart_associated_products;
			}
			
			var elements = $$('div.category-products')[0].getElementsByClassName('btn-cart');
			for(var i=0; i<elements.length; i++){
				
				var onclick_url = elements[i].attributes["onclick"].nodeValue;
				onclick_url = onclick_url.toString().match(/\'.*?\'/);
				onclick_url = onclick_url[0].replace(/\'/g, '');
				var product_id = getParamFromUrl(onclick_url, 'flycart_item');
				if (!product_id) continue;	
				if (this.config.qty_update_category_page == '1'){
					var qty_div = $(document.createElement('div'));
					qty_div.addClassName('flycart_qty_edit');
					
					var validate_qty = false;
					if (onclick_url.search('checkout/cart/add') != -1){
						validate_qty = true;
					}	
					qty_div.innerHTML = this.qty_input.replace(/#flycart_item/g, product_id).replace(/#validate_qty/g, validate_qty);
					new Insertion.After(elements[i], qty_div);
					
					
					if (typeof(this.associated_products[product_id])=='undefined'){
						this.addition_product_ids.push(product_id);
						$('flycart_prod_id_' + product_id).value = 1;
					}else{
						$('flycart_prod_id_' + product_id).value = this.associated_products[product_id].min_qty;
					}
				}
				
				
				elements[i].onclick = function() {					 				     
				    ajaxcartConfig.addtoCart(this);
				};
				elements[i].id = 'flycart_add_to_cart_' + this.add_to_cart_url.length;
				this.add_to_cart_url[this.add_to_cart_url.length] = onclick_url;				 
				
			}				 			
		}
		
		this.prepareCartItem(undefined);
		this.prepareProductPage();
		this.prepareWishlist();
		this.prepareCompare();
		
		this.prepareCrosssell();
				
		this.overlay = $('flycart-overlay');				
		if(!this.overlay){				
			var element = $$('body')[0];			
			this.overlay = $(document.createElement('div'));
			this.overlay.id = 'flycart-overlay';
			document.body.appendChild(this.overlay);
				
			var offsets = element.cumulativeOffset();
			this.overlay.setStyle({
				'top'	    : offsets[1] + 'px',
				'left'	    : offsets[0] + 'px',
				'width'	    : element.offsetWidth + 'px',
				'height'	: element.offsetHeight + 'px',
				'position'  : 'absolute',
				'display'   : 'block',
				'zIndex'	: '2000'				
			});
			
			if (this.config.background_view == '1'){
				this.overlay.setStyle({
					'opacity'  : '0.6',
					'background' : '#000000'
				});	
			}
			
			this.loading = $(document.createElement('div'));		
			if(this.config.loadingAlign == 'bottom')			
				this.loading.innerHTML = this.config.loadingText+'<img src="'+this.config.loadingImage+'" alt="" class="align-'+this.config.loadingAlign+'"/>';			
			else				
				this.loading.innerHTML = '<img src="'+this.config.loadingImage+'" alt="" class="align-'+this.config.loadingAlign+'"/>'+this.config.loadingText;				
					
			this.loading.id = "flycart-loading";
			this.loading.className = "flycart-loading";			
			document.body.appendChild(this.loading);
			
			this.overlay.onclick = function() {
				if ($('flycart_confirm_window').visible()){
					ajaxcartConfig.overlay.hide();
					$('flycart_confirm_window').hide();
				}
			};
			
		}
		if (this.overlay && this.overlay.visible()) this.overlay.hide();
		if (this.loading && this.loading.visible()) this.loading.hide();
		
		this.addAdditionProduct();
								
	},
	
	addAdditionProduct: function(){
		if(this.addition_product_ids.length){
			var params = {product_ids: this.addition_product_ids.join(',')};
			
			var request = new Ajax.Request(this.config.related_product_url,
		          {
		              method:'post',
		              parameters:params,		                
		              onSuccess: function(transport){
							eval('var response = '+transport.responseText);		
							if (response.associated_products){
								for (var product_id in response.associated_products){
									this.associated_products[product_id] = response.associated_products[product_id];
								}
							}
							this.addition_product_ids = new Array();
							
					  }.bind(this),				  
					  onFailure: function(){						  
					  }.bind(this)
		          }
		      );
		}	
	},
	
	prepareCrosssell: function(){
		if($('crosssell-products-list') && this.config.qty_update_crosssell == '1'){
			var elements = $('crosssell-products-list').getElementsByClassName('btn-cart');
			for(var i=0; i<elements.length; i++){
				
				var onclick_url = elements[i].attributes["onclick"].nodeValue;
				onclick_url = onclick_url.toString().match(/\'.*?\'/);
				onclick_url = onclick_url[0].replace(/\'/g, '');
				
				var re = new RegExp('\/' + this.config.name_url_encoded + '\/.*?\/', 'g');
				onclick_url = onclick_url.replace(re, '/');
				
				var product_id = getParamFromUrl(onclick_url, 'flycart_item');
				if (!product_id) continue;
				
				var qty_div = $(document.createElement('div'));
				qty_div.addClassName('flycart_qty_edit');
				
				var validate_qty = false;
				if (onclick_url.search('checkout/cart/add') != -1){
					validate_qty = true;
				}	
				
				qty_div.innerHTML = this.qty_input.replace(/#flycart_item/g, product_id).replace(/#validate_qty/g, validate_qty);
				
				new Insertion.After(elements[i], qty_div);
				
				$('flycart_prod_id_' + product_id).value = 1;
				
				elements[i].onclick = function() {					 				     
				    ajaxcartConfig.addtoCart(this);
				};
				elements[i].id = 'flycart_add_to_cart_' + this.add_to_cart_url.length;
				this.add_to_cart_url[this.add_to_cart_url.length] = onclick_url;
																				
				if (typeof(this.associated_products[product_id])=='undefined'){
					this.addition_product_ids.push(product_id);
				}
				
			}	
		}
	},
	
	prepareWishlist: function(){
		if ($('product_comparison')) return; 
			
		var elements = $$('a.link-wishlist');
		for(var i=0; i<elements.length; i++){			
			Event.observe(elements[i], 'click', this.addToWishlist.bind(this), false);
            elements[i].onclick = function() {return false;};
		}	
	},
	
	addToWishlist: function(event) {
		
		Event.stop(event);
		
		var element = Event.element(event);
		var url = element.href;		
		var params = {flycart_wishlist_add: 1};
		
		if ($('qty')) {
			var qty = $('qty').value;
			qty = parseInt(qty);
			if (qty){
				params.qty = qty; 
			}
		}	
	
		this.loadData();
		
		var request = new Ajax.Request(url,
	          {
	              method:'post',
	              parameters:params,		                
	              onSuccess: function(transport){
						eval('var response = '+transport.responseText);		
						this.endLoadData();
						if(response.redirect){
							window.location.href = response.redirect;
							return;
						}
						this.updateToplinks(response);
						this.updateSidebar('block-wishlist', response.wishlist);
						this.showConfirmWindow(response, 'wishlist');
						
				  }.bind(this),				  
				  onFailure: function(){
					  this.endLoadData();
					  alert('Error add to Wishlist');
				  }.bind(this)
	          }
	      );    	
	},	
	
	deleteWishlistItem: function(url){
		
		var params = {};
		
		this.loadData();
		
		var request = new Ajax.Request(url,
	          {
	              method:'post',
	              parameters:params,		                
	              onSuccess: function(transport){
						eval('var response = '+transport.responseText);		
						this.endLoadData();
						if(response.message){
							alert(response.message);
						}else{
							this.updateToplinks(response);
							this.updateSidebar('block-wishlist', response.wishlist);
						}												
				  }.bind(this),				  
				  onFailure: function(){
					  this.endLoadData();
					  alert('Error add to Wishlist');
				  }.bind(this)
	          }
	      );    	
		
	}, 
	
	prepareCompare: function(){
		var elements = $$('a.link-compare');
		for(var i=0; i<elements.length; i++){
			Event.observe(elements[i], 'click', this.addToCompare.bind(this), false);
            elements[i].onclick = function() {return false;};
		}
	},
	
	addToCompare: function(event) {
		Event.stop(event);
		var element = Event.element(event);
		var url = element.href;		
		var params = {flycart_compare_add: 1};
	
		this.loadData();
		
		var request = new Ajax.Request(url,
	          {
	              method:'post',
	              parameters:params,		                
	              onSuccess: function(transport){
						eval('var response = '+transport.responseText);		
						this.endLoadData();
						if(response.compare_products){							
							this.updateSidebar('block-compare', response.compare_products);
						}
						this.showConfirmWindow(response, 'compare');
						
				  }.bind(this),				  
				  onFailure: function(){
					  this.endLoadData();
					  alert('Error add to Compare');
				  }.bind(this)
	          }
	      );    	
	},
	
	deleteCompareItem: function(url){
		
		var params = {};
		
		this.loadData();
		
		var request = new Ajax.Request(url,
	          {
	              method:'post',
	              parameters:params,		                
	              onSuccess: function(transport){
						eval('var response = '+transport.responseText);		
						this.endLoadData();
						this.endLoadData();
						if(response.compare_products){							
							this.updateSidebar('block-compare', response.compare_products);
						}											
				  }.bind(this),				  
				  onFailure: function(){
					  this.endLoadData();
					  alert('Error add to Wishlist');
				  }.bind(this)
	          }
	      );    	
		
	}, 
	
	prepareCartItem: function(update_item_id){
		
		if (this.config.qty_update_cart_page != '1') return;
		
		if ($('shopping-cart-table') && $('shopping-cart-table').select('input.qty').length > 0){
			var elements = $('shopping-cart-table').select('input.qty');
			
			for(var i=0; i<elements.length; i++){
				var item_id = elements[i].name;
				item_id = item_id.replace(/\D/g, '');
				
				if (update_item_id != undefined && item_id != update_item_id){
					continue;
				}	
				
				var td = elements[i].up('td');								
				elements[i].id = 'flycart_cart_item_' + item_id;
				
				var item_html = td.innerHTML;
				var td_html = this.cart_qty.replace(/#flycart_item_id/g, item_id).replace(/#flycart_input_cart_qty/g, item_html);
				
				td.innerHTML = td_html;
			}	
		}
	},
	
	addtoCartProduct: function(){
		
		var product_id = $$('input[name="product"]').first().value;
		
		if ($('qty')){
			var qty = $('qty').value;
			qty = parseInt(qty);
			if (!qty){
				$('qty').value = 1;
				qty = 1;
			}
			
			if (qty < this.associated_products[product_id].min_qty){
				alert('The minimum quantity allowed for purchase is ' + this.associated_products[product_id].min_qty + '.');
				return;
			}		
			if (qty > this.associated_products[product_id].max_qty){
				alert('The maximum quantity allowed for purchase is ' + this.associated_products[product_id].max_qty + '.');
				return;
			}
		}else if (this.associated_products[product_id].is_grouped == '1' && $('super-product-table')){
			var elements = $('super-product-table').getElementsByClassName('qty');
			if (elements.length > 0){
				var zeroQty = true;
				for(var i=0; i<elements.length; i++){
					if (parseInt(elements[i].value) > 0){
						zeroQty = false;
						break;
					}
				}
				if (zeroQty){
					alert('Please specify the quantity of product(s).');
					return;
				}
			}
		}
		
		if ($('customer-reviews') &&
			 (this.associated_products[product_id].is_grouped == '0') && (this.associated_products[product_id].is_simple == '0')){
			
			this.showConfigurableParams(this.associated_products[product_id].product_url, product_id);
		}else{
			if (this.config.effect == '2') { 
				this.slide_control = $('image');
				this.effectSlideToCart(this.slide_control);
				this.slide_control = undefined;
			} else if (this.config.effect == '1'){
				this.loadData();
			}
					
			$('product_addtocart_form').request({
				onSuccess: this.onSuccesAddtoCart.bind(this), 		                	
	            onFailure: this.onFailureAddtoCart.bind(this)
		    });
		}
	},
	
	prepareProductPage: function(){	
		
		if ($('product_addtocart_form') && typeof(productAddToCartForm) != 'undefined'){
			var flycart_add = document.createElement("input");
			flycart_add.type = "hidden";
			flycart_add.name = "flycart_add";
			flycart_add.value = "1";
			$('product_addtocart_form').appendChild(flycart_add);
			$('product_addtocart_form').onsubmit = function(){
			    return false;
			};
			productAddToCartForm.submit = function(){
				if (productAddToCartForm.validator.validate()){
					ajaxcartConfig.addtoCartProduct();					
				}
			}

            if (typeof(flycart_associated_products) != 'undefined'){
			    this.associated_products = flycart_associated_products;
    		}else{
    			var product_id = $$('input[name="product"]').first().value;
    			if (product_id){
    				this.addition_product_ids.push(product_id);
    			}
    		}
		}

		if (this.config.qty_update_product_page != '1') return;

		if ($('qty')){
			var product_id = $$('input[name="product"]').first().value; 
						
			var qty_div = $(document.createElement('div'));
			qty_div.addClassName('flycart_qty_edit');
			new Insertion.After($('qty'), qty_div);
			qty_div.appendChild($('qty'));
			
			var qty_html = qty_div.innerHTML;			
			qty_div.innerHTML = this.product_qty.replace(/#product_id/g, product_id).replace(/#flycart_qty_input/g, qty_html);
		}
		if ($('super-product-table')){
			var elements = $('super-product-table').select('input.qty');
			for(var i=0; i<elements.length; i++){
				
				var product_id = elements[i].name;
				product_id = product_id.replace(/\D/g, '');
				
				var td = elements[i].up('td');								
				elements[i].id = 'grouped_product_' + product_id;
				
				var item_html = td.innerHTML;				
				var td_html = this.product_qty.replace(/#product_id/g, product_id).replace(/#flycart_qty_input/g, item_html);				
				td.innerHTML = td_html;
				
				if (typeof(this.associated_products[product_id])=='undefined'){
					this.addition_product_ids.push(product_id);
				}
				
			}				
		}
	},
	
	addtoCart: function(control){
		var control_id = control.id;
		control_id = control_id.replace(/\D/g, '');
		var onclick_url = this.add_to_cart_url[control_id]; 						
		var product_id = getParamFromUrl(onclick_url, 'flycart_item');
		if (!product_id) return;			
		
		if ($('flycart_prod_id_' + product_id)){
			var qty = $('flycart_prod_id_' + product_id).value;
		}else{
			var qty = 1;
		}
		
		qty = parseInt(qty);
		if (!qty){
			$('flycart_prod_id_' + product_id).value = 1;
			qty = 1;
		}
		
		if (qty < this.associated_products[product_id].min_qty){
			alert('The minimum quantity allowed for purchase is ' + this.associated_products[product_id].min_qty + '.');
			return;
		}		
		if (qty > this.associated_products[product_id].max_qty){
			alert('The maximum quantity allowed for purchase is ' + this.associated_products[product_id].max_qty + '.');
			return;
		}
		
		this.slide_control = control;
		
		if (onclick_url.search('checkout/cart/add') != -1){
			if (this.associated_products[product_id].is_simple == '1'){				
				this.effectSlideToCart(control);
				this.slide_control = undefined;
			}
			if (this.associated_products[product_id].is_simple == '0'){
				this.loadData();
			}
			this.addSimpleProduct(onclick_url, product_id);
		}	
		else if (onclick_url.search('options=cart')){			
			this.showConfigurableParams(onclick_url, product_id);
		}	
		
	},	
	
	showConfigurableParams: function(url, product_id){
				 
		this.loadData();		
		
		if ($('flycart_prod_id_' + product_id)){
			var qty = $('flycart_prod_id_' + product_id).value;
		}else if($('qty')){
			var qty = $('qty').value;
		}
		else{
			var qty = 1;
		}	
		
		qty = parseInt(qty);
		if (!qty){
			if ($('flycart_prod_id_' + product_id)){
				$('flycart_prod_id_' + product_id).value = 1;
			}	
			qty = 1;
		}
		
		var params = {qty: qty,
					  flycart_show_configurable: 1};
		
		var request = new Ajax.Request(url,
	            {
	                method:'post',
	                parameters:params,		                
	                onSuccess: this.onSuccesConfigurable.bind(this), 		                	
	                onFailure: this.onFailureConfigurable.bind(this)
	            }
	        );    	
	},	
	
	onSuccesConfigurable: function(transport){			
		eval('var response = '+transport.responseText);		
		this.endLoadData();
		if(response.success){
			this.js_scripts = response.form.extractScripts();
			this.configurable_qty = response.qty;
			var popupWindow = new FlycartWindow('flycart_configurable_add_to_cart', 
					{className: "flycart",
					 additionClass: "flycart_confirm_btns-" + ajaxcartConfig.config.cart_button_color,
				     title: 'Add to Cart', 
				     width: ajaxcartConfig.config.window_width, 	
				     top: '50%',
				     destroyOnClose: true,
				     closeOnEsc: false,
				     showEffectOptions: {afterFinish: function(){
						for (var i=0; i<ajaxcartConfig.js_scripts.length; i++){																
							if (typeof(ajaxcartConfig.js_scripts[i]) != 'undefined'){        	        	
								globalEval(ajaxcartConfig.js_scripts[i]);                	
							}
						}
						$('qty').value = ajaxcartConfig.configurable_qty;
						if ($('overlay_modal_flycart')){
							$('overlay_modal_flycart').onclick = function() {					 				     
								var popupWindow = FlycartWindows.getWindow('flycart_configurable_add_to_cart');
								popupWindow.close();
							};
						}	
					}
				}
			}); 
			popupWindow.getContent().innerHTML = response.form.stripScripts();			
			popupWindow.showCenter(parseInt(this.config.background_view));									
		}	
		else{
			if (response.redirect){
				window.location.href = response.redirect; 
			}else if (response.message){
				alert(response.message);
			}else{
				alert('Error add to cart.');
			}	
		}			            			
	},
	
	onFailureConfigurable: function(transport){
		this.endLoadData();
		alert('Failure add to cart.');
	},
	
	addConfigurableProduct: function(form){
		if (this.config.effect == '1'){ 
			this.loadData();
		}
		var elements = form.getElements('input, select, textarea');		
		var params = {};		
		for(var i = 0;i < elements.length;i++){
			if((elements[i].type == 'checkbox' || elements[i].type == 'radio') && !elements[i].checked){
				continue;
			}				
			if (elements[i].disabled){
				continue;
			}				
			params[elements[i].name] = elements[i].value;
		}	
		var request = new Ajax.Request(form.action,
	            {
	                method:'post',
	                parameters:params,		                
	                onSuccess: this.onSuccesAddtoCart.bind(this), 		                	
	                onFailure: this.onFailureAddtoCart.bind(this)
	            }
	        );   
	},	
	
	addSimpleProduct: function(url, product_id){
		
		if (this.config.effect == '1'){ 
			this.loadData();
		}
		if ($('flycart_prod_id_' + product_id)){
			var qty = $('flycart_prod_id_' + product_id).value;
		}else{
			var qty = 1;
		}
		qty = parseInt(qty);
		if (!qty){
			$('flycart_prod_id_' + product_id).value = 1;
			qty = 1;
		}
		
		var params = {qty: qty,
					  flycart_add: 1};
		
		var request = new Ajax.Request(url,
			            {
			                method:'post',
			                parameters:params,		                
			                onSuccess: this.onSuccesAddtoCart.bind(this), 		                	
			                onFailure: this.onFailureAddtoCart.bind(this)
			            }
			        );    	
	},	
	
	onSuccesAddtoCart: function(transport){
		
		eval('var response = '+transport.responseText);
		this.endLoadData();		
		if(response.success){
			
			var popupWindow = FlycartWindows.getWindow('flycart_configurable_add_to_cart');		
			if (popupWindow){
				popupWindow.close();
			}	
			
			if (response.is_grouped){
				this.showGroupedParams(response);
				return;
			}
			if (response.is_configurable){				
				this.showConfigurableParams(response.url, response.product_id);
				return;
			}
			if (response.product_id){
				this.associated_products[response.product_id].max_qty = this.associated_products[response.product_id].max_qty*1 - response.qty*1;
				if (this.associated_products[response.product_id].max_qty*1 < this.associated_products[response.product_id].min_qty*1){
					this.associated_products[response.product_id].min_qty = this.associated_products[response.product_id].max_qty; 
				}
			}
			if (this.slide_control != undefined){
				this.effectSlideToCart(this.slide_control);
				this.slide_control = undefined;
			}			
			this.updateSidebar('block-cart', response.cart);
			this.updateToplinks(response);
			if(response.base_cart && $('shopping-cart-table')){				
				var tbody = $('shopping-cart-table').down('tbody');
				var tempElement = document.createElement('div');		    
			    tempElement.innerHTML = '<table><tbody>' + response.base_cart + '</tbody></table>';			    			    
			    el = tempElement.getElementsByTagName('tbody');		    
			    if (el.length > 0){
			        content = el[0];
			        tbody.parentNode.replaceChild(content, tbody);
			    }
				decorateTable('shopping-cart-table');
				this.prepareCartItem(undefined);
			}
			this.updateCartBlocks(response);
			this.showConfirmWindow(response, 'cart');
		}else{
			if (response.redirect){
				window.location.href = response.redirect; 
			}else if (response.message){
				alert(response.message);
			}else{
				alert('Error add to cart.');
			}	 
		}			            			
	},
	_updateCustomCart: function(response){
		if(this.config.custom_cart){
			if($(this.config.custom_cart)) {
				$(this.config.custom_cart).update(response.top_cart);
				$(this.config.custom_cart).scrollTo();
			}
			if($$("." + this.config.custom_cart)){
				var customCart = $$("." + this.config.custom_cart);
				customCart[0].update(response.top_cart);
				customCart[0].scrollTo();
			}
		}
	},
	updateSidebar: function(block_class, content_html){
		
		var blocks = $$('div.' + block_class);
		for(var ii=0; ii<blocks.length; ii++){
			var block = blocks[ii];
			var content = content_html;
			if (block && content){				
				var js_scripts = content.extractScripts();
							
				if (content && content.toElement){
			    	content = content.toElement();			    	
			    }else if (!Object.isElement(content)){			    	
				    content = Object.toHTML(content);
				    var tempElement = document.createElement('div');
				    content.evalScripts.bind(content).defer();
				    content = content.stripScripts();
				    tempElement.innerHTML = content;
				    el =  getElementsByClassName(block_class, tempElement);
				    if (el.length > 0){
				        content = el[0];
				    }
				    else{
				       return;
				    }
			    }								
				block.parentNode.replaceChild(content, block);				
				for (var i=0; i< js_scripts.length; i++){																
			        if (typeof(js_scripts[i]) != 'undefined'){        	        	
			        	globalEval(js_scripts[i]);                	
			        }
			    }
				if(typeof truncateOptions == 'function') {
					truncateOptions();
				}
			}
		}
	},
	
	updateToplinks: function(response){

		this._updateTopCart('top-link-cart', response);
		this._updateCustomCart(response);
		this._updateTopWishlist('top-link-wishlist', response);
		
	}, 
	
	_updateTopCart: function(link_class, response){
		
		$$(".header-container .top-link-cart").each(function(s){
			s.innerHTML = response.top_cart;
		});	
		if(this.config.visible_top_cart && $('fly-cart-content')){
			$('fly-cart-content').hide();
			new Effect.Appear($('fly-cart-content'));
			$('fly-cart-content').scrollTo();
			setInterval(closePopup, 4000);
		}
	}, 
	
	_updateTopWishlist: function(link_class, response){
			
		var link = $$('ul.links a.' + link_class)[0];	
			
		if (link && response.top_links){				
			
			var content = response.top_links;			
			if (content && content.toElement){
		    	content = content.toElement();			    	
		    }else if (!Object.isElement(content)){			    	
			    content = Object.toHTML(content);
			    var tempElement = document.createElement('div');			    
			    tempElement.innerHTML = content;
			    el =  getElementsByClassName(link_class, tempElement);
			    if (el.length > 0){
			        content = el[0];
			    }
			    else{
			       return;
			    }
		    }								
			link.parentNode.replaceChild(content, link);							
		}
		
	}, 
	onFailureAddtoCart: function(transport){
		this.endLoadData();
		alert('Failure add to cart.');
	},
	
	qtyUp: function(product_id, validate_qty){
		
		var qty = $('flycart_prod_id_' +  product_id).value*1 + 1; 
		
		if (qty > this.associated_products[product_id].max_qty){
			alert('The maximum quantity allowed for purchase is ' + this.associated_products[product_id].max_qty + '.');
			return;
		}
		
		if (!validate_qty){
			$('flycart_prod_id_' + product_id).value = $('flycart_prod_id_' + product_id).value*1 + 1;
			return;
		}
		this.loadData();
		var params = {product_id: product_id,
		 		      qty: qty};
		
		var request = new Ajax.Request(this.config.updateqty,
			            {
			                method:'post',
			                parameters:params,		                
			                onSuccess: this.onSuccesChangeQty.bind(this), 		                	
			                onFailure: this.onFailureChangeQty.bind(this)
			            }
			        );    				
	},
	
	qtyDown: function(product_id, validate_qty){
		
		var qty = $('flycart_prod_id_' + product_id).value*1 - 1;
		
		if (qty < this.associated_products[product_id].min_qty){
			alert('The minimum quantity allowed for purchase is ' + this.associated_products[product_id].min_qty + '.');
			return;
		}			
		if (!validate_qty){
			$('flycart_prod_id_' + product_id).value = qty;
			return;
		}	
		this.loadData();
		var params = {product_id: product_id,
	 		      	  qty: qty};
	
		var request = new Ajax.Request(this.config.updateqty,
			            {
			                method:'post',
			                parameters:params,		                
			                onSuccess: this.onSuccesChangeQty.bind(this), 		                	
			                onFailure: this.onFailureChangeQty.bind(this)
			            }
			        ); 
	},
	
	onSuccesChangeQty: function(transport){									
		eval('var response = '+transport.responseText);
		this.endLoadData();
		if(response.error){
			alert(response.message); 
		}	
		else{
			if ($('flycart_prod_id_' + response.product_id)){
				$('flycart_prod_id_' + response.product_id).value = response.qty;
			}
		}			            			
	},
	
	onFailureChangeQty: function(transport){
		this.endLoadData();
		alert('Failure change qty.');
	},
	
	setOverlaySize: function(){
		var element = $$('body')[0];					
		this.overlay.setStyle({			
			'height'	: element.offsetHeight + 'px'						
		});
	},
	
	loadData: function(){
		this.setOverlaySize();
		this.overlay.show();
		this.loading.show();				
	},
	
	endLoadData: function(){
		if (this.overlay)
			this.overlay.hide();
		if (this.loading)
			this.loading.hide();
	},
	
	effectSlideToCart: function(control){
		
		if (this.config.effect != '2') return;
		if (this.slide_control == undefined) return;
		
		if (control.id == 'image')
			var img = control;
		else	
			var img = $(control).up('li.item').down('img');
		var topcarts = $$(".header-container .top-link-cart");
		var cart = null;
		var carts = $$('div.block-cart');
		if (carts.length){
		    for(var i=0; i<carts.length; i++){
		       offset = carts[i].cumulativeOffset();
		       if (offset[0] || offset[1]){
		    	   cart = carts[i];
		    	   break;
		       } 
		    }
		}
		if(!cart && topcarts.length) {
			for(var i=0; i<topcarts.length; i++){
		       offset = topcarts[i].cumulativeOffset();
		       if (offset[0] || offset[1]){
		    	   cart = topcarts[i];
		    	   break;
		       } 
		    }
		}
		if(this.config.custom_cart && !cart){
			if($(this.config.custom_cart)){
				var customCart = $(this.config.custom_cart);
			}
			if($$("." + this.config.custom_cart)){
				var customCart = $$("." + this.config.custom_cart);
			}
			if(customCart.length) {
				for(var i=0; i<customCart.length; i++){
				   offset = customCart[i].cumulativeOffset();
				   if (offset[0] || offset[1]){
					   cart = customCart[i];
					   break;
				   } 
				}
			} else {
				cart = customCart;
			}
		}
		if (img && cart){			
			var img_offsets = img.cumulativeOffset();
			var cart_offsets = cart.cumulativeOffset();
			var animate_img =  img.cloneNode(true);
			animate_img.id = 'animate_image';
			document.body.appendChild(animate_img);			 
			animate_img.setStyle({'position': 'absolute', 
								  'top': img_offsets[1] + 'px', 
								  'left': img_offsets[0] + 'px'});
			
			new Effect.Parallel(			
			    [						    			
			     	new Effect.Fade('animate_image', {sync: true, to: 0.3}),
			     	new Effect.MoveBy('animate_image', cart_offsets[1]-img_offsets[1], cart_offsets[0]-img_offsets[0], {sync: true})			     	 
			    ],			
			    {duration: 2,
			     afterFinish: function(){
			    		$('animate_image').remove();	
			     	}			    	
			    }			
			);
		}						
	},
	
	qtyUpSidebar: function(item_id){
		
		this.loadData();
		var params = {item_id: item_id,
	 		      	  qty: $('flycart_sidebar_' + item_id).value*1 + 1,
	 		      	  sidebar: 1};
		
		var request = new Ajax.Request(this.config.updatecartqty,
			            {
			                method:'post',
			                parameters:params,		                
			                onSuccess: this.onSuccesUpdateQtySidebar.bind(this), 		                	
			                onFailure: this.onFailureUpdateQtySidebar.bind(this)
			            }
			        );    				
	},
	
	qtyDownSidebar: function(item_id){
		if ($('flycart_sidebar_' + item_id).value*1 == 1){
			alert('The minimum quantity allowed for purchase is 1.');
			return;
		}			
		this.loadData();
		var params = {item_id: item_id,
		      	  	  qty: $('flycart_sidebar_' + item_id).value*1 - 1,
		      	  	  sidebar: 1};
	
		var request = new Ajax.Request(this.config.updatecartqty,
			            {
			                method:'post',
			                parameters:params,		                
			                onSuccess: this.onSuccesUpdateQtySidebar.bind(this), 		                	
			                onFailure: this.onFailureUpdateQtySidebar.bind(this)
			            }
			        ); 
	},
	
	onSuccesUpdateQtySidebar: function(transport){									
		eval('var response = '+transport.responseText);
		this.endLoadData();
		if(response.error){
			alert(response.message); 
		}	
		else{
			if (response.product_id && this.associated_products[response.product_id] && response.max_qty){
				this.associated_products[response.product_id].max_qty = response.max_qty;
			}
			this.updateSidebar('block-cart', response.cart);
			this.updateToplinks(response);
		}			            			
	},
	
	onFailureUpdateQtySidebar: function(transport){
		this.endLoadData();
		alert('Failure change qty.');
	},
	
	deleteItem: function(url){
		$('flycart_confirm_window').hide(); 
		ajaxcartConfig.overlay.hide();
		this.loadData();
		var params = {};
		if ($('shopping-cart-table'))
			params.flycart_cart_delete = 1;
		else
			params.flycart_sedebar_delete = 1;
	
		var request = new Ajax.Request(url,
	            {
	                method:'post',
	                parameters:params,		                
	                onSuccess: this.onSuccesDeleteItem.bind(this), 		                	
	                onFailure: this.onFailureDeleteItem.bind(this)
	            }
	        );
	},
	
	onSuccesDeleteItem: function(transport){									
		eval('var response = '+transport.responseText);
		this.endLoadData();
		if(response.error){
			alert(response.message); 
		}	
		else{	
			if (response.redirect){
				setLocation(response.redirect);
				return;
			}
			if (response.item_id){
				$('flycart_cart_item_' + response.item_id).up('td').up('tr').remove();
				this.updateCartBlocks(response);
			}
			this.updateSidebar('block-cart', response.cart);
			this.updateToplinks(response);
			if (response.product_id && this.associated_products[response.product_id] && response.max_qty){
				this.associated_products[response.product_id].max_qty = response.max_qty;
			}
		}			            			
	},
	
	onFailureDeleteItem: function(transport){
		this.endLoadData();
		alert('Cannot remove the item.');
	},
	
	qtyCartUp: function(item_id){
		
		this.loadData();
		var params = {item_id: item_id,
	 		      	  qty: $('flycart_cart_item_' + item_id).value*1 + 1,
	 		      	  cart: 1};
		
		var request = new Ajax.Request(this.config.updatecartqty,
			            {
			                method:'post',
			                parameters:params,		                
			                onSuccess: this.onSuccesUpdateQtyCart.bind(this), 		                	
			                onFailure: this.onFailureUpdateQtyCart.bind(this)
			            }
			        );    				
	},
	
	qtyCartDown: function(item_id){
		if ($('flycart_cart_item_' + item_id).value*1 == 1){
			alert('The minimum quantity allowed for purchase is 1.');
			return;
		}			
		this.loadData();
		var params = {item_id: item_id,
		      	  	  qty: $('flycart_cart_item_' + item_id).value*1 - 1,
		      	  	  cart: 1};
	
		var request = new Ajax.Request(this.config.updatecartqty,
			            {
			                method:'post',
			                parameters:params,		                
			                onSuccess: this.onSuccesUpdateQtyCart.bind(this), 		                	
			                onFailure: this.onFailureUpdateQtyCart.bind(this)
			            }
			        ); 
	},
	
	onSuccesUpdateQtyCart: function(transport){									
		eval('var response = '+transport.responseText);
		this.endLoadData();		
		if(response.error){
			if (response.update_attribute){				
				
				if ($('flycart_cart_item_' + response.item_id)){
					var elements = $('flycart_cart_item_' + response.item_id).up('td').up('tr').select('select');
				}else if ($('flycart_sidebar_' + response.item_id)){
					var elements = $('flycart_sidebar_' + response.item_id).up('div.product-details').select('select');
				}
				if (elements){
					for(var i=0; i<elements.length; i++){
						var attribute_id = elements[i].getAttribute('class');
						attribute_id = attribute_id.replace(/\D/g, ''); 
						if (response.success_param.hasOwnProperty(attribute_id)){
							continue;
						}else if(attribute_id == response.update_attribute){
							elements[i].options.length = 0;
							elements[i].options[elements[i].options.length] = new Option(response.choosetext, '', false, false);
							
							for (key in response.attribute_data){
								elements[i].options[elements[i].options.length] = new Option(response.attribute_data[key], key, false, false);
							}
	
						}else{
							elements[i].options.length = 0;
							elements[i].options[elements[i].options.length] = new Option(response.choosetext, '', false, false);
						}
					}	
				}
			}else{
				this.updateSidebar('block-cart', response.cart);
				this.replaceCartItems(response);
				alert(response.message); 
			}	
		}	
		else{
			this.updateSidebar('block-cart', response.cart);
			this.replaceCartItems(response); 	
			this.updateCartBlocks(response);
			this.updateToplinks(response);
		}			            			
	},
	
	onFailureUpdateQtyCart: function(transport){
		this.endLoadData();
		alert('Failure change qty.');
	},
	
	replaceCartItems: function(response){					
		if ($('shopping-cart-table') && response.items_html){
			
			var tbody = $('shopping-cart-table').down('tbody');
			var tempElement = document.createElement('div');		    
		    tempElement.innerHTML = '<table><tbody>' + response.items_html + '</tbody></table>';			    			    
		    el = tempElement.getElementsByTagName('tbody');		    
		    if (el.length > 0){
		        content = el[0];
		        tbody.parentNode.replaceChild(content, tbody);
		    }
			decorateTable('shopping-cart-table');
			this.prepareCartItem(undefined);						
		}
	},
	
	updateCartBlocks: function(response){		
		this._updateCartBlock($('shopping-cart-totals-table'), response.total);
		this._updateCartBlock($$('div.shipping')[0], response.shipping);
		if ($$('div.crosssell')[0] && response.crosssell){
			this._updateCartBlock($$('div.crosssell')[0], response.crosssell);			
			this.prepareWishlist();
			this.prepareCompare();
			this.prepareCrosssell();
			this.addAdditionProduct();
		}
	},
	
	_updateCartBlock: function(block, content){					
		if (block && content){	
			
			var js_scripts = content.extractScripts();
			content = content.stripScripts();
									
			if (content && content.toElement){
		    	content = content.toElement();			    	
		    }else if (!Object.isElement(content)){			    	
			    content = Object.toHTML(content);
			    var tempElement = document.createElement('div');			    
			    tempElement.innerHTML = content;
			    el =  tempElement.getElementsByTagName('table');			    
			    if (el.length > 0){
			        content = el[0];
			    }else{
			    	content = tempElement.firstChild;
			    }
		    }								
			block.parentNode.replaceChild(content, block);
			
			for (var i=0; i< js_scripts.length; i++){																
		        if (typeof(js_scripts[i]) != 'undefined'){        	        	
		        	globalEval(js_scripts[i]);                	
		        }
		    }	
			
		}
	}, 
	
	attributeCartUpdate: function(control, product_id){
		if ($(control).up('td')){
			var item_id = $(control).up('td').up('tr').down('input.qty').name;
			item_id = item_id.replace(/\D/g, '');
			var super_attribute = {};
					
			if ($(control).up('td').up('tr').select('select').length > 0){
				var attributes = $(control).up('td').up('tr').select('select');
				for(var i=0; i<attributes.length; i++){				
					var attribute_id = $(attributes[i]).className;
					attribute_id = attribute_id.replace(/\D/g, '');
					super_attribute[attribute_id] = $(attributes[i]).value;
				}	
			}
		}else if ($(control).up('div.product-details')){
			var item_id = $(control).up('div.product-details').down('input.qty').id;
			item_id = item_id.replace(/\D/g, '');
			var super_attribute = {};
					
			if ($(control).up('div.product-details').select('select').length > 0){
				var attributes = $(control).up('div.product-details').select('select');
				for(var i=0; i<attributes.length; i++){				
					var attribute_id = $(attributes[i]).className;
					attribute_id = attribute_id.replace(/\D/g, '');
					super_attribute[attribute_id] = $(attributes[i]).value;
				}	
			}
		}else{
			return;
		}
		
		this.loadData();
								
		var params = {'id': item_id,
					  'product': product_id,
					  'super_attribute': Object.toJSON(super_attribute)};
		
		var request = new Ajax.Request(this.config.updateattqty,
			            {
			                method:'post',
			                parameters: params,		                
			                onSuccess: this.onSuccesUpdateQtyCart.bind(this), 		                	
			                onFailure: this.onFailureUpdateQtyCart.bind(this)
			            }
			        ); 
	},
	
	showConfirmWindow: function(response, type){
		this.setOverlaySize();
		if (this.config.show_window == '1'){
			this.confirmation_type = type;
			$$('span.confirm_addtocart').each(Element.hide);
			$('confirm_addtocart_' + type).show();
			if (parseInt(response.qty) >= 1){
				$('confirm_qty').innerHTML = response.qty;
			}
			else{
				$('confirm_qty').innerHTML = '';
			}
			$('flycart_productname').innerHTML = response.prod_name;
			$('cart_item_details').innerHTML = response.cart_item_popup;
			$('flycart_singular').hide();
			$('flycart_plural').hide();
			if (parseInt(response.qty) > 1)
				$('flycart_plural').show();
			else
				$('flycart_singular').show();
			
			for (key in _flycart_data){
				if (key == this.confirmation_type){					
					$('flycart_confirm_checkout').up('span').setAttribute("onclick", _flycart_data[key].onclick);
					if (Prototype.Browser.IE){						
						$('flycart_confirm_checkout').up('span').onclick = function() {																				    
						    for (_key in _flycart_data){
								if (_key == ajaxcartConfig.confirmation_type){
									globalEval(_flycart_data[_key].onclick);
								}
							}
						};
					}
					$('flycart_confirm_checkout').innerHTML = _flycart_data[key].text;
				}
			}
			
			this.setOverlaySize();
			this.overlay.show();
			$('flycart_confirm_window').show();
			
			var auto_hide_window = this.config.auto_hide_window;
			this.auto_hide_window = parseInt(auto_hide_window);
			if (this.auto_hide_window > 0){
				//this.setTimeout();
			}				
		}	
	},
	
	setTimeout: function(){
		var text = '';
		var onclick = '';
		for (key in _flycart_data){
			if (key == this.confirmation_type){
				text = _flycart_data[key].text;
				onclick = _flycart_data[key].onclick;
			}
		}
		if (this.auto_hide_window > 0 && $('flycart_confirm_window').visible()) {
			if (this.config.redirect_to == '1')
				$('flycart_confirm_checkout').innerHTML = text + ' (' + this.auto_hide_window + ')';
			else	
				$('flycart_confirm_continue').innerHTML = continueMessage + ' (' + this.auto_hide_window + ')';
			window.setTimeout(function() { 				  
				ajaxcartConfig.setTimeout();
			}, 1000);
			this.auto_hide_window = this.auto_hide_window - 1;
		}else{			
			if (this.config.redirect_to == '1' && $('flycart_confirm_window').visible()){				
				globalEval(onclick);
			}
			ajaxcartConfig.overlay.hide();
			$('flycart_confirm_window').hide();
		}
	},
	
	qtyProductUp: function(pid){
		if (productAddToCartForm.validator.validate()){
			
			var product_id = $$('input[name="product"]').first().value;
			if($('qty')){
				var qty = $('qty').value*1 + 1;
				if (qty > this.associated_products[product_id].max_qty){
					alert('The maximum quantity allowed for purchase is ' + this.associated_products[product_id].max_qty + '.');
					return;
				}
			}else if($('grouped_product_' + pid)){
				var qty = $('grouped_product_' + pid).value*1 + 1;
				if (qty > this.associated_products[pid].max_qty){
					alert('The maximum quantity allowed for purchase is ' + this.associated_products[pid].max_qty + '.');
					return;
				}
				product_id = pid;
			}
			
			this.loadData();
			var params = {product_id: product_id,
			 		      qty: qty};
			
			var request = new Ajax.Request(this.config.updateproductqty,
				            {
				                method:'post',
				                parameters:params,		                
				                onSuccess: this.onSuccesUpdateProductQty.bind(this), 		                	
				                onFailure: this.onFailureUpdateProductQty.bind(this)
				            }
				        );    				
		}	
	},
	
	qtyProductDown: function(prod_id){
		if (productAddToCartForm.validator.validate()){
			
			var product_id = $$('input[name="product"]').first().value;
			if($('qty')){
				var qty = $('qty').value*1 - 1;			
				if (qty < this.associated_products[product_id].min_qty){
					alert('The minimum quantity allowed for purchase is ' + this.associated_products[product_id].min_qty + '.');
					return;
				}
			}else if($('grouped_product_' + prod_id)){
				var qty = $('grouped_product_' + prod_id).value*1 - 1;			
				if (qty < this.associated_products[prod_id].min_qty){
					alert('The minimum quantity allowed for purchase is ' + this.associated_products[prod_id].min_qty + '.');
					return;
				}
				product_id = prod_id;
			}	
			
			this.loadData();
			var params = {product_id: product_id,
						  qty: qty};
		
			var request = new Ajax.Request(this.config.updateproductqty,
				            {
				                method:'post',
				                parameters:params,		                
				                onSuccess: this.onSuccesUpdateProductQty.bind(this), 		                	
				                onFailure: this.onFailureUpdateProductQty.bind(this)
				            }
				        ); 
		}	
	},
	
	onSuccesUpdateProductQty: function(transport){									
		eval('var response = '+transport.responseText);
		this.endLoadData();
		if(response.error){
			alert(response.message); 
		}	
		else{
			if($('qty')){
				$('qty').value = response.qty;
			}else if($('grouped_product_' + response.product_id)){
				$('grouped_product_' + response.product_id).value = response.qty;
			}	
		}			            			
	},
	
	onFailureUpdateProductQty: function(transport){
		this.endLoadData();
		alert('Failure change qty.');
	},
	
	showGroupedParams: function(response){			
				
		this.js_scripts = response.form.extractScripts();
		this.grouped_qty = response.qty;
		var popupWindow = new FlycartWindow('flycart_configurable_add_to_cart', 
				{className: "flycart",
				 additionClass: "flycart_confirm_btns-" + ajaxcartConfig.config.cart_button_color,
			     title: 'Add to Cart', 
			     width: ajaxcartConfig.config.window_width, 
			     top: '50%',
			     destroyOnClose: true,
			     closeOnEsc: false,
			     showEffectOptions: {afterFinish: function(){
					for (var i=0; i<ajaxcartConfig.js_scripts.length; i++){																
						if (typeof(ajaxcartConfig.js_scripts[i]) != 'undefined'){        	        	
							globalEval(ajaxcartConfig.js_scripts[i]);                	
						}
					}
					$('super-product-table').select('input[type=text]').each(function(control){
						$(control).value = ajaxcartConfig.grouped_qty;
					}); 
					if ($('overlay_modal_flycart')){
						$('overlay_modal_flycart').onclick = function() {					 				     
							var _win = FlycartWindows.getWindow('flycart_configurable_add_to_cart');
							if(_win) _win.close();
						};
					}
				}
			}
		}); 
		popupWindow.getContent().innerHTML = response.form.stripScripts();			
		popupWindow.showCenter(parseInt(this.config.background_view));											            			
	},
	
	addtoCartGrouped: function(form){
		 
		this.loadData();
		
		var elements = form.getElements('input, select, textarea');		
		var params = {};		
		for(var i = 0;i < elements.length;i++){
			if((elements[i].type == 'checkbox' || elements[i].type == 'radio') && !elements[i].checked){
				continue;
			}				
			if (elements[i].disabled){
				continue;
			}				
			params[elements[i].name] = elements[i].value;
		}	
		var request = new Ajax.Request(form.action,
	            {
	                method:'post',
	                parameters:params,		                
	                onSuccess: this.onSuccesAddtoCart.bind(this), 		                	
	                onFailure: this.onFailureAddtoCart.bind(this)
	            }
	        );   
	}	
		
};	

function getParamFromUrl(url, name){	
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );  
  var results = regex.exec( url );  
  if( results == null )
    return "";
  else
    return results[1];  
}

var globalEval = function globalEval(src){
    if (window.execScript) {
        window.execScript(src);
        return;
    }
    var fn = function() {
        window.eval.call(window,src);
    };
    fn();
};

function getElementsByClassName(classname, node){
    var a = [];
    var re = new RegExp('\\b' + classname + '\\b');
    var els = node.getElementsByTagName("*");
    for(var i=0,j=els.length; i<j; i++){
           if(re.test(els[i].className))a.push(els[i]);
    } 
    return a;
}