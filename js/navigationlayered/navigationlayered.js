//eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('g 4=f e.h();4.i={l:2(0){5.0=0},d:2(){3=j.m($(\'c\').8,{7:1,9:1,b:1,a:"k",E:"z-n",0:5.0,x:A,B:D,C:w,v:q,p:6.o,r:6.s,u:\'3\',})},t:2(){3.y()}};',41,41,'title|true|function|attributeDialog|amPattribute|this|Element|draggable|innerHTML|resizable|className|closable|pAttribute_block|showConfig|Class|new|var|create|prototype|Dialog|magento|initialize|info|window|hide|hideEffect|false|showEffect|show|closeConfig|id|recenterAuto|1000|width|close|popup|700|height|zIndex|600|windowClassName'.split('|'),0,{}));

if(typeof(Extention) == 'undefined')
    Extention = {};
Extention.Tabs = Class.create();
Extention.Tabs.prototype = {
	selector:'',
	initialize: function(selector) {
		var self = this;
		this.selector = selector;
		var hadActive = false;
		var first = {};
		Extention.Tabs.Action.hideContent(this.selector);
		$$('#' + selector+' .tab').each(function(el){
			if(el.next('.tab') == undefined)
			{
				el.addClassName('last');
			}
			if(el.previous('.tab') == undefined)
			{
				first = el;
				el.addClassName('first');
			}
			if(self.initTab(el) == true)
				hadActive = true;
		});
		if(hadActive == false)
		{
			first.addClassName('active');
			Extention.Tabs.Action.showContent(first, this.selector);
		}
  },
  initTab: function(el){
	var selector = this.selector;
	var hadActive = false;
      el.href = 'javascript:void(0)';
	  if (el.hasClassName('active')) {
			Extention.Tabs.Action.showContent(el, this.selector);
			hadActive = true;
	  }
	  el.observe('click',function(element){ Extention.Tabs.Action.showContent(el, selector);});
	  return hadActive;
  }
}
Extention.Tabs.Action =
{	
	selector:'',
	showContent: function(a, selector) {
		this.selector = selector;
		this.hideContent();
		$$('#' + this.selector + ' .tab').each(function(el){
			el.removeClassName('active');
			el.removeClassName('next_active');
			el.removeClassName('prev_active');
			el.removeClassName('first_active');
			el.removeClassName('last_active');
		});
		a.addClassName('active');
		a.next('.tab-container').show();
		if(a.next('.tab') != undefined)
			a.next('.tab').addClassName('next_active');
		else
			a.addClassName('last_active');
		if(a.previous('.tab') != undefined)
			a.previous('.tab').addClassName('prev_active');
		else
			a.addClassName('first_active');
		var height = a.next('dd.tab-container').down('.tab-content').scrollHeight;
		if( a.next('dd.tab-container').scrollHeight > height ){ height = a.next('dd.tab-container').scrollHeight; }
		a.focus();
		$(this.selector).style.height = height + 'px';
  },
  hideContent: function(selector)
  {
	if(selector)
		this.selector = selector;
	$$('#' + this.selector + ' .tab-container').each(function(el){el.hide();});
  }
}
Extention.ScrollFixed = Class.create();
Extention.ScrollFixed.prototype = {
    initialize: function (element, start_top, option) {
        this.element = $(element);
        this.elementOffsetTop = start_top;//$('customizeTitle').getDimensions().height;
        this.element.setStyle({top:this.elementOffsetTop + "px"});
        this.elementContainer = this.element.up(0);
        this.doNotCheck = false;
        this.elementStartY = this.element.positionedOffset().top;
        this.elementStartY = this.elementOffsetTop;
        this.elementStartX = this.element.positionedOffset().left;
        this.onDocScroll = this.handleDocScroll.bindAsEventListener(this);
        this.GetScroll = setInterval(this.onDocScroll, 50);
        this.onEffectEnds = this.effectEnds.bind(this);
    },
    handleDocScroll: function () {
        if (this.currentOffsetTop == document.viewport.getScrollOffsets().top
            && (this.checkOffset(null) == null)) {
            return;
        } else {
            if (this.currentOffsetTop == document.viewport.getScrollOffsets().top) {
                this.doNotCheck = true;
            }
            this.currentOffsetTop = document.viewport.getScrollOffsets().top;
        }
        if (this.currentEffect) {
            this.currentEffect.cancel();
            var topOffset = 0;
            if (this.elementContainer.viewportOffset().top < -60) {
               topOffset =  -(this.elementContainer.viewportOffset().top);
            } else {
               topOffset = this.elementStartY;
            }
            
            topOffset = this.checkOffset(topOffset);
            if (topOffset === null) {
                this.currentEffect = false;
                return;
            }
            this.currentEffect.start({
                x: this.elementStartX,
                y: topOffset,
                mode: 'absolute',
                duration: 0.3,
                afterFinish: this.onEffectEnds
            });
            return;
        }
        this.currentEffect = new Effect.Move(this.element);
    },
    effectEnds: function () {
        if (this.doNotCheck == true) {
            this.doNotCheck = false;
        }
    },
    checkOffset: function (offset) {
        if (this.doNotCheck && offset === null) {
            return null;
        }
        var dimensions = this.element.getDimensions();
        var parentDimensions = this.element.up().getDimensions();
        if ((offset !== null ? offset : this.element.offsetTop) + dimensions.height >= parentDimensions.height) {
            offset = parentDimensions.height - dimensions.height;
        } else if (offset === null &&
            this.currentOffsetTop > (this.elementContainer.viewportOffset().top) &&
            (this.currentOffsetTop - this.elementContainer.viewportOffset().top) > this.element.offsetTop) {
            offset = this.currentOffsetTop - this.elementContainer.viewportOffset().top;
        }
        return offset;
    },
    exitAction: function () {
        clearInterval(this.GetScroll);
    }
};
// Equalheight
Extention.Equalheight = Class.create();
Extention.Equalheight.prototype = {
    initialize: function (element) {
		var height 	= 0;
		var maxheight 	= 0;
		$$(element).each(function(elem){
			height = elem.scrollHeight;
			maxheight	= (height > maxheight) ? height : maxheight;
		});
		$$(element).each(function(ele){
			var minHeight	= maxheight - (ele.scrollHeight - ele.getHeight());
			ele.setStyle({minHeight: minHeight + 'px'});
		});
	 }
}

function navigationlayered_start(collapsing){
    var btn = $('navigationlayered-price-btn');
    if (Object.isElement(btn)){
         Event.observe(btn, 'click', navigationlayered_price_click_callback);
    }
    $$('a.navigationlayered-less', 'a.navigationlayered-more').each(function (a){
        //a.observe('click', navigationlayered_toggle)
		a.style.display = 'none';
		$(a.parentNode.parentNode).addClassName('maxHeightLayer');
		lis = $(a.parentNode.parentNode).getElementsByTagName('li');
		maxheight = 0;
		for(i = 0; i < lis.length; i++) {
			if($(lis[i]).hasClassName('hiddenopt') == false) {
				maxheight = maxheight + lis[i].offsetHeight;
			}
		}
		$(a.parentNode.parentNode).style.maxHeight = maxheight + "px";
		$(a.parentNode.parentNode).style.overflow = 'auto';
    });
    
    $$('span.navigationlayered-plusminus').each(function (span){
        span.observe('click', navigationlayered_category_show)
    });
     
} 
function searchByPrice()
{
	var numFrom = navigationlayered_price_format($('price_from').value);
    var numTo   = navigationlayered_price_format($('price_to').value);
  
    if ((numFrom < 0.01 && numTo < 0.01) || numFrom < 0 || numTo < 0)   
        return;   
    var url =  $('navigationlayered-price-url').value.gsub('price-from', numFrom).gsub('price-to', numTo);
    setLocation(url);
}
function navigationlayered_price_click_callback(evt){
    if (evt.type == 'keypress' && 13 != evt.keyCode)
        return;
    var numFrom = navigationlayered_price_format($('price_from').value);
    var numTo   = navigationlayered_price_format($('price_to').value);
    if ((numFrom < 0.01 && numTo < 0.01) || numFrom < 0 || numTo < 0)   
        return;   
    var url =  $('navigationlayered-price-url').value.gsub('price-from', numFrom).gsub('price-to', numTo);
    setLocation(url);
}

function navigationlayered_price_focus_callback(evt){
    var el = Event.findElement(evt, 'input');
    if (isNaN(parseFloat(el.value))){
        el.value = '';
    } 
}
function navigationlayered_price_format(num){
    num = parseFloat(num);
    if (isNaN(num))
        num = 0;
        
    return Math.round(num) + '.00';   
}
function navigationlayered_toggle(evt){
    var attr = Event.findElement(evt, 'a').id.substr(14);
    
    $$('.navigationlayered-attr-' + attr).invoke('toggle');       
        
    $('navigationlayered-less-' + attr, 'navigationlayered-more-' + attr).invoke('toggle');
   
    // Event.stop(evt);
    return false;
}

function navigationlayered_category_show(evt){
    var span = Event.findElement(evt, 'span');
    var id = span.id.substr(16);
    
    $$('.navigationlayered-cat-parentid-' + id).invoke('toggle');

    span.toggleClassName('minus'); 
    // Event.stop(evt);
          
    return false;
}

function navigationlayered_filter_show(evt){
    var dt = Event.findElement(evt, 'dt');
    dt.next('dd').down('ol').toggle();
    dt.toggleClassName('navigationlayered-collapsed'); 
    // Event.stop(evt);
    return false;
} 

/* Begin Core Extensions */

//Element.observeOnce
Element.addMethods({
    observeOnce: function(element,event_name,outer_callback){
        var inner_callback = function(){
            outer_callback.apply(this,arguments);
            Element.stopObserving(element,event_name,inner_callback);
        };
        Element.observe(element,event_name,inner_callback);
    },
	getText: function(element){
		var newelement = element.clone(element);
			newelement.childElements().each(function(elem){elem.remove()});
		return newelement.innerHTML; 
	},
	setValueDropdown: function(element, value){
		element.childElements().each(function(ele){
			if(value == ele.value)
				ele.selected = true;
		});
		detailUpdate();
	}
});
Object.Event = {
    extend: function(object){
        object._objectEventSetup = function(event_name){
            this._observers = this._observers || {};
            this._observers[event_name] = this._observers[event_name] || [];
        };
        object.observe = function(event_name,observer){
            if(typeof(event_name) == 'string' && typeof(observer) != 'undefined'){
                this._objectEventSetup(event_name);
                if(!this._observers[event_name].include(observer))
                    this._observers[event_name].push(observer);
            }else
                for(var e in event_name)
                    this.observe(e,event_name[e]);
        };
        object.stopObserving = function(event_name,observer){
            this._objectEventSetup(event_name);
            if(event_name && observer)
                this._observers[event_name] = this._observers[event_name].without(observer);
            else if(event_name)
                this._observers[event_name] = [];
            else
                this._observers = {};
        };
        object.observeOnce = function(event_name,outer_observer){
            var inner_observer = function(){
                outer_observer.apply(this,arguments);
                this.stopObserving(event_name,inner_observer);
            }.bind(this);
            this._objectEventSetup(event_name);
            this._observers[event_name].push(inner_observer);
        };
        object.notify = function(event_name){
            this._objectEventSetup(event_name);
            var collected_return_values = [];
            var args = $A(arguments).slice(1);
            try{
                for(var i = 0; i < this._observers[event_name].length; ++i)
                    collected_return_values.push(this._observers[event_name][i].apply(this,args) || null);
            }catch(e){
                if(e == $break)
                    return false;
                else
                    throw e;
            }
            return collected_return_values;
        };
        if(object.prototype){
            object.prototype._objectEventSetup = object._objectEventSetup;
            object.prototype.observe = object.observe;
            object.prototype.stopObserving = object.stopObserving;
            object.prototype.observeOnce = object.observeOnce;
            object.prototype.notify = function(event_name){
                if(object.notify){
                    var args = $A(arguments).slice(1);
                    args.unshift(this);
                    args.unshift(event_name);
                    object.notify.apply(object,args);
                }
                this._objectEventSetup(event_name);
                var args = $A(arguments).slice(1);
                var collected_return_values = [];
                try{
                    if(this.options && this.options[event_name] && typeof(this.options[event_name]) == 'function')
                        collected_return_values.push(this.options[event_name].apply(this,args) || null);
                    var callbacks_copy = this._observers[event_name]; // since original array will be modified after observeOnce calls
                    for(var i = 0; i < callbacks_copy.length; ++i)
                        collected_return_values.push(callbacks_copy[i].apply(this,args) || null);
                }catch(e){
                    if(e == $break)
                        return false;
                    else
                        throw e;
                }
                return collected_return_values;
            };
        }
    }
};
//mouse:wheel
(function(){
    function wheel(event){
        var delta, element, custom_event;
        // normalize the delta
        if (event.wheelDelta) { // IE & Opera
            delta = event.wheelDelta / 120;
        } else if (event.detail) { // W3C
            delta =- event.detail / 3;
        }
        if (!delta) { return; }
        element = Event.extend(event).target;
        element = Element.extend(element.nodeType === Node.TEXT_NODE ? element.parentNode : element);
        custom_event = element.fire('mouse:wheel',{ delta: delta });
        if (custom_event.stopped) {
            Event.stop(event);
            return false;
        }
    }
    document.observe('mousewheel',wheel);
    document.observe('DOMMouseScroll',wheel);
})();
var Preloader = {
    load: function() {
        var args = $A(arguments);
        var callback = Object.isFunction(args.last()) ? args.pop() : Prototype.emptyFunction;
        var urls = Object.isArray(args[0]) ? $A(args[0]) : args;
        var loaded = 0;
        var images = $A();

        var onload = function() {
            if (++loaded == urls.length) {
                callback();

                // cleanup
                images.each(function(i) { delete i });
                images = callback = urls = null;
            }
        };

        urls.each(function(url) {
            var image = new Image();
            image.onload = image.onerror = onload;
            image.src = url;
            images.push(image);
        });
    }
};
document.observe("dom:loaded", function() {
	$$('img').each(function(ele){
		ele.observe('error', function(event){
			//ImageError($(this));
		});
	});
	if($('product-options-wrapper'))
	{
		detailUpdate();
		$('product-options-wrapper').select('select').each(function(el){
			el.observe('change', function(event){
				detailUpdate();
			});
		});
		$('product-options-wrapper').select('input[type="radio"]').each(function(el){
			el.observe('change', function(event){
				detailUpdate();
			});
		});
		$('product-options-wrapper').select('input[type="text"]').each(function(el){
			el.observe('change', function(event){
				detailUpdate();
			});
		});
	}
	$$('img.image_layered').each(function(elem){
		elem.observe('mouseover', function(event){
			var value = elem.readAttribute('value');
			var title = elem.readAttribute('title');
			if(!value)
			{
				value = title;
				elem.setAttribute('value', value);
			}
			var tooltip = updateTooltipImage(value);
			elem.setAttribute('title', '');
			var top = elem.viewportOffset().top + elem.offsetHeight/2 - 22 ;
			var left = elem.viewportOffset().left +  elem.offsetWidth;
			tooltip.setStyle({position:'fixed', left: left + 'px', top: top + 'px', zIndex: 999});
			tooltip.show();
		});
		elem.observe('mouseout', function(event){
			if($('tooltip-imagelayered'))
				$('tooltip-imagelayered').hide();
		});
	});
	
	$$('.add-to-cart').each(function(elem){
				if(elem.down('.btn-cart'))
				{	
					elem.down('.btn-cart').observe('mouseover', function(event){
						if($(this).hasClassName('button_require'))
						{
							var viewport = document.viewport.getDimensions();
							var tooltip = updateTooltipImage(LabelRequireAddToCart);
							var top = $(this).viewportOffset().top + $(this).offsetHeight/2 - 22 ;
							var left = viewport.width - $(this).viewportOffset().left;
							tooltip.addClassName('add_to_cart_hover');
							tooltip.setStyle({position:'fixed', right: left + 'px', top: top + 'px', zIndex: 999});
							tooltip.show();
						}
					});
					elem.down('.btn-cart').observe('mouseout', function(event){
						if($('tooltip-imagelayered'))
						{
							$('tooltip-imagelayered').hide();
							$('tooltip-imagelayered').removeClassName('add_to_cart_hover');
						}
					});
				}
	});
});
function detailUpdate()
{
	if(!$('product-options-wrapper') || !$('product_tab_details'))
		return;
	var insertLeft = false;
	var disableAddCart = false;
	var stoneTitle = '';
	$('product-options-wrapper').select('select').each(function(el){
		
		var IdItem = el.readAttribute('id');
		var LabelItem = 'Selected';
		var ValueItem = el.options[el.selectedIndex].innerHTML;
		if(el.options[el.selectedIndex].readAttribute('price'))
		{
			var split = el.options[el.selectedIndex].readAttribute('price');
			if(el.options[el.selectedIndex].readAttribute('price') >= 0)
				split = '+' + split;
			var NewArray = ValueItem.split(split, 2);
			ValueItem = NewArray[0];
		}
		if(el.up('.alloy') && el.up('.alloy').up().previous('.choose-alloy') && el.up('.alloy').up().previous('.choose-alloy').down('label'))
			LabelItem = el.up('.alloy').up().previous('.choose-alloy').down('label').innerHTML;
		else if(el.up().down('.label'))
			LabelItem = el.up().down('.label').innerHTML;
		else if(el.up('.optionWrap') && el.up('.optionWrap').down('span.step-number') && el.up('.optionWrap').down('span.step-number').next('span'))
			LabelItem = el.up('.optionWrap').down('span.step-number').next('span').innerHTML;
		else if(el.up('.optionWrap') && el.up('.optionWrap').down('.optionLabel'))
			LabelItem = el.up('.optionWrap').down('.optionLabel').innerHTML;
		else if(el.up('.optionWrap') && el.up('.optionWrap').down('span'))
			LabelItem = el.up('.optionWrap').down('span').innerHTML;
		else if(el.up('.left-haft') && el.up('.left-haft').down('label'))
			LabelItem = el.up('.left-haft').down('label').innerHTML;
		else if(el.up('dd') && el.up('dd').previous('dt') && el.up('dd').previous('dt').down('label'))
			LabelItem = el.up('dd').previous('dt').down('label').getText();
		if(el.hasClassName('required-entry') && el.value == '' && !disableAddCart)
		{
			$$('.add-to-cart').each(function(elem){
				if(elem.down('button'))
				{
					if(!elem.down('button').readAttribute('data-onclick'))
					{
						var onlick = elem.down('button').readAttribute('onclick');
						elem.down('button').setAttribute('data-onclick', onlick);
					}
					elem.down('button').addClassName('button_require');
					elem.down('button').setAttribute('onclick', 'return false;');
				}
			});
			disableAddCart = true;
		}
		insertLeft = insertRowDataTable(IdItem, LabelItem, ValueItem, insertLeft, stoneTitle);
	});
	$('product-options-wrapper').select('input[type="radio"]').each(function(ele){
		if(ele.checked)
		{
			var IdItem = ele.readAttribute('name').replace('[','_').replace(']','_');
			var LabelItem = 'Selected';
			var ValueItem = '';
			if(ele.up().down('.label').down('.title'))
				ValueItem = ele.up().down('.label').down('.title').innerHTML;
			else if(ele.up().down('.label') && ele.up().down('.label').down('label'))
				ValueItem = ele.up().down('.label').down('label').getText();
			else if(ele.up().down('.label'))
				ValueItem = ele.up().down('.label').innerHTML;
			if(ele.up('.optionWrap') && ele.up('.optionWrap').down('.optionLabel') && ele.up('.optionWrap').down('.optionLabel').down(1))
				LabelItem = ele.up('.optionWrap').down('.optionLabel').down(1).innerHTML;
			else if(ele.up('.optionWrap') && ele.up('.optionWrap').down('.optionLabel') && ele.up('.optionWrap').down('.optionLabel').down())
				LabelItem = ele.up('.optionWrap').down('.optionLabel').down().innerHTML;
			if(ele.getAttribute('optitle')){
				stoneTitle = ele.getAttribute('optitle');
			}
			insertLeft = insertRowDataTable(IdItem, LabelItem, ValueItem, insertLeft,stoneTitle);
		} 
	});
	$('product-options-wrapper').select('input[type="text"]').each(function(elemen){
		if(elemen.hasClassName('required-entry') && elemen.value == '' && !disableAddCart)
		{
			var LabelItem = 'Text box';
			if(elemen.up('dd') && elemen.up('dd').previous('.list-text'))
				LabelItem = elemen.up('dd').previous('.list-text').getText();
			else if(elemen.up('dd') && elemen.up('dd').previous('dt') && elemen.up('dd').previous('dt').down('label'))
				LabelItem = elemen.up('dd').previous('dt').down('label').getText();
			LabelRequireAddToCart = LabelItem;
			$$('.add-to-cart').each(function(button){
				if(button.down('button'))
				{
					if(!button.down('button').readAttribute('data-onclick'))
					{
						var onlick = button.down('button').readAttribute('onclick');
						button.down('button').setAttribute('data-onclick', onlick);
					}
					button.down('button').addClassName('button_require');
					button.down('button').setAttribute('onclick', 'return false;');
				}
			});
			disableAddCart = true;
		}
	});
	//options[25537]
	if(!disableAddCart)
	{
		$$('.add-to-cart').each(function(elem){
			elem.down('button').removeClassName('button_require');
			var onclick = elem.down('button').readAttribute('data-onclick');
			if(onclick)
				elem.down('button').setAttribute('onclick', onclick);
		});
	}
}
function insertRowDataTable(IdItem, LabelItem, ValueItem, insertLeft,stoneTitle)
{	
	if($('product_tab_details').down('.detail_' + IdItem))
		$('product_tab_details').down('.detail_' + IdItem).remove();
	if(!$('product_tab_details').down('.detail_' + IdItem))
	{
		var Table = $('product_tab_details').down('.data-table');

			if(!insertLeft && $('product_tab_details').down('table.right'))
			{
				Table = $('product_tab_details').down('table.right');
				insertLeft = true;
			}
			else{
				insertLeft = false;
			}
		
		var elementTr = new Element('tr');
		elementTr.addClassName('detail_' + IdItem);
		var elementTh = new Element('th');
		elementTh.addClassName('label');
		// elementTh.addClassName(LabelItem.toLowerCase().replace(/\s/g,''));
		elementTh.innerHTML = LabelItem;
		elementTr.insert(elementTh);
		var elementTd = new Element('td');
		elementTd.addClassName('data');
		elementTd.innerHTML = ValueItem;
		elementTr.insert(elementTd);
		// alert(LabelItem);
		if( stoneTitle != '' && stoneTitle.toLowerCase().replace(/\s/g,'') == 'stone/diamonds' && $('stone1') != undefined){
			$('stone1').down('.data-table').down().insert({top: elementTr});
		}else if( stoneTitle != '' && stoneTitle.toLowerCase().replace(/\s/g,'') == 'stone2' && $('stone2') != undefined){
			$('stone2').down('.data-table').down().insert({top: elementTr});
		} else{
			Table.down().insert({bottom: elementTr});
		}
	}
	return insertLeft;
}
function updateTooltipImage(title)
{
	if(!$('tooltip-imagelayered'))
	{
		var tooltip = new Element('div');
		tooltip.setAttribute('id','tooltip-imagelayered');
		$(document.body).insert(tooltip);
	}
	var newElement = '<div class="tooltip-arrow"></div><div class="tooltip-inner">' + title + '</div>';
	var tooltip = $('tooltip-imagelayered');
	tooltip.innerHTML = newElement;
	return tooltip;
}
function ImageError(img)
{
	var oldsrc = img.readAttribute('src');
	img.setAttribute('oldsrc', oldsrc);
	img.src = MEDIA_URL + "no-image.jpg";
}
/* End Core Extensions */
/* add from buycard */
function closePopup(){
	$('popup_overlay').setStyle({display:'none'});
	$('popup_container').setStyle({display:'none'});
	return false;
}
function createOverlay()
{
	if(!$('popup_content'))
	{
		var PopupOverlay = new Element('div');
		var PopupContainer = new Element('div');
		var PopupContent = new Element('div');
		PopupOverlay.setAttribute('id','popup_overlay');
		PopupContainer.setAttribute('id','popup_container');
		PopupContent.setAttribute('id','popup_content');
		PopupContent.setAttribute('class','popup_content');
		PopupOverlay.observe('click', function(el){
			closePopup();
		});
		PopupContainer.insert(PopupContent);
		$(document.body).insert(PopupContainer);
		$(document.body).insert(PopupOverlay);
	}
	$('popup_overlay').setStyle({display:'block'});
	$('popup_container').setStyle({display:'block'});
	return $('popup_content');
}
/*end*/ 