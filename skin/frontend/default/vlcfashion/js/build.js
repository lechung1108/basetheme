$j = jQuery.noConflict();

$j(document).ready(function() {
	// collapsible layered-nav
	$j(".block-layered-nav .block-title, .block-layered-nav dt").toggle(function() {
		$j(this).addClass("active").next().slideDown();
	}, function() {
		$j(this).removeClass("active").next().fadeOut();
	});

	$j(".mobile-nav .nav > li").addClass("dropdown");
	$j(".mobile-nav .nav > li > a").addClass("dropdown-toggle").attr({'data-toggle': 'dropdown'});
	$j(".mobile-nav .nav > li > ul").addClass("dropdown-menu");

});