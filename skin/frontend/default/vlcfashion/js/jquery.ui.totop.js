/*
|--------------------------------------------------------------------------
| UItoTop jQuery Plugin 1.2 by Matt Varone
| http://www.mattvarone.com/web-design/uitotop-jquery-plugin/
|--------------------------------------------------------------------------
*/
(function($j){
	$j.fn.UItoTop = function(options) {

 		var defaults = {
    			text: 'To Top',
    			min: 200,
    			inDelay:600,
    			outDelay:400,
      			containerID: 'toTop',
    			containerHoverID: 'toTopHover',
    			scrollSpeed: 1200,
    			easingType: 'linear'
 		    },
            settings = $j.extend(defaults, options),
            containerIDhash = '#' + settings.containerID,
            containerHoverIDHash = '#'+settings.containerHoverID;
		
		$j('body').append('<a href="#" id="'+settings.containerID+'">'+settings.text+'</a>');
		$j(containerIDhash).hide().on('click.UItoTop',function(){
			$j('html, body').animate({scrollTop:0}, settings.scrollSpeed, settings.easingType);
			$j('#'+settings.containerHoverID, this).stop().animate({'opacity': 0 }, settings.inDelay, settings.easingType);
			return false;
		})
		.prepend('<span id="'+settings.containerHoverID+'"></span>')
		.hover(function() {
				$j(containerHoverIDHash, this).stop().animate({
					'opacity': 1
				}, 600, 'linear');
			}, function() { 
				$j(containerHoverIDHash, this).stop().animate({
					'opacity': 0
				}, 700, 'linear');
			});
					
		$j(window).scroll(function() {
			var sd = $j(window).scrollTop();
			if(typeof document.body.style.maxHeight === "undefined") {
				$j(containerIDhash).css({
					'position': 'absolute',
					'top': sd + $j(window).height() - 50
				});
			}
			if ( sd > settings.min ) 
				$j(containerIDhash).fadeIn(settings.inDelay);
			else 
				$j(containerIDhash).fadeOut(settings.Outdelay);
		});
};
})(jQuery);