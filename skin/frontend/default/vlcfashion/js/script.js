/**
 * Author: Thompson Le
 * Date: 12/24/2013
 * Time: 15:35 PM
 */
jQuery(function () { 
    jQuery("[data-toggle='tooltip']").tooltip(); 
});

//jQuery('.social-links a').tooltip('toggle');	  
		  
	equalheight = function(container){

	var currentTallest = 0,
		 currentRowStart = 0,
		 rowDivs = new Array(),
		 $el,
		 topPosition = 0;
	 jQuery(container).each(function() {

	   $el = jQuery(this);
	   jQuery($el).height('auto')
	   topPostion = $el.position().top;

	   if (currentRowStart != topPostion) {
		 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		   rowDivs[currentDiv].height(currentTallest);
		 }
		 rowDivs.length = 0; // empty the array
		 currentRowStart = topPostion;
		 currentTallest = $el.height();
		 rowDivs.push($el);
	   } else {
		 rowDivs.push($el);
		 currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		 rowDivs[currentDiv].height(currentTallest);
	   }
	 });
	}

	jQuery(window).load(function() {
		equalheight('.products-grid .item');
		equalheight('.products-grid .item-inner');
		equalheight('.products-grid .item-inner .normal');
		equalheight('.products-grid .item-inner .overlay');
		equalheight('.products-grid .item-inner .product-info');
		equalheight('.products-grid .item-inner .product-name');
		equalheight('.products-grid .item-inner .price-box');
		
		equalheight('.products-grid .item-inner .ratings');
		equalheight('.account-login .col-sm-6');
		equalheight('.account-login .col-sm-6 .content');
	});
	jQuery(window).resize(function(){
		equalheight('.products-grid .item');
		equalheight('.products-grid .item-inner');
		equalheight('.products-grid .item-inner .normal');
		equalheight('.products-grid .item-inner .overlay');
		equalheight('.products-grid .item-inner .product-info');
		equalheight('.products-grid .item-inner .product-name');
		equalheight('.products-grid .item-inner .price-box');
	
		equalheight('.products-grid .item-inner .ratings');
		equalheight('.account-login .col-sm-6');
		equalheight('.account-login .col-sm-6 .content');
	});

	jQuery(function(jQuery) {
		jQuery('.collapsible').each(function(index){
			jQuery(this).prepend('<span class="opener">&nbsp;</span>');
			if (jQuery(this).hasClass('active'))
			{
				jQuery(this).children('.block-content').css('display', 'block');
			}
			else
			{
				jQuery(this).children('.block-content').css('display', 'none');
			}			
		});
		
		jQuery('.collapsible .opener').click(function() {
			
			var parent = jQuery(this).parent();
			if (parent.hasClass('active'))
			{
				jQuery(this).siblings('.block-content').stop(true).slideUp(300);
				parent.removeClass('active');
			}
			else
			{
				jQuery(this).siblings('.block-content').stop(true).slideDown(300);
				parent.addClass('active');
			}
			
		});
	}); 
$j(document).ready(function() {
	// Totop
	$j().UItoTop({ easingType: 'easeOutQuart' });
	
	// Testimonial Slider 		
		var owl = $j("#owl-testimonial");

		  owl.owlCarousel({
			slideSpeed : 500,
			autoPlay:5000,
			navigation : true,
			singleItem : true,
			
		  });

		  //Select Transtion Type
		  $j("#transitionType").change(function(){
			var newValue = $(this).val();

			//TransitionTypes is owlCarousel inner method.
			owl.data("owlCarousel").transitionTypes(newValue);

			//After change type go to next slide
			owl.trigger("owl.next");
		  });
	
});
		