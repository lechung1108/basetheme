﻿
 // DOM ready
	 jQuery(function() {
	   
      // Create the dropdown base
      jQuery("<select />").appendTo("#mobile-menu");
      
      // Create default option "Menu"
      jQuery("<option />", {
         "selected": "selected",
         "value"   : "#",
         "text"    : "Menu"
      }).appendTo("nav select");
      
      // create blank line
      
      jQuery("<optgroup />", {
         "value"   : "",
         "text"    : ""
      }).appendTo("nav select");

      
      // Populate dropdown with menu items
      jQuery("nav > ul > li").each(function() {
      
      	var el = jQuery(this);
      
      	var hasChildren = el.find("ul"),
      	    children    = el.find("li");
       
      	if (hasChildren.length) {
      	
      		jQuery("<optgroup />", {
      			"label": el.find("> a").text()
      		}).appendTo("nav select");
      		
      		children.find('a')
      		.each (function() {
      		      			
      			jQuery("<option />", {
      				"text": " - " + jQuery(this).text(),
      				"value": jQuery(this).attr("href")
      			}).appendTo("optgroup:last");
      		
      		});
      		
      		 // create blank line
      
      jQuery("<optgroup />", {
         "value"   : "",
         "text"    : ""
      }).appendTo("nav select");

      		      	
      	} else {
      		
      	
      		jQuery("<option />", {
	           //"value"   : el.attr("href"),
	           "value" : el.find('a').attr('href'),
	           //"text"    : el.text()
	           "text" : el.find('a').text()
	       }).appendTo("nav select");
      	
      	}
      	
 // create blank line
      
      jQuery("<optgroup />", {
         "value"   : "",
         "text"    : ""
      }).appendTo("nav select");
 
             
      });
      
	   // To make dropdown actually work
	   // To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
      jQuery("nav select").change(function() {
        window.location = jQuery(this).find("option:selected").val();
      });
	 
	 });
